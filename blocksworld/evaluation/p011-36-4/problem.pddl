

(define (problem BW-rand-36)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 )
(:init
(on b1 b19)
(on b2 b18)
(on b3 b32)
(on-table b4)
(on b5 b23)
(on b6 b30)
(on b7 b4)
(on b8 b16)
(on b9 b1)
(on b10 b36)
(on b11 b2)
(on b12 b29)
(on b13 b15)
(on b14 b28)
(on b15 b12)
(on b16 b9)
(on b17 b7)
(on b18 b6)
(on b19 b17)
(on b20 b26)
(on b21 b31)
(on b22 b14)
(on b23 b13)
(on b24 b27)
(on b25 b35)
(on b26 b33)
(on b27 b25)
(on b28 b5)
(on b29 b34)
(on-table b30)
(on-table b31)
(on b32 b21)
(on b33 b11)
(on b34 b20)
(on-table b35)
(on b36 b24)
(clear b3)
(clear b8)
(clear b10)
(clear b22)
)
(:goal
(and
(on b1 b26)
(on b2 b21)
(on b3 b27)
(on b4 b16)
(on b5 b11)
(on b7 b29)
(on b11 b32)
(on b12 b17)
(on b13 b7)
(on b14 b35)
(on b15 b30)
(on b16 b36)
(on b17 b8)
(on b18 b13)
(on b19 b15)
(on b20 b12)
(on b21 b23)
(on b22 b25)
(on b23 b18)
(on b24 b1)
(on b25 b2)
(on b26 b4)
(on b27 b9)
(on b28 b20)
(on b29 b24)
(on b30 b6)
(on b31 b33)
(on b32 b19)
(on b33 b34)
(on b36 b5))
)
)


