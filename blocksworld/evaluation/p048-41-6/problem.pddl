

(define (problem BW-rand-41)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 )
(:init
(on-table b1)
(on b2 b25)
(on b3 b6)
(on b4 b21)
(on b5 b8)
(on b6 b1)
(on b7 b38)
(on-table b8)
(on b9 b24)
(on b10 b12)
(on b11 b34)
(on b12 b39)
(on b13 b16)
(on b14 b41)
(on b15 b20)
(on b16 b40)
(on b17 b28)
(on b18 b19)
(on b19 b27)
(on b20 b11)
(on b21 b35)
(on b22 b31)
(on b23 b13)
(on b24 b26)
(on b25 b9)
(on-table b26)
(on b27 b7)
(on b28 b33)
(on b29 b23)
(on b30 b4)
(on b31 b5)
(on b32 b18)
(on-table b33)
(on b34 b29)
(on b35 b15)
(on b36 b17)
(on b37 b32)
(on b38 b10)
(on b39 b2)
(on b40 b22)
(on-table b41)
(clear b3)
(clear b14)
(clear b30)
(clear b36)
(clear b37)
)
(:goal
(and
(on b1 b37)
(on b2 b20)
(on b3 b27)
(on b4 b38)
(on b5 b16)
(on b7 b41)
(on b8 b1)
(on b9 b32)
(on b10 b39)
(on b12 b18)
(on b13 b31)
(on b14 b35)
(on b15 b34)
(on b16 b17)
(on b17 b9)
(on b18 b36)
(on b20 b24)
(on b21 b6)
(on b22 b25)
(on b23 b19)
(on b24 b15)
(on b25 b40)
(on b27 b12)
(on b28 b11)
(on b29 b13)
(on b31 b14)
(on b32 b22)
(on b34 b5)
(on b35 b30)
(on b36 b4)
(on b37 b7)
(on b38 b28)
(on b39 b29)
(on b40 b26)
(on b41 b3))
)
)


