

(define (problem BW-rand-43)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 )
(:init
(on b1 b18)
(on b2 b14)
(on b3 b16)
(on b4 b35)
(on b5 b37)
(on-table b6)
(on b7 b4)
(on b8 b22)
(on b9 b20)
(on-table b10)
(on-table b11)
(on b12 b6)
(on-table b13)
(on b14 b29)
(on b15 b43)
(on b16 b31)
(on b17 b13)
(on b18 b10)
(on b19 b5)
(on b20 b7)
(on b21 b17)
(on b22 b2)
(on b23 b19)
(on b24 b25)
(on b25 b28)
(on b26 b3)
(on b27 b12)
(on-table b28)
(on b29 b30)
(on b30 b27)
(on b31 b40)
(on b32 b42)
(on-table b33)
(on b34 b11)
(on b35 b39)
(on b36 b9)
(on-table b37)
(on b38 b21)
(on b39 b8)
(on b40 b38)
(on b41 b33)
(on b42 b41)
(on-table b43)
(clear b1)
(clear b15)
(clear b23)
(clear b24)
(clear b26)
(clear b32)
(clear b34)
(clear b36)
)
(:goal
(and
(on b1 b16)
(on b3 b10)
(on b4 b14)
(on b5 b29)
(on b6 b21)
(on b7 b42)
(on b8 b41)
(on b9 b35)
(on b10 b33)
(on b12 b1)
(on b13 b34)
(on b14 b3)
(on b15 b12)
(on b16 b28)
(on b18 b39)
(on b19 b15)
(on b20 b9)
(on b21 b8)
(on b22 b26)
(on b23 b24)
(on b25 b19)
(on b27 b5)
(on b28 b30)
(on b29 b18)
(on b30 b20)
(on b31 b38)
(on b32 b4)
(on b34 b37)
(on b35 b11)
(on b36 b6)
(on b37 b25)
(on b38 b13)
(on b39 b17)
(on b40 b23)
(on b42 b32)
(on b43 b27))
)
)


