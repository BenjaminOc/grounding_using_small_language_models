

(define (problem BW-rand-35)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 )
(:init
(on b1 b23)
(on-table b2)
(on-table b3)
(on b4 b24)
(on-table b5)
(on b6 b26)
(on b7 b35)
(on b8 b33)
(on b9 b15)
(on b10 b5)
(on b11 b31)
(on b12 b13)
(on-table b13)
(on b14 b18)
(on b15 b22)
(on b16 b12)
(on b17 b20)
(on b18 b3)
(on b19 b9)
(on b20 b6)
(on-table b21)
(on b22 b29)
(on b23 b34)
(on b24 b25)
(on b25 b16)
(on b26 b7)
(on b27 b2)
(on b28 b10)
(on b29 b1)
(on b30 b19)
(on b31 b30)
(on b32 b28)
(on b33 b21)
(on b34 b32)
(on-table b35)
(clear b4)
(clear b8)
(clear b11)
(clear b14)
(clear b17)
(clear b27)
)
(:goal
(and
(on b1 b12)
(on b3 b21)
(on b4 b22)
(on b5 b33)
(on b6 b1)
(on b7 b24)
(on b8 b4)
(on b9 b26)
(on b10 b34)
(on b12 b17)
(on b14 b32)
(on b15 b31)
(on b17 b35)
(on b18 b7)
(on b20 b5)
(on b21 b2)
(on b24 b27)
(on b25 b8)
(on b26 b16)
(on b27 b15)
(on b30 b28)
(on b31 b30)
(on b32 b23)
(on b33 b25)
(on b34 b13)
(on b35 b10))
)
)


