

(define (problem BW-rand-42)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 )
(:init
(on b1 b16)
(on-table b2)
(on b3 b28)
(on b4 b17)
(on b5 b7)
(on b6 b19)
(on b7 b14)
(on b8 b24)
(on-table b9)
(on b10 b31)
(on b11 b4)
(on b12 b2)
(on b13 b18)
(on b14 b13)
(on b15 b11)
(on b16 b23)
(on b17 b41)
(on b18 b29)
(on b19 b15)
(on b20 b21)
(on b21 b37)
(on b22 b35)
(on b23 b33)
(on b24 b40)
(on b25 b5)
(on b26 b12)
(on b27 b1)
(on-table b28)
(on b29 b6)
(on b30 b32)
(on b31 b36)
(on b32 b3)
(on b33 b20)
(on b34 b22)
(on b35 b38)
(on b36 b8)
(on b37 b26)
(on b38 b10)
(on b39 b34)
(on b40 b25)
(on-table b41)
(on-table b42)
(clear b9)
(clear b27)
(clear b30)
(clear b39)
(clear b42)
)
(:goal
(and
(on b2 b34)
(on b4 b1)
(on b5 b28)
(on b7 b11)
(on b8 b6)
(on b9 b32)
(on b10 b13)
(on b11 b19)
(on b12 b7)
(on b14 b9)
(on b15 b21)
(on b16 b15)
(on b18 b24)
(on b19 b30)
(on b20 b36)
(on b21 b4)
(on b22 b40)
(on b23 b5)
(on b25 b27)
(on b27 b8)
(on b28 b26)
(on b30 b38)
(on b31 b33)
(on b32 b10)
(on b33 b41)
(on b35 b2)
(on b36 b17)
(on b37 b20)
(on b38 b18)
(on b39 b42)
(on b40 b39)
(on b41 b14)
(on b42 b25))
)
)


