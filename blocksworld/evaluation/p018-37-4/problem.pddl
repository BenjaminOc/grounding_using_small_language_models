

(define (problem BW-rand-37)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 )
(:init
(on b1 b37)
(on b2 b25)
(on b3 b17)
(on b4 b19)
(on b5 b22)
(on-table b6)
(on b7 b11)
(on b8 b4)
(on b9 b14)
(on b10 b2)
(on b11 b34)
(on b12 b29)
(on b13 b3)
(on b14 b7)
(on b15 b36)
(on b16 b18)
(on b17 b10)
(on-table b18)
(on b19 b30)
(on b20 b8)
(on b21 b12)
(on b22 b15)
(on b23 b24)
(on-table b24)
(on b25 b27)
(on b26 b20)
(on b27 b28)
(on b28 b33)
(on-table b29)
(on b30 b35)
(on b31 b1)
(on b32 b9)
(on-table b33)
(on-table b34)
(on b35 b32)
(on b36 b31)
(on b37 b26)
(clear b5)
(clear b6)
(clear b13)
(clear b16)
(clear b21)
(clear b23)
)
(:goal
(and
(on b1 b19)
(on b3 b21)
(on b5 b11)
(on b6 b3)
(on b7 b2)
(on b8 b36)
(on b9 b22)
(on b10 b1)
(on b11 b34)
(on b12 b32)
(on b13 b37)
(on b14 b33)
(on b15 b7)
(on b17 b25)
(on b19 b17)
(on b20 b15)
(on b21 b23)
(on b23 b26)
(on b24 b6)
(on b25 b28)
(on b26 b29)
(on b27 b12)
(on b28 b31)
(on b29 b4)
(on b30 b18)
(on b32 b5)
(on b33 b10)
(on b34 b20)
(on b35 b14)
(on b36 b35)
(on b37 b30))
)
)


