

(define (problem BW-rand-92)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 b78 b79 b80 b81 b82 b83 b84 b85 b86 b87 b88 b89 b90 b91 b92 )
(:init
(on-table b1)
(on b2 b68)
(on b3 b30)
(on-table b4)
(on b5 b53)
(on b6 b44)
(on b7 b28)
(on-table b8)
(on b9 b72)
(on b10 b41)
(on b11 b10)
(on b12 b14)
(on-table b13)
(on-table b14)
(on b15 b37)
(on b16 b52)
(on b17 b19)
(on b18 b29)
(on b19 b60)
(on b20 b77)
(on b21 b22)
(on b22 b38)
(on b23 b34)
(on b24 b31)
(on b25 b9)
(on-table b26)
(on b27 b48)
(on b28 b78)
(on b29 b84)
(on b30 b40)
(on b31 b27)
(on b32 b13)
(on b33 b47)
(on b34 b8)
(on b35 b71)
(on b36 b64)
(on b37 b74)
(on b38 b23)
(on b39 b92)
(on b40 b81)
(on b41 b43)
(on b42 b80)
(on b43 b83)
(on b44 b15)
(on b45 b2)
(on b46 b67)
(on b47 b36)
(on b48 b46)
(on b49 b39)
(on b50 b61)
(on b51 b63)
(on b52 b12)
(on-table b53)
(on b54 b76)
(on b55 b91)
(on b56 b50)
(on b57 b89)
(on b58 b85)
(on b59 b51)
(on b60 b49)
(on b61 b25)
(on b62 b3)
(on b63 b5)
(on-table b64)
(on b65 b26)
(on b66 b7)
(on b67 b79)
(on b68 b62)
(on b69 b24)
(on b70 b58)
(on b71 b57)
(on b72 b69)
(on b73 b4)
(on b74 b75)
(on b75 b45)
(on b76 b88)
(on b77 b86)
(on b78 b20)
(on b79 b59)
(on b80 b33)
(on-table b81)
(on b82 b56)
(on b83 b32)
(on b84 b35)
(on-table b85)
(on b86 b21)
(on b87 b66)
(on b88 b65)
(on b89 b11)
(on-table b90)
(on b91 b70)
(on b92 b55)
(clear b1)
(clear b6)
(clear b16)
(clear b17)
(clear b18)
(clear b42)
(clear b54)
(clear b73)
(clear b82)
(clear b87)
(clear b90)
)
(:goal
(and
(on b3 b14)
(on b4 b3)
(on b5 b71)
(on b6 b1)
(on b7 b19)
(on b8 b65)
(on b10 b18)
(on b11 b7)
(on b12 b21)
(on b13 b35)
(on b14 b54)
(on b15 b57)
(on b18 b64)
(on b19 b26)
(on b20 b49)
(on b21 b88)
(on b22 b61)
(on b23 b32)
(on b24 b40)
(on b25 b33)
(on b26 b24)
(on b27 b12)
(on b28 b89)
(on b29 b56)
(on b30 b34)
(on b31 b83)
(on b32 b45)
(on b33 b85)
(on b35 b70)
(on b36 b22)
(on b37 b81)
(on b38 b6)
(on b39 b28)
(on b40 b60)
(on b41 b30)
(on b42 b76)
(on b43 b48)
(on b44 b47)
(on b45 b41)
(on b46 b78)
(on b47 b17)
(on b48 b74)
(on b49 b62)
(on b50 b77)
(on b51 b59)
(on b52 b63)
(on b53 b9)
(on b54 b52)
(on b56 b68)
(on b58 b36)
(on b60 b66)
(on b61 b5)
(on b62 b51)
(on b63 b42)
(on b64 b11)
(on b65 b79)
(on b66 b8)
(on b67 b87)
(on b68 b67)
(on b69 b2)
(on b70 b37)
(on b71 b82)
(on b72 b25)
(on b73 b39)
(on b76 b38)
(on b77 b20)
(on b79 b44)
(on b80 b53)
(on b81 b91)
(on b83 b75)
(on b84 b4)
(on b85 b46)
(on b86 b29)
(on b87 b58)
(on b88 b80)
(on b89 b90)
(on b92 b73))
)
)


