

(define (problem BW-rand-77)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 )
(:init
(on b1 b43)
(on b2 b72)
(on b3 b26)
(on b4 b56)
(on b5 b30)
(on b6 b36)
(on b7 b23)
(on b8 b63)
(on b9 b52)
(on b10 b13)
(on b11 b25)
(on b12 b8)
(on b13 b2)
(on b14 b20)
(on b15 b74)
(on-table b16)
(on b17 b59)
(on b18 b44)
(on b19 b27)
(on b20 b47)
(on b21 b38)
(on b22 b32)
(on b23 b77)
(on b24 b37)
(on b25 b45)
(on b26 b71)
(on-table b27)
(on b28 b6)
(on b29 b31)
(on b30 b64)
(on b31 b60)
(on b32 b65)
(on b33 b18)
(on b34 b5)
(on b35 b66)
(on b36 b33)
(on b37 b70)
(on b38 b34)
(on b39 b76)
(on b40 b51)
(on b41 b61)
(on b42 b21)
(on b43 b40)
(on b44 b39)
(on b45 b24)
(on b46 b48)
(on-table b47)
(on b48 b9)
(on b49 b67)
(on b50 b35)
(on-table b51)
(on b52 b14)
(on-table b53)
(on b54 b29)
(on b55 b46)
(on b56 b55)
(on b57 b1)
(on b58 b22)
(on b59 b69)
(on b60 b17)
(on b61 b16)
(on b62 b3)
(on b63 b4)
(on b64 b12)
(on-table b65)
(on b66 b28)
(on b67 b42)
(on-table b68)
(on b69 b57)
(on b70 b19)
(on b71 b68)
(on b72 b50)
(on b73 b11)
(on b74 b75)
(on b75 b7)
(on-table b76)
(on b77 b49)
(clear b10)
(clear b15)
(clear b41)
(clear b53)
(clear b54)
(clear b58)
(clear b62)
(clear b73)
)
(:goal
(and
(on b1 b52)
(on b2 b65)
(on b3 b18)
(on b4 b37)
(on b5 b43)
(on b6 b64)
(on b7 b21)
(on b9 b45)
(on b10 b41)
(on b12 b11)
(on b13 b69)
(on b14 b51)
(on b15 b32)
(on b16 b66)
(on b17 b25)
(on b18 b46)
(on b19 b22)
(on b20 b75)
(on b21 b40)
(on b22 b50)
(on b23 b3)
(on b24 b13)
(on b25 b29)
(on b26 b36)
(on b27 b4)
(on b28 b24)
(on b29 b74)
(on b30 b33)
(on b32 b5)
(on b33 b39)
(on b34 b23)
(on b35 b34)
(on b36 b58)
(on b37 b57)
(on b38 b68)
(on b39 b59)
(on b40 b8)
(on b41 b70)
(on b42 b9)
(on b43 b28)
(on b44 b72)
(on b45 b7)
(on b47 b76)
(on b49 b44)
(on b50 b31)
(on b51 b2)
(on b53 b1)
(on b54 b77)
(on b55 b14)
(on b56 b12)
(on b57 b61)
(on b58 b71)
(on b59 b42)
(on b60 b27)
(on b61 b15)
(on b62 b16)
(on b63 b56)
(on b64 b63)
(on b65 b38)
(on b66 b35)
(on b67 b54)
(on b69 b26)
(on b70 b67)
(on b71 b20)
(on b72 b53)
(on b73 b49)
(on b74 b60)
(on b75 b62)
(on b76 b10)
(on b77 b48))
)
)


