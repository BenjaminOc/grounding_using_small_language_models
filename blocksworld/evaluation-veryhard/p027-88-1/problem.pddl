

(define (problem BW-rand-88)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 b78 b79 b80 b81 b82 b83 b84 b85 b86 b87 b88 )
(:init
(on b1 b77)
(on b2 b78)
(on b3 b38)
(on b4 b1)
(on b5 b27)
(on b6 b88)
(on b7 b87)
(on b8 b60)
(on b9 b16)
(on b10 b32)
(on b11 b15)
(on b12 b25)
(on-table b13)
(on-table b14)
(on b15 b65)
(on b16 b59)
(on b17 b13)
(on b18 b47)
(on b19 b53)
(on b20 b58)
(on b21 b85)
(on b22 b2)
(on b23 b34)
(on b24 b3)
(on-table b25)
(on-table b26)
(on b27 b72)
(on-table b28)
(on b29 b86)
(on b30 b55)
(on b31 b20)
(on b32 b54)
(on b33 b4)
(on b34 b51)
(on b35 b68)
(on b36 b42)
(on b37 b64)
(on b38 b7)
(on b39 b82)
(on b40 b62)
(on b41 b26)
(on-table b42)
(on b43 b6)
(on b44 b81)
(on-table b45)
(on b46 b49)
(on-table b47)
(on-table b48)
(on b49 b24)
(on b50 b9)
(on b51 b14)
(on b52 b17)
(on b53 b61)
(on-table b54)
(on b55 b12)
(on b56 b41)
(on b57 b79)
(on b58 b10)
(on b59 b33)
(on b60 b18)
(on b61 b57)
(on b62 b29)
(on b63 b40)
(on b64 b75)
(on b65 b22)
(on b66 b8)
(on b67 b31)
(on-table b68)
(on b69 b52)
(on b70 b35)
(on b71 b76)
(on b72 b19)
(on b73 b37)
(on b74 b66)
(on b75 b84)
(on-table b76)
(on-table b77)
(on-table b78)
(on b79 b83)
(on b80 b30)
(on b81 b23)
(on b82 b11)
(on b83 b21)
(on b84 b44)
(on b85 b70)
(on b86 b43)
(on b87 b71)
(on b88 b80)
(clear b5)
(clear b28)
(clear b36)
(clear b39)
(clear b45)
(clear b46)
(clear b48)
(clear b50)
(clear b56)
(clear b63)
(clear b67)
(clear b69)
(clear b73)
(clear b74)
)
(:goal
(and
(on b1 b10)
(on b2 b39)
(on b3 b74)
(on b4 b12)
(on b5 b54)
(on b6 b59)
(on b8 b20)
(on b9 b79)
(on b10 b72)
(on b11 b75)
(on b12 b77)
(on b15 b33)
(on b16 b22)
(on b17 b88)
(on b18 b48)
(on b19 b78)
(on b21 b34)
(on b22 b35)
(on b24 b5)
(on b25 b47)
(on b26 b14)
(on b27 b25)
(on b28 b18)
(on b29 b24)
(on b30 b60)
(on b31 b82)
(on b32 b45)
(on b34 b70)
(on b35 b67)
(on b36 b85)
(on b37 b46)
(on b38 b30)
(on b40 b53)
(on b41 b73)
(on b42 b29)
(on b43 b27)
(on b44 b61)
(on b45 b37)
(on b46 b42)
(on b47 b64)
(on b48 b41)
(on b49 b86)
(on b50 b56)
(on b51 b84)
(on b52 b8)
(on b53 b19)
(on b54 b28)
(on b55 b26)
(on b56 b40)
(on b58 b65)
(on b59 b15)
(on b60 b44)
(on b61 b68)
(on b62 b50)
(on b63 b2)
(on b64 b9)
(on b65 b38)
(on b66 b52)
(on b67 b49)
(on b68 b51)
(on b69 b71)
(on b70 b36)
(on b71 b66)
(on b72 b57)
(on b73 b58)
(on b74 b81)
(on b75 b83)
(on b76 b17)
(on b77 b69)
(on b79 b87)
(on b80 b63)
(on b81 b13)
(on b82 b4)
(on b83 b55)
(on b84 b6)
(on b85 b23)
(on b86 b76)
(on b87 b21)
(on b88 b11))
)
)


