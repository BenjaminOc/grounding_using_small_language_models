

(define (problem BW-rand-91)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 b78 b79 b80 b81 b82 b83 b84 b85 b86 b87 b88 b89 b90 b91 )
(:init
(on b1 b33)
(on b2 b26)
(on-table b3)
(on-table b4)
(on b5 b25)
(on b6 b55)
(on b7 b80)
(on b8 b72)
(on b9 b45)
(on b10 b44)
(on b11 b61)
(on b12 b51)
(on b13 b79)
(on b14 b49)
(on b15 b31)
(on b16 b23)
(on b17 b32)
(on b18 b50)
(on b19 b81)
(on b20 b39)
(on b21 b40)
(on b22 b16)
(on b23 b46)
(on b24 b34)
(on b25 b58)
(on b26 b19)
(on b27 b74)
(on b28 b35)
(on b29 b82)
(on b30 b8)
(on b31 b9)
(on b32 b13)
(on b33 b71)
(on b34 b15)
(on b35 b6)
(on b36 b85)
(on b37 b69)
(on b38 b66)
(on b39 b43)
(on b40 b48)
(on b41 b2)
(on b42 b54)
(on b43 b21)
(on-table b44)
(on b45 b62)
(on-table b46)
(on b47 b12)
(on b48 b17)
(on b49 b52)
(on b50 b29)
(on b51 b86)
(on b52 b20)
(on b53 b63)
(on b54 b3)
(on b55 b7)
(on b56 b65)
(on b57 b37)
(on b58 b78)
(on b59 b5)
(on-table b60)
(on b61 b14)
(on b62 b36)
(on b63 b88)
(on b64 b22)
(on b65 b60)
(on-table b66)
(on b67 b70)
(on b68 b24)
(on b69 b27)
(on b70 b68)
(on b71 b11)
(on-table b72)
(on b73 b89)
(on b74 b41)
(on b75 b73)
(on b76 b56)
(on b77 b53)
(on b78 b76)
(on b79 b28)
(on b80 b59)
(on b81 b77)
(on b82 b38)
(on b83 b57)
(on b84 b75)
(on b85 b10)
(on-table b86)
(on b87 b30)
(on b88 b47)
(on-table b89)
(on b90 b87)
(on b91 b42)
(clear b1)
(clear b4)
(clear b18)
(clear b64)
(clear b67)
(clear b83)
(clear b84)
(clear b90)
(clear b91)
)
(:goal
(and
(on b1 b47)
(on b2 b46)
(on b3 b10)
(on b4 b59)
(on b5 b38)
(on b6 b88)
(on b7 b91)
(on b8 b32)
(on b10 b48)
(on b11 b53)
(on b12 b81)
(on b13 b24)
(on b15 b31)
(on b16 b62)
(on b17 b23)
(on b18 b68)
(on b19 b65)
(on b20 b87)
(on b21 b26)
(on b22 b86)
(on b24 b29)
(on b25 b76)
(on b26 b89)
(on b27 b82)
(on b28 b37)
(on b29 b56)
(on b30 b61)
(on b31 b66)
(on b32 b57)
(on b34 b75)
(on b35 b83)
(on b36 b51)
(on b37 b78)
(on b38 b60)
(on b39 b72)
(on b40 b28)
(on b41 b67)
(on b42 b58)
(on b43 b13)
(on b44 b12)
(on b45 b19)
(on b46 b1)
(on b47 b3)
(on b48 b52)
(on b49 b84)
(on b51 b90)
(on b52 b20)
(on b53 b77)
(on b54 b16)
(on b55 b22)
(on b57 b55)
(on b59 b36)
(on b61 b21)
(on b62 b43)
(on b63 b14)
(on b64 b85)
(on b65 b80)
(on b66 b40)
(on b67 b33)
(on b68 b4)
(on b69 b41)
(on b70 b63)
(on b71 b73)
(on b72 b9)
(on b74 b11)
(on b75 b49)
(on b76 b64)
(on b77 b2)
(on b78 b17)
(on b79 b18)
(on b80 b69)
(on b81 b30)
(on b82 b70)
(on b83 b5)
(on b84 b27)
(on b85 b74)
(on b86 b45)
(on b87 b54)
(on b88 b25)
(on b89 b35)
(on b90 b34)
(on b91 b8))
)
)


