

(define (problem BW-rand-96)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 b78 b79 b80 b81 b82 b83 b84 b85 b86 b87 b88 b89 b90 b91 b92 b93 b94 b95 b96 )
(:init
(on b1 b9)
(on b2 b84)
(on b3 b93)
(on b4 b16)
(on b5 b56)
(on b6 b44)
(on b7 b79)
(on b8 b15)
(on b9 b45)
(on b10 b41)
(on b11 b61)
(on b12 b80)
(on b13 b40)
(on b14 b89)
(on b15 b3)
(on b16 b62)
(on b17 b33)
(on b18 b24)
(on b19 b31)
(on b20 b90)
(on-table b21)
(on b22 b85)
(on b23 b63)
(on b24 b95)
(on b25 b39)
(on b26 b46)
(on b27 b10)
(on-table b28)
(on b29 b5)
(on b30 b22)
(on b31 b12)
(on-table b32)
(on b33 b8)
(on b34 b21)
(on b35 b73)
(on b36 b88)
(on b37 b70)
(on b38 b18)
(on b39 b76)
(on b40 b83)
(on b41 b96)
(on b42 b29)
(on b43 b14)
(on b44 b78)
(on b45 b75)
(on-table b46)
(on b47 b4)
(on b48 b43)
(on b49 b11)
(on b50 b13)
(on b51 b6)
(on b52 b82)
(on b53 b87)
(on b54 b49)
(on b55 b91)
(on b56 b47)
(on b57 b77)
(on b58 b26)
(on b59 b92)
(on-table b60)
(on b61 b67)
(on b62 b36)
(on b63 b20)
(on b64 b52)
(on b65 b86)
(on b66 b35)
(on b67 b74)
(on b68 b37)
(on b69 b30)
(on b70 b38)
(on b71 b1)
(on b72 b50)
(on b73 b54)
(on b74 b2)
(on b75 b65)
(on b76 b69)
(on b77 b68)
(on b78 b72)
(on b79 b71)
(on b80 b51)
(on b81 b64)
(on b82 b25)
(on b83 b48)
(on-table b84)
(on b85 b57)
(on b86 b94)
(on b87 b7)
(on b88 b34)
(on b89 b58)
(on-table b90)
(on b91 b27)
(on b92 b19)
(on b93 b60)
(on b94 b66)
(on b95 b23)
(on-table b96)
(clear b17)
(clear b28)
(clear b32)
(clear b42)
(clear b53)
(clear b55)
(clear b59)
(clear b81)
)
(:goal
(and
(on b1 b86)
(on b2 b83)
(on b3 b60)
(on b4 b72)
(on b5 b77)
(on b6 b35)
(on b7 b66)
(on b9 b61)
(on b10 b84)
(on b11 b68)
(on b12 b82)
(on b13 b22)
(on b14 b79)
(on b15 b46)
(on b16 b30)
(on b17 b49)
(on b18 b27)
(on b19 b67)
(on b20 b93)
(on b21 b94)
(on b22 b53)
(on b23 b5)
(on b24 b33)
(on b25 b80)
(on b26 b58)
(on b27 b85)
(on b28 b21)
(on b29 b25)
(on b30 b51)
(on b31 b16)
(on b32 b15)
(on b33 b70)
(on b34 b3)
(on b35 b62)
(on b36 b43)
(on b37 b24)
(on b38 b23)
(on b39 b44)
(on b40 b32)
(on b41 b95)
(on b43 b39)
(on b44 b81)
(on b45 b59)
(on b46 b6)
(on b47 b75)
(on b48 b19)
(on b49 b56)
(on b51 b17)
(on b52 b8)
(on b53 b89)
(on b54 b76)
(on b55 b91)
(on b56 b45)
(on b57 b37)
(on b59 b52)
(on b60 b18)
(on b62 b54)
(on b64 b40)
(on b65 b73)
(on b66 b29)
(on b68 b14)
(on b69 b20)
(on b70 b26)
(on b71 b88)
(on b72 b92)
(on b73 b41)
(on b74 b57)
(on b76 b10)
(on b77 b63)
(on b79 b31)
(on b80 b38)
(on b81 b55)
(on b82 b87)
(on b83 b47)
(on b84 b65)
(on b85 b4)
(on b86 b34)
(on b87 b1)
(on b88 b13)
(on b89 b7)
(on b90 b96)
(on b91 b12)
(on b92 b69)
(on b93 b64)
(on b94 b50)
(on b95 b74))
)
)


