

(define (problem BW-rand-93)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 b66 b67 b68 b69 b70 b71 b72 b73 b74 b75 b76 b77 b78 b79 b80 b81 b82 b83 b84 b85 b86 b87 b88 b89 b90 b91 b92 b93 )
(:init
(on b1 b50)
(on-table b2)
(on b3 b5)
(on b4 b74)
(on b5 b51)
(on b6 b55)
(on-table b7)
(on b8 b33)
(on b9 b86)
(on b10 b20)
(on b11 b41)
(on-table b12)
(on-table b13)
(on b14 b36)
(on b15 b18)
(on b16 b37)
(on b17 b81)
(on b18 b93)
(on b19 b35)
(on b20 b1)
(on b21 b58)
(on b22 b26)
(on b23 b85)
(on b24 b32)
(on b25 b7)
(on b26 b88)
(on b27 b11)
(on b28 b66)
(on b29 b27)
(on b30 b83)
(on b31 b48)
(on b32 b56)
(on b33 b77)
(on b34 b40)
(on b35 b71)
(on b36 b15)
(on b37 b69)
(on b38 b89)
(on-table b39)
(on b40 b45)
(on b41 b2)
(on b42 b63)
(on b43 b9)
(on-table b44)
(on b45 b28)
(on b46 b68)
(on b47 b21)
(on b48 b79)
(on b49 b22)
(on b50 b62)
(on-table b51)
(on b52 b57)
(on b53 b47)
(on b54 b70)
(on b55 b44)
(on b56 b53)
(on b57 b60)
(on b58 b64)
(on b59 b91)
(on b60 b72)
(on b61 b38)
(on b62 b42)
(on b63 b39)
(on b64 b75)
(on b65 b24)
(on b66 b87)
(on b67 b16)
(on b68 b43)
(on-table b69)
(on b70 b12)
(on b71 b34)
(on b72 b82)
(on b73 b76)
(on b74 b92)
(on b75 b61)
(on b76 b29)
(on b77 b17)
(on b78 b84)
(on-table b79)
(on b80 b65)
(on b81 b19)
(on b82 b25)
(on b83 b6)
(on b84 b52)
(on b85 b54)
(on b86 b31)
(on b87 b4)
(on b88 b90)
(on b89 b23)
(on b90 b14)
(on b91 b73)
(on b92 b59)
(on b93 b80)
(clear b3)
(clear b8)
(clear b10)
(clear b13)
(clear b30)
(clear b46)
(clear b49)
(clear b67)
(clear b78)
)
(:goal
(and
(on b1 b78)
(on b2 b63)
(on b3 b20)
(on b4 b59)
(on b5 b42)
(on b6 b23)
(on b7 b4)
(on b8 b48)
(on b9 b52)
(on b10 b32)
(on b12 b36)
(on b13 b30)
(on b14 b38)
(on b15 b10)
(on b17 b51)
(on b18 b25)
(on b20 b19)
(on b21 b89)
(on b22 b55)
(on b23 b66)
(on b24 b14)
(on b25 b13)
(on b26 b12)
(on b27 b46)
(on b28 b31)
(on b29 b56)
(on b31 b1)
(on b33 b64)
(on b34 b77)
(on b35 b27)
(on b36 b86)
(on b37 b61)
(on b38 b28)
(on b39 b74)
(on b40 b65)
(on b41 b43)
(on b42 b16)
(on b43 b40)
(on b44 b17)
(on b46 b88)
(on b47 b93)
(on b48 b50)
(on b49 b26)
(on b50 b85)
(on b51 b68)
(on b52 b45)
(on b53 b3)
(on b54 b84)
(on b55 b73)
(on b56 b70)
(on b58 b91)
(on b59 b39)
(on b60 b47)
(on b61 b57)
(on b62 b82)
(on b63 b44)
(on b64 b24)
(on b66 b35)
(on b67 b60)
(on b68 b5)
(on b69 b11)
(on b70 b62)
(on b71 b69)
(on b72 b76)
(on b73 b75)
(on b74 b58)
(on b75 b29)
(on b76 b41)
(on b77 b67)
(on b78 b53)
(on b79 b83)
(on b80 b9)
(on b81 b79)
(on b82 b87)
(on b83 b90)
(on b84 b81)
(on b85 b71)
(on b86 b72)
(on b87 b21)
(on b88 b37)
(on b89 b2)
(on b90 b18)
(on b91 b8)
(on b92 b33)
(on b93 b22))
)
)


