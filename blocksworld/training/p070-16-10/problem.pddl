

(define (problem BW-rand-16)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 )
(:init
(on b1 b4)
(on b2 b14)
(on b3 b9)
(on-table b4)
(on b5 b8)
(on b6 b2)
(on b7 b5)
(on b8 b10)
(on b9 b11)
(on b10 b16)
(on b11 b12)
(on b12 b1)
(on b13 b7)
(on-table b14)
(on b15 b13)
(on b16 b3)
(clear b6)
(clear b15)
)
(:goal
(and
(on b1 b9)
(on b2 b11)
(on b8 b4)
(on b9 b7)
(on b10 b13)
(on b12 b15)
(on b15 b8)
(on b16 b12))
)
)


