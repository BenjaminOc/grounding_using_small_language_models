

(define (problem BW-rand-14)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 )
(:init
(on b1 b8)
(on b2 b11)
(on-table b3)
(on b4 b14)
(on b5 b6)
(on b6 b13)
(on b7 b2)
(on b8 b5)
(on b9 b1)
(on-table b10)
(on b11 b12)
(on b12 b4)
(on b13 b7)
(on-table b14)
(clear b3)
(clear b9)
(clear b10)
)
(:goal
(and
(on b3 b10)
(on b4 b8)
(on b6 b3)
(on b7 b1)
(on b8 b7)
(on b9 b14)
(on b10 b2)
(on b11 b9)
(on b13 b5))
)
)


