

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on b1 b3)
(on-table b2)
(on-table b3)
(on b4 b6)
(on b5 b10)
(on-table b6)
(on b7 b1)
(on b8 b5)
(on-table b9)
(on-table b10)
(clear b2)
(clear b4)
(clear b7)
(clear b8)
(clear b9)
)
(:goal
(and
(on b1 b3)
(on b2 b6)
(on b3 b9)
(on b4 b5)
(on b5 b1)
(on b7 b4)
(on b8 b10)
(on b10 b7))
)
)


