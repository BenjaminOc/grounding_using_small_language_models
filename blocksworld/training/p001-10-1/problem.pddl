

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on-table b1)
(on-table b2)
(on b3 b6)
(on b4 b1)
(on b5 b8)
(on b6 b2)
(on b7 b5)
(on b8 b9)
(on-table b9)
(on b10 b4)
(clear b3)
(clear b7)
(clear b10)
)
(:goal
(and
(on b1 b9)
(on b3 b1)
(on b4 b2)
(on b6 b10)
(on b7 b8)
(on b8 b3)
(on b10 b7))
)
)


