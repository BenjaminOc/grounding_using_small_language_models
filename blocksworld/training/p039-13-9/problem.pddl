

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on b1 b4)
(on b2 b5)
(on-table b3)
(on-table b4)
(on b5 b1)
(on b6 b7)
(on-table b7)
(on b8 b12)
(on-table b9)
(on b10 b8)
(on b11 b13)
(on-table b12)
(on b13 b2)
(clear b3)
(clear b6)
(clear b9)
(clear b10)
(clear b11)
)
(:goal
(and
(on b3 b10)
(on b5 b9)
(on b7 b6)
(on b8 b7)
(on b9 b4)
(on b10 b11)
(on b11 b2)
(on b13 b12))
)
)


