

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on b1 b10)
(on b2 b13)
(on b3 b9)
(on b4 b3)
(on b5 b1)
(on b6 b7)
(on-table b7)
(on b8 b2)
(on-table b9)
(on b10 b4)
(on-table b11)
(on b12 b5)
(on b13 b12)
(clear b6)
(clear b8)
(clear b11)
)
(:goal
(and
(on b1 b11)
(on b4 b10)
(on b5 b7)
(on b6 b1)
(on b8 b6)
(on b9 b8)
(on b10 b3)
(on b13 b5))
)
)


