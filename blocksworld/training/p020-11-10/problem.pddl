

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on b1 b8)
(on b2 b6)
(on b3 b10)
(on b4 b9)
(on-table b5)
(on b6 b3)
(on-table b7)
(on-table b8)
(on-table b9)
(on b10 b1)
(on b11 b7)
(clear b2)
(clear b4)
(clear b5)
(clear b11)
)
(:goal
(and
(on b2 b11)
(on b3 b1)
(on b5 b3)
(on b6 b9)
(on b7 b6)
(on b8 b7)
(on b9 b10)
(on b11 b4))
)
)


