

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on b1 b6)
(on-table b2)
(on b3 b1)
(on b4 b2)
(on b5 b9)
(on b6 b5)
(on-table b7)
(on b8 b4)
(on b9 b7)
(on b10 b8)
(clear b3)
(clear b10)
)
(:goal
(and
(on b1 b3)
(on b2 b4)
(on b4 b7)
(on b5 b8)
(on b6 b9)
(on b7 b6)
(on b9 b5)
(on b10 b2))
)
)


