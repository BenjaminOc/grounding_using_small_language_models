

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on-table b1)
(on b2 b1)
(on b3 b5)
(on b4 b6)
(on b5 b9)
(on b6 b8)
(on b7 b10)
(on b8 b2)
(on b9 b11)
(on b10 b3)
(on b11 b4)
(clear b7)
)
(:goal
(and
(on b1 b2)
(on b2 b3)
(on b4 b8)
(on b5 b7)
(on b6 b11)
(on b8 b10)
(on b9 b1)
(on b10 b5)
(on b11 b9))
)
)


