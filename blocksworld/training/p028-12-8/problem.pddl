

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on b1 b4)
(on b2 b11)
(on b3 b12)
(on b4 b2)
(on b5 b9)
(on b6 b10)
(on b7 b8)
(on b8 b5)
(on b9 b3)
(on b10 b7)
(on-table b11)
(on b12 b1)
(clear b6)
)
(:goal
(and
(on b1 b7)
(on b2 b3)
(on b3 b11)
(on b5 b2)
(on b6 b8)
(on b7 b5)
(on b8 b4)
(on b9 b1)
(on b10 b6)
(on b12 b10))
)
)


