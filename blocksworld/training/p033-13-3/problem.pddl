

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on-table b1)
(on b2 b8)
(on b3 b10)
(on b4 b11)
(on b5 b6)
(on b6 b7)
(on-table b7)
(on b8 b1)
(on-table b9)
(on b10 b4)
(on b11 b12)
(on b12 b2)
(on b13 b5)
(clear b3)
(clear b9)
(clear b13)
)
(:goal
(and
(on b2 b11)
(on b4 b2)
(on b5 b7)
(on b6 b12)
(on b7 b8)
(on b8 b4)
(on b10 b5)
(on b11 b1)
(on b12 b3)
(on b13 b6))
)
)


