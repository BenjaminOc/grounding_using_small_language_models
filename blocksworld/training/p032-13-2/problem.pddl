

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on-table b1)
(on b2 b12)
(on b3 b7)
(on b4 b13)
(on-table b5)
(on b6 b3)
(on b7 b11)
(on b8 b5)
(on b9 b4)
(on b10 b6)
(on b11 b9)
(on b12 b10)
(on b13 b8)
(clear b1)
(clear b2)
)
(:goal
(and
(on b1 b12)
(on b2 b9)
(on b4 b7)
(on b8 b6)
(on b9 b4)
(on b11 b10)
(on b12 b13)
(on b13 b3))
)
)


