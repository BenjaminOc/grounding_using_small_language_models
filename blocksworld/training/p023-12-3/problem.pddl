

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on-table b1)
(on b2 b4)
(on b3 b2)
(on b4 b10)
(on b5 b8)
(on b6 b5)
(on b7 b12)
(on b8 b7)
(on b9 b1)
(on-table b10)
(on b11 b3)
(on b12 b11)
(clear b6)
(clear b9)
)
(:goal
(and
(on b1 b6)
(on b2 b7)
(on b3 b2)
(on b4 b5)
(on b5 b9)
(on b6 b10)
(on b7 b11)
(on b9 b3)
(on b10 b12)
(on b11 b8))
)
)


