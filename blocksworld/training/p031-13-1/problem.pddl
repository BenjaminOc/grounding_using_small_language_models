

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on b1 b5)
(on b2 b11)
(on b3 b9)
(on b4 b6)
(on b5 b4)
(on b6 b2)
(on b7 b1)
(on-table b8)
(on b9 b13)
(on-table b10)
(on b11 b10)
(on b12 b3)
(on b13 b7)
(clear b8)
(clear b12)
)
(:goal
(and
(on b1 b10)
(on b2 b13)
(on b3 b6)
(on b4 b5)
(on b5 b8)
(on b6 b12)
(on b9 b2)
(on b10 b9)
(on b12 b7)
(on b13 b4))
)
)


