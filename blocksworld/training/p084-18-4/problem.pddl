

(define (problem BW-rand-18)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 )
(:init
(on b1 b3)
(on b2 b9)
(on-table b3)
(on b4 b14)
(on b5 b11)
(on-table b6)
(on b7 b5)
(on b8 b16)
(on b9 b8)
(on b10 b18)
(on b11 b13)
(on-table b12)
(on b13 b4)
(on b14 b1)
(on b15 b10)
(on-table b16)
(on b17 b6)
(on b18 b2)
(clear b7)
(clear b12)
(clear b15)
(clear b17)
)
(:goal
(and
(on b1 b12)
(on b3 b6)
(on b4 b11)
(on b6 b7)
(on b7 b18)
(on b8 b13)
(on b9 b14)
(on b10 b2)
(on b11 b15)
(on b12 b9)
(on b13 b1)
(on b15 b5)
(on b16 b17)
(on b17 b8)
(on b18 b16))
)
)


