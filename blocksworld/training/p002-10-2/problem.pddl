

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on b1 b10)
(on b2 b9)
(on-table b3)
(on-table b4)
(on-table b5)
(on b6 b8)
(on b7 b3)
(on b8 b4)
(on-table b9)
(on b10 b7)
(clear b1)
(clear b2)
(clear b5)
(clear b6)
)
(:goal
(and
(on b2 b1)
(on b3 b4)
(on b4 b9)
(on b6 b8)
(on b7 b10)
(on b8 b2)
(on b10 b5))
)
)


