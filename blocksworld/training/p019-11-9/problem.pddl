

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on b1 b11)
(on b2 b7)
(on b3 b6)
(on b4 b9)
(on b5 b10)
(on b6 b2)
(on-table b7)
(on b8 b5)
(on b9 b1)
(on-table b10)
(on b11 b8)
(clear b3)
(clear b4)
)
(:goal
(and
(on b1 b7)
(on b2 b3)
(on b4 b1)
(on b5 b2)
(on b6 b11)
(on b8 b5)
(on b11 b8))
)
)


