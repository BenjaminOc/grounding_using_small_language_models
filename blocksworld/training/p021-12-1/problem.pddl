

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on-table b1)
(on b2 b11)
(on b3 b12)
(on-table b4)
(on-table b5)
(on b6 b1)
(on b7 b10)
(on b8 b7)
(on b9 b5)
(on b10 b2)
(on b11 b3)
(on b12 b6)
(clear b4)
(clear b8)
(clear b9)
)
(:goal
(and
(on b1 b4)
(on b2 b1)
(on b3 b2)
(on b4 b6)
(on b6 b9)
(on b7 b3)
(on b8 b11)
(on b9 b10)
(on b10 b12)
(on b11 b7)
(on b12 b5))
)
)


