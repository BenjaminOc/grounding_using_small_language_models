

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on b1 b5)
(on b2 b3)
(on b3 b10)
(on-table b4)
(on b5 b7)
(on b6 b4)
(on-table b7)
(on b8 b11)
(on b9 b1)
(on-table b10)
(on-table b11)
(on b12 b2)
(clear b6)
(clear b8)
(clear b9)
(clear b12)
)
(:goal
(and
(on b1 b10)
(on b2 b5)
(on b3 b7)
(on b4 b2)
(on b5 b9)
(on b6 b1)
(on b7 b11)
(on b10 b8)
(on b11 b4)
(on b12 b3))
)
)


