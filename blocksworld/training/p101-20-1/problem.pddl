

(define (problem BW-rand-20)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 )
(:init
(on b1 b16)
(on b2 b20)
(on b3 b12)
(on b4 b9)
(on b5 b13)
(on b6 b18)
(on b7 b2)
(on-table b8)
(on-table b9)
(on-table b10)
(on b11 b8)
(on b12 b7)
(on b13 b10)
(on b14 b11)
(on b15 b5)
(on b16 b15)
(on b17 b3)
(on b18 b1)
(on-table b19)
(on b20 b14)
(clear b4)
(clear b6)
(clear b17)
(clear b19)
)
(:goal
(and
(on b1 b17)
(on b2 b9)
(on b3 b7)
(on b4 b19)
(on b5 b8)
(on b7 b11)
(on b8 b12)
(on b10 b18)
(on b11 b15)
(on b12 b10)
(on b14 b5)
(on b15 b16)
(on b16 b4)
(on b18 b1)
(on b19 b6)
(on b20 b2))
)
)


