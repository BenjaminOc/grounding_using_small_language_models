

(define (problem BW-rand-18)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 )
(:init
(on-table b1)
(on b2 b14)
(on b3 b11)
(on b4 b8)
(on b5 b6)
(on b6 b18)
(on b7 b2)
(on-table b8)
(on b9 b5)
(on-table b10)
(on b11 b13)
(on b12 b9)
(on-table b13)
(on b14 b10)
(on b15 b3)
(on-table b16)
(on-table b17)
(on b18 b4)
(clear b1)
(clear b7)
(clear b12)
(clear b15)
(clear b16)
(clear b17)
)
(:goal
(and
(on b2 b18)
(on b3 b16)
(on b4 b17)
(on b5 b9)
(on b7 b3)
(on b8 b12)
(on b9 b2)
(on b10 b4)
(on b11 b7)
(on b12 b6)
(on b13 b8)
(on b14 b13)
(on b16 b5)
(on b17 b11)
(on b18 b14))
)
)


