

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on-table b1)
(on b2 b11)
(on b3 b6)
(on b4 b5)
(on b5 b10)
(on b6 b2)
(on b7 b12)
(on b8 b4)
(on b9 b7)
(on-table b10)
(on b11 b1)
(on-table b12)
(clear b3)
(clear b8)
(clear b9)
)
(:goal
(and
(on b2 b12)
(on b3 b9)
(on b4 b5)
(on b5 b11)
(on b6 b2)
(on b8 b1)
(on b9 b6)
(on b10 b4)
(on b11 b3))
)
)


