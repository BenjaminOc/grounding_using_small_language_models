

(define (problem BW-rand-25)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 )
(:init
(on b1 b15)
(on b2 b1)
(on-table b3)
(on b4 b2)
(on b5 b13)
(on b6 b21)
(on b7 b20)
(on b8 b5)
(on-table b9)
(on b10 b23)
(on b11 b9)
(on-table b12)
(on b13 b11)
(on-table b14)
(on b15 b18)
(on b16 b17)
(on-table b17)
(on b18 b3)
(on b19 b22)
(on-table b20)
(on-table b21)
(on b22 b7)
(on b23 b8)
(on b24 b19)
(on b25 b4)
(clear b6)
(clear b10)
(clear b12)
(clear b14)
(clear b16)
(clear b24)
(clear b25)
)
(:goal
(and
(on b1 b11)
(on b2 b20)
(on b6 b7)
(on b7 b14)
(on b8 b25)
(on b10 b12)
(on b11 b18)
(on b12 b5)
(on b13 b22)
(on b14 b8)
(on b15 b24)
(on b16 b19)
(on b17 b13)
(on b18 b17)
(on b19 b10)
(on b20 b23)
(on b21 b1)
(on b22 b15)
(on b23 b9)
(on b24 b6)
(on b25 b16))
)
)


