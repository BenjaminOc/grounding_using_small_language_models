

(define (problem BW-rand-12)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 )
(:init
(on b1 b10)
(on b2 b12)
(on b3 b2)
(on b4 b8)
(on b5 b11)
(on b6 b9)
(on b7 b6)
(on-table b8)
(on b9 b4)
(on-table b10)
(on b11 b7)
(on b12 b1)
(clear b3)
(clear b5)
)
(:goal
(and
(on b1 b2)
(on b2 b10)
(on b4 b7)
(on b5 b9)
(on b6 b11)
(on b8 b5)
(on b9 b1)
(on b10 b12)
(on b11 b8)
(on b12 b4))
)
)


