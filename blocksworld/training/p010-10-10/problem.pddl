

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on b1 b5)
(on b2 b6)
(on-table b3)
(on-table b4)
(on b5 b3)
(on b6 b8)
(on-table b7)
(on b8 b9)
(on b9 b4)
(on b10 b2)
(clear b1)
(clear b7)
(clear b10)
)
(:goal
(and
(on b1 b7)
(on b2 b4)
(on b3 b5)
(on b5 b8)
(on b7 b9)
(on b10 b6))
)
)


