

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on b1 b11)
(on b2 b5)
(on b3 b6)
(on-table b4)
(on-table b5)
(on b6 b9)
(on b7 b10)
(on b8 b7)
(on b9 b2)
(on b10 b1)
(on b11 b4)
(clear b3)
(clear b8)
)
(:goal
(and
(on b1 b5)
(on b3 b6)
(on b4 b2)
(on b5 b9)
(on b7 b1)
(on b8 b10)
(on b9 b8)
(on b10 b3)
(on b11 b7))
)
)


