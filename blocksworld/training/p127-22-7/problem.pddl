

(define (problem BW-rand-22)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 )
(:init
(on b1 b17)
(on b2 b11)
(on b3 b10)
(on-table b4)
(on b5 b14)
(on b6 b19)
(on b7 b16)
(on b8 b9)
(on b9 b2)
(on b10 b6)
(on b11 b1)
(on b12 b18)
(on b13 b3)
(on b14 b7)
(on b15 b21)
(on b16 b12)
(on-table b17)
(on-table b18)
(on b19 b15)
(on b20 b8)
(on b21 b4)
(on b22 b13)
(clear b5)
(clear b20)
(clear b22)
)
(:goal
(and
(on b1 b12)
(on b2 b4)
(on b3 b15)
(on b4 b1)
(on b5 b9)
(on b7 b8)
(on b8 b6)
(on b10 b3)
(on b11 b2)
(on b12 b5)
(on b13 b19)
(on b14 b20)
(on b15 b14)
(on b16 b7)
(on b17 b22)
(on b19 b11)
(on b20 b17)
(on b21 b18)
(on b22 b16))
)
)


