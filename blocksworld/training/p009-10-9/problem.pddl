

(define (problem BW-rand-10)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 )
(:init
(on b1 b9)
(on-table b2)
(on-table b3)
(on b4 b10)
(on b5 b7)
(on b6 b5)
(on b7 b4)
(on b8 b1)
(on b9 b2)
(on b10 b3)
(clear b6)
(clear b8)
)
(:goal
(and
(on b1 b7)
(on b2 b3)
(on b3 b8)
(on b5 b2)
(on b6 b10)
(on b7 b4)
(on b8 b6)
(on b9 b5)
(on b10 b1))
)
)


