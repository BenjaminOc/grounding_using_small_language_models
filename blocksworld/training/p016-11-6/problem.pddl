

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on-table b1)
(on b2 b9)
(on b3 b4)
(on b4 b6)
(on b5 b7)
(on-table b6)
(on b7 b10)
(on-table b8)
(on-table b9)
(on-table b10)
(on b11 b5)
(clear b1)
(clear b2)
(clear b3)
(clear b8)
(clear b11)
)
(:goal
(and
(on b1 b6)
(on b3 b9)
(on b4 b5)
(on b5 b11)
(on b6 b10)
(on b8 b4)
(on b9 b2)
(on b10 b7))
)
)


