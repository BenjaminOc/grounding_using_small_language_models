

(define (problem BW-rand-13)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 )
(:init
(on b1 b13)
(on b2 b4)
(on b3 b11)
(on b4 b12)
(on-table b5)
(on b6 b3)
(on b7 b6)
(on b8 b2)
(on b9 b1)
(on b10 b5)
(on-table b11)
(on b12 b10)
(on-table b13)
(clear b7)
(clear b8)
(clear b9)
)
(:goal
(and
(on b1 b10)
(on b2 b11)
(on b3 b5)
(on b4 b9)
(on b5 b4)
(on b8 b7)
(on b9 b2)
(on b10 b13)
(on b12 b6)
(on b13 b8))
)
)


