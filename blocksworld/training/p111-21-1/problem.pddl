

(define (problem BW-rand-21)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 )
(:init
(on b1 b7)
(on b2 b15)
(on b3 b21)
(on-table b4)
(on b5 b4)
(on b6 b5)
(on b7 b8)
(on b8 b18)
(on b9 b19)
(on-table b10)
(on b11 b2)
(on b12 b14)
(on b13 b1)
(on b14 b10)
(on b15 b6)
(on b16 b11)
(on b17 b13)
(on b18 b16)
(on b19 b3)
(on b20 b9)
(on b21 b12)
(clear b17)
(clear b20)
)
(:goal
(and
(on b1 b6)
(on b2 b15)
(on b3 b13)
(on b4 b17)
(on b5 b21)
(on b6 b16)
(on b7 b10)
(on b9 b2)
(on b10 b14)
(on b11 b7)
(on b12 b8)
(on b13 b4)
(on b16 b18)
(on b17 b5)
(on b18 b19)
(on b20 b9)
(on b21 b20))
)
)


