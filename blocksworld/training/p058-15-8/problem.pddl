

(define (problem BW-rand-15)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 )
(:init
(on b1 b13)
(on b2 b10)
(on b3 b4)
(on b4 b15)
(on b5 b1)
(on-table b6)
(on b7 b8)
(on b8 b3)
(on-table b9)
(on b10 b6)
(on b11 b12)
(on b12 b14)
(on b13 b11)
(on-table b14)
(on b15 b5)
(clear b2)
(clear b7)
(clear b9)
)
(:goal
(and
(on b1 b9)
(on b2 b11)
(on b3 b4)
(on b4 b2)
(on b5 b12)
(on b6 b1)
(on b7 b15)
(on b10 b3)
(on b11 b8)
(on b12 b10)
(on b13 b6)
(on b14 b7)
(on b15 b13))
)
)


