

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on b1 b9)
(on b2 b11)
(on b3 b8)
(on-table b4)
(on-table b5)
(on b6 b2)
(on b7 b1)
(on b8 b7)
(on b9 b10)
(on b10 b4)
(on b11 b3)
(clear b5)
(clear b6)
)
(:goal
(and
(on b1 b4)
(on b2 b6)
(on b3 b7)
(on b4 b10)
(on b6 b9)
(on b7 b8)
(on b8 b2)
(on b9 b11))
)
)


