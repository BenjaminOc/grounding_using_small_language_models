

(define (problem BW-rand-11)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 )
(:init
(on b1 b5)
(on b2 b9)
(on b3 b1)
(on b4 b11)
(on b5 b4)
(on-table b6)
(on-table b7)
(on-table b8)
(on b9 b7)
(on b10 b3)
(on-table b11)
(clear b2)
(clear b6)
(clear b8)
(clear b10)
)
(:goal
(and
(on b1 b11)
(on b3 b6)
(on b4 b8)
(on b5 b7)
(on b8 b1)
(on b9 b10)
(on b10 b2))
)
)


