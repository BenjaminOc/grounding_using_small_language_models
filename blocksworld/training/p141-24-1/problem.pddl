

(define (problem BW-rand-24)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 )
(:init
(on b1 b17)
(on b2 b16)
(on-table b3)
(on b4 b23)
(on b5 b13)
(on b6 b14)
(on b7 b2)
(on b8 b19)
(on b9 b7)
(on b10 b8)
(on-table b11)
(on-table b12)
(on b13 b22)
(on b14 b15)
(on b15 b10)
(on-table b16)
(on b17 b4)
(on b18 b9)
(on b19 b18)
(on b20 b3)
(on b21 b6)
(on b22 b20)
(on b23 b21)
(on b24 b12)
(clear b1)
(clear b5)
(clear b11)
(clear b24)
)
(:goal
(and
(on b2 b3)
(on b3 b14)
(on b5 b17)
(on b6 b5)
(on b7 b9)
(on b9 b6)
(on b10 b8)
(on b11 b4)
(on b12 b7)
(on b14 b23)
(on b15 b11)
(on b16 b15)
(on b17 b22)
(on b18 b12)
(on b19 b20)
(on b20 b21)
(on b21 b1)
(on b22 b10)
(on b23 b19)
(on b24 b16))
)
)


