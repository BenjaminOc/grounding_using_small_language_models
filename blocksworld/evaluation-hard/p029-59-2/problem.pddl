

(define (problem BW-rand-59)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 )
(:init
(on b1 b54)
(on b2 b37)
(on b3 b41)
(on b4 b48)
(on b5 b46)
(on b6 b18)
(on b7 b24)
(on b8 b12)
(on-table b9)
(on b10 b9)
(on b11 b4)
(on b12 b23)
(on b13 b10)
(on b14 b44)
(on b15 b40)
(on b16 b7)
(on b17 b13)
(on b18 b21)
(on b19 b29)
(on b20 b38)
(on b21 b17)
(on b22 b58)
(on-table b23)
(on b24 b56)
(on b25 b59)
(on b26 b49)
(on b27 b28)
(on b28 b32)
(on b29 b14)
(on b30 b26)
(on b31 b22)
(on b32 b20)
(on b33 b42)
(on b34 b36)
(on b35 b47)
(on b36 b52)
(on b37 b57)
(on b38 b45)
(on b39 b8)
(on b40 b33)
(on b41 b15)
(on-table b42)
(on b43 b1)
(on b44 b6)
(on-table b45)
(on b46 b19)
(on b47 b3)
(on-table b48)
(on b49 b34)
(on b50 b39)
(on b51 b50)
(on b52 b27)
(on b53 b55)
(on b54 b30)
(on-table b55)
(on b56 b35)
(on b57 b53)
(on b58 b5)
(on b59 b31)
(clear b2)
(clear b11)
(clear b16)
(clear b25)
(clear b43)
(clear b51)
)
(:goal
(and
(on b1 b9)
(on b2 b17)
(on b3 b43)
(on b4 b46)
(on b5 b33)
(on b6 b1)
(on b7 b36)
(on b8 b20)
(on b9 b32)
(on b10 b13)
(on b11 b47)
(on b13 b7)
(on b14 b40)
(on b17 b11)
(on b18 b19)
(on b19 b24)
(on b21 b53)
(on b22 b34)
(on b23 b5)
(on b25 b45)
(on b27 b48)
(on b28 b35)
(on b29 b8)
(on b30 b39)
(on b31 b14)
(on b32 b59)
(on b34 b38)
(on b35 b25)
(on b36 b31)
(on b37 b12)
(on b38 b57)
(on b40 b49)
(on b41 b6)
(on b42 b23)
(on b45 b4)
(on b46 b58)
(on b47 b52)
(on b48 b41)
(on b49 b37)
(on b50 b2)
(on b51 b18)
(on b52 b10)
(on b53 b44)
(on b54 b51)
(on b55 b3)
(on b56 b27)
(on b57 b55)
(on b58 b16)
(on b59 b22))
)
)


