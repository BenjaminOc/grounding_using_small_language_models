

(define (problem BW-rand-55)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 )
(:init
(on b1 b11)
(on-table b2)
(on b3 b30)
(on b4 b52)
(on-table b5)
(on b6 b39)
(on b7 b23)
(on b8 b16)
(on b9 b37)
(on b10 b44)
(on-table b11)
(on b12 b42)
(on b13 b4)
(on b14 b7)
(on b15 b22)
(on b16 b55)
(on b17 b45)
(on-table b18)
(on b19 b9)
(on b20 b49)
(on b21 b5)
(on b22 b31)
(on b23 b3)
(on b24 b36)
(on b25 b28)
(on b26 b25)
(on b27 b47)
(on b28 b53)
(on b29 b46)
(on b30 b43)
(on b31 b1)
(on b32 b38)
(on b33 b32)
(on-table b34)
(on b35 b13)
(on b36 b10)
(on b37 b51)
(on b38 b19)
(on b39 b50)
(on b40 b34)
(on b41 b21)
(on b42 b24)
(on-table b43)
(on b44 b26)
(on b45 b40)
(on b46 b8)
(on b47 b18)
(on b48 b20)
(on b49 b35)
(on b50 b14)
(on b51 b29)
(on b52 b17)
(on b53 b2)
(on b54 b41)
(on b55 b48)
(clear b6)
(clear b12)
(clear b15)
(clear b27)
(clear b33)
(clear b54)
)
(:goal
(and
(on b1 b55)
(on b4 b29)
(on b5 b18)
(on b6 b31)
(on b7 b4)
(on b8 b54)
(on b9 b36)
(on b10 b25)
(on b11 b38)
(on b12 b7)
(on b13 b46)
(on b14 b27)
(on b15 b2)
(on b16 b35)
(on b17 b23)
(on b18 b34)
(on b19 b16)
(on b20 b26)
(on b21 b48)
(on b22 b41)
(on b23 b5)
(on b24 b39)
(on b25 b6)
(on b26 b49)
(on b27 b24)
(on b28 b3)
(on b29 b17)
(on b30 b8)
(on b31 b12)
(on b32 b37)
(on b33 b42)
(on b34 b15)
(on b35 b44)
(on b36 b43)
(on b37 b1)
(on b38 b53)
(on b39 b10)
(on b41 b19)
(on b42 b52)
(on b43 b51)
(on b44 b9)
(on b45 b40)
(on b46 b33)
(on b47 b14)
(on b48 b50)
(on b49 b45)
(on b51 b30)
(on b52 b28)
(on b53 b47)
(on b54 b20)
(on b55 b21))
)
)


