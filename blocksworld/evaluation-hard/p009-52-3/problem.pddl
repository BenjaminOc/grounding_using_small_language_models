

(define (problem BW-rand-52)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 )
(:init
(on b1 b25)
(on-table b2)
(on b3 b31)
(on b4 b32)
(on b5 b6)
(on b6 b13)
(on b7 b27)
(on b8 b20)
(on b9 b48)
(on b10 b14)
(on-table b11)
(on b12 b22)
(on-table b13)
(on b14 b18)
(on b15 b24)
(on b16 b12)
(on b17 b26)
(on-table b18)
(on b19 b36)
(on b20 b39)
(on b21 b1)
(on b22 b37)
(on b23 b35)
(on b24 b50)
(on b25 b33)
(on b26 b3)
(on-table b27)
(on b28 b17)
(on b29 b2)
(on b30 b47)
(on-table b31)
(on b32 b29)
(on b33 b51)
(on b34 b21)
(on-table b35)
(on b36 b5)
(on b37 b9)
(on b38 b42)
(on b39 b30)
(on b40 b49)
(on b41 b16)
(on b42 b7)
(on b43 b46)
(on b44 b41)
(on b45 b34)
(on b46 b40)
(on b47 b23)
(on b48 b45)
(on b49 b8)
(on b50 b4)
(on-table b51)
(on b52 b19)
(clear b10)
(clear b11)
(clear b15)
(clear b28)
(clear b38)
(clear b43)
(clear b44)
(clear b52)
)
(:goal
(and
(on b1 b28)
(on b2 b45)
(on b4 b52)
(on b6 b46)
(on b7 b16)
(on b8 b35)
(on b9 b19)
(on b11 b41)
(on b12 b11)
(on b13 b1)
(on b14 b8)
(on b16 b38)
(on b17 b12)
(on b20 b44)
(on b21 b34)
(on b22 b50)
(on b23 b33)
(on b24 b37)
(on b25 b7)
(on b26 b24)
(on b27 b25)
(on b28 b3)
(on b29 b30)
(on b30 b26)
(on b31 b20)
(on b32 b15)
(on b33 b21)
(on b34 b31)
(on b36 b49)
(on b37 b43)
(on b38 b18)
(on b39 b2)
(on b40 b39)
(on b41 b36)
(on b42 b17)
(on b43 b13)
(on b44 b32)
(on b45 b47)
(on b46 b40)
(on b47 b4)
(on b48 b29)
(on b49 b6)
(on b50 b9)
(on b51 b14))
)
)


