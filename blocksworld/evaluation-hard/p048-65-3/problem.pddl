

(define (problem BW-rand-65)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 b37 b38 b39 b40 b41 b42 b43 b44 b45 b46 b47 b48 b49 b50 b51 b52 b53 b54 b55 b56 b57 b58 b59 b60 b61 b62 b63 b64 b65 )
(:init
(on b1 b28)
(on b2 b4)
(on-table b3)
(on b4 b45)
(on b5 b64)
(on b6 b41)
(on b7 b35)
(on b8 b36)
(on b9 b39)
(on b10 b31)
(on b11 b7)
(on-table b12)
(on b13 b57)
(on b14 b46)
(on-table b15)
(on b16 b54)
(on b17 b8)
(on b18 b38)
(on b19 b53)
(on b20 b65)
(on b21 b48)
(on b22 b30)
(on-table b23)
(on b24 b18)
(on b25 b62)
(on b26 b25)
(on b27 b51)
(on b28 b50)
(on-table b29)
(on-table b30)
(on b31 b42)
(on b32 b33)
(on b33 b56)
(on b34 b44)
(on b35 b3)
(on-table b36)
(on b37 b29)
(on-table b38)
(on b39 b32)
(on b40 b59)
(on b41 b13)
(on b42 b60)
(on b43 b23)
(on b44 b1)
(on b45 b6)
(on b46 b40)
(on b47 b12)
(on b48 b11)
(on b49 b20)
(on b50 b22)
(on b51 b37)
(on b52 b5)
(on b53 b21)
(on b54 b49)
(on b55 b24)
(on-table b56)
(on b57 b19)
(on-table b58)
(on b59 b9)
(on b60 b2)
(on b61 b52)
(on b62 b47)
(on b63 b34)
(on b64 b16)
(on b65 b15)
(clear b10)
(clear b14)
(clear b17)
(clear b26)
(clear b27)
(clear b43)
(clear b55)
(clear b58)
(clear b61)
(clear b63)
)
(:goal
(and
(on b1 b52)
(on b2 b4)
(on b6 b63)
(on b7 b62)
(on b8 b1)
(on b9 b51)
(on b10 b14)
(on b11 b3)
(on b12 b10)
(on b13 b38)
(on b14 b22)
(on b15 b16)
(on b16 b12)
(on b17 b13)
(on b18 b56)
(on b20 b5)
(on b21 b2)
(on b22 b50)
(on b23 b21)
(on b24 b25)
(on b25 b19)
(on b26 b53)
(on b27 b55)
(on b28 b49)
(on b29 b32)
(on b30 b47)
(on b31 b44)
(on b33 b64)
(on b34 b6)
(on b35 b60)
(on b36 b35)
(on b37 b58)
(on b38 b43)
(on b40 b20)
(on b41 b30)
(on b42 b23)
(on b43 b42)
(on b44 b26)
(on b45 b46)
(on b46 b39)
(on b47 b57)
(on b48 b37)
(on b49 b8)
(on b50 b7)
(on b51 b41)
(on b52 b40)
(on b54 b65)
(on b55 b29)
(on b57 b48)
(on b58 b45)
(on b59 b24)
(on b60 b11)
(on b61 b9)
(on b62 b61)
(on b63 b17)
(on b64 b28)
(on b65 b36))
)
)


