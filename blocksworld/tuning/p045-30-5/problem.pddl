

(define (problem BW-rand-30)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 )
(:init
(on b1 b10)
(on b2 b3)
(on b3 b29)
(on b4 b8)
(on b5 b15)
(on b6 b26)
(on-table b7)
(on b8 b12)
(on-table b9)
(on b10 b5)
(on b11 b7)
(on b12 b20)
(on b13 b11)
(on b14 b27)
(on b15 b2)
(on b16 b14)
(on b17 b4)
(on-table b18)
(on b19 b17)
(on b20 b9)
(on b21 b1)
(on b22 b16)
(on b23 b30)
(on b24 b21)
(on-table b25)
(on b26 b24)
(on b27 b19)
(on b28 b18)
(on-table b29)
(on b30 b22)
(clear b6)
(clear b13)
(clear b23)
(clear b25)
(clear b28)
)
(:goal
(and
(on b1 b18)
(on b2 b4)
(on b3 b22)
(on b4 b23)
(on b5 b11)
(on b7 b19)
(on b8 b27)
(on b9 b20)
(on b10 b6)
(on b11 b15)
(on b12 b26)
(on b15 b8)
(on b16 b5)
(on b17 b14)
(on b18 b25)
(on b19 b21)
(on b20 b12)
(on b21 b30)
(on b22 b16)
(on b23 b10)
(on b25 b2)
(on b26 b3)
(on b27 b29)
(on b30 b28))
)
)


