

(define (problem BW-rand-27)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 )
(:init
(on b1 b5)
(on-table b2)
(on b3 b13)
(on-table b4)
(on b5 b10)
(on b6 b14)
(on-table b7)
(on b8 b25)
(on b9 b27)
(on b10 b23)
(on b11 b2)
(on b12 b3)
(on b13 b4)
(on b14 b7)
(on b15 b21)
(on b16 b12)
(on b17 b19)
(on-table b18)
(on b19 b26)
(on b20 b6)
(on-table b21)
(on b22 b17)
(on b23 b8)
(on b24 b22)
(on b25 b9)
(on b26 b16)
(on b27 b11)
(clear b1)
(clear b15)
(clear b18)
(clear b20)
(clear b24)
)
(:goal
(and
(on b1 b6)
(on b2 b24)
(on b3 b17)
(on b5 b21)
(on b6 b15)
(on b7 b9)
(on b8 b23)
(on b9 b22)
(on b10 b19)
(on b12 b14)
(on b13 b25)
(on b14 b8)
(on b15 b16)
(on b16 b12)
(on b18 b1)
(on b19 b5)
(on b20 b26)
(on b21 b7)
(on b22 b27)
(on b24 b18)
(on b25 b3)
(on b26 b10)
(on b27 b13))
)
)


