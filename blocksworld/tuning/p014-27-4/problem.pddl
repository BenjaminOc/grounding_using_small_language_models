

(define (problem BW-rand-27)
(:domain blocksworld-3ops)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 )
(:init
(on b1 b18)
(on b2 b10)
(on b3 b27)
(on-table b4)
(on b5 b4)
(on-table b6)
(on b7 b26)
(on b8 b5)
(on b9 b2)
(on b10 b6)
(on b11 b23)
(on-table b12)
(on b13 b7)
(on b14 b9)
(on-table b15)
(on b16 b12)
(on b17 b24)
(on b18 b14)
(on-table b19)
(on b20 b11)
(on b21 b1)
(on b22 b20)
(on-table b23)
(on b24 b25)
(on b25 b19)
(on b26 b16)
(on b27 b8)
(clear b3)
(clear b13)
(clear b15)
(clear b17)
(clear b21)
(clear b22)
)
(:goal
(and
(on b1 b20)
(on b3 b15)
(on b4 b12)
(on b5 b10)
(on b6 b2)
(on b7 b18)
(on b8 b17)
(on b10 b7)
(on b11 b19)
(on b13 b3)
(on b14 b24)
(on b16 b21)
(on b18 b26)
(on b19 b27)
(on b20 b4)
(on b21 b14)
(on b22 b8)
(on b23 b22)
(on b24 b9)
(on b25 b11)
(on b26 b1)
(on b27 b5))
)
)


