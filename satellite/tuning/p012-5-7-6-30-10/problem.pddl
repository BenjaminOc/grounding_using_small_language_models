(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	satellite1 - satellite
	instrument1 - instrument
	instrument2 - instrument
	satellite2 - satellite
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	satellite3 - satellite
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	satellite4 - satellite
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	thermograph3 - mode
	spectrograph4 - mode
	image1 - mode
	infrared5 - mode
	thermograph0 - mode
	infrared2 - mode
	GroundStation8 - direction
	GroundStation10 - direction
	GroundStation27 - direction
	GroundStation22 - direction
	Star15 - direction
	Star0 - direction
	Star13 - direction
	Star11 - direction
	Star9 - direction
	Star18 - direction
	GroundStation28 - direction
	Star7 - direction
	Star25 - direction
	Star24 - direction
	GroundStation29 - direction
	Star26 - direction
	Star2 - direction
	GroundStation14 - direction
	Star5 - direction
	GroundStation20 - direction
	Star1 - direction
	GroundStation16 - direction
	Star23 - direction
	GroundStation3 - direction
	Star21 - direction
	GroundStation19 - direction
	GroundStation12 - direction
	Star17 - direction
	GroundStation4 - direction
	Star6 - direction
	Star30 - direction
	Planet31 - direction
	Phenomenon32 - direction
	Phenomenon33 - direction
	Planet34 - direction
	Star35 - direction
	Phenomenon36 - direction
	Star37 - direction
	Star38 - direction
	Phenomenon39 - direction
)
(:init
	(supports instrument0 infrared5)
	(calibration_target instrument0 GroundStation29)
	(calibration_target instrument0 Star6)
	(calibration_target instrument0 Star13)
	(calibration_target instrument0 GroundStation16)
	(calibration_target instrument0 GroundStation3)
	(on_board instrument0 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Phenomenon33)
	(supports instrument1 infrared5)
	(supports instrument1 infrared2)
	(calibration_target instrument1 Star25)
	(calibration_target instrument1 Star13)
	(supports instrument2 infrared2)
	(supports instrument2 thermograph0)
	(supports instrument2 infrared5)
	(calibration_target instrument2 GroundStation22)
	(calibration_target instrument2 Star13)
	(on_board instrument1 satellite1)
	(on_board instrument2 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Planet31)
	(supports instrument3 thermograph0)
	(supports instrument3 infrared2)
	(supports instrument3 image1)
	(calibration_target instrument3 Star18)
	(calibration_target instrument3 Star24)
	(calibration_target instrument3 GroundStation3)
	(supports instrument4 thermograph3)
	(calibration_target instrument4 Star1)
	(calibration_target instrument4 Star0)
	(calibration_target instrument4 Star15)
	(calibration_target instrument4 Star9)
	(calibration_target instrument4 Star18)
	(calibration_target instrument4 Star21)
	(supports instrument5 infrared5)
	(supports instrument5 spectrograph4)
	(supports instrument5 thermograph0)
	(calibration_target instrument5 Star18)
	(calibration_target instrument5 Star26)
	(calibration_target instrument5 GroundStation14)
	(calibration_target instrument5 Star17)
	(calibration_target instrument5 Star13)
	(on_board instrument3 satellite2)
	(on_board instrument4 satellite2)
	(on_board instrument5 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star17)
	(supports instrument6 infrared2)
	(supports instrument6 spectrograph4)
	(calibration_target instrument6 GroundStation12)
	(calibration_target instrument6 GroundStation3)
	(supports instrument7 infrared2)
	(supports instrument7 spectrograph4)
	(supports instrument7 image1)
	(calibration_target instrument7 GroundStation28)
	(calibration_target instrument7 Star18)
	(calibration_target instrument7 Star5)
	(calibration_target instrument7 Star9)
	(calibration_target instrument7 Star11)
	(calibration_target instrument7 Star13)
	(calibration_target instrument7 Star6)
	(supports instrument8 infrared2)
	(supports instrument8 thermograph3)
	(calibration_target instrument8 GroundStation20)
	(calibration_target instrument8 Star24)
	(calibration_target instrument8 Star17)
	(calibration_target instrument8 Star25)
	(calibration_target instrument8 GroundStation3)
	(calibration_target instrument8 GroundStation19)
	(calibration_target instrument8 Star21)
	(calibration_target instrument8 GroundStation29)
	(calibration_target instrument8 Star7)
	(on_board instrument6 satellite3)
	(on_board instrument7 satellite3)
	(on_board instrument8 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star7)
	(supports instrument9 spectrograph4)
	(supports instrument9 infrared5)
	(supports instrument9 infrared2)
	(calibration_target instrument9 Star2)
	(calibration_target instrument9 GroundStation20)
	(calibration_target instrument9 Star23)
	(calibration_target instrument9 Star5)
	(calibration_target instrument9 Star26)
	(calibration_target instrument9 Star21)
	(calibration_target instrument9 GroundStation19)
	(calibration_target instrument9 GroundStation29)
	(supports instrument10 image1)
	(supports instrument10 infrared2)
	(calibration_target instrument10 GroundStation20)
	(calibration_target instrument10 Star5)
	(calibration_target instrument10 GroundStation14)
	(supports instrument11 infrared5)
	(calibration_target instrument11 Star23)
	(calibration_target instrument11 GroundStation16)
	(calibration_target instrument11 Star1)
	(supports instrument12 thermograph0)
	(calibration_target instrument12 GroundStation4)
	(calibration_target instrument12 Star6)
	(calibration_target instrument12 Star17)
	(calibration_target instrument12 GroundStation12)
	(calibration_target instrument12 GroundStation19)
	(calibration_target instrument12 Star21)
	(calibration_target instrument12 GroundStation3)
	(supports instrument13 infrared2)
	(calibration_target instrument13 Star6)
	(on_board instrument9 satellite4)
	(on_board instrument10 satellite4)
	(on_board instrument11 satellite4)
	(on_board instrument12 satellite4)
	(on_board instrument13 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star37)
)
(:goal (and
	(pointing satellite3 GroundStation14)
	(pointing satellite4 GroundStation10)
	(have_image Star30 infrared2)
	(have_image Star30 image1)
	(have_image Planet31 infrared5)
	(have_image Phenomenon32 thermograph3)
	(have_image Phenomenon33 infrared2)
	(have_image Star35 infrared2)
	(have_image Phenomenon36 infrared2)
	(have_image Star37 image1)
	(have_image Star38 image1)
))

)
