(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	satellite1 - satellite
	instrument4 - instrument
	satellite2 - satellite
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	instrument10 - instrument
	satellite3 - satellite
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	satellite4 - satellite
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	satellite5 - satellite
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	satellite6 - satellite
	instrument24 - instrument
	instrument25 - instrument
	instrument26 - instrument
	satellite7 - satellite
	instrument27 - instrument
	instrument28 - instrument
	instrument29 - instrument
	instrument30 - instrument
	image3 - mode
	thermograph4 - mode
	spectrograph0 - mode
	spectrograph5 - mode
	image1 - mode
	infrared2 - mode
	GroundStation24 - direction
	Star1 - direction
	Star18 - direction
	Star23 - direction
	Star7 - direction
	GroundStation16 - direction
	GroundStation27 - direction
	Star4 - direction
	GroundStation14 - direction
	GroundStation11 - direction
	GroundStation5 - direction
	Star0 - direction
	GroundStation28 - direction
	GroundStation21 - direction
	Star26 - direction
	Star3 - direction
	GroundStation10 - direction
	Star12 - direction
	Star22 - direction
	Star25 - direction
	Star9 - direction
	GroundStation15 - direction
	Star2 - direction
	GroundStation20 - direction
	GroundStation13 - direction
	Star17 - direction
	Star6 - direction
	Star19 - direction
	GroundStation29 - direction
	Star8 - direction
	Planet30 - direction
	Star31 - direction
	Planet32 - direction
	Phenomenon33 - direction
	Planet34 - direction
	Star35 - direction
	Star36 - direction
	Planet37 - direction
	Planet38 - direction
	Planet39 - direction
)
(:init
	(supports instrument0 thermograph4)
	(supports instrument0 spectrograph0)
	(supports instrument0 spectrograph5)
	(calibration_target instrument0 Star8)
	(calibration_target instrument0 Star1)
	(calibration_target instrument0 Star12)
	(calibration_target instrument0 Star18)
	(calibration_target instrument0 Star7)
	(supports instrument1 image3)
	(calibration_target instrument1 GroundStation10)
	(calibration_target instrument1 Star19)
	(calibration_target instrument1 GroundStation15)
	(calibration_target instrument1 Star25)
	(calibration_target instrument1 GroundStation20)
	(calibration_target instrument1 GroundStation28)
	(calibration_target instrument1 GroundStation24)
	(supports instrument2 spectrograph0)
	(supports instrument2 image3)
	(calibration_target instrument2 Star25)
	(calibration_target instrument2 GroundStation15)
	(supports instrument3 spectrograph5)
	(calibration_target instrument3 Star8)
	(calibration_target instrument3 GroundStation5)
	(calibration_target instrument3 Star4)
	(calibration_target instrument3 GroundStation10)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Planet38)
	(supports instrument4 infrared2)
	(supports instrument4 image3)
	(supports instrument4 spectrograph5)
	(calibration_target instrument4 GroundStation11)
	(calibration_target instrument4 GroundStation29)
	(calibration_target instrument4 GroundStation24)
	(calibration_target instrument4 Star17)
	(calibration_target instrument4 Star23)
	(calibration_target instrument4 Star12)
	(calibration_target instrument4 GroundStation15)
	(calibration_target instrument4 Star25)
	(calibration_target instrument4 GroundStation20)
	(on_board instrument4 satellite1)
	(power_avail satellite1)
	(pointing satellite1 GroundStation15)
	(supports instrument5 image3)
	(calibration_target instrument5 Star8)
	(supports instrument6 image3)
	(supports instrument6 image1)
	(supports instrument6 spectrograph5)
	(calibration_target instrument6 GroundStation28)
	(calibration_target instrument6 Star9)
	(supports instrument7 thermograph4)
	(supports instrument7 spectrograph5)
	(supports instrument7 infrared2)
	(calibration_target instrument7 Star23)
	(calibration_target instrument7 GroundStation24)
	(calibration_target instrument7 GroundStation13)
	(calibration_target instrument7 Star12)
	(calibration_target instrument7 Star26)
	(supports instrument8 infrared2)
	(supports instrument8 spectrograph0)
	(calibration_target instrument8 GroundStation11)
	(calibration_target instrument8 Star9)
	(supports instrument9 infrared2)
	(supports instrument9 image1)
	(supports instrument9 spectrograph0)
	(calibration_target instrument9 GroundStation27)
	(calibration_target instrument9 GroundStation13)
	(calibration_target instrument9 Star26)
	(calibration_target instrument9 GroundStation16)
	(calibration_target instrument9 Star9)
	(supports instrument10 image1)
	(supports instrument10 spectrograph0)
	(calibration_target instrument10 GroundStation28)
	(calibration_target instrument10 Star4)
	(calibration_target instrument10 Star22)
	(calibration_target instrument10 Star8)
	(on_board instrument5 satellite2)
	(on_board instrument6 satellite2)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star9)
	(supports instrument11 image3)
	(calibration_target instrument11 Star4)
	(calibration_target instrument11 Star22)
	(calibration_target instrument11 GroundStation16)
	(calibration_target instrument11 GroundStation27)
	(calibration_target instrument11 GroundStation21)
	(calibration_target instrument11 GroundStation5)
	(calibration_target instrument11 Star18)
	(supports instrument12 infrared2)
	(supports instrument12 thermograph4)
	(supports instrument12 image3)
	(calibration_target instrument12 Star3)
	(calibration_target instrument12 GroundStation27)
	(calibration_target instrument12 GroundStation21)
	(supports instrument13 image1)
	(supports instrument13 spectrograph0)
	(supports instrument13 infrared2)
	(calibration_target instrument13 GroundStation27)
	(calibration_target instrument13 Star22)
	(calibration_target instrument13 Star12)
	(calibration_target instrument13 GroundStation28)
	(calibration_target instrument13 Star8)
	(calibration_target instrument13 Star26)
	(on_board instrument11 satellite3)
	(on_board instrument12 satellite3)
	(on_board instrument13 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star17)
	(supports instrument14 spectrograph5)
	(supports instrument14 thermograph4)
	(supports instrument14 spectrograph0)
	(calibration_target instrument14 Star22)
	(calibration_target instrument14 GroundStation16)
	(calibration_target instrument14 GroundStation11)
	(calibration_target instrument14 Star25)
	(calibration_target instrument14 Star2)
	(calibration_target instrument14 Star23)
	(calibration_target instrument14 Star8)
	(calibration_target instrument14 Star0)
	(supports instrument15 image3)
	(calibration_target instrument15 Star26)
	(calibration_target instrument15 Star1)
	(calibration_target instrument15 Star12)
	(calibration_target instrument15 GroundStation20)
	(calibration_target instrument15 GroundStation28)
	(calibration_target instrument15 GroundStation27)
	(calibration_target instrument15 Star3)
	(calibration_target instrument15 GroundStation13)
	(calibration_target instrument15 Star23)
	(calibration_target instrument15 GroundStation16)
	(supports instrument16 spectrograph0)
	(supports instrument16 spectrograph5)
	(supports instrument16 image3)
	(calibration_target instrument16 Star22)
	(calibration_target instrument16 Star19)
	(calibration_target instrument16 Star8)
	(calibration_target instrument16 GroundStation16)
	(calibration_target instrument16 GroundStation10)
	(calibration_target instrument16 Star2)
	(supports instrument17 image1)
	(calibration_target instrument17 Star6)
	(calibration_target instrument17 Star25)
	(calibration_target instrument17 GroundStation14)
	(calibration_target instrument17 Star8)
	(calibration_target instrument17 Star18)
	(calibration_target instrument17 Star7)
	(on_board instrument14 satellite4)
	(on_board instrument15 satellite4)
	(on_board instrument16 satellite4)
	(on_board instrument17 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star6)
	(supports instrument18 spectrograph5)
	(supports instrument18 thermograph4)
	(supports instrument18 image3)
	(calibration_target instrument18 Star3)
	(calibration_target instrument18 GroundStation16)
	(calibration_target instrument18 GroundStation10)
	(calibration_target instrument18 GroundStation5)
	(calibration_target instrument18 Star7)
	(calibration_target instrument18 GroundStation15)
	(calibration_target instrument18 Star22)
	(calibration_target instrument18 Star18)
	(calibration_target instrument18 GroundStation27)
	(supports instrument19 thermograph4)
	(supports instrument19 image1)
	(supports instrument19 infrared2)
	(calibration_target instrument19 GroundStation10)
	(calibration_target instrument19 Star19)
	(calibration_target instrument19 Star25)
	(calibration_target instrument19 GroundStation5)
	(calibration_target instrument19 Star4)
	(calibration_target instrument19 Star7)
	(calibration_target instrument19 GroundStation21)
	(calibration_target instrument19 Star3)
	(calibration_target instrument19 Star23)
	(calibration_target instrument19 Star22)
	(supports instrument20 spectrograph0)
	(supports instrument20 image1)
	(supports instrument20 spectrograph5)
	(calibration_target instrument20 Star22)
	(calibration_target instrument20 GroundStation10)
	(supports instrument21 spectrograph0)
	(calibration_target instrument21 GroundStation16)
	(calibration_target instrument21 GroundStation14)
	(calibration_target instrument21 Star9)
	(supports instrument22 image1)
	(supports instrument22 image3)
	(supports instrument22 spectrograph5)
	(calibration_target instrument22 Star22)
	(calibration_target instrument22 GroundStation28)
	(calibration_target instrument22 GroundStation27)
	(supports instrument23 spectrograph0)
	(supports instrument23 spectrograph5)
	(calibration_target instrument23 GroundStation11)
	(calibration_target instrument23 Star19)
	(calibration_target instrument23 GroundStation14)
	(calibration_target instrument23 Star9)
	(calibration_target instrument23 Star4)
	(calibration_target instrument23 GroundStation5)
	(calibration_target instrument23 Star0)
	(on_board instrument18 satellite5)
	(on_board instrument19 satellite5)
	(on_board instrument20 satellite5)
	(on_board instrument21 satellite5)
	(on_board instrument22 satellite5)
	(on_board instrument23 satellite5)
	(power_avail satellite5)
	(pointing satellite5 GroundStation10)
	(supports instrument24 spectrograph5)
	(supports instrument24 spectrograph0)
	(supports instrument24 infrared2)
	(calibration_target instrument24 Star3)
	(calibration_target instrument24 GroundStation5)
	(calibration_target instrument24 Star25)
	(supports instrument25 image3)
	(supports instrument25 infrared2)
	(supports instrument25 image1)
	(calibration_target instrument25 Star0)
	(supports instrument26 thermograph4)
	(supports instrument26 infrared2)
	(calibration_target instrument26 GroundStation10)
	(calibration_target instrument26 Star3)
	(calibration_target instrument26 Star26)
	(calibration_target instrument26 Star8)
	(calibration_target instrument26 Star17)
	(calibration_target instrument26 GroundStation21)
	(calibration_target instrument26 Star2)
	(calibration_target instrument26 GroundStation28)
	(on_board instrument24 satellite6)
	(on_board instrument25 satellite6)
	(on_board instrument26 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Star0)
	(supports instrument27 thermograph4)
	(calibration_target instrument27 Star9)
	(calibration_target instrument27 Star19)
	(calibration_target instrument27 GroundStation15)
	(supports instrument28 image3)
	(calibration_target instrument28 Star12)
	(calibration_target instrument28 GroundStation29)
	(supports instrument29 infrared2)
	(supports instrument29 spectrograph0)
	(supports instrument29 thermograph4)
	(calibration_target instrument29 Star8)
	(calibration_target instrument29 Star19)
	(calibration_target instrument29 GroundStation13)
	(calibration_target instrument29 GroundStation29)
	(calibration_target instrument29 GroundStation20)
	(calibration_target instrument29 Star2)
	(calibration_target instrument29 GroundStation15)
	(calibration_target instrument29 Star9)
	(calibration_target instrument29 Star25)
	(calibration_target instrument29 Star22)
	(supports instrument30 infrared2)
	(supports instrument30 image1)
	(supports instrument30 spectrograph5)
	(calibration_target instrument30 Star8)
	(calibration_target instrument30 GroundStation29)
	(calibration_target instrument30 Star19)
	(calibration_target instrument30 Star6)
	(calibration_target instrument30 Star17)
	(on_board instrument27 satellite7)
	(on_board instrument28 satellite7)
	(on_board instrument29 satellite7)
	(on_board instrument30 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Star1)
)
(:goal (and
	(pointing satellite0 Star26)
	(pointing satellite2 Star0)
	(pointing satellite3 Star31)
	(pointing satellite5 GroundStation5)
	(have_image Planet30 image3)
	(have_image Star31 spectrograph0)
	(have_image Star31 spectrograph5)
	(have_image Planet32 thermograph4)
	(have_image Phenomenon33 thermograph4)
	(have_image Phenomenon33 spectrograph0)
	(have_image Planet34 infrared2)
	(have_image Planet34 spectrograph5)
	(have_image Star35 image3)
	(have_image Star35 image1)
	(have_image Star36 spectrograph0)
	(have_image Planet37 infrared2)
	(have_image Planet37 spectrograph5)
	(have_image Planet38 spectrograph0)
	(have_image Planet38 image3)
))

)
