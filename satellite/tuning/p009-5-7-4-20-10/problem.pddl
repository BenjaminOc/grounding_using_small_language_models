(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	satellite1 - satellite
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	satellite2 - satellite
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	satellite3 - satellite
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	satellite4 - satellite
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	instrument25 - instrument
	image3 - mode
	infrared2 - mode
	infrared0 - mode
	spectrograph1 - mode
	GroundStation15 - direction
	Star5 - direction
	Star6 - direction
	GroundStation1 - direction
	Star17 - direction
	Star13 - direction
	GroundStation11 - direction
	GroundStation3 - direction
	GroundStation10 - direction
	GroundStation9 - direction
	Star12 - direction
	GroundStation16 - direction
	GroundStation18 - direction
	Star19 - direction
	Star8 - direction
	Star2 - direction
	GroundStation4 - direction
	GroundStation0 - direction
	GroundStation7 - direction
	Star14 - direction
	Star20 - direction
	Star21 - direction
	Phenomenon22 - direction
	Phenomenon23 - direction
	Star24 - direction
	Planet25 - direction
	Phenomenon26 - direction
	Planet27 - direction
	Planet28 - direction
	Star29 - direction
)
(:init
	(supports instrument0 infrared0)
	(calibration_target instrument0 Star2)
	(calibration_target instrument0 GroundStation18)
	(supports instrument1 infrared0)
	(supports instrument1 spectrograph1)
	(supports instrument1 image3)
	(calibration_target instrument1 GroundStation3)
	(calibration_target instrument1 Star14)
	(calibration_target instrument1 Star2)
	(calibration_target instrument1 GroundStation15)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation15)
	(supports instrument2 image3)
	(calibration_target instrument2 GroundStation7)
	(calibration_target instrument2 Star12)
	(supports instrument3 image3)
	(supports instrument3 spectrograph1)
	(calibration_target instrument3 Star14)
	(calibration_target instrument3 Star13)
	(calibration_target instrument3 Star12)
	(calibration_target instrument3 GroundStation10)
	(supports instrument4 image3)
	(supports instrument4 infrared0)
	(supports instrument4 infrared2)
	(calibration_target instrument4 GroundStation9)
	(calibration_target instrument4 Star12)
	(calibration_target instrument4 GroundStation7)
	(calibration_target instrument4 GroundStation3)
	(calibration_target instrument4 GroundStation15)
	(supports instrument5 infrared0)
	(supports instrument5 image3)
	(supports instrument5 infrared2)
	(calibration_target instrument5 GroundStation3)
	(calibration_target instrument5 GroundStation1)
	(supports instrument6 image3)
	(supports instrument6 spectrograph1)
	(calibration_target instrument6 GroundStation7)
	(calibration_target instrument6 Star5)
	(calibration_target instrument6 GroundStation9)
	(calibration_target instrument6 GroundStation10)
	(calibration_target instrument6 GroundStation4)
	(supports instrument7 infrared0)
	(supports instrument7 spectrograph1)
	(calibration_target instrument7 Star6)
	(calibration_target instrument7 Star19)
	(calibration_target instrument7 Star14)
	(supports instrument8 infrared0)
	(supports instrument8 image3)
	(supports instrument8 spectrograph1)
	(calibration_target instrument8 GroundStation16)
	(calibration_target instrument8 Star2)
	(calibration_target instrument8 GroundStation1)
	(calibration_target instrument8 Star19)
	(calibration_target instrument8 Star13)
	(calibration_target instrument8 GroundStation11)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star24)
	(supports instrument9 spectrograph1)
	(supports instrument9 infrared0)
	(calibration_target instrument9 GroundStation4)
	(calibration_target instrument9 Star12)
	(calibration_target instrument9 Star2)
	(calibration_target instrument9 GroundStation18)
	(calibration_target instrument9 GroundStation0)
	(calibration_target instrument9 GroundStation10)
	(supports instrument10 infrared0)
	(supports instrument10 infrared2)
	(calibration_target instrument10 Star19)
	(calibration_target instrument10 Star14)
	(calibration_target instrument10 Star13)
	(calibration_target instrument10 Star12)
	(calibration_target instrument10 GroundStation10)
	(calibration_target instrument10 Star2)
	(supports instrument11 infrared2)
	(supports instrument11 image3)
	(calibration_target instrument11 Star12)
	(calibration_target instrument11 Star17)
	(calibration_target instrument11 GroundStation9)
	(calibration_target instrument11 Star13)
	(calibration_target instrument11 GroundStation11)
	(calibration_target instrument11 Star8)
	(supports instrument12 infrared0)
	(supports instrument12 image3)
	(supports instrument12 spectrograph1)
	(calibration_target instrument12 GroundStation3)
	(calibration_target instrument12 Star6)
	(calibration_target instrument12 GroundStation18)
	(calibration_target instrument12 Star12)
	(calibration_target instrument12 Star5)
	(supports instrument13 spectrograph1)
	(supports instrument13 image3)
	(supports instrument13 infrared0)
	(calibration_target instrument13 Star14)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(on_board instrument12 satellite2)
	(on_board instrument13 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation4)
	(supports instrument14 spectrograph1)
	(supports instrument14 infrared0)
	(supports instrument14 infrared2)
	(calibration_target instrument14 Star6)
	(calibration_target instrument14 Star12)
	(calibration_target instrument14 Star8)
	(supports instrument15 infrared2)
	(calibration_target instrument15 Star19)
	(calibration_target instrument15 GroundStation16)
	(calibration_target instrument15 Star17)
	(supports instrument16 spectrograph1)
	(calibration_target instrument16 GroundStation0)
	(calibration_target instrument16 Star14)
	(calibration_target instrument16 GroundStation1)
	(supports instrument17 infrared0)
	(supports instrument17 infrared2)
	(calibration_target instrument17 GroundStation0)
	(calibration_target instrument17 Star17)
	(supports instrument18 spectrograph1)
	(supports instrument18 infrared0)
	(calibration_target instrument18 Star19)
	(calibration_target instrument18 GroundStation4)
	(calibration_target instrument18 GroundStation10)
	(calibration_target instrument18 GroundStation11)
	(calibration_target instrument18 Star8)
	(supports instrument19 infrared0)
	(supports instrument19 image3)
	(supports instrument19 infrared2)
	(calibration_target instrument19 Star13)
	(calibration_target instrument19 Star12)
	(supports instrument20 infrared0)
	(supports instrument20 infrared2)
	(supports instrument20 spectrograph1)
	(calibration_target instrument20 GroundStation3)
	(calibration_target instrument20 Star12)
	(calibration_target instrument20 GroundStation11)
	(calibration_target instrument20 Star19)
	(calibration_target instrument20 Star2)
	(calibration_target instrument20 Star8)
	(on_board instrument14 satellite3)
	(on_board instrument15 satellite3)
	(on_board instrument16 satellite3)
	(on_board instrument17 satellite3)
	(on_board instrument18 satellite3)
	(on_board instrument19 satellite3)
	(on_board instrument20 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Planet27)
	(supports instrument21 spectrograph1)
	(supports instrument21 infrared0)
	(calibration_target instrument21 Star2)
	(calibration_target instrument21 Star14)
	(calibration_target instrument21 Star8)
	(calibration_target instrument21 GroundStation10)
	(calibration_target instrument21 GroundStation16)
	(supports instrument22 infrared0)
	(calibration_target instrument22 Star8)
	(calibration_target instrument22 GroundStation18)
	(calibration_target instrument22 GroundStation16)
	(calibration_target instrument22 Star12)
	(calibration_target instrument22 GroundStation9)
	(calibration_target instrument22 Star2)
	(supports instrument23 image3)
	(supports instrument23 infrared2)
	(supports instrument23 spectrograph1)
	(calibration_target instrument23 Star8)
	(supports instrument24 infrared2)
	(supports instrument24 spectrograph1)
	(calibration_target instrument24 GroundStation7)
	(calibration_target instrument24 GroundStation0)
	(calibration_target instrument24 GroundStation4)
	(calibration_target instrument24 Star2)
	(calibration_target instrument24 Star8)
	(calibration_target instrument24 Star19)
	(supports instrument25 spectrograph1)
	(supports instrument25 infrared0)
	(calibration_target instrument25 Star14)
	(on_board instrument21 satellite4)
	(on_board instrument22 satellite4)
	(on_board instrument23 satellite4)
	(on_board instrument24 satellite4)
	(on_board instrument25 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star12)
)
(:goal (and
	(pointing satellite2 Star21)
	(have_image Star20 spectrograph1)
	(have_image Star21 infrared2)
	(have_image Phenomenon22 spectrograph1)
	(have_image Phenomenon23 image3)
	(have_image Planet25 infrared0)
	(have_image Phenomenon26 spectrograph1)
	(have_image Planet27 image3)
	(have_image Planet28 infrared0)
	(have_image Star29 infrared0)
))

)
