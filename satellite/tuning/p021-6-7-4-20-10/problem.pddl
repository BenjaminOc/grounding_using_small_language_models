(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	satellite1 - satellite
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite2 - satellite
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	satellite3 - satellite
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	satellite4 - satellite
	instrument20 - instrument
	instrument21 - instrument
	satellite5 - satellite
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	instrument25 - instrument
	instrument26 - instrument
	spectrograph3 - mode
	image2 - mode
	spectrograph0 - mode
	infrared1 - mode
	GroundStation9 - direction
	GroundStation8 - direction
	GroundStation19 - direction
	GroundStation6 - direction
	Star12 - direction
	GroundStation4 - direction
	GroundStation1 - direction
	Star2 - direction
	GroundStation18 - direction
	Star11 - direction
	GroundStation5 - direction
	Star3 - direction
	GroundStation16 - direction
	GroundStation7 - direction
	GroundStation14 - direction
	GroundStation10 - direction
	GroundStation15 - direction
	GroundStation13 - direction
	GroundStation17 - direction
	Star0 - direction
	Phenomenon20 - direction
	Star21 - direction
	Phenomenon22 - direction
	Planet23 - direction
	Phenomenon24 - direction
	Star25 - direction
	Planet26 - direction
	Star27 - direction
	Star28 - direction
	Planet29 - direction
)
(:init
	(supports instrument0 spectrograph0)
	(supports instrument0 infrared1)
	(supports instrument0 image2)
	(calibration_target instrument0 Star3)
	(calibration_target instrument0 GroundStation17)
	(calibration_target instrument0 GroundStation10)
	(calibration_target instrument0 GroundStation19)
	(calibration_target instrument0 Star11)
	(supports instrument1 image2)
	(supports instrument1 spectrograph0)
	(calibration_target instrument1 GroundStation4)
	(calibration_target instrument1 GroundStation5)
	(calibration_target instrument1 GroundStation6)
	(calibration_target instrument1 GroundStation9)
	(supports instrument2 spectrograph0)
	(supports instrument2 image2)
	(supports instrument2 spectrograph3)
	(calibration_target instrument2 GroundStation15)
	(calibration_target instrument2 Star3)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Star28)
	(supports instrument3 infrared1)
	(supports instrument3 spectrograph3)
	(calibration_target instrument3 GroundStation14)
	(calibration_target instrument3 Star0)
	(supports instrument4 image2)
	(calibration_target instrument4 GroundStation7)
	(calibration_target instrument4 GroundStation14)
	(calibration_target instrument4 Star0)
	(calibration_target instrument4 GroundStation17)
	(calibration_target instrument4 GroundStation9)
	(supports instrument5 spectrograph3)
	(supports instrument5 image2)
	(calibration_target instrument5 Star3)
	(calibration_target instrument5 GroundStation10)
	(calibration_target instrument5 GroundStation18)
	(supports instrument6 spectrograph0)
	(calibration_target instrument6 GroundStation4)
	(calibration_target instrument6 GroundStation15)
	(calibration_target instrument6 GroundStation19)
	(supports instrument7 spectrograph0)
	(supports instrument7 spectrograph3)
	(calibration_target instrument7 GroundStation15)
	(calibration_target instrument7 Star11)
	(supports instrument8 spectrograph0)
	(supports instrument8 infrared1)
	(supports instrument8 spectrograph3)
	(calibration_target instrument8 GroundStation5)
	(calibration_target instrument8 GroundStation14)
	(calibration_target instrument8 GroundStation19)
	(supports instrument9 image2)
	(supports instrument9 infrared1)
	(supports instrument9 spectrograph3)
	(calibration_target instrument9 GroundStation19)
	(calibration_target instrument9 Star0)
	(calibration_target instrument9 GroundStation1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(on_board instrument9 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Phenomenon24)
	(supports instrument10 spectrograph3)
	(supports instrument10 image2)
	(calibration_target instrument10 GroundStation18)
	(calibration_target instrument10 GroundStation17)
	(supports instrument11 infrared1)
	(supports instrument11 spectrograph3)
	(supports instrument11 spectrograph0)
	(calibration_target instrument11 GroundStation14)
	(calibration_target instrument11 GroundStation16)
	(calibration_target instrument11 GroundStation8)
	(calibration_target instrument11 GroundStation5)
	(supports instrument12 image2)
	(calibration_target instrument12 Star12)
	(calibration_target instrument12 GroundStation7)
	(calibration_target instrument12 GroundStation18)
	(calibration_target instrument12 GroundStation19)
	(calibration_target instrument12 GroundStation17)
	(supports instrument13 spectrograph3)
	(supports instrument13 image2)
	(calibration_target instrument13 GroundStation4)
	(calibration_target instrument13 GroundStation14)
	(supports instrument14 infrared1)
	(supports instrument14 spectrograph3)
	(supports instrument14 image2)
	(calibration_target instrument14 GroundStation8)
	(calibration_target instrument14 GroundStation16)
	(calibration_target instrument14 Star11)
	(supports instrument15 spectrograph0)
	(supports instrument15 spectrograph3)
	(supports instrument15 infrared1)
	(calibration_target instrument15 Star3)
	(calibration_target instrument15 Star12)
	(calibration_target instrument15 GroundStation5)
	(calibration_target instrument15 GroundStation16)
	(calibration_target instrument15 Star2)
	(supports instrument16 image2)
	(supports instrument16 spectrograph0)
	(supports instrument16 spectrograph3)
	(calibration_target instrument16 GroundStation7)
	(calibration_target instrument16 Star12)
	(calibration_target instrument16 Star0)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(on_board instrument12 satellite2)
	(on_board instrument13 satellite2)
	(on_board instrument14 satellite2)
	(on_board instrument15 satellite2)
	(on_board instrument16 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star21)
	(supports instrument17 spectrograph3)
	(supports instrument17 infrared1)
	(supports instrument17 spectrograph0)
	(calibration_target instrument17 GroundStation8)
	(calibration_target instrument17 GroundStation9)
	(supports instrument18 spectrograph3)
	(supports instrument18 image2)
	(calibration_target instrument18 GroundStation16)
	(calibration_target instrument18 GroundStation1)
	(calibration_target instrument18 GroundStation18)
	(calibration_target instrument18 GroundStation15)
	(supports instrument19 image2)
	(supports instrument19 spectrograph3)
	(calibration_target instrument19 GroundStation14)
	(calibration_target instrument19 Star12)
	(calibration_target instrument19 GroundStation1)
	(calibration_target instrument19 Star3)
	(on_board instrument17 satellite3)
	(on_board instrument18 satellite3)
	(on_board instrument19 satellite3)
	(power_avail satellite3)
	(pointing satellite3 GroundStation16)
	(supports instrument20 spectrograph3)
	(supports instrument20 spectrograph0)
	(calibration_target instrument20 GroundStation19)
	(calibration_target instrument20 Star0)
	(calibration_target instrument20 GroundStation4)
	(calibration_target instrument20 GroundStation5)
	(calibration_target instrument20 GroundStation6)
	(calibration_target instrument20 GroundStation10)
	(supports instrument21 spectrograph0)
	(calibration_target instrument21 GroundStation4)
	(calibration_target instrument21 Star12)
	(calibration_target instrument21 GroundStation7)
	(calibration_target instrument21 GroundStation18)
	(calibration_target instrument21 Star11)
	(calibration_target instrument21 GroundStation6)
	(on_board instrument20 satellite4)
	(on_board instrument21 satellite4)
	(power_avail satellite4)
	(pointing satellite4 GroundStation16)
	(supports instrument22 spectrograph0)
	(supports instrument22 spectrograph3)
	(supports instrument22 infrared1)
	(calibration_target instrument22 GroundStation16)
	(supports instrument23 infrared1)
	(calibration_target instrument23 Star11)
	(calibration_target instrument23 GroundStation15)
	(calibration_target instrument23 Star0)
	(calibration_target instrument23 GroundStation1)
	(calibration_target instrument23 GroundStation5)
	(supports instrument24 spectrograph3)
	(calibration_target instrument24 Star3)
	(calibration_target instrument24 GroundStation5)
	(calibration_target instrument24 Star11)
	(calibration_target instrument24 GroundStation18)
	(calibration_target instrument24 Star2)
	(supports instrument25 spectrograph0)
	(supports instrument25 image2)
	(calibration_target instrument25 GroundStation15)
	(calibration_target instrument25 GroundStation10)
	(calibration_target instrument25 GroundStation14)
	(calibration_target instrument25 GroundStation7)
	(calibration_target instrument25 GroundStation13)
	(calibration_target instrument25 GroundStation16)
	(supports instrument26 infrared1)
	(calibration_target instrument26 Star0)
	(calibration_target instrument26 GroundStation17)
	(calibration_target instrument26 GroundStation13)
	(on_board instrument22 satellite5)
	(on_board instrument23 satellite5)
	(on_board instrument24 satellite5)
	(on_board instrument25 satellite5)
	(on_board instrument26 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Star2)
)
(:goal (and
	(pointing satellite0 GroundStation14)
	(pointing satellite1 GroundStation4)
	(pointing satellite3 GroundStation19)
	(pointing satellite5 Star3)
	(have_image Phenomenon20 spectrograph0)
	(have_image Star21 spectrograph3)
	(have_image Phenomenon22 spectrograph0)
	(have_image Phenomenon24 infrared1)
	(have_image Star25 infrared1)
	(have_image Planet26 spectrograph0)
	(have_image Star27 infrared1)
	(have_image Star28 infrared1)
	(have_image Planet29 infrared1)
))

)
