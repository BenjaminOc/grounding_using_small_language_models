(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	satellite1 - satellite
	instrument5 - instrument
	instrument6 - instrument
	satellite2 - satellite
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite3 - satellite
	instrument10 - instrument
	instrument11 - instrument
	satellite4 - satellite
	instrument12 - instrument
	infrared3 - mode
	spectrograph0 - mode
	infrared2 - mode
	thermograph1 - mode
	Star4 - direction
	Star9 - direction
	GroundStation14 - direction
	GroundStation21 - direction
	Star23 - direction
	GroundStation17 - direction
	GroundStation16 - direction
	GroundStation27 - direction
	GroundStation25 - direction
	Star19 - direction
	Star0 - direction
	Star22 - direction
	GroundStation28 - direction
	Star2 - direction
	Star1 - direction
	GroundStation7 - direction
	Star10 - direction
	Star6 - direction
	Star13 - direction
	GroundStation8 - direction
	GroundStation5 - direction
	Star20 - direction
	GroundStation24 - direction
	Star3 - direction
	GroundStation15 - direction
	Star12 - direction
	Star26 - direction
	Star18 - direction
	GroundStation11 - direction
	GroundStation29 - direction
	Phenomenon30 - direction
	Phenomenon31 - direction
	Planet32 - direction
	Phenomenon33 - direction
	Star34 - direction
	Phenomenon35 - direction
	Planet36 - direction
	Planet37 - direction
	Star38 - direction
	Phenomenon39 - direction
)
(:init
	(supports instrument0 infrared3)
	(calibration_target instrument0 Star22)
	(calibration_target instrument0 GroundStation25)
	(calibration_target instrument0 Star20)
	(calibration_target instrument0 GroundStation28)
	(calibration_target instrument0 Star23)
	(calibration_target instrument0 GroundStation29)
	(calibration_target instrument0 GroundStation8)
	(calibration_target instrument0 Star2)
	(supports instrument1 thermograph1)
	(supports instrument1 infrared3)
	(supports instrument1 infrared2)
	(calibration_target instrument1 Star6)
	(calibration_target instrument1 Star13)
	(calibration_target instrument1 Star18)
	(calibration_target instrument1 GroundStation28)
	(supports instrument2 infrared3)
	(supports instrument2 infrared2)
	(supports instrument2 thermograph1)
	(calibration_target instrument2 GroundStation11)
	(calibration_target instrument2 GroundStation17)
	(calibration_target instrument2 GroundStation7)
	(supports instrument3 infrared2)
	(supports instrument3 spectrograph0)
	(supports instrument3 thermograph1)
	(calibration_target instrument3 Star3)
	(calibration_target instrument3 GroundStation15)
	(calibration_target instrument3 Star0)
	(calibration_target instrument3 Star22)
	(supports instrument4 spectrograph0)
	(supports instrument4 infrared3)
	(supports instrument4 thermograph1)
	(calibration_target instrument4 GroundStation15)
	(calibration_target instrument4 Star22)
	(calibration_target instrument4 Star10)
	(calibration_target instrument4 Star2)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(on_board instrument4 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation27)
	(supports instrument5 infrared2)
	(supports instrument5 spectrograph0)
	(calibration_target instrument5 GroundStation29)
	(calibration_target instrument5 GroundStation7)
	(calibration_target instrument5 Star19)
	(calibration_target instrument5 GroundStation28)
	(calibration_target instrument5 Star12)
	(calibration_target instrument5 Star26)
	(supports instrument6 thermograph1)
	(supports instrument6 spectrograph0)
	(supports instrument6 infrared3)
	(calibration_target instrument6 GroundStation16)
	(calibration_target instrument6 GroundStation5)
	(calibration_target instrument6 GroundStation29)
	(calibration_target instrument6 Star20)
	(calibration_target instrument6 GroundStation7)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star2)
	(supports instrument7 infrared2)
	(supports instrument7 thermograph1)
	(calibration_target instrument7 Star1)
	(calibration_target instrument7 Star19)
	(calibration_target instrument7 GroundStation27)
	(supports instrument8 thermograph1)
	(supports instrument8 spectrograph0)
	(calibration_target instrument8 GroundStation7)
	(supports instrument9 infrared3)
	(supports instrument9 spectrograph0)
	(supports instrument9 infrared2)
	(calibration_target instrument9 Star19)
	(calibration_target instrument9 GroundStation25)
	(calibration_target instrument9 Star26)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation25)
	(supports instrument10 spectrograph0)
	(supports instrument10 thermograph1)
	(supports instrument10 infrared2)
	(calibration_target instrument10 Star6)
	(calibration_target instrument10 Star13)
	(calibration_target instrument10 Star10)
	(calibration_target instrument10 GroundStation7)
	(calibration_target instrument10 Star1)
	(calibration_target instrument10 Star2)
	(calibration_target instrument10 GroundStation28)
	(calibration_target instrument10 Star22)
	(calibration_target instrument10 Star0)
	(calibration_target instrument10 GroundStation5)
	(supports instrument11 infrared3)
	(supports instrument11 infrared2)
	(supports instrument11 spectrograph0)
	(calibration_target instrument11 Star20)
	(calibration_target instrument11 Star12)
	(calibration_target instrument11 Star18)
	(calibration_target instrument11 GroundStation5)
	(calibration_target instrument11 Star26)
	(calibration_target instrument11 GroundStation8)
	(calibration_target instrument11 Star13)
	(on_board instrument10 satellite3)
	(on_board instrument11 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Phenomenon33)
	(supports instrument12 thermograph1)
	(supports instrument12 infrared2)
	(supports instrument12 spectrograph0)
	(calibration_target instrument12 GroundStation29)
	(calibration_target instrument12 GroundStation11)
	(calibration_target instrument12 Star18)
	(calibration_target instrument12 Star26)
	(calibration_target instrument12 Star12)
	(calibration_target instrument12 GroundStation15)
	(calibration_target instrument12 Star3)
	(calibration_target instrument12 GroundStation24)
	(calibration_target instrument12 Star20)
	(on_board instrument12 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star23)
)
(:goal (and
	(pointing satellite0 Star19)
	(pointing satellite1 GroundStation7)
	(pointing satellite4 GroundStation11)
	(have_image Phenomenon30 infrared3)
	(have_image Phenomenon31 infrared3)
	(have_image Planet32 spectrograph0)
	(have_image Phenomenon33 infrared2)
	(have_image Star34 infrared3)
	(have_image Phenomenon35 infrared2)
	(have_image Planet36 infrared2)
	(have_image Planet37 spectrograph0)
	(have_image Star38 thermograph1)
	(have_image Phenomenon39 infrared3)
))

)
