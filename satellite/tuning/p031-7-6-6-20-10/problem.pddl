(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	satellite1 - satellite
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	satellite2 - satellite
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	satellite3 - satellite
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	satellite4 - satellite
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	satellite5 - satellite
	instrument20 - instrument
	satellite6 - satellite
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	thermograph2 - mode
	image5 - mode
	spectrograph0 - mode
	image3 - mode
	thermograph1 - mode
	image4 - mode
	Star4 - direction
	Star9 - direction
	Star5 - direction
	Star0 - direction
	Star7 - direction
	GroundStation1 - direction
	GroundStation14 - direction
	GroundStation13 - direction
	Star12 - direction
	GroundStation6 - direction
	Star16 - direction
	GroundStation18 - direction
	Star10 - direction
	Star3 - direction
	GroundStation2 - direction
	GroundStation15 - direction
	GroundStation8 - direction
	Star17 - direction
	Star11 - direction
	Star19 - direction
	Phenomenon20 - direction
	Star21 - direction
	Phenomenon22 - direction
	Star23 - direction
	Planet24 - direction
	Planet25 - direction
	Star26 - direction
	Phenomenon27 - direction
	Planet28 - direction
	Phenomenon29 - direction
)
(:init
	(supports instrument0 spectrograph0)
	(calibration_target instrument0 Star4)
	(calibration_target instrument0 GroundStation2)
	(calibration_target instrument0 Star5)
	(calibration_target instrument0 Star10)
	(calibration_target instrument0 Star0)
	(calibration_target instrument0 GroundStation14)
	(supports instrument1 thermograph1)
	(supports instrument1 spectrograph0)
	(supports instrument1 thermograph2)
	(calibration_target instrument1 GroundStation6)
	(calibration_target instrument1 Star19)
	(calibration_target instrument1 GroundStation1)
	(calibration_target instrument1 Star9)
	(calibration_target instrument1 Star4)
	(calibration_target instrument1 Star10)
	(supports instrument2 image3)
	(supports instrument2 thermograph2)
	(calibration_target instrument2 GroundStation2)
	(calibration_target instrument2 GroundStation1)
	(calibration_target instrument2 Star9)
	(calibration_target instrument2 Star12)
	(calibration_target instrument2 GroundStation6)
	(supports instrument3 thermograph2)
	(supports instrument3 spectrograph0)
	(calibration_target instrument3 Star4)
	(supports instrument4 spectrograph0)
	(supports instrument4 image3)
	(supports instrument4 thermograph1)
	(calibration_target instrument4 Star19)
	(calibration_target instrument4 GroundStation15)
	(calibration_target instrument4 Star12)
	(calibration_target instrument4 Star5)
	(supports instrument5 image5)
	(supports instrument5 spectrograph0)
	(calibration_target instrument5 Star19)
	(calibration_target instrument5 Star16)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(on_board instrument4 satellite0)
	(on_board instrument5 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation15)
	(supports instrument6 image4)
	(supports instrument6 image5)
	(calibration_target instrument6 Star4)
	(calibration_target instrument6 GroundStation2)
	(supports instrument7 thermograph1)
	(supports instrument7 image4)
	(calibration_target instrument7 Star7)
	(supports instrument8 spectrograph0)
	(calibration_target instrument8 Star10)
	(calibration_target instrument8 Star0)
	(calibration_target instrument8 Star16)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star7)
	(supports instrument9 image3)
	(calibration_target instrument9 Star17)
	(calibration_target instrument9 Star19)
	(calibration_target instrument9 GroundStation6)
	(calibration_target instrument9 Star10)
	(supports instrument10 image4)
	(supports instrument10 image3)
	(calibration_target instrument10 Star16)
	(calibration_target instrument10 Star9)
	(calibration_target instrument10 Star3)
	(calibration_target instrument10 Star11)
	(supports instrument11 thermograph2)
	(supports instrument11 spectrograph0)
	(supports instrument11 image4)
	(calibration_target instrument11 GroundStation2)
	(calibration_target instrument11 Star10)
	(calibration_target instrument11 Star16)
	(supports instrument12 image4)
	(supports instrument12 thermograph2)
	(supports instrument12 spectrograph0)
	(calibration_target instrument12 Star19)
	(calibration_target instrument12 Star5)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(on_board instrument12 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star3)
	(supports instrument13 spectrograph0)
	(calibration_target instrument13 Star17)
	(calibration_target instrument13 Star7)
	(calibration_target instrument13 GroundStation18)
	(calibration_target instrument13 GroundStation8)
	(calibration_target instrument13 Star0)
	(calibration_target instrument13 Star11)
	(supports instrument14 thermograph2)
	(calibration_target instrument14 GroundStation13)
	(supports instrument15 thermograph1)
	(supports instrument15 spectrograph0)
	(supports instrument15 thermograph2)
	(calibration_target instrument15 Star11)
	(supports instrument16 thermograph1)
	(supports instrument16 thermograph2)
	(calibration_target instrument16 GroundStation1)
	(calibration_target instrument16 Star3)
	(calibration_target instrument16 GroundStation13)
	(calibration_target instrument16 Star19)
	(calibration_target instrument16 GroundStation8)
	(calibration_target instrument16 GroundStation14)
	(on_board instrument13 satellite3)
	(on_board instrument14 satellite3)
	(on_board instrument15 satellite3)
	(on_board instrument16 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Phenomenon29)
	(supports instrument17 image5)
	(supports instrument17 thermograph2)
	(calibration_target instrument17 GroundStation1)
	(supports instrument18 thermograph2)
	(supports instrument18 image3)
	(supports instrument18 image5)
	(calibration_target instrument18 GroundStation15)
	(calibration_target instrument18 GroundStation13)
	(calibration_target instrument18 Star17)
	(supports instrument19 spectrograph0)
	(calibration_target instrument19 GroundStation14)
	(calibration_target instrument19 GroundStation2)
	(calibration_target instrument19 Star3)
	(calibration_target instrument19 Star19)
	(on_board instrument17 satellite4)
	(on_board instrument18 satellite4)
	(on_board instrument19 satellite4)
	(power_avail satellite4)
	(pointing satellite4 GroundStation6)
	(supports instrument20 spectrograph0)
	(calibration_target instrument20 Star3)
	(calibration_target instrument20 Star12)
	(calibration_target instrument20 GroundStation13)
	(calibration_target instrument20 Star10)
	(calibration_target instrument20 GroundStation6)
	(calibration_target instrument20 GroundStation8)
	(on_board instrument20 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Star9)
	(supports instrument21 thermograph1)
	(supports instrument21 image4)
	(calibration_target instrument21 GroundStation2)
	(calibration_target instrument21 Star16)
	(calibration_target instrument21 GroundStation6)
	(calibration_target instrument21 GroundStation8)
	(supports instrument22 spectrograph0)
	(supports instrument22 thermograph1)
	(supports instrument22 image5)
	(calibration_target instrument22 Star19)
	(calibration_target instrument22 GroundStation2)
	(calibration_target instrument22 Star3)
	(calibration_target instrument22 Star10)
	(calibration_target instrument22 GroundStation18)
	(supports instrument23 image4)
	(supports instrument23 thermograph1)
	(supports instrument23 image3)
	(calibration_target instrument23 Star19)
	(calibration_target instrument23 Star11)
	(calibration_target instrument23 Star17)
	(calibration_target instrument23 GroundStation8)
	(calibration_target instrument23 GroundStation15)
	(on_board instrument21 satellite6)
	(on_board instrument22 satellite6)
	(on_board instrument23 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Planet28)
)
(:goal (and
	(pointing satellite3 Planet24)
	(pointing satellite5 Star12)
	(have_image Phenomenon20 image5)
	(have_image Star21 spectrograph0)
	(have_image Star21 image5)
	(have_image Phenomenon22 thermograph2)
	(have_image Phenomenon22 image5)
	(have_image Star23 thermograph2)
	(have_image Planet25 image3)
	(have_image Star26 image3)
	(have_image Star26 image4)
	(have_image Phenomenon27 thermograph1)
	(have_image Planet28 thermograph2)
	(have_image Phenomenon29 image3)
	(have_image Phenomenon29 spectrograph0)
))

)
