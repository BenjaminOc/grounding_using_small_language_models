(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	satellite1 - satellite
	instrument1 - instrument
	instrument2 - instrument
	satellite2 - satellite
	instrument3 - instrument
	instrument4 - instrument
	satellite3 - satellite
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite4 - satellite
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	satellite5 - satellite
	instrument13 - instrument
	instrument14 - instrument
	satellite6 - satellite
	instrument15 - instrument
	instrument16 - instrument
	satellite7 - satellite
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	thermograph6 - mode
	infrared1 - mode
	image3 - mode
	spectrograph2 - mode
	image4 - mode
	image8 - mode
	thermograph7 - mode
	image0 - mode
	image5 - mode
	spectrograph9 - mode
	Star9 - direction
	Star25 - direction
	GroundStation16 - direction
	GroundStation35 - direction
	GroundStation32 - direction
	Star27 - direction
	Star0 - direction
	Star41 - direction
	Star28 - direction
	Star14 - direction
	GroundStation2 - direction
	GroundStation42 - direction
	Star46 - direction
	GroundStation33 - direction
	Star49 - direction
	Star47 - direction
	Star29 - direction
	Star8 - direction
	GroundStation13 - direction
	GroundStation3 - direction
	GroundStation15 - direction
	GroundStation23 - direction
	GroundStation10 - direction
	GroundStation11 - direction
	Star37 - direction
	GroundStation36 - direction
	GroundStation38 - direction
	Star44 - direction
	GroundStation7 - direction
	Star45 - direction
	Star1 - direction
	Star26 - direction
	GroundStation24 - direction
	GroundStation20 - direction
	GroundStation43 - direction
	Star39 - direction
	Star17 - direction
	Star22 - direction
	Star5 - direction
	GroundStation31 - direction
	GroundStation40 - direction
	Star34 - direction
	GroundStation4 - direction
	Star19 - direction
	Star6 - direction
	GroundStation21 - direction
	GroundStation18 - direction
	Star12 - direction
	Star30 - direction
	GroundStation48 - direction
	Star50 - direction
	Phenomenon51 - direction
	Planet52 - direction
	Phenomenon53 - direction
	Star54 - direction
	Phenomenon55 - direction
	Phenomenon56 - direction
	Planet57 - direction
	Star58 - direction
	Star59 - direction
	Phenomenon60 - direction
	Planet61 - direction
	Planet62 - direction
	Planet63 - direction
	Star64 - direction
	Star65 - direction
	Planet66 - direction
	Phenomenon67 - direction
	Star68 - direction
	Phenomenon69 - direction
	Planet70 - direction
	Star71 - direction
	Star72 - direction
	Star73 - direction
	Star74 - direction
	Star75 - direction
	Star76 - direction
	Planet77 - direction
	Planet78 - direction
	Planet79 - direction
	Planet80 - direction
	Star81 - direction
	Planet82 - direction
	Planet83 - direction
	Star84 - direction
	Star85 - direction
	Phenomenon86 - direction
	Star87 - direction
	Planet88 - direction
	Planet89 - direction
	Planet90 - direction
	Planet91 - direction
	Star92 - direction
	Planet93 - direction
	Star94 - direction
	Phenomenon95 - direction
	Planet96 - direction
	Planet97 - direction
	Star98 - direction
	Phenomenon99 - direction
)
(:init
	(supports instrument0 image0)
	(supports instrument0 image8)
	(supports instrument0 image3)
	(calibration_target instrument0 Star22)
	(calibration_target instrument0 Star27)
	(calibration_target instrument0 GroundStation31)
	(calibration_target instrument0 GroundStation43)
	(calibration_target instrument0 GroundStation48)
	(calibration_target instrument0 GroundStation16)
	(calibration_target instrument0 GroundStation23)
	(calibration_target instrument0 Star29)
	(calibration_target instrument0 GroundStation15)
	(calibration_target instrument0 Star26)
	(calibration_target instrument0 Star9)
	(calibration_target instrument0 GroundStation40)
	(calibration_target instrument0 Star25)
	(calibration_target instrument0 GroundStation42)
	(calibration_target instrument0 Star1)
	(calibration_target instrument0 GroundStation7)
	(on_board instrument0 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Planet77)
	(supports instrument1 image3)
	(supports instrument1 infrared1)
	(calibration_target instrument1 GroundStation21)
	(calibration_target instrument1 GroundStation33)
	(calibration_target instrument1 Star30)
	(calibration_target instrument1 GroundStation38)
	(calibration_target instrument1 Star34)
	(calibration_target instrument1 Star25)
	(supports instrument2 thermograph6)
	(supports instrument2 image5)
	(calibration_target instrument2 Star29)
	(calibration_target instrument2 GroundStation13)
	(calibration_target instrument2 GroundStation3)
	(calibration_target instrument2 Star1)
	(calibration_target instrument2 Star30)
	(calibration_target instrument2 Star27)
	(calibration_target instrument2 Star28)
	(calibration_target instrument2 GroundStation40)
	(calibration_target instrument2 Star0)
	(calibration_target instrument2 Star49)
	(calibration_target instrument2 GroundStation21)
	(calibration_target instrument2 Star45)
	(calibration_target instrument2 GroundStation32)
	(calibration_target instrument2 Star46)
	(on_board instrument1 satellite1)
	(on_board instrument2 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Planet90)
	(supports instrument3 image5)
	(calibration_target instrument3 GroundStation3)
	(calibration_target instrument3 Star49)
	(calibration_target instrument3 GroundStation36)
	(calibration_target instrument3 Star30)
	(calibration_target instrument3 Star39)
	(calibration_target instrument3 GroundStation38)
	(calibration_target instrument3 GroundStation42)
	(calibration_target instrument3 GroundStation18)
	(calibration_target instrument3 GroundStation7)
	(calibration_target instrument3 Star47)
	(calibration_target instrument3 GroundStation23)
	(calibration_target instrument3 Star25)
	(supports instrument4 infrared1)
	(calibration_target instrument4 GroundStation16)
	(calibration_target instrument4 Star39)
	(calibration_target instrument4 GroundStation33)
	(calibration_target instrument4 GroundStation11)
	(calibration_target instrument4 GroundStation18)
	(calibration_target instrument4 Star44)
	(calibration_target instrument4 GroundStation31)
	(calibration_target instrument4 Star25)
	(calibration_target instrument4 Star27)
	(calibration_target instrument4 Star14)
	(calibration_target instrument4 GroundStation24)
	(calibration_target instrument4 GroundStation36)
	(on_board instrument3 satellite2)
	(on_board instrument4 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation10)
	(supports instrument5 image4)
	(supports instrument5 image0)
	(supports instrument5 infrared1)
	(calibration_target instrument5 Star14)
	(calibration_target instrument5 GroundStation32)
	(calibration_target instrument5 GroundStation36)
	(calibration_target instrument5 Star0)
	(calibration_target instrument5 GroundStation23)
	(calibration_target instrument5 GroundStation4)
	(calibration_target instrument5 Star45)
	(calibration_target instrument5 Star37)
	(calibration_target instrument5 Star49)
	(calibration_target instrument5 GroundStation11)
	(calibration_target instrument5 Star28)
	(calibration_target instrument5 GroundStation35)
	(calibration_target instrument5 Star44)
	(calibration_target instrument5 GroundStation20)
	(calibration_target instrument5 Star17)
	(calibration_target instrument5 Star39)
	(supports instrument6 image4)
	(calibration_target instrument6 Star0)
	(calibration_target instrument6 Star5)
	(calibration_target instrument6 GroundStation33)
	(calibration_target instrument6 Star26)
	(supports instrument7 image4)
	(supports instrument7 spectrograph9)
	(calibration_target instrument7 GroundStation11)
	(calibration_target instrument7 GroundStation3)
	(calibration_target instrument7 GroundStation20)
	(calibration_target instrument7 GroundStation2)
	(calibration_target instrument7 GroundStation48)
	(calibration_target instrument7 Star27)
	(calibration_target instrument7 Star30)
	(supports instrument8 image5)
	(calibration_target instrument8 Star8)
	(calibration_target instrument8 GroundStation36)
	(calibration_target instrument8 GroundStation7)
	(calibration_target instrument8 Star12)
	(calibration_target instrument8 GroundStation38)
	(calibration_target instrument8 Star44)
	(calibration_target instrument8 GroundStation33)
	(supports instrument9 thermograph6)
	(supports instrument9 infrared1)
	(supports instrument9 image5)
	(calibration_target instrument9 Star41)
	(calibration_target instrument9 Star39)
	(calibration_target instrument9 Star12)
	(calibration_target instrument9 Star26)
	(calibration_target instrument9 GroundStation24)
	(calibration_target instrument9 Star22)
	(calibration_target instrument9 Star28)
	(calibration_target instrument9 Star46)
	(calibration_target instrument9 GroundStation13)
	(calibration_target instrument9 Star45)
	(calibration_target instrument9 Star0)
	(on_board instrument5 satellite3)
	(on_board instrument6 satellite3)
	(on_board instrument7 satellite3)
	(on_board instrument8 satellite3)
	(on_board instrument9 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star22)
	(supports instrument10 thermograph7)
	(supports instrument10 image4)
	(supports instrument10 image3)
	(calibration_target instrument10 GroundStation31)
	(calibration_target instrument10 Star44)
	(supports instrument11 infrared1)
	(supports instrument11 image0)
	(supports instrument11 thermograph6)
	(calibration_target instrument11 Star28)
	(calibration_target instrument11 Star26)
	(calibration_target instrument11 Star1)
	(supports instrument12 image5)
	(supports instrument12 thermograph7)
	(supports instrument12 infrared1)
	(calibration_target instrument12 GroundStation2)
	(calibration_target instrument12 Star6)
	(calibration_target instrument12 GroundStation11)
	(calibration_target instrument12 Star14)
	(calibration_target instrument12 GroundStation20)
	(calibration_target instrument12 GroundStation21)
	(calibration_target instrument12 Star49)
	(calibration_target instrument12 GroundStation13)
	(calibration_target instrument12 GroundStation24)
	(on_board instrument10 satellite4)
	(on_board instrument11 satellite4)
	(on_board instrument12 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star41)
	(supports instrument13 image8)
	(supports instrument13 image0)
	(supports instrument13 image3)
	(calibration_target instrument13 GroundStation42)
	(calibration_target instrument13 GroundStation13)
	(supports instrument14 image5)
	(calibration_target instrument14 Star39)
	(calibration_target instrument14 GroundStation23)
	(calibration_target instrument14 GroundStation31)
	(calibration_target instrument14 GroundStation4)
	(calibration_target instrument14 Star47)
	(calibration_target instrument14 Star49)
	(calibration_target instrument14 Star22)
	(calibration_target instrument14 Star17)
	(calibration_target instrument14 Star12)
	(calibration_target instrument14 GroundStation20)
	(calibration_target instrument14 GroundStation11)
	(calibration_target instrument14 GroundStation24)
	(calibration_target instrument14 GroundStation33)
	(calibration_target instrument14 Star46)
	(calibration_target instrument14 Star26)
	(on_board instrument13 satellite5)
	(on_board instrument14 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Planet89)
	(supports instrument15 image8)
	(supports instrument15 spectrograph2)
	(calibration_target instrument15 GroundStation24)
	(calibration_target instrument15 Star22)
	(calibration_target instrument15 GroundStation4)
	(calibration_target instrument15 GroundStation36)
	(calibration_target instrument15 Star6)
	(calibration_target instrument15 GroundStation10)
	(calibration_target instrument15 Star29)
	(supports instrument16 image8)
	(supports instrument16 spectrograph9)
	(calibration_target instrument16 GroundStation10)
	(calibration_target instrument16 GroundStation18)
	(calibration_target instrument16 Star5)
	(calibration_target instrument16 Star1)
	(calibration_target instrument16 GroundStation13)
	(calibration_target instrument16 GroundStation31)
	(calibration_target instrument16 Star8)
	(on_board instrument15 satellite6)
	(on_board instrument16 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Planet79)
	(supports instrument17 image4)
	(supports instrument17 spectrograph2)
	(calibration_target instrument17 Star45)
	(calibration_target instrument17 GroundStation7)
	(calibration_target instrument17 Star44)
	(calibration_target instrument17 GroundStation38)
	(calibration_target instrument17 GroundStation21)
	(calibration_target instrument17 GroundStation36)
	(calibration_target instrument17 Star37)
	(calibration_target instrument17 GroundStation11)
	(calibration_target instrument17 GroundStation10)
	(calibration_target instrument17 GroundStation23)
	(calibration_target instrument17 GroundStation40)
	(calibration_target instrument17 Star22)
	(calibration_target instrument17 GroundStation15)
	(calibration_target instrument17 GroundStation3)
	(calibration_target instrument17 GroundStation18)
	(supports instrument18 thermograph7)
	(supports instrument18 image8)
	(calibration_target instrument18 GroundStation4)
	(calibration_target instrument18 Star34)
	(calibration_target instrument18 GroundStation18)
	(calibration_target instrument18 GroundStation40)
	(calibration_target instrument18 GroundStation31)
	(calibration_target instrument18 Star5)
	(calibration_target instrument18 Star12)
	(calibration_target instrument18 Star22)
	(calibration_target instrument18 Star17)
	(calibration_target instrument18 Star39)
	(calibration_target instrument18 GroundStation43)
	(calibration_target instrument18 GroundStation20)
	(calibration_target instrument18 GroundStation24)
	(calibration_target instrument18 Star26)
	(calibration_target instrument18 Star1)
	(supports instrument19 spectrograph9)
	(supports instrument19 image5)
	(supports instrument19 image0)
	(calibration_target instrument19 GroundStation48)
	(calibration_target instrument19 Star30)
	(calibration_target instrument19 Star12)
	(calibration_target instrument19 GroundStation18)
	(calibration_target instrument19 GroundStation21)
	(calibration_target instrument19 Star6)
	(calibration_target instrument19 Star19)
	(on_board instrument17 satellite7)
	(on_board instrument18 satellite7)
	(on_board instrument19 satellite7)
	(power_avail satellite7)
	(pointing satellite7 GroundStation3)
)
(:goal (and
	(pointing satellite1 Star85)
	(pointing satellite2 Planet89)
	(pointing satellite7 Star39)
	(have_image Star50 thermograph6)
	(have_image Star50 infrared1)
	(have_image Phenomenon51 image3)
	(have_image Phenomenon51 image4)
	(have_image Planet52 spectrograph2)
	(have_image Planet52 image4)
	(have_image Star54 image4)
	(have_image Star54 image8)
	(have_image Phenomenon55 thermograph7)
	(have_image Phenomenon56 infrared1)
	(have_image Planet57 image5)
	(have_image Star58 image0)
	(have_image Star58 thermograph7)
	(have_image Star59 infrared1)
	(have_image Star59 image3)
	(have_image Star59 spectrograph2)
	(have_image Phenomenon60 image5)
	(have_image Phenomenon60 thermograph7)
	(have_image Planet61 thermograph7)
	(have_image Planet61 infrared1)
	(have_image Planet62 thermograph7)
	(have_image Planet62 thermograph6)
	(have_image Planet62 image4)
	(have_image Planet63 image8)
	(have_image Planet63 thermograph7)
	(have_image Planet63 infrared1)
	(have_image Star64 spectrograph2)
	(have_image Star65 spectrograph9)
	(have_image Star65 thermograph7)
	(have_image Planet66 thermograph7)
	(have_image Phenomenon67 spectrograph9)
	(have_image Star68 infrared1)
	(have_image Star68 thermograph6)
	(have_image Planet70 image3)
	(have_image Planet70 spectrograph9)
	(have_image Planet70 image8)
	(have_image Star71 spectrograph2)
	(have_image Star71 image0)
	(have_image Star71 thermograph7)
	(have_image Star72 spectrograph9)
	(have_image Star72 image4)
	(have_image Star72 image3)
	(have_image Star73 infrared1)
	(have_image Star74 image5)
	(have_image Star75 image3)
	(have_image Star75 spectrograph2)
	(have_image Star75 thermograph7)
	(have_image Planet77 spectrograph9)
	(have_image Planet77 image3)
	(have_image Planet77 image8)
	(have_image Planet78 spectrograph2)
	(have_image Planet78 image0)
	(have_image Planet78 thermograph6)
	(have_image Planet79 spectrograph9)
	(have_image Planet80 thermograph7)
	(have_image Planet80 image4)
	(have_image Planet80 image0)
	(have_image Star81 image4)
	(have_image Star81 infrared1)
	(have_image Star81 spectrograph2)
	(have_image Planet82 thermograph7)
	(have_image Planet83 image5)
	(have_image Planet83 image4)
	(have_image Planet83 image8)
	(have_image Star84 image3)
	(have_image Star85 image5)
	(have_image Star85 thermograph7)
	(have_image Star85 image8)
	(have_image Phenomenon86 thermograph6)
	(have_image Phenomenon86 infrared1)
	(have_image Phenomenon86 spectrograph9)
	(have_image Planet88 infrared1)
	(have_image Planet89 infrared1)
	(have_image Planet89 image8)
	(have_image Planet90 infrared1)
	(have_image Planet90 image0)
	(have_image Planet90 spectrograph2)
	(have_image Planet91 spectrograph9)
	(have_image Planet91 infrared1)
	(have_image Planet91 thermograph7)
	(have_image Star92 spectrograph2)
	(have_image Star92 image4)
	(have_image Star94 image8)
	(have_image Star94 image0)
	(have_image Phenomenon95 image8)
	(have_image Phenomenon95 thermograph6)
	(have_image Planet96 infrared1)
	(have_image Planet96 spectrograph9)
	(have_image Star98 spectrograph9)
	(have_image Phenomenon99 thermograph7)
	(have_image Phenomenon99 infrared1)
	(have_image Phenomenon99 image3)
))

)
