(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	satellite1 - satellite
	instrument5 - instrument
	satellite2 - satellite
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite3 - satellite
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	satellite4 - satellite
	instrument16 - instrument
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	instrument21 - instrument
	satellite5 - satellite
	instrument22 - instrument
	instrument23 - instrument
	satellite6 - satellite
	instrument24 - instrument
	instrument25 - instrument
	instrument26 - instrument
	instrument27 - instrument
	satellite7 - satellite
	instrument28 - instrument
	spectrograph0 - mode
	spectrograph1 - mode
	infrared4 - mode
	image2 - mode
	infrared3 - mode
	thermograph7 - mode
	spectrograph6 - mode
	spectrograph8 - mode
	infrared5 - mode
	GroundStation23 - direction
	Star11 - direction
	Star1 - direction
	GroundStation8 - direction
	GroundStation42 - direction
	GroundStation7 - direction
	Star33 - direction
	GroundStation19 - direction
	GroundStation28 - direction
	Star17 - direction
	Star6 - direction
	GroundStation41 - direction
	Star26 - direction
	GroundStation30 - direction
	Star48 - direction
	GroundStation45 - direction
	GroundStation21 - direction
	GroundStation16 - direction
	GroundStation44 - direction
	Star9 - direction
	Star37 - direction
	GroundStation40 - direction
	GroundStation47 - direction
	GroundStation38 - direction
	GroundStation32 - direction
	Star31 - direction
	GroundStation20 - direction
	GroundStation39 - direction
	Star29 - direction
	Star3 - direction
	GroundStation36 - direction
	GroundStation46 - direction
	Star15 - direction
	GroundStation24 - direction
	GroundStation4 - direction
	GroundStation18 - direction
	Star27 - direction
	GroundStation10 - direction
	GroundStation2 - direction
	GroundStation14 - direction
	Star35 - direction
	GroundStation22 - direction
	Star49 - direction
	Star43 - direction
	Star34 - direction
	GroundStation25 - direction
	Star5 - direction
	Star0 - direction
	Star13 - direction
	Star12 - direction
	Star50 - direction
	Phenomenon51 - direction
	Planet52 - direction
	Phenomenon53 - direction
	Planet54 - direction
	Star55 - direction
	Star56 - direction
	Phenomenon57 - direction
	Planet58 - direction
	Star59 - direction
	Planet60 - direction
	Planet61 - direction
	Phenomenon62 - direction
	Star63 - direction
	Phenomenon64 - direction
	Star65 - direction
	Star66 - direction
	Phenomenon67 - direction
	Phenomenon68 - direction
	Star69 - direction
	Phenomenon70 - direction
	Star71 - direction
	Phenomenon72 - direction
	Planet73 - direction
	Planet74 - direction
	Planet75 - direction
	Phenomenon76 - direction
	Star77 - direction
	Star78 - direction
	Planet79 - direction
	Planet80 - direction
	Star81 - direction
	Phenomenon82 - direction
	Planet83 - direction
	Star84 - direction
	Phenomenon85 - direction
	Planet86 - direction
	Planet87 - direction
	Phenomenon88 - direction
	Star89 - direction
	Planet90 - direction
	Planet91 - direction
	Planet92 - direction
	Phenomenon93 - direction
	Planet94 - direction
	Planet95 - direction
	Star96 - direction
	Phenomenon97 - direction
	Phenomenon98 - direction
	Star99 - direction
)
(:init
	(supports instrument0 thermograph7)
	(calibration_target instrument0 GroundStation14)
	(calibration_target instrument0 GroundStation47)
	(calibration_target instrument0 GroundStation10)
	(calibration_target instrument0 GroundStation25)
	(calibration_target instrument0 GroundStation18)
	(supports instrument1 image2)
	(calibration_target instrument1 GroundStation44)
	(calibration_target instrument1 GroundStation21)
	(supports instrument2 thermograph7)
	(calibration_target instrument2 Star1)
	(calibration_target instrument2 Star49)
	(calibration_target instrument2 Star9)
	(calibration_target instrument2 GroundStation36)
	(calibration_target instrument2 GroundStation45)
	(calibration_target instrument2 GroundStation22)
	(calibration_target instrument2 GroundStation46)
	(calibration_target instrument2 Star29)
	(calibration_target instrument2 Star33)
	(calibration_target instrument2 Star27)
	(calibration_target instrument2 Star43)
	(calibration_target instrument2 GroundStation25)
	(calibration_target instrument2 GroundStation23)
	(calibration_target instrument2 Star0)
	(supports instrument3 thermograph7)
	(calibration_target instrument3 GroundStation19)
	(calibration_target instrument3 Star33)
	(calibration_target instrument3 Star43)
	(calibration_target instrument3 GroundStation45)
	(calibration_target instrument3 Star48)
	(calibration_target instrument3 Star11)
	(calibration_target instrument3 GroundStation21)
	(calibration_target instrument3 GroundStation7)
	(calibration_target instrument3 Star0)
	(calibration_target instrument3 Star13)
	(calibration_target instrument3 GroundStation36)
	(supports instrument4 thermograph7)
	(supports instrument4 spectrograph1)
	(supports instrument4 image2)
	(calibration_target instrument4 GroundStation39)
	(calibration_target instrument4 GroundStation16)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(on_board instrument4 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Phenomenon67)
	(supports instrument5 spectrograph1)
	(supports instrument5 spectrograph8)
	(calibration_target instrument5 GroundStation40)
	(calibration_target instrument5 Star11)
	(calibration_target instrument5 GroundStation42)
	(calibration_target instrument5 Star35)
	(calibration_target instrument5 GroundStation24)
	(calibration_target instrument5 GroundStation18)
	(calibration_target instrument5 GroundStation19)
	(calibration_target instrument5 GroundStation25)
	(calibration_target instrument5 GroundStation36)
	(on_board instrument5 satellite1)
	(power_avail satellite1)
	(pointing satellite1 GroundStation45)
	(supports instrument6 thermograph7)
	(calibration_target instrument6 Star35)
	(calibration_target instrument6 GroundStation42)
	(calibration_target instrument6 GroundStation45)
	(calibration_target instrument6 Star34)
	(supports instrument7 image2)
	(supports instrument7 infrared5)
	(calibration_target instrument7 GroundStation19)
	(calibration_target instrument7 GroundStation16)
	(calibration_target instrument7 GroundStation8)
	(calibration_target instrument7 GroundStation40)
	(calibration_target instrument7 GroundStation46)
	(calibration_target instrument7 Star48)
	(calibration_target instrument7 GroundStation4)
	(supports instrument8 image2)
	(supports instrument8 infrared4)
	(calibration_target instrument8 Star26)
	(calibration_target instrument8 GroundStation30)
	(supports instrument9 infrared4)
	(supports instrument9 spectrograph0)
	(calibration_target instrument9 Star48)
	(calibration_target instrument9 GroundStation42)
	(calibration_target instrument9 GroundStation40)
	(calibration_target instrument9 GroundStation22)
	(on_board instrument6 satellite2)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation30)
	(supports instrument10 spectrograph8)
	(supports instrument10 spectrograph1)
	(calibration_target instrument10 Star11)
	(calibration_target instrument10 GroundStation32)
	(calibration_target instrument10 GroundStation16)
	(calibration_target instrument10 Star35)
	(calibration_target instrument10 GroundStation23)
	(calibration_target instrument10 Star3)
	(calibration_target instrument10 GroundStation4)
	(calibration_target instrument10 GroundStation47)
	(calibration_target instrument10 GroundStation21)
	(calibration_target instrument10 Star33)
	(supports instrument11 spectrograph1)
	(calibration_target instrument11 GroundStation30)
	(calibration_target instrument11 Star29)
	(calibration_target instrument11 GroundStation39)
	(calibration_target instrument11 GroundStation45)
	(calibration_target instrument11 GroundStation47)
	(calibration_target instrument11 GroundStation21)
	(calibration_target instrument11 GroundStation4)
	(calibration_target instrument11 Star1)
	(calibration_target instrument11 GroundStation36)
	(calibration_target instrument11 Star6)
	(calibration_target instrument11 GroundStation41)
	(calibration_target instrument11 GroundStation44)
	(calibration_target instrument11 Star0)
	(calibration_target instrument11 GroundStation14)
	(calibration_target instrument11 Star15)
	(supports instrument12 infrared5)
	(supports instrument12 thermograph7)
	(supports instrument12 image2)
	(calibration_target instrument12 GroundStation10)
	(supports instrument13 spectrograph1)
	(supports instrument13 image2)
	(supports instrument13 infrared5)
	(calibration_target instrument13 GroundStation30)
	(calibration_target instrument13 GroundStation8)
	(calibration_target instrument13 Star27)
	(calibration_target instrument13 Star17)
	(calibration_target instrument13 GroundStation38)
	(calibration_target instrument13 Star12)
	(calibration_target instrument13 GroundStation41)
	(calibration_target instrument13 Star26)
	(calibration_target instrument13 Star0)
	(calibration_target instrument13 GroundStation45)
	(calibration_target instrument13 GroundStation47)
	(calibration_target instrument13 GroundStation20)
	(calibration_target instrument13 GroundStation39)
	(calibration_target instrument13 Star29)
	(calibration_target instrument13 GroundStation14)
	(supports instrument14 infrared5)
	(calibration_target instrument14 GroundStation19)
	(calibration_target instrument14 GroundStation45)
	(calibration_target instrument14 GroundStation16)
	(calibration_target instrument14 GroundStation42)
	(calibration_target instrument14 Star6)
	(calibration_target instrument14 GroundStation38)
	(supports instrument15 spectrograph8)
	(supports instrument15 thermograph7)
	(supports instrument15 infrared4)
	(calibration_target instrument15 GroundStation24)
	(calibration_target instrument15 GroundStation36)
	(calibration_target instrument15 Star31)
	(calibration_target instrument15 GroundStation25)
	(calibration_target instrument15 GroundStation10)
	(calibration_target instrument15 Star0)
	(calibration_target instrument15 GroundStation40)
	(calibration_target instrument15 Star1)
	(calibration_target instrument15 GroundStation16)
	(calibration_target instrument15 GroundStation28)
	(on_board instrument10 satellite3)
	(on_board instrument11 satellite3)
	(on_board instrument12 satellite3)
	(on_board instrument13 satellite3)
	(on_board instrument14 satellite3)
	(on_board instrument15 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Planet87)
	(supports instrument16 spectrograph0)
	(supports instrument16 infrared5)
	(calibration_target instrument16 GroundStation20)
	(calibration_target instrument16 Star5)
	(calibration_target instrument16 GroundStation22)
	(supports instrument17 infrared3)
	(calibration_target instrument17 Star13)
	(calibration_target instrument17 Star12)
	(calibration_target instrument17 GroundStation22)
	(calibration_target instrument17 GroundStation25)
	(calibration_target instrument17 Star35)
	(calibration_target instrument17 Star15)
	(calibration_target instrument17 GroundStation39)
	(calibration_target instrument17 GroundStation41)
	(calibration_target instrument17 GroundStation7)
	(calibration_target instrument17 GroundStation38)
	(calibration_target instrument17 GroundStation42)
	(calibration_target instrument17 GroundStation20)
	(calibration_target instrument17 Star33)
	(calibration_target instrument17 GroundStation8)
	(supports instrument18 spectrograph1)
	(supports instrument18 spectrograph6)
	(supports instrument18 infrared3)
	(calibration_target instrument18 GroundStation22)
	(calibration_target instrument18 GroundStation38)
	(calibration_target instrument18 GroundStation18)
	(calibration_target instrument18 GroundStation4)
	(calibration_target instrument18 GroundStation21)
	(calibration_target instrument18 Star31)
	(calibration_target instrument18 Star27)
	(calibration_target instrument18 Star35)
	(calibration_target instrument18 GroundStation14)
	(calibration_target instrument18 Star17)
	(calibration_target instrument18 GroundStation46)
	(calibration_target instrument18 Star15)
	(calibration_target instrument18 GroundStation30)
	(calibration_target instrument18 GroundStation45)
	(supports instrument19 infrared5)
	(supports instrument19 spectrograph6)
	(calibration_target instrument19 GroundStation28)
	(calibration_target instrument19 Star31)
	(calibration_target instrument19 Star27)
	(calibration_target instrument19 GroundStation41)
	(calibration_target instrument19 Star5)
	(calibration_target instrument19 GroundStation47)
	(calibration_target instrument19 Star33)
	(calibration_target instrument19 GroundStation14)
	(calibration_target instrument19 Star12)
	(calibration_target instrument19 Star13)
	(supports instrument20 thermograph7)
	(supports instrument20 infrared4)
	(supports instrument20 spectrograph1)
	(calibration_target instrument20 GroundStation22)
	(calibration_target instrument20 GroundStation30)
	(calibration_target instrument20 GroundStation20)
	(calibration_target instrument20 Star6)
	(calibration_target instrument20 GroundStation40)
	(calibration_target instrument20 Star43)
	(calibration_target instrument20 Star17)
	(calibration_target instrument20 Star29)
	(calibration_target instrument20 GroundStation28)
	(calibration_target instrument20 Star26)
	(calibration_target instrument20 Star12)
	(calibration_target instrument20 GroundStation19)
	(calibration_target instrument20 Star33)
	(calibration_target instrument20 Star15)
	(supports instrument21 infrared4)
	(supports instrument21 image2)
	(supports instrument21 infrared5)
	(calibration_target instrument21 GroundStation38)
	(calibration_target instrument21 GroundStation30)
	(calibration_target instrument21 Star26)
	(calibration_target instrument21 Star5)
	(calibration_target instrument21 Star13)
	(calibration_target instrument21 Star34)
	(calibration_target instrument21 GroundStation41)
	(on_board instrument16 satellite4)
	(on_board instrument17 satellite4)
	(on_board instrument18 satellite4)
	(on_board instrument19 satellite4)
	(on_board instrument20 satellite4)
	(on_board instrument21 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star81)
	(supports instrument22 infrared3)
	(supports instrument22 infrared4)
	(supports instrument22 spectrograph8)
	(calibration_target instrument22 GroundStation40)
	(calibration_target instrument22 GroundStation22)
	(calibration_target instrument22 GroundStation21)
	(calibration_target instrument22 GroundStation45)
	(calibration_target instrument22 GroundStation10)
	(calibration_target instrument22 Star48)
	(calibration_target instrument22 GroundStation25)
	(calibration_target instrument22 Star3)
	(supports instrument23 thermograph7)
	(supports instrument23 infrared3)
	(calibration_target instrument23 GroundStation18)
	(calibration_target instrument23 Star9)
	(calibration_target instrument23 GroundStation38)
	(calibration_target instrument23 GroundStation44)
	(calibration_target instrument23 GroundStation46)
	(calibration_target instrument23 GroundStation16)
	(calibration_target instrument23 GroundStation2)
	(calibration_target instrument23 Star29)
	(calibration_target instrument23 GroundStation32)
	(on_board instrument22 satellite5)
	(on_board instrument23 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Phenomenon53)
	(supports instrument24 infrared5)
	(calibration_target instrument24 Star13)
	(calibration_target instrument24 GroundStation47)
	(calibration_target instrument24 GroundStation40)
	(calibration_target instrument24 Star37)
	(supports instrument25 spectrograph6)
	(supports instrument25 image2)
	(supports instrument25 infrared5)
	(calibration_target instrument25 Star31)
	(calibration_target instrument25 GroundStation32)
	(calibration_target instrument25 Star0)
	(calibration_target instrument25 GroundStation38)
	(supports instrument26 spectrograph6)
	(supports instrument26 infrared3)
	(calibration_target instrument26 GroundStation46)
	(calibration_target instrument26 GroundStation4)
	(supports instrument27 spectrograph6)
	(supports instrument27 thermograph7)
	(calibration_target instrument27 GroundStation2)
	(calibration_target instrument27 GroundStation10)
	(calibration_target instrument27 Star27)
	(calibration_target instrument27 Star12)
	(calibration_target instrument27 GroundStation18)
	(calibration_target instrument27 GroundStation4)
	(calibration_target instrument27 GroundStation24)
	(calibration_target instrument27 Star0)
	(calibration_target instrument27 Star15)
	(calibration_target instrument27 GroundStation46)
	(calibration_target instrument27 GroundStation36)
	(calibration_target instrument27 Star3)
	(calibration_target instrument27 Star29)
	(calibration_target instrument27 GroundStation39)
	(calibration_target instrument27 GroundStation20)
	(on_board instrument24 satellite6)
	(on_board instrument25 satellite6)
	(on_board instrument26 satellite6)
	(on_board instrument27 satellite6)
	(power_avail satellite6)
	(pointing satellite6 GroundStation19)
	(supports instrument28 infrared5)
	(supports instrument28 spectrograph8)
	(supports instrument28 spectrograph6)
	(calibration_target instrument28 Star12)
	(calibration_target instrument28 Star13)
	(calibration_target instrument28 Star0)
	(calibration_target instrument28 Star5)
	(calibration_target instrument28 GroundStation25)
	(calibration_target instrument28 Star34)
	(calibration_target instrument28 Star43)
	(calibration_target instrument28 Star49)
	(calibration_target instrument28 GroundStation22)
	(calibration_target instrument28 Star35)
	(calibration_target instrument28 GroundStation14)
	(on_board instrument28 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Planet74)
)
(:goal (and
	(pointing satellite0 Phenomenon97)
	(pointing satellite2 Star6)
	(pointing satellite4 Phenomenon51)
	(have_image Star50 thermograph7)
	(have_image Star50 image2)
	(have_image Star50 infrared5)
	(have_image Phenomenon51 infrared5)
	(have_image Phenomenon51 spectrograph8)
	(have_image Phenomenon51 spectrograph0)
	(have_image Planet52 spectrograph0)
	(have_image Phenomenon53 image2)
	(have_image Planet54 spectrograph6)
	(have_image Star56 spectrograph0)
	(have_image Phenomenon57 spectrograph6)
	(have_image Star59 spectrograph1)
	(have_image Star59 spectrograph0)
	(have_image Star59 spectrograph6)
	(have_image Planet60 thermograph7)
	(have_image Planet60 spectrograph8)
	(have_image Planet60 spectrograph1)
	(have_image Planet61 infrared4)
	(have_image Planet61 infrared3)
	(have_image Phenomenon62 spectrograph0)
	(have_image Phenomenon62 image2)
	(have_image Phenomenon62 thermograph7)
	(have_image Star63 infrared4)
	(have_image Star63 infrared3)
	(have_image Star63 image2)
	(have_image Phenomenon64 spectrograph6)
	(have_image Phenomenon64 spectrograph8)
	(have_image Phenomenon64 image2)
	(have_image Star65 image2)
	(have_image Star65 spectrograph1)
	(have_image Star65 infrared5)
	(have_image Star66 spectrograph0)
	(have_image Star66 infrared5)
	(have_image Star66 infrared4)
	(have_image Phenomenon67 spectrograph1)
	(have_image Star69 infrared5)
	(have_image Star69 image2)
	(have_image Phenomenon70 spectrograph6)
	(have_image Phenomenon70 infrared3)
	(have_image Phenomenon70 infrared5)
	(have_image Star71 spectrograph6)
	(have_image Phenomenon72 spectrograph8)
	(have_image Planet73 thermograph7)
	(have_image Planet74 spectrograph8)
	(have_image Planet74 infrared4)
	(have_image Planet75 spectrograph0)
	(have_image Planet75 infrared3)
	(have_image Planet75 infrared4)
	(have_image Phenomenon76 spectrograph1)
	(have_image Star77 spectrograph8)
	(have_image Star78 spectrograph1)
	(have_image Planet79 image2)
	(have_image Planet79 infrared5)
	(have_image Planet80 spectrograph1)
	(have_image Planet80 infrared4)
	(have_image Phenomenon82 spectrograph8)
	(have_image Planet83 spectrograph8)
	(have_image Planet83 infrared3)
	(have_image Planet83 spectrograph6)
	(have_image Star84 spectrograph6)
	(have_image Phenomenon85 image2)
	(have_image Planet86 spectrograph8)
	(have_image Planet86 infrared4)
	(have_image Planet87 spectrograph0)
	(have_image Planet87 thermograph7)
	(have_image Planet87 infrared5)
	(have_image Phenomenon88 spectrograph1)
	(have_image Phenomenon88 infrared3)
	(have_image Star89 spectrograph1)
	(have_image Star89 spectrograph8)
	(have_image Planet90 spectrograph0)
	(have_image Planet90 infrared5)
	(have_image Planet90 infrared3)
	(have_image Planet91 infrared4)
	(have_image Planet91 spectrograph8)
	(have_image Planet91 spectrograph1)
	(have_image Planet92 infrared3)
	(have_image Phenomenon93 thermograph7)
	(have_image Planet94 infrared3)
	(have_image Planet95 spectrograph0)
	(have_image Planet95 thermograph7)
	(have_image Planet95 spectrograph6)
	(have_image Phenomenon97 spectrograph1)
	(have_image Phenomenon98 spectrograph1)
	(have_image Phenomenon98 thermograph7)
	(have_image Phenomenon98 infrared3)
	(have_image Star99 infrared5)
))

)
