(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	satellite1 - satellite
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	satellite2 - satellite
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	instrument10 - instrument
	satellite3 - satellite
	instrument11 - instrument
	instrument12 - instrument
	satellite4 - satellite
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	satellite5 - satellite
	instrument18 - instrument
	satellite6 - satellite
	instrument19 - instrument
	instrument20 - instrument
	instrument21 - instrument
	satellite7 - satellite
	instrument22 - instrument
	satellite8 - satellite
	instrument23 - instrument
	instrument24 - instrument
	thermograph0 - mode
	infrared4 - mode
	infrared6 - mode
	spectrograph3 - mode
	thermograph8 - mode
	spectrograph7 - mode
	spectrograph2 - mode
	thermograph1 - mode
	thermograph5 - mode
	Star71 - direction
	GroundStation51 - direction
	Star55 - direction
	GroundStation21 - direction
	GroundStation66 - direction
	GroundStation85 - direction
	GroundStation99 - direction
	GroundStation24 - direction
	GroundStation28 - direction
	Star86 - direction
	GroundStation10 - direction
	Star44 - direction
	Star48 - direction
	GroundStation5 - direction
	Star40 - direction
	Star67 - direction
	GroundStation61 - direction
	GroundStation29 - direction
	GroundStation9 - direction
	Star16 - direction
	Star42 - direction
	Star15 - direction
	GroundStation19 - direction
	GroundStation95 - direction
	Star6 - direction
	GroundStation45 - direction
	Star57 - direction
	Star89 - direction
	GroundStation52 - direction
	Star3 - direction
	GroundStation26 - direction
	GroundStation4 - direction
	Star59 - direction
	GroundStation37 - direction
	Star69 - direction
	GroundStation56 - direction
	GroundStation70 - direction
	GroundStation58 - direction
	Star65 - direction
	GroundStation76 - direction
	GroundStation11 - direction
	Star50 - direction
	Star39 - direction
	Star30 - direction
	GroundStation83 - direction
	GroundStation81 - direction
	Star77 - direction
	GroundStation12 - direction
	GroundStation80 - direction
	GroundStation14 - direction
	Star20 - direction
	GroundStation53 - direction
	GroundStation88 - direction
	GroundStation33 - direction
	Star68 - direction
	GroundStation31 - direction
	Star47 - direction
	Star64 - direction
	GroundStation13 - direction
	GroundStation75 - direction
	GroundStation0 - direction
	GroundStation35 - direction
	Star34 - direction
	Star94 - direction
	Star97 - direction
	GroundStation93 - direction
	GroundStation22 - direction
	GroundStation84 - direction
	Star54 - direction
	Star96 - direction
	Star2 - direction
	GroundStation25 - direction
	Star7 - direction
	Star23 - direction
	Star32 - direction
	Star43 - direction
	GroundStation38 - direction
	GroundStation72 - direction
	GroundStation18 - direction
	GroundStation74 - direction
	Star41 - direction
	GroundStation8 - direction
	Star90 - direction
	Star87 - direction
	Star1 - direction
	GroundStation78 - direction
	Star60 - direction
	GroundStation92 - direction
	GroundStation91 - direction
	GroundStation46 - direction
	Star98 - direction
	GroundStation62 - direction
	GroundStation73 - direction
	GroundStation49 - direction
	GroundStation82 - direction
	GroundStation63 - direction
	Star27 - direction
	Star36 - direction
	Star17 - direction
	GroundStation79 - direction
	Phenomenon100 - direction
	Planet101 - direction
	Star102 - direction
	Planet103 - direction
	Star104 - direction
	Phenomenon105 - direction
	Phenomenon106 - direction
	Planet107 - direction
	Star108 - direction
	Star109 - direction
	Planet110 - direction
	Star111 - direction
	Planet112 - direction
	Star113 - direction
	Planet114 - direction
	Star115 - direction
	Planet116 - direction
	Planet117 - direction
	Phenomenon118 - direction
	Phenomenon119 - direction
	Star120 - direction
	Planet121 - direction
	Phenomenon122 - direction
	Star123 - direction
	Planet124 - direction
	Planet125 - direction
	Star126 - direction
	Phenomenon127 - direction
	Star128 - direction
	Phenomenon129 - direction
	Phenomenon130 - direction
	Star131 - direction
	Star132 - direction
	Planet133 - direction
	Phenomenon134 - direction
	Planet135 - direction
	Phenomenon136 - direction
	Star137 - direction
	Planet138 - direction
	Phenomenon139 - direction
	Planet140 - direction
	Phenomenon141 - direction
	Planet142 - direction
	Planet143 - direction
	Star144 - direction
	Phenomenon145 - direction
	Star146 - direction
	Phenomenon147 - direction
	Star148 - direction
	Planet149 - direction
	Star150 - direction
	Star151 - direction
	Planet152 - direction
	Planet153 - direction
	Phenomenon154 - direction
	Star155 - direction
	Star156 - direction
	Planet157 - direction
	Star158 - direction
	Planet159 - direction
	Star160 - direction
	Planet161 - direction
	Phenomenon162 - direction
	Planet163 - direction
	Phenomenon164 - direction
	Phenomenon165 - direction
	Star166 - direction
	Phenomenon167 - direction
	Phenomenon168 - direction
	Phenomenon169 - direction
	Phenomenon170 - direction
	Star171 - direction
	Star172 - direction
	Planet173 - direction
	Phenomenon174 - direction
	Star175 - direction
	Star176 - direction
	Planet177 - direction
	Star178 - direction
	Planet179 - direction
	Planet180 - direction
	Star181 - direction
	Planet182 - direction
	Phenomenon183 - direction
	Planet184 - direction
	Planet185 - direction
	Phenomenon186 - direction
	Star187 - direction
	Phenomenon188 - direction
	Planet189 - direction
	Planet190 - direction
	Phenomenon191 - direction
	Planet192 - direction
	Planet193 - direction
	Phenomenon194 - direction
	Star195 - direction
	Phenomenon196 - direction
	Star197 - direction
	Planet198 - direction
	Phenomenon199 - direction
)
(:init
	(supports instrument0 thermograph5)
	(supports instrument0 spectrograph3)
	(calibration_target instrument0 GroundStation25)
	(calibration_target instrument0 GroundStation58)
	(calibration_target instrument0 GroundStation63)
	(calibration_target instrument0 GroundStation0)
	(calibration_target instrument0 Star64)
	(calibration_target instrument0 Star3)
	(calibration_target instrument0 Star41)
	(calibration_target instrument0 Star98)
	(calibration_target instrument0 Star67)
	(calibration_target instrument0 GroundStation12)
	(calibration_target instrument0 Star30)
	(calibration_target instrument0 Star94)
	(supports instrument1 thermograph8)
	(supports instrument1 infrared4)
	(calibration_target instrument1 GroundStation26)
	(calibration_target instrument1 GroundStation19)
	(calibration_target instrument1 GroundStation62)
	(calibration_target instrument1 GroundStation53)
	(calibration_target instrument1 Star86)
	(calibration_target instrument1 GroundStation21)
	(calibration_target instrument1 Star54)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation10)
	(supports instrument2 thermograph0)
	(supports instrument2 spectrograph3)
	(supports instrument2 thermograph8)
	(calibration_target instrument2 GroundStation22)
	(calibration_target instrument2 Star60)
	(calibration_target instrument2 GroundStation78)
	(calibration_target instrument2 Star27)
	(calibration_target instrument2 GroundStation85)
	(calibration_target instrument2 Star67)
	(calibration_target instrument2 GroundStation45)
	(calibration_target instrument2 GroundStation31)
	(calibration_target instrument2 Star39)
	(calibration_target instrument2 GroundStation13)
	(calibration_target instrument2 GroundStation51)
	(calibration_target instrument2 Star97)
	(calibration_target instrument2 Star68)
	(calibration_target instrument2 Star77)
	(calibration_target instrument2 GroundStation84)
	(calibration_target instrument2 Star64)
	(calibration_target instrument2 Star57)
	(calibration_target instrument2 Star87)
	(calibration_target instrument2 Star69)
	(calibration_target instrument2 Star34)
	(calibration_target instrument2 GroundStation35)
	(calibration_target instrument2 Star96)
	(calibration_target instrument2 GroundStation18)
	(calibration_target instrument2 Star6)
	(calibration_target instrument2 GroundStation75)
	(calibration_target instrument2 Star16)
	(calibration_target instrument2 Star3)
	(calibration_target instrument2 GroundStation10)
	(calibration_target instrument2 GroundStation83)
	(supports instrument3 thermograph0)
	(supports instrument3 thermograph8)
	(supports instrument3 infrared4)
	(calibration_target instrument3 Star7)
	(calibration_target instrument3 GroundStation74)
	(calibration_target instrument3 Star6)
	(calibration_target instrument3 Star27)
	(calibration_target instrument3 Star69)
	(calibration_target instrument3 Star32)
	(calibration_target instrument3 Star17)
	(calibration_target instrument3 GroundStation99)
	(calibration_target instrument3 GroundStation53)
	(calibration_target instrument3 GroundStation25)
	(calibration_target instrument3 GroundStation8)
	(calibration_target instrument3 GroundStation19)
	(calibration_target instrument3 Star34)
	(calibration_target instrument3 Star44)
	(calibration_target instrument3 Star30)
	(calibration_target instrument3 GroundStation73)
	(calibration_target instrument3 Star20)
	(calibration_target instrument3 Star60)
	(calibration_target instrument3 GroundStation56)
	(calibration_target instrument3 GroundStation46)
	(calibration_target instrument3 GroundStation33)
	(calibration_target instrument3 GroundStation79)
	(calibration_target instrument3 GroundStation82)
	(calibration_target instrument3 Star39)
	(calibration_target instrument3 Star54)
	(supports instrument4 spectrograph7)
	(calibration_target instrument4 GroundStation0)
	(supports instrument5 spectrograph7)
	(supports instrument5 thermograph0)
	(calibration_target instrument5 GroundStation66)
	(calibration_target instrument5 Star96)
	(calibration_target instrument5 GroundStation58)
	(calibration_target instrument5 Star68)
	(calibration_target instrument5 Star87)
	(calibration_target instrument5 GroundStation62)
	(calibration_target instrument5 Star27)
	(supports instrument6 thermograph1)
	(calibration_target instrument6 Star55)
	(calibration_target instrument6 GroundStation13)
	(calibration_target instrument6 Star54)
	(calibration_target instrument6 Star90)
	(calibration_target instrument6 Star50)
	(calibration_target instrument6 GroundStation82)
	(calibration_target instrument6 GroundStation45)
	(calibration_target instrument6 Star59)
	(calibration_target instrument6 GroundStation37)
	(calibration_target instrument6 GroundStation25)
	(calibration_target instrument6 Star7)
	(calibration_target instrument6 GroundStation24)
	(calibration_target instrument6 Star44)
	(calibration_target instrument6 GroundStation28)
	(calibration_target instrument6 GroundStation95)
	(calibration_target instrument6 GroundStation92)
	(calibration_target instrument6 GroundStation84)
	(calibration_target instrument6 Star1)
	(calibration_target instrument6 Star68)
	(calibration_target instrument6 Star15)
	(calibration_target instrument6 Star87)
	(calibration_target instrument6 GroundStation12)
	(calibration_target instrument6 Star20)
	(calibration_target instrument6 Star89)
	(calibration_target instrument6 GroundStation49)
	(calibration_target instrument6 GroundStation81)
	(calibration_target instrument6 Star34)
	(calibration_target instrument6 GroundStation26)
	(calibration_target instrument6 GroundStation63)
	(calibration_target instrument6 GroundStation29)
	(calibration_target instrument6 Star94)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Planet159)
	(supports instrument7 thermograph8)
	(supports instrument7 spectrograph2)
	(calibration_target instrument7 GroundStation74)
	(calibration_target instrument7 GroundStation75)
	(calibration_target instrument7 GroundStation26)
	(calibration_target instrument7 GroundStation13)
	(calibration_target instrument7 GroundStation45)
	(calibration_target instrument7 GroundStation33)
	(calibration_target instrument7 GroundStation18)
	(calibration_target instrument7 GroundStation46)
	(calibration_target instrument7 Star16)
	(calibration_target instrument7 GroundStation66)
	(calibration_target instrument7 GroundStation31)
	(calibration_target instrument7 Star90)
	(calibration_target instrument7 Star3)
	(calibration_target instrument7 GroundStation53)
	(calibration_target instrument7 GroundStation24)
	(calibration_target instrument7 Star68)
	(calibration_target instrument7 Star47)
	(calibration_target instrument7 GroundStation76)
	(supports instrument8 thermograph0)
	(calibration_target instrument8 GroundStation19)
	(calibration_target instrument8 GroundStation52)
	(calibration_target instrument8 GroundStation72)
	(calibration_target instrument8 GroundStation73)
	(calibration_target instrument8 GroundStation0)
	(calibration_target instrument8 GroundStation25)
	(calibration_target instrument8 Star77)
	(calibration_target instrument8 Star97)
	(calibration_target instrument8 GroundStation66)
	(calibration_target instrument8 Star94)
	(calibration_target instrument8 Star44)
	(calibration_target instrument8 GroundStation74)
	(calibration_target instrument8 Star50)
	(calibration_target instrument8 GroundStation79)
	(calibration_target instrument8 GroundStation38)
	(calibration_target instrument8 GroundStation4)
	(calibration_target instrument8 Star67)
	(calibration_target instrument8 GroundStation45)
	(calibration_target instrument8 Star90)
	(calibration_target instrument8 GroundStation84)
	(calibration_target instrument8 Star60)
	(calibration_target instrument8 GroundStation24)
	(calibration_target instrument8 GroundStation29)
	(calibration_target instrument8 Star27)
	(calibration_target instrument8 GroundStation70)
	(supports instrument9 spectrograph7)
	(calibration_target instrument9 GroundStation38)
	(calibration_target instrument9 GroundStation14)
	(calibration_target instrument9 GroundStation24)
	(calibration_target instrument9 GroundStation11)
	(calibration_target instrument9 GroundStation31)
	(calibration_target instrument9 GroundStation10)
	(calibration_target instrument9 GroundStation56)
	(calibration_target instrument9 GroundStation70)
	(calibration_target instrument9 GroundStation58)
	(calibration_target instrument9 Star36)
	(calibration_target instrument9 GroundStation52)
	(calibration_target instrument9 Star27)
	(calibration_target instrument9 Star86)
	(calibration_target instrument9 GroundStation46)
	(calibration_target instrument9 GroundStation74)
	(calibration_target instrument9 GroundStation53)
	(calibration_target instrument9 GroundStation26)
	(calibration_target instrument9 Star94)
	(calibration_target instrument9 Star3)
	(calibration_target instrument9 Star87)
	(supports instrument10 infrared6)
	(supports instrument10 spectrograph7)
	(supports instrument10 thermograph5)
	(calibration_target instrument10 GroundStation19)
	(calibration_target instrument10 Star44)
	(calibration_target instrument10 Star59)
	(calibration_target instrument10 Star67)
	(calibration_target instrument10 GroundStation12)
	(calibration_target instrument10 Star42)
	(calibration_target instrument10 GroundStation18)
	(calibration_target instrument10 GroundStation14)
	(calibration_target instrument10 Star3)
	(calibration_target instrument10 Star16)
	(calibration_target instrument10 Star6)
	(calibration_target instrument10 GroundStation80)
	(calibration_target instrument10 GroundStation45)
	(calibration_target instrument10 GroundStation92)
	(calibration_target instrument10 GroundStation79)
	(calibration_target instrument10 GroundStation85)
	(calibration_target instrument10 GroundStation35)
	(calibration_target instrument10 GroundStation88)
	(calibration_target instrument10 GroundStation52)
	(calibration_target instrument10 GroundStation66)
	(calibration_target instrument10 Star68)
	(calibration_target instrument10 GroundStation21)
	(calibration_target instrument10 Star20)
	(calibration_target instrument10 GroundStation63)
	(calibration_target instrument10 GroundStation78)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Phenomenon129)
	(supports instrument11 thermograph5)
	(supports instrument11 thermograph0)
	(calibration_target instrument11 GroundStation63)
	(calibration_target instrument11 Star2)
	(calibration_target instrument11 Star68)
	(calibration_target instrument11 Star1)
	(calibration_target instrument11 Star44)
	(calibration_target instrument11 GroundStation8)
	(calibration_target instrument11 GroundStation12)
	(calibration_target instrument11 GroundStation0)
	(calibration_target instrument11 GroundStation53)
	(calibration_target instrument11 GroundStation82)
	(calibration_target instrument11 GroundStation11)
	(calibration_target instrument11 Star54)
	(calibration_target instrument11 GroundStation14)
	(calibration_target instrument11 GroundStation73)
	(calibration_target instrument11 Star86)
	(calibration_target instrument11 Star57)
	(calibration_target instrument11 GroundStation58)
	(calibration_target instrument11 GroundStation91)
	(calibration_target instrument11 GroundStation4)
	(calibration_target instrument11 Star27)
	(calibration_target instrument11 GroundStation62)
	(calibration_target instrument11 GroundStation29)
	(calibration_target instrument11 GroundStation35)
	(calibration_target instrument11 Star6)
	(calibration_target instrument11 GroundStation52)
	(calibration_target instrument11 GroundStation99)
	(calibration_target instrument11 Star65)
	(supports instrument12 thermograph1)
	(calibration_target instrument12 Star89)
	(calibration_target instrument12 Star67)
	(calibration_target instrument12 Star40)
	(on_board instrument11 satellite3)
	(on_board instrument12 satellite3)
	(power_avail satellite3)
	(pointing satellite3 GroundStation12)
	(supports instrument13 infrared4)
	(supports instrument13 spectrograph3)
	(supports instrument13 spectrograph2)
	(calibration_target instrument13 GroundStation35)
	(calibration_target instrument13 GroundStation11)
	(calibration_target instrument13 Star43)
	(calibration_target instrument13 GroundStation83)
	(calibration_target instrument13 Star86)
	(calibration_target instrument13 GroundStation93)
	(calibration_target instrument13 GroundStation22)
	(calibration_target instrument13 Star41)
	(calibration_target instrument13 GroundStation88)
	(calibration_target instrument13 Star2)
	(calibration_target instrument13 GroundStation28)
	(calibration_target instrument13 GroundStation8)
	(calibration_target instrument13 Star48)
	(calibration_target instrument13 GroundStation70)
	(calibration_target instrument13 GroundStation45)
	(calibration_target instrument13 Star98)
	(calibration_target instrument13 GroundStation24)
	(calibration_target instrument13 GroundStation10)
	(supports instrument14 thermograph8)
	(calibration_target instrument14 Star48)
	(calibration_target instrument14 GroundStation11)
	(calibration_target instrument14 Star7)
	(calibration_target instrument14 Star68)
	(calibration_target instrument14 Star23)
	(calibration_target instrument14 GroundStation4)
	(calibration_target instrument14 GroundStation56)
	(calibration_target instrument14 Star17)
	(calibration_target instrument14 GroundStation37)
	(calibration_target instrument14 Star2)
	(calibration_target instrument14 GroundStation88)
	(calibration_target instrument14 Star42)
	(calibration_target instrument14 GroundStation75)
	(calibration_target instrument14 Star3)
	(calibration_target instrument14 Star65)
	(calibration_target instrument14 GroundStation52)
	(calibration_target instrument14 GroundStation83)
	(calibration_target instrument14 Star59)
	(calibration_target instrument14 Star44)
	(calibration_target instrument14 Star6)
	(calibration_target instrument14 GroundStation58)
	(calibration_target instrument14 GroundStation93)
	(calibration_target instrument14 GroundStation63)
	(calibration_target instrument14 Star20)
	(calibration_target instrument14 GroundStation5)
	(calibration_target instrument14 GroundStation61)
	(calibration_target instrument14 GroundStation46)
	(calibration_target instrument14 GroundStation45)
	(calibration_target instrument14 Star50)
	(calibration_target instrument14 GroundStation38)
	(calibration_target instrument14 GroundStation10)
	(calibration_target instrument14 GroundStation95)
	(supports instrument15 thermograph8)
	(supports instrument15 thermograph1)
	(supports instrument15 thermograph0)
	(calibration_target instrument15 Star34)
	(calibration_target instrument15 GroundStation26)
	(calibration_target instrument15 GroundStation70)
	(calibration_target instrument15 Star67)
	(calibration_target instrument15 GroundStation46)
	(calibration_target instrument15 GroundStation76)
	(calibration_target instrument15 Star40)
	(calibration_target instrument15 GroundStation5)
	(calibration_target instrument15 GroundStation31)
	(calibration_target instrument15 GroundStation0)
	(supports instrument16 infrared4)
	(supports instrument16 infrared6)
	(supports instrument16 spectrograph2)
	(calibration_target instrument16 GroundStation72)
	(calibration_target instrument16 GroundStation9)
	(calibration_target instrument16 GroundStation14)
	(calibration_target instrument16 GroundStation75)
	(calibration_target instrument16 GroundStation29)
	(calibration_target instrument16 Star59)
	(calibration_target instrument16 Star68)
	(calibration_target instrument16 GroundStation61)
	(calibration_target instrument16 GroundStation79)
	(calibration_target instrument16 Star57)
	(calibration_target instrument16 GroundStation19)
	(calibration_target instrument16 GroundStation12)
	(calibration_target instrument16 GroundStation26)
	(calibration_target instrument16 Star15)
	(calibration_target instrument16 Star3)
	(calibration_target instrument16 Star98)
	(calibration_target instrument16 GroundStation82)
	(calibration_target instrument16 Star67)
	(supports instrument17 infrared4)
	(supports instrument17 spectrograph7)
	(calibration_target instrument17 GroundStation73)
	(calibration_target instrument17 GroundStation95)
	(calibration_target instrument17 GroundStation13)
	(calibration_target instrument17 Star27)
	(calibration_target instrument17 Star65)
	(calibration_target instrument17 GroundStation19)
	(calibration_target instrument17 Star3)
	(calibration_target instrument17 Star32)
	(calibration_target instrument17 Star15)
	(calibration_target instrument17 GroundStation35)
	(calibration_target instrument17 Star47)
	(calibration_target instrument17 GroundStation79)
	(calibration_target instrument17 Star42)
	(calibration_target instrument17 Star50)
	(calibration_target instrument17 Star57)
	(calibration_target instrument17 GroundStation37)
	(calibration_target instrument17 GroundStation8)
	(calibration_target instrument17 GroundStation83)
	(calibration_target instrument17 Star16)
	(calibration_target instrument17 GroundStation45)
	(calibration_target instrument17 GroundStation4)
	(calibration_target instrument17 GroundStation38)
	(calibration_target instrument17 GroundStation82)
	(on_board instrument13 satellite4)
	(on_board instrument14 satellite4)
	(on_board instrument15 satellite4)
	(on_board instrument16 satellite4)
	(on_board instrument17 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Planet184)
	(supports instrument18 infrared4)
	(supports instrument18 thermograph0)
	(supports instrument18 spectrograph7)
	(calibration_target instrument18 GroundStation45)
	(calibration_target instrument18 GroundStation82)
	(calibration_target instrument18 Star6)
	(calibration_target instrument18 GroundStation73)
	(calibration_target instrument18 Star39)
	(calibration_target instrument18 GroundStation18)
	(on_board instrument18 satellite5)
	(power_avail satellite5)
	(pointing satellite5 GroundStation11)
	(supports instrument19 thermograph8)
	(supports instrument19 thermograph5)
	(supports instrument19 infrared6)
	(calibration_target instrument19 GroundStation78)
	(calibration_target instrument19 GroundStation31)
	(calibration_target instrument19 GroundStation52)
	(calibration_target instrument19 GroundStation25)
	(calibration_target instrument19 GroundStation79)
	(calibration_target instrument19 Star41)
	(calibration_target instrument19 Star89)
	(calibration_target instrument19 GroundStation26)
	(calibration_target instrument19 GroundStation84)
	(calibration_target instrument19 Star57)
	(calibration_target instrument19 Star98)
	(calibration_target instrument19 Star87)
	(calibration_target instrument19 GroundStation81)
	(calibration_target instrument19 Star39)
	(calibration_target instrument19 GroundStation8)
	(calibration_target instrument19 GroundStation4)
	(calibration_target instrument19 Star17)
	(supports instrument20 thermograph1)
	(supports instrument20 spectrograph2)
	(calibration_target instrument20 GroundStation82)
	(calibration_target instrument20 Star43)
	(calibration_target instrument20 Star50)
	(calibration_target instrument20 GroundStation11)
	(calibration_target instrument20 GroundStation76)
	(calibration_target instrument20 Star65)
	(calibration_target instrument20 GroundStation58)
	(calibration_target instrument20 GroundStation53)
	(calibration_target instrument20 GroundStation70)
	(calibration_target instrument20 GroundStation81)
	(calibration_target instrument20 Star77)
	(calibration_target instrument20 GroundStation56)
	(calibration_target instrument20 GroundStation25)
	(calibration_target instrument20 Star69)
	(calibration_target instrument20 Star96)
	(calibration_target instrument20 GroundStation37)
	(calibration_target instrument20 Star59)
	(calibration_target instrument20 Star68)
	(calibration_target instrument20 GroundStation4)
	(calibration_target instrument20 GroundStation26)
	(calibration_target instrument20 Star7)
	(calibration_target instrument20 Star64)
	(calibration_target instrument20 Star3)
	(calibration_target instrument20 Star17)
	(calibration_target instrument20 GroundStation88)
	(calibration_target instrument20 GroundStation33)
	(calibration_target instrument20 Star47)
	(calibration_target instrument20 GroundStation52)
	(calibration_target instrument20 GroundStation91)
	(supports instrument21 thermograph8)
	(supports instrument21 infrared6)
	(calibration_target instrument21 GroundStation33)
	(calibration_target instrument21 GroundStation88)
	(calibration_target instrument21 GroundStation53)
	(calibration_target instrument21 Star20)
	(calibration_target instrument21 GroundStation14)
	(calibration_target instrument21 GroundStation80)
	(calibration_target instrument21 GroundStation92)
	(calibration_target instrument21 GroundStation12)
	(calibration_target instrument21 Star77)
	(calibration_target instrument21 GroundStation81)
	(calibration_target instrument21 GroundStation83)
	(calibration_target instrument21 Star30)
	(calibration_target instrument21 Star39)
	(calibration_target instrument21 GroundStation8)
	(calibration_target instrument21 GroundStation22)
	(on_board instrument19 satellite6)
	(on_board instrument20 satellite6)
	(on_board instrument21 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Planet103)
	(supports instrument22 thermograph8)
	(supports instrument22 spectrograph3)
	(calibration_target instrument22 Star27)
	(calibration_target instrument22 Star97)
	(calibration_target instrument22 Star94)
	(calibration_target instrument22 Star36)
	(calibration_target instrument22 Star34)
	(calibration_target instrument22 Star60)
	(calibration_target instrument22 GroundStation35)
	(calibration_target instrument22 GroundStation0)
	(calibration_target instrument22 Star23)
	(calibration_target instrument22 GroundStation75)
	(calibration_target instrument22 GroundStation13)
	(calibration_target instrument22 Star64)
	(calibration_target instrument22 Star47)
	(calibration_target instrument22 GroundStation31)
	(calibration_target instrument22 Star68)
	(on_board instrument22 satellite7)
	(power_avail satellite7)
	(pointing satellite7 GroundStation85)
	(supports instrument23 spectrograph7)
	(supports instrument23 spectrograph2)
	(calibration_target instrument23 GroundStation78)
	(calibration_target instrument23 Star1)
	(calibration_target instrument23 Star87)
	(calibration_target instrument23 Star90)
	(calibration_target instrument23 GroundStation92)
	(calibration_target instrument23 GroundStation8)
	(calibration_target instrument23 Star41)
	(calibration_target instrument23 GroundStation74)
	(calibration_target instrument23 GroundStation18)
	(calibration_target instrument23 GroundStation72)
	(calibration_target instrument23 GroundStation73)
	(calibration_target instrument23 GroundStation63)
	(calibration_target instrument23 GroundStation38)
	(calibration_target instrument23 Star43)
	(calibration_target instrument23 Star32)
	(calibration_target instrument23 Star23)
	(calibration_target instrument23 GroundStation62)
	(calibration_target instrument23 GroundStation49)
	(calibration_target instrument23 Star7)
	(calibration_target instrument23 GroundStation25)
	(calibration_target instrument23 Star2)
	(calibration_target instrument23 Star96)
	(calibration_target instrument23 Star54)
	(calibration_target instrument23 GroundStation84)
	(calibration_target instrument23 GroundStation22)
	(calibration_target instrument23 Star60)
	(calibration_target instrument23 GroundStation82)
	(calibration_target instrument23 GroundStation93)
	(supports instrument24 thermograph5)
	(supports instrument24 thermograph1)
	(supports instrument24 spectrograph2)
	(calibration_target instrument24 GroundStation79)
	(calibration_target instrument24 Star17)
	(calibration_target instrument24 Star36)
	(calibration_target instrument24 Star27)
	(calibration_target instrument24 GroundStation63)
	(calibration_target instrument24 GroundStation82)
	(calibration_target instrument24 GroundStation49)
	(calibration_target instrument24 GroundStation73)
	(calibration_target instrument24 GroundStation62)
	(calibration_target instrument24 Star98)
	(calibration_target instrument24 GroundStation46)
	(calibration_target instrument24 GroundStation91)
	(calibration_target instrument24 GroundStation92)
	(calibration_target instrument24 Star60)
	(on_board instrument23 satellite8)
	(on_board instrument24 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Star90)
)
(:goal (and
	(pointing satellite2 Star2)
	(pointing satellite3 GroundStation53)
	(pointing satellite4 Phenomenon164)
	(pointing satellite5 Star158)
	(pointing satellite7 GroundStation53)
	(pointing satellite8 Planet121)
	(have_image Star102 spectrograph2)
	(have_image Star102 thermograph0)
	(have_image Planet103 infrared6)
	(have_image Planet103 spectrograph3)
	(have_image Planet103 thermograph0)
	(have_image Star104 spectrograph2)
	(have_image Star104 thermograph0)
	(have_image Star104 thermograph5)
	(have_image Phenomenon106 thermograph1)
	(have_image Phenomenon106 thermograph8)
	(have_image Planet107 spectrograph2)
	(have_image Star108 thermograph5)
	(have_image Star109 spectrograph2)
	(have_image Star109 infrared6)
	(have_image Planet110 thermograph0)
	(have_image Star111 infrared6)
	(have_image Star111 thermograph8)
	(have_image Star111 thermograph5)
	(have_image Planet112 thermograph5)
	(have_image Planet112 thermograph1)
	(have_image Planet112 thermograph8)
	(have_image Star113 spectrograph2)
	(have_image Planet114 infrared4)
	(have_image Planet114 spectrograph2)
	(have_image Planet114 spectrograph7)
	(have_image Star115 spectrograph7)
	(have_image Star115 thermograph8)
	(have_image Star115 thermograph0)
	(have_image Planet116 infrared4)
	(have_image Planet117 thermograph1)
	(have_image Planet117 infrared6)
	(have_image Planet117 thermograph8)
	(have_image Phenomenon118 thermograph8)
	(have_image Phenomenon118 infrared4)
	(have_image Phenomenon119 spectrograph2)
	(have_image Star120 thermograph1)
	(have_image Planet121 spectrograph3)
	(have_image Planet121 thermograph1)
	(have_image Planet121 infrared4)
	(have_image Phenomenon122 spectrograph3)
	(have_image Phenomenon122 thermograph1)
	(have_image Planet124 thermograph0)
	(have_image Planet125 spectrograph3)
	(have_image Planet125 infrared4)
	(have_image Star126 thermograph1)
	(have_image Star126 spectrograph2)
	(have_image Star128 thermograph8)
	(have_image Star128 thermograph5)
	(have_image Phenomenon129 spectrograph2)
	(have_image Phenomenon130 infrared6)
	(have_image Phenomenon130 thermograph1)
	(have_image Phenomenon130 thermograph0)
	(have_image Star131 spectrograph3)
	(have_image Star131 infrared4)
	(have_image Star132 spectrograph7)
	(have_image Star132 thermograph5)
	(have_image Star132 thermograph1)
	(have_image Planet133 spectrograph3)
	(have_image Planet133 spectrograph7)
	(have_image Phenomenon134 infrared4)
	(have_image Phenomenon134 spectrograph2)
	(have_image Planet135 spectrograph3)
	(have_image Planet135 thermograph0)
	(have_image Phenomenon136 spectrograph2)
	(have_image Phenomenon136 thermograph1)
	(have_image Star137 spectrograph2)
	(have_image Planet138 thermograph0)
	(have_image Planet138 thermograph5)
	(have_image Planet138 spectrograph7)
	(have_image Phenomenon139 thermograph8)
	(have_image Phenomenon139 infrared4)
	(have_image Phenomenon139 thermograph1)
	(have_image Planet140 thermograph1)
	(have_image Phenomenon141 infrared4)
	(have_image Planet142 thermograph0)
	(have_image Planet142 thermograph5)
	(have_image Planet142 spectrograph2)
	(have_image Planet143 infrared4)
	(have_image Planet143 spectrograph3)
	(have_image Planet143 thermograph8)
	(have_image Star144 infrared6)
	(have_image Phenomenon145 thermograph1)
	(have_image Phenomenon145 spectrograph7)
	(have_image Phenomenon145 thermograph5)
	(have_image Star146 thermograph8)
	(have_image Star146 spectrograph7)
	(have_image Star146 thermograph0)
	(have_image Phenomenon147 thermograph1)
	(have_image Star148 infrared6)
	(have_image Planet149 thermograph1)
	(have_image Planet149 thermograph5)
	(have_image Planet149 infrared6)
	(have_image Star150 spectrograph2)
	(have_image Star150 spectrograph7)
	(have_image Star150 thermograph1)
	(have_image Star151 spectrograph2)
	(have_image Star151 spectrograph7)
	(have_image Planet152 spectrograph7)
	(have_image Planet152 spectrograph2)
	(have_image Phenomenon154 spectrograph3)
	(have_image Star155 spectrograph3)
	(have_image Star155 infrared6)
	(have_image Star156 spectrograph3)
	(have_image Star156 thermograph0)
	(have_image Star158 spectrograph3)
	(have_image Star158 infrared6)
	(have_image Planet159 thermograph5)
	(have_image Planet159 thermograph8)
	(have_image Planet159 spectrograph2)
	(have_image Star160 spectrograph7)
	(have_image Star160 spectrograph2)
	(have_image Planet161 thermograph1)
	(have_image Planet161 spectrograph7)
	(have_image Planet161 spectrograph3)
	(have_image Planet163 thermograph0)
	(have_image Planet163 spectrograph7)
	(have_image Phenomenon164 spectrograph3)
	(have_image Phenomenon164 spectrograph7)
	(have_image Phenomenon165 thermograph1)
	(have_image Phenomenon165 spectrograph3)
	(have_image Phenomenon165 spectrograph2)
	(have_image Star166 infrared4)
	(have_image Star166 spectrograph2)
	(have_image Star166 thermograph5)
	(have_image Phenomenon167 infrared4)
	(have_image Phenomenon167 spectrograph7)
	(have_image Phenomenon167 infrared6)
	(have_image Phenomenon168 thermograph1)
	(have_image Phenomenon168 spectrograph2)
	(have_image Phenomenon169 thermograph0)
	(have_image Star171 thermograph5)
	(have_image Star171 spectrograph7)
	(have_image Star171 infrared6)
	(have_image Star172 thermograph5)
	(have_image Star172 spectrograph2)
	(have_image Planet173 thermograph0)
	(have_image Planet173 infrared6)
	(have_image Phenomenon174 spectrograph7)
	(have_image Phenomenon174 spectrograph3)
	(have_image Star176 infrared4)
	(have_image Star176 thermograph0)
	(have_image Star176 spectrograph7)
	(have_image Star178 thermograph8)
	(have_image Planet179 thermograph0)
	(have_image Planet179 thermograph5)
	(have_image Planet180 thermograph5)
	(have_image Planet180 spectrograph7)
	(have_image Star181 infrared4)
	(have_image Star181 thermograph8)
	(have_image Star181 spectrograph2)
	(have_image Planet182 spectrograph2)
	(have_image Planet182 spectrograph7)
	(have_image Phenomenon183 spectrograph3)
	(have_image Phenomenon183 infrared6)
	(have_image Planet184 spectrograph7)
	(have_image Planet184 infrared4)
	(have_image Planet184 spectrograph3)
	(have_image Planet185 spectrograph3)
	(have_image Phenomenon186 thermograph8)
	(have_image Star187 infrared6)
	(have_image Star187 spectrograph3)
	(have_image Phenomenon188 spectrograph2)
	(have_image Phenomenon188 infrared6)
	(have_image Phenomenon188 thermograph8)
	(have_image Planet189 thermograph1)
	(have_image Planet189 spectrograph3)
	(have_image Planet189 thermograph0)
	(have_image Planet190 thermograph8)
	(have_image Planet190 spectrograph3)
	(have_image Phenomenon191 thermograph0)
	(have_image Phenomenon191 spectrograph7)
	(have_image Planet192 infrared6)
	(have_image Planet193 thermograph8)
	(have_image Planet193 infrared6)
	(have_image Planet193 thermograph5)
	(have_image Phenomenon194 thermograph0)
	(have_image Phenomenon194 spectrograph2)
	(have_image Star195 spectrograph2)
	(have_image Star195 thermograph8)
	(have_image Phenomenon196 thermograph5)
	(have_image Phenomenon196 infrared6)
	(have_image Star197 spectrograph7)
	(have_image Star197 thermograph8)
	(have_image Planet198 thermograph1)
	(have_image Phenomenon199 thermograph8)
	(have_image Phenomenon199 thermograph0)
))

)
