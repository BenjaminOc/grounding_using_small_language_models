(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	satellite1 - satellite
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite2 - satellite
	instrument10 - instrument
	instrument11 - instrument
	satellite3 - satellite
	instrument12 - instrument
	instrument13 - instrument
	satellite4 - satellite
	instrument14 - instrument
	satellite5 - satellite
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	satellite6 - satellite
	instrument20 - instrument
	satellite7 - satellite
	instrument21 - instrument
	instrument22 - instrument
	satellite8 - satellite
	instrument23 - instrument
	instrument24 - instrument
	instrument25 - instrument
	instrument26 - instrument
	instrument27 - instrument
	instrument28 - instrument
	image1 - mode
	spectrograph6 - mode
	thermograph0 - mode
	spectrograph3 - mode
	thermograph5 - mode
	thermograph4 - mode
	infrared8 - mode
	infrared2 - mode
	infrared7 - mode
	GroundStation3 - direction
	GroundStation34 - direction
	GroundStation20 - direction
	GroundStation11 - direction
	Star32 - direction
	Star42 - direction
	Star46 - direction
	Star12 - direction
	GroundStation33 - direction
	Star45 - direction
	GroundStation47 - direction
	GroundStation27 - direction
	Star43 - direction
	GroundStation4 - direction
	GroundStation41 - direction
	Star28 - direction
	Star21 - direction
	GroundStation23 - direction
	Star10 - direction
	GroundStation26 - direction
	Star37 - direction
	Star0 - direction
	Star1 - direction
	Star29 - direction
	Star31 - direction
	Star17 - direction
	GroundStation25 - direction
	GroundStation18 - direction
	Star49 - direction
	Star16 - direction
	Star6 - direction
	GroundStation13 - direction
	Star30 - direction
	Star22 - direction
	Star14 - direction
	GroundStation9 - direction
	Star48 - direction
	Star2 - direction
	Star19 - direction
	Star15 - direction
	Star38 - direction
	GroundStation39 - direction
	Star35 - direction
	Star5 - direction
	Star24 - direction
	Star40 - direction
	Star44 - direction
	GroundStation7 - direction
	GroundStation36 - direction
	Star8 - direction
	Phenomenon50 - direction
	Star51 - direction
	Phenomenon52 - direction
	Phenomenon53 - direction
	Phenomenon54 - direction
	Phenomenon55 - direction
	Planet56 - direction
	Star57 - direction
	Planet58 - direction
	Planet59 - direction
	Phenomenon60 - direction
	Phenomenon61 - direction
	Phenomenon62 - direction
	Planet63 - direction
	Star64 - direction
	Star65 - direction
	Star66 - direction
	Phenomenon67 - direction
	Planet68 - direction
	Star69 - direction
	Phenomenon70 - direction
	Star71 - direction
	Star72 - direction
	Phenomenon73 - direction
	Star74 - direction
	Planet75 - direction
	Phenomenon76 - direction
	Star77 - direction
	Phenomenon78 - direction
	Planet79 - direction
	Phenomenon80 - direction
	Phenomenon81 - direction
	Planet82 - direction
	Phenomenon83 - direction
	Phenomenon84 - direction
	Planet85 - direction
	Star86 - direction
	Star87 - direction
	Phenomenon88 - direction
	Phenomenon89 - direction
	Star90 - direction
	Planet91 - direction
	Phenomenon92 - direction
	Star93 - direction
	Star94 - direction
	Phenomenon95 - direction
	Phenomenon96 - direction
	Phenomenon97 - direction
	Planet98 - direction
	Phenomenon99 - direction
)
(:init
	(supports instrument0 thermograph0)
	(calibration_target instrument0 Star2)
	(calibration_target instrument0 Star6)
	(calibration_target instrument0 Star22)
	(calibration_target instrument0 GroundStation9)
	(calibration_target instrument0 Star38)
	(calibration_target instrument0 Star45)
	(calibration_target instrument0 Star8)
	(calibration_target instrument0 GroundStation13)
	(calibration_target instrument0 GroundStation25)
	(calibration_target instrument0 Star31)
	(calibration_target instrument0 Star21)
	(calibration_target instrument0 Star43)
	(calibration_target instrument0 Star10)
	(calibration_target instrument0 Star1)
	(calibration_target instrument0 Star35)
	(supports instrument1 thermograph0)
	(calibration_target instrument1 Star10)
	(calibration_target instrument1 GroundStation4)
	(calibration_target instrument1 Star14)
	(calibration_target instrument1 GroundStation47)
	(calibration_target instrument1 Star17)
	(calibration_target instrument1 Star28)
	(calibration_target instrument1 GroundStation11)
	(supports instrument2 thermograph0)
	(calibration_target instrument2 Star29)
	(calibration_target instrument2 Star21)
	(calibration_target instrument2 Star0)
	(calibration_target instrument2 Star10)
	(calibration_target instrument2 GroundStation20)
	(calibration_target instrument2 Star45)
	(calibration_target instrument2 Star35)
	(calibration_target instrument2 Star49)
	(calibration_target instrument2 Star16)
	(calibration_target instrument2 Star40)
	(calibration_target instrument2 GroundStation11)
	(calibration_target instrument2 GroundStation39)
	(calibration_target instrument2 GroundStation33)
	(supports instrument3 thermograph4)
	(supports instrument3 spectrograph3)
	(calibration_target instrument3 GroundStation27)
	(calibration_target instrument3 Star16)
	(calibration_target instrument3 Star30)
	(calibration_target instrument3 GroundStation26)
	(calibration_target instrument3 Star40)
	(calibration_target instrument3 Star1)
	(calibration_target instrument3 Star21)
	(calibration_target instrument3 Star48)
	(calibration_target instrument3 Star29)
	(calibration_target instrument3 Star12)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Planet85)
	(supports instrument4 infrared2)
	(calibration_target instrument4 GroundStation11)
	(calibration_target instrument4 GroundStation33)
	(calibration_target instrument4 GroundStation41)
	(calibration_target instrument4 Star1)
	(calibration_target instrument4 Star48)
	(calibration_target instrument4 Star15)
	(calibration_target instrument4 Star44)
	(supports instrument5 thermograph5)
	(supports instrument5 image1)
	(calibration_target instrument5 GroundStation20)
	(calibration_target instrument5 Star16)
	(calibration_target instrument5 Star2)
	(calibration_target instrument5 Star30)
	(supports instrument6 infrared7)
	(calibration_target instrument6 GroundStation39)
	(calibration_target instrument6 GroundStation26)
	(calibration_target instrument6 Star0)
	(calibration_target instrument6 GroundStation34)
	(calibration_target instrument6 GroundStation3)
	(calibration_target instrument6 GroundStation4)
	(calibration_target instrument6 GroundStation18)
	(calibration_target instrument6 Star24)
	(calibration_target instrument6 Star1)
	(calibration_target instrument6 GroundStation33)
	(calibration_target instrument6 Star45)
	(calibration_target instrument6 GroundStation23)
	(calibration_target instrument6 Star44)
	(calibration_target instrument6 GroundStation20)
	(calibration_target instrument6 Star46)
	(calibration_target instrument6 Star31)
	(supports instrument7 spectrograph6)
	(supports instrument7 thermograph0)
	(calibration_target instrument7 GroundStation20)
	(calibration_target instrument7 GroundStation9)
	(calibration_target instrument7 Star40)
	(calibration_target instrument7 Star48)
	(calibration_target instrument7 Star37)
	(calibration_target instrument7 Star42)
	(calibration_target instrument7 Star16)
	(supports instrument8 infrared2)
	(supports instrument8 spectrograph6)
	(calibration_target instrument8 Star6)
	(calibration_target instrument8 Star2)
	(calibration_target instrument8 GroundStation25)
	(calibration_target instrument8 Star46)
	(calibration_target instrument8 Star43)
	(calibration_target instrument8 GroundStation23)
	(calibration_target instrument8 Star28)
	(calibration_target instrument8 Star24)
	(calibration_target instrument8 Star45)
	(calibration_target instrument8 Star44)
	(calibration_target instrument8 Star42)
	(calibration_target instrument8 GroundStation26)
	(calibration_target instrument8 Star5)
	(supports instrument9 infrared2)
	(supports instrument9 thermograph4)
	(supports instrument9 thermograph0)
	(calibration_target instrument9 GroundStation18)
	(calibration_target instrument9 Star10)
	(calibration_target instrument9 Star45)
	(calibration_target instrument9 Star32)
	(calibration_target instrument9 GroundStation41)
	(calibration_target instrument9 Star30)
	(calibration_target instrument9 Star35)
	(calibration_target instrument9 Star19)
	(calibration_target instrument9 GroundStation7)
	(calibration_target instrument9 GroundStation23)
	(calibration_target instrument9 Star43)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(on_board instrument9 satellite1)
	(power_avail satellite1)
	(pointing satellite1 GroundStation47)
	(supports instrument10 infrared2)
	(supports instrument10 thermograph0)
	(supports instrument10 spectrograph3)
	(calibration_target instrument10 GroundStation7)
	(calibration_target instrument10 GroundStation26)
	(calibration_target instrument10 Star17)
	(calibration_target instrument10 Star22)
	(calibration_target instrument10 Star24)
	(calibration_target instrument10 Star49)
	(calibration_target instrument10 Star10)
	(calibration_target instrument10 Star40)
	(calibration_target instrument10 Star45)
	(calibration_target instrument10 Star19)
	(calibration_target instrument10 GroundStation13)
	(calibration_target instrument10 GroundStation23)
	(supports instrument11 infrared7)
	(supports instrument11 spectrograph3)
	(supports instrument11 thermograph4)
	(calibration_target instrument11 GroundStation26)
	(calibration_target instrument11 GroundStation4)
	(calibration_target instrument11 Star38)
	(calibration_target instrument11 Star2)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation34)
	(supports instrument12 spectrograph6)
	(supports instrument12 thermograph4)
	(calibration_target instrument12 Star42)
	(calibration_target instrument12 Star17)
	(calibration_target instrument12 Star45)
	(calibration_target instrument12 GroundStation26)
	(calibration_target instrument12 Star16)
	(calibration_target instrument12 Star2)
	(calibration_target instrument12 GroundStation18)
	(calibration_target instrument12 Star14)
	(calibration_target instrument12 GroundStation39)
	(calibration_target instrument12 Star44)
	(supports instrument13 image1)
	(supports instrument13 infrared2)
	(supports instrument13 spectrograph6)
	(calibration_target instrument13 Star2)
	(calibration_target instrument13 Star1)
	(calibration_target instrument13 Star35)
	(calibration_target instrument13 GroundStation33)
	(calibration_target instrument13 Star16)
	(calibration_target instrument13 Star21)
	(calibration_target instrument13 Star49)
	(calibration_target instrument13 GroundStation4)
	(calibration_target instrument13 GroundStation18)
	(calibration_target instrument13 Star19)
	(calibration_target instrument13 GroundStation11)
	(calibration_target instrument13 Star44)
	(calibration_target instrument13 Star28)
	(calibration_target instrument13 Star0)
	(calibration_target instrument13 Star31)
	(calibration_target instrument13 Star45)
	(on_board instrument12 satellite3)
	(on_board instrument13 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star10)
	(supports instrument14 thermograph5)
	(supports instrument14 thermograph0)
	(calibration_target instrument14 GroundStation26)
	(calibration_target instrument14 Star24)
	(calibration_target instrument14 Star10)
	(calibration_target instrument14 GroundStation7)
	(calibration_target instrument14 GroundStation23)
	(on_board instrument14 satellite4)
	(power_avail satellite4)
	(pointing satellite4 GroundStation23)
	(supports instrument15 thermograph5)
	(supports instrument15 infrared7)
	(calibration_target instrument15 Star32)
	(calibration_target instrument15 GroundStation27)
	(calibration_target instrument15 GroundStation36)
	(calibration_target instrument15 Star10)
	(calibration_target instrument15 Star44)
	(calibration_target instrument15 Star29)
	(calibration_target instrument15 GroundStation13)
	(calibration_target instrument15 Star1)
	(calibration_target instrument15 Star6)
	(calibration_target instrument15 Star19)
	(calibration_target instrument15 GroundStation39)
	(calibration_target instrument15 GroundStation33)
	(calibration_target instrument15 GroundStation9)
	(supports instrument16 infrared8)
	(calibration_target instrument16 GroundStation41)
	(calibration_target instrument16 GroundStation39)
	(calibration_target instrument16 GroundStation27)
	(calibration_target instrument16 Star42)
	(calibration_target instrument16 GroundStation33)
	(calibration_target instrument16 Star44)
	(calibration_target instrument16 Star12)
	(calibration_target instrument16 Star1)
	(calibration_target instrument16 Star40)
	(calibration_target instrument16 Star10)
	(supports instrument17 thermograph5)
	(calibration_target instrument17 Star21)
	(calibration_target instrument17 Star17)
	(calibration_target instrument17 Star29)
	(calibration_target instrument17 Star48)
	(calibration_target instrument17 GroundStation27)
	(calibration_target instrument17 GroundStation13)
	(calibration_target instrument17 Star46)
	(supports instrument18 thermograph0)
	(supports instrument18 infrared7)
	(calibration_target instrument18 GroundStation41)
	(calibration_target instrument18 Star12)
	(calibration_target instrument18 Star46)
	(calibration_target instrument18 GroundStation13)
	(calibration_target instrument18 Star21)
	(calibration_target instrument18 Star31)
	(calibration_target instrument18 Star43)
	(supports instrument19 infrared2)
	(supports instrument19 infrared8)
	(supports instrument19 thermograph0)
	(calibration_target instrument19 GroundStation33)
	(calibration_target instrument19 Star49)
	(on_board instrument15 satellite5)
	(on_board instrument16 satellite5)
	(on_board instrument17 satellite5)
	(on_board instrument18 satellite5)
	(on_board instrument19 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Phenomenon52)
	(supports instrument20 infrared2)
	(supports instrument20 thermograph0)
	(calibration_target instrument20 Star43)
	(calibration_target instrument20 GroundStation27)
	(calibration_target instrument20 Star44)
	(calibration_target instrument20 GroundStation47)
	(calibration_target instrument20 GroundStation9)
	(calibration_target instrument20 Star24)
	(calibration_target instrument20 Star45)
	(on_board instrument20 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Planet79)
	(supports instrument21 spectrograph6)
	(calibration_target instrument21 Star28)
	(calibration_target instrument21 Star2)
	(calibration_target instrument21 GroundStation39)
	(calibration_target instrument21 GroundStation41)
	(calibration_target instrument21 GroundStation4)
	(calibration_target instrument21 GroundStation23)
	(calibration_target instrument21 GroundStation25)
	(calibration_target instrument21 Star30)
	(calibration_target instrument21 Star5)
	(calibration_target instrument21 GroundStation9)
	(supports instrument22 infrared8)
	(supports instrument22 thermograph4)
	(calibration_target instrument22 Star37)
	(calibration_target instrument22 Star24)
	(calibration_target instrument22 GroundStation26)
	(calibration_target instrument22 Star10)
	(calibration_target instrument22 GroundStation23)
	(calibration_target instrument22 Star21)
	(calibration_target instrument22 Star2)
	(calibration_target instrument22 Star22)
	(on_board instrument21 satellite7)
	(on_board instrument22 satellite7)
	(power_avail satellite7)
	(pointing satellite7 GroundStation7)
	(supports instrument23 infrared8)
	(supports instrument23 thermograph0)
	(calibration_target instrument23 Star1)
	(calibration_target instrument23 Star19)
	(calibration_target instrument23 Star31)
	(calibration_target instrument23 Star22)
	(calibration_target instrument23 Star40)
	(calibration_target instrument23 Star0)
	(supports instrument24 thermograph5)
	(supports instrument24 spectrograph3)
	(calibration_target instrument24 Star6)
	(calibration_target instrument24 Star16)
	(calibration_target instrument24 Star49)
	(calibration_target instrument24 GroundStation13)
	(calibration_target instrument24 GroundStation18)
	(calibration_target instrument24 Star38)
	(calibration_target instrument24 GroundStation7)
	(calibration_target instrument24 GroundStation25)
	(calibration_target instrument24 Star17)
	(calibration_target instrument24 Star35)
	(calibration_target instrument24 Star30)
	(calibration_target instrument24 Star31)
	(calibration_target instrument24 Star14)
	(calibration_target instrument24 GroundStation36)
	(calibration_target instrument24 Star29)
	(calibration_target instrument24 GroundStation9)
	(supports instrument25 infrared2)
	(supports instrument25 infrared7)
	(calibration_target instrument25 Star5)
	(calibration_target instrument25 Star2)
	(calibration_target instrument25 Star48)
	(calibration_target instrument25 GroundStation9)
	(calibration_target instrument25 Star40)
	(calibration_target instrument25 Star14)
	(calibration_target instrument25 Star22)
	(calibration_target instrument25 Star30)
	(calibration_target instrument25 GroundStation13)
	(calibration_target instrument25 GroundStation7)
	(supports instrument26 infrared8)
	(calibration_target instrument26 Star38)
	(calibration_target instrument26 GroundStation36)
	(calibration_target instrument26 Star15)
	(calibration_target instrument26 Star19)
	(supports instrument27 thermograph4)
	(supports instrument27 infrared7)
	(calibration_target instrument27 Star40)
	(calibration_target instrument27 Star24)
	(calibration_target instrument27 Star5)
	(calibration_target instrument27 Star35)
	(calibration_target instrument27 GroundStation39)
	(supports instrument28 infrared7)
	(supports instrument28 infrared2)
	(supports instrument28 infrared8)
	(calibration_target instrument28 Star8)
	(calibration_target instrument28 GroundStation36)
	(calibration_target instrument28 GroundStation7)
	(calibration_target instrument28 Star44)
	(on_board instrument23 satellite8)
	(on_board instrument24 satellite8)
	(on_board instrument25 satellite8)
	(on_board instrument26 satellite8)
	(on_board instrument27 satellite8)
	(on_board instrument28 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Planet85)
)
(:goal (and
	(pointing satellite2 GroundStation36)
	(pointing satellite5 Star10)
	(pointing satellite6 Star0)
	(pointing satellite8 Phenomenon70)
	(have_image Phenomenon50 infrared7)
	(have_image Phenomenon50 thermograph4)
	(have_image Star51 image1)
	(have_image Star51 thermograph5)
	(have_image Star51 thermograph4)
	(have_image Phenomenon52 thermograph5)
	(have_image Phenomenon54 spectrograph6)
	(have_image Phenomenon55 infrared8)
	(have_image Phenomenon55 spectrograph3)
	(have_image Phenomenon55 infrared2)
	(have_image Planet56 image1)
	(have_image Planet56 thermograph4)
	(have_image Star57 infrared7)
	(have_image Star57 spectrograph3)
	(have_image Star57 image1)
	(have_image Planet58 spectrograph6)
	(have_image Planet59 thermograph5)
	(have_image Planet59 spectrograph3)
	(have_image Planet59 thermograph0)
	(have_image Phenomenon60 infrared2)
	(have_image Phenomenon60 spectrograph3)
	(have_image Phenomenon60 thermograph5)
	(have_image Phenomenon61 image1)
	(have_image Phenomenon61 infrared8)
	(have_image Phenomenon62 infrared8)
	(have_image Planet63 infrared7)
	(have_image Planet63 image1)
	(have_image Star64 infrared8)
	(have_image Star65 thermograph5)
	(have_image Star65 image1)
	(have_image Star65 infrared8)
	(have_image Star66 spectrograph3)
	(have_image Star66 infrared7)
	(have_image Phenomenon67 infrared8)
	(have_image Phenomenon67 thermograph5)
	(have_image Planet68 thermograph5)
	(have_image Star69 thermograph4)
	(have_image Star69 spectrograph6)
	(have_image Star69 thermograph5)
	(have_image Phenomenon70 image1)
	(have_image Star71 infrared2)
	(have_image Star72 thermograph0)
	(have_image Star72 spectrograph6)
	(have_image Phenomenon73 thermograph5)
	(have_image Phenomenon73 thermograph0)
	(have_image Phenomenon73 infrared8)
	(have_image Star74 spectrograph6)
	(have_image Planet75 spectrograph6)
	(have_image Planet75 image1)
	(have_image Phenomenon76 infrared2)
	(have_image Phenomenon76 thermograph0)
	(have_image Phenomenon76 thermograph4)
	(have_image Star77 thermograph5)
	(have_image Planet79 infrared8)
	(have_image Planet79 image1)
	(have_image Planet79 spectrograph3)
	(have_image Phenomenon81 infrared8)
	(have_image Planet82 thermograph5)
	(have_image Planet82 infrared8)
	(have_image Planet82 infrared2)
	(have_image Phenomenon83 image1)
	(have_image Phenomenon83 thermograph4)
	(have_image Phenomenon84 spectrograph6)
	(have_image Planet85 infrared8)
	(have_image Planet85 spectrograph3)
	(have_image Star86 spectrograph3)
	(have_image Star87 thermograph5)
	(have_image Star87 image1)
	(have_image Star87 infrared7)
	(have_image Phenomenon88 infrared2)
	(have_image Phenomenon88 thermograph0)
	(have_image Phenomenon88 infrared8)
	(have_image Phenomenon89 thermograph0)
	(have_image Phenomenon89 infrared2)
	(have_image Phenomenon89 infrared8)
	(have_image Planet91 spectrograph6)
	(have_image Planet91 spectrograph3)
	(have_image Planet91 infrared8)
	(have_image Phenomenon92 infrared8)
	(have_image Star93 spectrograph3)
	(have_image Star93 thermograph0)
	(have_image Star93 thermograph5)
	(have_image Star94 image1)
	(have_image Phenomenon95 infrared7)
	(have_image Phenomenon95 image1)
	(have_image Phenomenon95 thermograph5)
	(have_image Phenomenon96 spectrograph3)
	(have_image Phenomenon96 thermograph4)
	(have_image Phenomenon97 spectrograph6)
	(have_image Planet98 thermograph0)
	(have_image Planet98 infrared2)
	(have_image Planet98 spectrograph3)
	(have_image Phenomenon99 infrared7)
))

)
