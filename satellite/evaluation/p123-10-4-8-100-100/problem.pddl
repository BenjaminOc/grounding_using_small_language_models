(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	satellite1 - satellite
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	satellite2 - satellite
	instrument6 - instrument
	satellite3 - satellite
	instrument7 - instrument
	satellite4 - satellite
	instrument8 - instrument
	instrument9 - instrument
	satellite5 - satellite
	instrument10 - instrument
	instrument11 - instrument
	satellite6 - satellite
	instrument12 - instrument
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	satellite7 - satellite
	instrument16 - instrument
	satellite8 - satellite
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	satellite9 - satellite
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	spectrograph1 - mode
	thermograph7 - mode
	spectrograph3 - mode
	spectrograph4 - mode
	infrared5 - mode
	image6 - mode
	thermograph0 - mode
	infrared2 - mode
	Star96 - direction
	Star80 - direction
	Star14 - direction
	GroundStation0 - direction
	Star28 - direction
	GroundStation58 - direction
	Star20 - direction
	GroundStation40 - direction
	GroundStation13 - direction
	Star3 - direction
	GroundStation83 - direction
	GroundStation48 - direction
	Star46 - direction
	Star25 - direction
	GroundStation85 - direction
	GroundStation99 - direction
	Star59 - direction
	Star35 - direction
	GroundStation34 - direction
	Star42 - direction
	Star50 - direction
	Star8 - direction
	GroundStation26 - direction
	GroundStation47 - direction
	GroundStation39 - direction
	Star45 - direction
	GroundStation81 - direction
	Star33 - direction
	Star41 - direction
	GroundStation67 - direction
	GroundStation84 - direction
	GroundStation94 - direction
	GroundStation21 - direction
	GroundStation70 - direction
	Star55 - direction
	Star19 - direction
	GroundStation75 - direction
	Star30 - direction
	GroundStation66 - direction
	Star24 - direction
	GroundStation27 - direction
	Star91 - direction
	GroundStation43 - direction
	Star89 - direction
	Star79 - direction
	GroundStation22 - direction
	Star49 - direction
	GroundStation92 - direction
	GroundStation62 - direction
	GroundStation63 - direction
	GroundStation5 - direction
	Star60 - direction
	GroundStation72 - direction
	GroundStation16 - direction
	GroundStation7 - direction
	GroundStation82 - direction
	Star9 - direction
	GroundStation51 - direction
	GroundStation12 - direction
	GroundStation54 - direction
	GroundStation18 - direction
	GroundStation95 - direction
	Star65 - direction
	GroundStation52 - direction
	GroundStation23 - direction
	Star68 - direction
	GroundStation88 - direction
	Star56 - direction
	GroundStation38 - direction
	Star71 - direction
	GroundStation86 - direction
	GroundStation36 - direction
	GroundStation4 - direction
	Star76 - direction
	Star6 - direction
	GroundStation69 - direction
	GroundStation97 - direction
	Star53 - direction
	Star29 - direction
	Star93 - direction
	GroundStation90 - direction
	Star73 - direction
	GroundStation10 - direction
	Star37 - direction
	GroundStation74 - direction
	GroundStation31 - direction
	Star32 - direction
	Star57 - direction
	GroundStation64 - direction
	GroundStation17 - direction
	GroundStation87 - direction
	GroundStation44 - direction
	Star1 - direction
	Star2 - direction
	GroundStation11 - direction
	GroundStation15 - direction
	GroundStation78 - direction
	Star77 - direction
	GroundStation61 - direction
	Star98 - direction
	Phenomenon100 - direction
	Planet101 - direction
	Star102 - direction
	Planet103 - direction
	Star104 - direction
	Phenomenon105 - direction
	Phenomenon106 - direction
	Phenomenon107 - direction
	Phenomenon108 - direction
	Phenomenon109 - direction
	Phenomenon110 - direction
	Phenomenon111 - direction
	Phenomenon112 - direction
	Planet113 - direction
	Star114 - direction
	Planet115 - direction
	Star116 - direction
	Planet117 - direction
	Phenomenon118 - direction
	Planet119 - direction
	Planet120 - direction
	Planet121 - direction
	Planet122 - direction
	Planet123 - direction
	Planet124 - direction
	Star125 - direction
	Planet126 - direction
	Phenomenon127 - direction
	Star128 - direction
	Phenomenon129 - direction
	Phenomenon130 - direction
	Planet131 - direction
	Star132 - direction
	Star133 - direction
	Planet134 - direction
	Star135 - direction
	Star136 - direction
	Planet137 - direction
	Planet138 - direction
	Star139 - direction
	Planet140 - direction
	Phenomenon141 - direction
	Planet142 - direction
	Planet143 - direction
	Planet144 - direction
	Planet145 - direction
	Star146 - direction
	Star147 - direction
	Planet148 - direction
	Planet149 - direction
	Star150 - direction
	Star151 - direction
	Phenomenon152 - direction
	Star153 - direction
	Star154 - direction
	Star155 - direction
	Planet156 - direction
	Star157 - direction
	Star158 - direction
	Phenomenon159 - direction
	Star160 - direction
	Phenomenon161 - direction
	Planet162 - direction
	Planet163 - direction
	Phenomenon164 - direction
	Phenomenon165 - direction
	Star166 - direction
	Planet167 - direction
	Phenomenon168 - direction
	Star169 - direction
	Planet170 - direction
	Planet171 - direction
	Star172 - direction
	Star173 - direction
	Star174 - direction
	Planet175 - direction
	Planet176 - direction
	Phenomenon177 - direction
	Phenomenon178 - direction
	Phenomenon179 - direction
	Star180 - direction
	Planet181 - direction
	Phenomenon182 - direction
	Star183 - direction
	Star184 - direction
	Star185 - direction
	Phenomenon186 - direction
	Star187 - direction
	Planet188 - direction
	Phenomenon189 - direction
	Star190 - direction
	Phenomenon191 - direction
	Planet192 - direction
	Planet193 - direction
	Star194 - direction
	Star195 - direction
	Star196 - direction
	Phenomenon197 - direction
	Star198 - direction
	Planet199 - direction
)
(:init
	(supports instrument0 infrared5)
	(supports instrument0 thermograph0)
	(calibration_target instrument0 Star25)
	(calibration_target instrument0 GroundStation87)
	(calibration_target instrument0 Star77)
	(calibration_target instrument0 GroundStation92)
	(calibration_target instrument0 GroundStation66)
	(calibration_target instrument0 GroundStation62)
	(calibration_target instrument0 GroundStation52)
	(calibration_target instrument0 Star14)
	(calibration_target instrument0 Star55)
	(calibration_target instrument0 GroundStation61)
	(calibration_target instrument0 Star28)
	(calibration_target instrument0 Star96)
	(calibration_target instrument0 Star93)
	(calibration_target instrument0 Star53)
	(calibration_target instrument0 Star46)
	(calibration_target instrument0 GroundStation58)
	(calibration_target instrument0 GroundStation11)
	(supports instrument1 thermograph0)
	(calibration_target instrument1 GroundStation67)
	(calibration_target instrument1 Star2)
	(calibration_target instrument1 Star30)
	(calibration_target instrument1 GroundStation21)
	(calibration_target instrument1 GroundStation36)
	(calibration_target instrument1 Star24)
	(calibration_target instrument1 GroundStation13)
	(calibration_target instrument1 Star28)
	(calibration_target instrument1 GroundStation85)
	(calibration_target instrument1 GroundStation88)
	(calibration_target instrument1 Star3)
	(calibration_target instrument1 Star29)
	(calibration_target instrument1 GroundStation7)
	(calibration_target instrument1 GroundStation31)
	(calibration_target instrument1 GroundStation22)
	(calibration_target instrument1 Star93)
	(calibration_target instrument1 Star89)
	(calibration_target instrument1 GroundStation12)
	(calibration_target instrument1 Star37)
	(calibration_target instrument1 GroundStation15)
	(calibration_target instrument1 GroundStation48)
	(calibration_target instrument1 Star1)
	(calibration_target instrument1 GroundStation66)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation47)
	(supports instrument2 spectrograph4)
	(supports instrument2 thermograph7)
	(supports instrument2 thermograph0)
	(calibration_target instrument2 Star73)
	(calibration_target instrument2 GroundStation5)
	(calibration_target instrument2 Star49)
	(calibration_target instrument2 Star76)
	(calibration_target instrument2 GroundStation11)
	(calibration_target instrument2 GroundStation0)
	(calibration_target instrument2 GroundStation92)
	(calibration_target instrument2 Star68)
	(calibration_target instrument2 Star9)
	(calibration_target instrument2 GroundStation39)
	(calibration_target instrument2 GroundStation54)
	(calibration_target instrument2 GroundStation21)
	(calibration_target instrument2 GroundStation69)
	(calibration_target instrument2 Star28)
	(calibration_target instrument2 Star3)
	(calibration_target instrument2 GroundStation48)
	(calibration_target instrument2 Star98)
	(calibration_target instrument2 GroundStation17)
	(calibration_target instrument2 Star65)
	(supports instrument3 thermograph0)
	(supports instrument3 thermograph7)
	(supports instrument3 image6)
	(calibration_target instrument3 Star41)
	(calibration_target instrument3 Star42)
	(calibration_target instrument3 GroundStation87)
	(calibration_target instrument3 Star49)
	(calibration_target instrument3 Star30)
	(calibration_target instrument3 Star37)
	(calibration_target instrument3 GroundStation75)
	(calibration_target instrument3 GroundStation11)
	(calibration_target instrument3 GroundStation66)
	(calibration_target instrument3 GroundStation84)
	(calibration_target instrument3 GroundStation0)
	(calibration_target instrument3 GroundStation62)
	(calibration_target instrument3 Star96)
	(calibration_target instrument3 GroundStation67)
	(calibration_target instrument3 Star46)
	(calibration_target instrument3 Star80)
	(calibration_target instrument3 Star24)
	(calibration_target instrument3 Star91)
	(calibration_target instrument3 GroundStation48)
	(calibration_target instrument3 GroundStation40)
	(calibration_target instrument3 GroundStation81)
	(calibration_target instrument3 Star53)
	(calibration_target instrument3 Star19)
	(calibration_target instrument3 GroundStation4)
	(supports instrument4 spectrograph3)
	(calibration_target instrument4 GroundStation21)
	(calibration_target instrument4 GroundStation40)
	(calibration_target instrument4 Star42)
	(calibration_target instrument4 GroundStation69)
	(calibration_target instrument4 Star20)
	(calibration_target instrument4 Star28)
	(calibration_target instrument4 Star65)
	(calibration_target instrument4 GroundStation23)
	(calibration_target instrument4 Star45)
	(calibration_target instrument4 GroundStation67)
	(calibration_target instrument4 GroundStation5)
	(calibration_target instrument4 GroundStation48)
	(calibration_target instrument4 GroundStation64)
	(calibration_target instrument4 GroundStation16)
	(calibration_target instrument4 Star14)
	(calibration_target instrument4 Star55)
	(calibration_target instrument4 Star41)
	(calibration_target instrument4 GroundStation58)
	(calibration_target instrument4 Star29)
	(calibration_target instrument4 GroundStation88)
	(calibration_target instrument4 Star80)
	(calibration_target instrument4 GroundStation54)
	(calibration_target instrument4 Star19)
	(calibration_target instrument4 Star24)
	(calibration_target instrument4 GroundStation31)
	(calibration_target instrument4 GroundStation12)
	(calibration_target instrument4 Star57)
	(calibration_target instrument4 Star71)
	(calibration_target instrument4 GroundStation4)
	(calibration_target instrument4 GroundStation52)
	(supports instrument5 thermograph7)
	(calibration_target instrument5 GroundStation22)
	(calibration_target instrument5 GroundStation5)
	(calibration_target instrument5 Star76)
	(calibration_target instrument5 GroundStation31)
	(calibration_target instrument5 Star57)
	(calibration_target instrument5 GroundStation97)
	(calibration_target instrument5 GroundStation51)
	(calibration_target instrument5 Star24)
	(calibration_target instrument5 GroundStation40)
	(calibration_target instrument5 GroundStation95)
	(calibration_target instrument5 Star91)
	(calibration_target instrument5 GroundStation48)
	(calibration_target instrument5 Star9)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star190)
	(supports instrument6 spectrograph4)
	(calibration_target instrument6 GroundStation15)
	(calibration_target instrument6 GroundStation0)
	(calibration_target instrument6 GroundStation52)
	(calibration_target instrument6 Star25)
	(calibration_target instrument6 GroundStation21)
	(calibration_target instrument6 GroundStation72)
	(calibration_target instrument6 GroundStation78)
	(calibration_target instrument6 GroundStation13)
	(calibration_target instrument6 Star32)
	(calibration_target instrument6 Star73)
	(calibration_target instrument6 GroundStation43)
	(calibration_target instrument6 GroundStation12)
	(calibration_target instrument6 Star19)
	(calibration_target instrument6 GroundStation4)
	(calibration_target instrument6 GroundStation16)
	(calibration_target instrument6 Star3)
	(calibration_target instrument6 Star41)
	(calibration_target instrument6 Star79)
	(calibration_target instrument6 Star68)
	(calibration_target instrument6 Star24)
	(calibration_target instrument6 Star1)
	(calibration_target instrument6 GroundStation22)
	(calibration_target instrument6 Star8)
	(calibration_target instrument6 GroundStation11)
	(calibration_target instrument6 GroundStation74)
	(calibration_target instrument6 Star46)
	(calibration_target instrument6 Star60)
	(calibration_target instrument6 Star93)
	(calibration_target instrument6 GroundStation84)
	(on_board instrument6 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Phenomenon110)
	(supports instrument7 image6)
	(supports instrument7 infrared2)
	(supports instrument7 infrared5)
	(calibration_target instrument7 Star2)
	(calibration_target instrument7 Star59)
	(calibration_target instrument7 GroundStation92)
	(calibration_target instrument7 Star33)
	(calibration_target instrument7 Star76)
	(calibration_target instrument7 Star49)
	(calibration_target instrument7 Star42)
	(calibration_target instrument7 Star57)
	(calibration_target instrument7 GroundStation90)
	(calibration_target instrument7 GroundStation81)
	(calibration_target instrument7 GroundStation47)
	(calibration_target instrument7 Star73)
	(calibration_target instrument7 GroundStation54)
	(calibration_target instrument7 Star28)
	(calibration_target instrument7 GroundStation95)
	(calibration_target instrument7 Star35)
	(calibration_target instrument7 GroundStation99)
	(calibration_target instrument7 GroundStation16)
	(calibration_target instrument7 GroundStation72)
	(calibration_target instrument7 GroundStation61)
	(calibration_target instrument7 Star9)
	(calibration_target instrument7 GroundStation66)
	(calibration_target instrument7 Star29)
	(calibration_target instrument7 GroundStation94)
	(calibration_target instrument7 GroundStation12)
	(calibration_target instrument7 Star45)
	(calibration_target instrument7 Star6)
	(calibration_target instrument7 Star30)
	(on_board instrument7 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star46)
	(supports instrument8 infrared5)
	(supports instrument8 spectrograph3)
	(supports instrument8 image6)
	(calibration_target instrument8 Star33)
	(calibration_target instrument8 Star32)
	(calibration_target instrument8 GroundStation21)
	(calibration_target instrument8 Star28)
	(calibration_target instrument8 GroundStation84)
	(calibration_target instrument8 GroundStation78)
	(calibration_target instrument8 GroundStation36)
	(calibration_target instrument8 GroundStation54)
	(calibration_target instrument8 GroundStation87)
	(calibration_target instrument8 Star68)
	(calibration_target instrument8 GroundStation40)
	(calibration_target instrument8 Star6)
	(calibration_target instrument8 Star57)
	(calibration_target instrument8 Star53)
	(calibration_target instrument8 GroundStation94)
	(calibration_target instrument8 Star24)
	(calibration_target instrument8 GroundStation51)
	(calibration_target instrument8 Star2)
	(calibration_target instrument8 GroundStation86)
	(supports instrument9 spectrograph1)
	(supports instrument9 infrared5)
	(calibration_target instrument9 Star24)
	(calibration_target instrument9 GroundStation54)
	(calibration_target instrument9 GroundStation62)
	(calibration_target instrument9 Star30)
	(calibration_target instrument9 Star3)
	(calibration_target instrument9 GroundStation16)
	(calibration_target instrument9 GroundStation15)
	(calibration_target instrument9 GroundStation99)
	(calibration_target instrument9 Star68)
	(calibration_target instrument9 Star2)
	(calibration_target instrument9 Star42)
	(calibration_target instrument9 GroundStation22)
	(calibration_target instrument9 Star41)
	(calibration_target instrument9 GroundStation10)
	(calibration_target instrument9 Star8)
	(calibration_target instrument9 GroundStation83)
	(calibration_target instrument9 GroundStation40)
	(calibration_target instrument9 Star53)
	(calibration_target instrument9 Star45)
	(calibration_target instrument9 GroundStation23)
	(calibration_target instrument9 GroundStation17)
	(calibration_target instrument9 Star76)
	(calibration_target instrument9 GroundStation74)
	(calibration_target instrument9 GroundStation69)
	(calibration_target instrument9 GroundStation31)
	(calibration_target instrument9 GroundStation58)
	(calibration_target instrument9 Star49)
	(calibration_target instrument9 GroundStation21)
	(calibration_target instrument9 Star91)
	(calibration_target instrument9 GroundStation81)
	(calibration_target instrument9 GroundStation78)
	(calibration_target instrument9 GroundStation75)
	(calibration_target instrument9 Star9)
	(on_board instrument8 satellite4)
	(on_board instrument9 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Planet123)
	(supports instrument10 thermograph0)
	(supports instrument10 thermograph7)
	(supports instrument10 spectrograph3)
	(calibration_target instrument10 Star91)
	(calibration_target instrument10 GroundStation48)
	(calibration_target instrument10 GroundStation69)
	(calibration_target instrument10 GroundStation75)
	(supports instrument11 thermograph0)
	(calibration_target instrument11 Star91)
	(calibration_target instrument11 Star71)
	(calibration_target instrument11 GroundStation67)
	(calibration_target instrument11 GroundStation83)
	(calibration_target instrument11 GroundStation78)
	(calibration_target instrument11 GroundStation23)
	(calibration_target instrument11 GroundStation17)
	(calibration_target instrument11 GroundStation38)
	(calibration_target instrument11 GroundStation47)
	(calibration_target instrument11 GroundStation94)
	(calibration_target instrument11 Star3)
	(calibration_target instrument11 Star37)
	(calibration_target instrument11 GroundStation85)
	(calibration_target instrument11 GroundStation82)
	(calibration_target instrument11 Star57)
	(on_board instrument10 satellite5)
	(on_board instrument11 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Planet181)
	(supports instrument12 infrared2)
	(supports instrument12 infrared5)
	(supports instrument12 spectrograph3)
	(calibration_target instrument12 GroundStation31)
	(calibration_target instrument12 Star2)
	(calibration_target instrument12 Star65)
	(calibration_target instrument12 Star20)
	(supports instrument13 spectrograph1)
	(supports instrument13 image6)
	(supports instrument13 infrared5)
	(calibration_target instrument13 GroundStation13)
	(calibration_target instrument13 GroundStation44)
	(calibration_target instrument13 Star68)
	(calibration_target instrument13 Star24)
	(calibration_target instrument13 Star59)
	(calibration_target instrument13 Star91)
	(calibration_target instrument13 GroundStation40)
	(calibration_target instrument13 GroundStation94)
	(calibration_target instrument13 GroundStation61)
	(calibration_target instrument13 GroundStation75)
	(calibration_target instrument13 GroundStation70)
	(calibration_target instrument13 GroundStation84)
	(calibration_target instrument13 Star41)
	(calibration_target instrument13 GroundStation63)
	(supports instrument14 image6)
	(supports instrument14 infrared2)
	(supports instrument14 spectrograph4)
	(calibration_target instrument14 Star25)
	(calibration_target instrument14 GroundStation78)
	(calibration_target instrument14 GroundStation95)
	(calibration_target instrument14 Star1)
	(calibration_target instrument14 GroundStation36)
	(calibration_target instrument14 Star46)
	(calibration_target instrument14 Star30)
	(calibration_target instrument14 Star60)
	(calibration_target instrument14 Star29)
	(calibration_target instrument14 Star55)
	(calibration_target instrument14 GroundStation61)
	(calibration_target instrument14 GroundStation51)
	(calibration_target instrument14 GroundStation63)
	(calibration_target instrument14 GroundStation26)
	(calibration_target instrument14 Star56)
	(calibration_target instrument14 GroundStation48)
	(calibration_target instrument14 GroundStation17)
	(calibration_target instrument14 Star33)
	(calibration_target instrument14 GroundStation69)
	(calibration_target instrument14 Star37)
	(calibration_target instrument14 Star45)
	(calibration_target instrument14 Star89)
	(calibration_target instrument14 GroundStation22)
	(calibration_target instrument14 GroundStation18)
	(calibration_target instrument14 GroundStation97)
	(calibration_target instrument14 GroundStation83)
	(calibration_target instrument14 Star3)
	(calibration_target instrument14 GroundStation34)
	(calibration_target instrument14 Star73)
	(calibration_target instrument14 GroundStation38)
	(supports instrument15 thermograph7)
	(supports instrument15 image6)
	(supports instrument15 spectrograph3)
	(calibration_target instrument15 GroundStation34)
	(calibration_target instrument15 Star19)
	(calibration_target instrument15 GroundStation17)
	(calibration_target instrument15 Star35)
	(calibration_target instrument15 Star49)
	(calibration_target instrument15 Star59)
	(calibration_target instrument15 Star55)
	(calibration_target instrument15 Star42)
	(calibration_target instrument15 GroundStation51)
	(calibration_target instrument15 GroundStation99)
	(calibration_target instrument15 GroundStation85)
	(on_board instrument12 satellite6)
	(on_board instrument13 satellite6)
	(on_board instrument14 satellite6)
	(on_board instrument15 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Phenomenon141)
	(supports instrument16 thermograph7)
	(calibration_target instrument16 GroundStation47)
	(calibration_target instrument16 Star45)
	(calibration_target instrument16 GroundStation17)
	(calibration_target instrument16 GroundStation26)
	(calibration_target instrument16 Star8)
	(calibration_target instrument16 GroundStation78)
	(calibration_target instrument16 GroundStation52)
	(calibration_target instrument16 Star6)
	(calibration_target instrument16 Star9)
	(calibration_target instrument16 GroundStation97)
	(calibration_target instrument16 Star57)
	(calibration_target instrument16 GroundStation51)
	(calibration_target instrument16 Star50)
	(calibration_target instrument16 Star32)
	(calibration_target instrument16 GroundStation15)
	(calibration_target instrument16 Star42)
	(calibration_target instrument16 GroundStation23)
	(calibration_target instrument16 Star33)
	(calibration_target instrument16 Star49)
	(calibration_target instrument16 Star29)
	(calibration_target instrument16 GroundStation43)
	(calibration_target instrument16 Star1)
	(calibration_target instrument16 Star79)
	(calibration_target instrument16 Star89)
	(calibration_target instrument16 GroundStation7)
	(on_board instrument16 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Star135)
	(supports instrument17 spectrograph4)
	(calibration_target instrument17 GroundStation94)
	(calibration_target instrument17 GroundStation81)
	(calibration_target instrument17 Star41)
	(calibration_target instrument17 Star71)
	(calibration_target instrument17 Star45)
	(calibration_target instrument17 GroundStation11)
	(calibration_target instrument17 GroundStation36)
	(calibration_target instrument17 Star60)
	(calibration_target instrument17 GroundStation39)
	(calibration_target instrument17 GroundStation86)
	(calibration_target instrument17 Star37)
	(calibration_target instrument17 GroundStation95)
	(calibration_target instrument17 GroundStation72)
	(calibration_target instrument17 GroundStation70)
	(calibration_target instrument17 Star76)
	(supports instrument18 spectrograph1)
	(supports instrument18 image6)
	(supports instrument18 infrared5)
	(calibration_target instrument18 GroundStation23)
	(calibration_target instrument18 Star65)
	(calibration_target instrument18 Star24)
	(calibration_target instrument18 Star2)
	(calibration_target instrument18 GroundStation75)
	(calibration_target instrument18 Star55)
	(calibration_target instrument18 GroundStation70)
	(calibration_target instrument18 Star71)
	(calibration_target instrument18 Star32)
	(calibration_target instrument18 GroundStation16)
	(calibration_target instrument18 GroundStation21)
	(calibration_target instrument18 GroundStation92)
	(calibration_target instrument18 Star37)
	(calibration_target instrument18 GroundStation94)
	(calibration_target instrument18 GroundStation84)
	(calibration_target instrument18 GroundStation61)
	(calibration_target instrument18 GroundStation54)
	(calibration_target instrument18 GroundStation27)
	(calibration_target instrument18 GroundStation67)
	(calibration_target instrument18 GroundStation86)
	(calibration_target instrument18 Star73)
	(calibration_target instrument18 Star41)
	(calibration_target instrument18 Star60)
	(calibration_target instrument18 Star33)
	(supports instrument19 spectrograph1)
	(calibration_target instrument19 GroundStation15)
	(calibration_target instrument19 Star32)
	(calibration_target instrument19 Star49)
	(calibration_target instrument19 Star91)
	(calibration_target instrument19 GroundStation27)
	(calibration_target instrument19 Star24)
	(calibration_target instrument19 GroundStation36)
	(calibration_target instrument19 GroundStation12)
	(calibration_target instrument19 GroundStation66)
	(calibration_target instrument19 Star30)
	(calibration_target instrument19 GroundStation87)
	(calibration_target instrument19 GroundStation75)
	(calibration_target instrument19 GroundStation22)
	(calibration_target instrument19 Star1)
	(calibration_target instrument19 Star93)
	(calibration_target instrument19 Star6)
	(calibration_target instrument19 Star19)
	(calibration_target instrument19 GroundStation4)
	(calibration_target instrument19 GroundStation54)
	(calibration_target instrument19 GroundStation90)
	(supports instrument20 thermograph0)
	(calibration_target instrument20 GroundStation72)
	(calibration_target instrument20 Star60)
	(calibration_target instrument20 Star37)
	(calibration_target instrument20 GroundStation82)
	(calibration_target instrument20 GroundStation5)
	(calibration_target instrument20 GroundStation54)
	(calibration_target instrument20 GroundStation15)
	(calibration_target instrument20 GroundStation44)
	(calibration_target instrument20 GroundStation31)
	(calibration_target instrument20 GroundStation63)
	(calibration_target instrument20 GroundStation62)
	(calibration_target instrument20 GroundStation92)
	(calibration_target instrument20 Star49)
	(calibration_target instrument20 Star56)
	(calibration_target instrument20 GroundStation22)
	(calibration_target instrument20 Star79)
	(calibration_target instrument20 Star76)
	(calibration_target instrument20 Star32)
	(calibration_target instrument20 Star89)
	(calibration_target instrument20 GroundStation43)
	(on_board instrument17 satellite8)
	(on_board instrument18 satellite8)
	(on_board instrument19 satellite8)
	(on_board instrument20 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Planet117)
	(supports instrument21 thermograph0)
	(supports instrument21 thermograph7)
	(supports instrument21 spectrograph3)
	(calibration_target instrument21 GroundStation69)
	(calibration_target instrument21 Star6)
	(calibration_target instrument21 Star76)
	(calibration_target instrument21 GroundStation90)
	(calibration_target instrument21 Star77)
	(calibration_target instrument21 GroundStation4)
	(calibration_target instrument21 GroundStation36)
	(calibration_target instrument21 GroundStation86)
	(calibration_target instrument21 Star71)
	(calibration_target instrument21 GroundStation17)
	(calibration_target instrument21 Star2)
	(calibration_target instrument21 GroundStation38)
	(calibration_target instrument21 Star56)
	(calibration_target instrument21 GroundStation31)
	(calibration_target instrument21 Star32)
	(calibration_target instrument21 GroundStation88)
	(calibration_target instrument21 Star68)
	(calibration_target instrument21 GroundStation23)
	(calibration_target instrument21 GroundStation52)
	(calibration_target instrument21 Star1)
	(calibration_target instrument21 Star65)
	(calibration_target instrument21 GroundStation95)
	(calibration_target instrument21 GroundStation18)
	(calibration_target instrument21 GroundStation54)
	(calibration_target instrument21 GroundStation78)
	(calibration_target instrument21 GroundStation74)
	(calibration_target instrument21 GroundStation12)
	(calibration_target instrument21 GroundStation51)
	(calibration_target instrument21 Star9)
	(calibration_target instrument21 GroundStation82)
	(calibration_target instrument21 GroundStation7)
	(calibration_target instrument21 GroundStation16)
	(supports instrument22 spectrograph4)
	(supports instrument22 spectrograph3)
	(calibration_target instrument22 Star57)
	(calibration_target instrument22 Star32)
	(calibration_target instrument22 GroundStation31)
	(calibration_target instrument22 GroundStation74)
	(calibration_target instrument22 Star37)
	(calibration_target instrument22 GroundStation87)
	(calibration_target instrument22 GroundStation10)
	(calibration_target instrument22 Star73)
	(calibration_target instrument22 GroundStation90)
	(calibration_target instrument22 Star93)
	(calibration_target instrument22 Star29)
	(calibration_target instrument22 Star53)
	(calibration_target instrument22 GroundStation97)
	(calibration_target instrument22 Star1)
	(supports instrument23 image6)
	(supports instrument23 thermograph0)
	(supports instrument23 infrared5)
	(calibration_target instrument23 GroundStation44)
	(calibration_target instrument23 GroundStation87)
	(calibration_target instrument23 GroundStation17)
	(calibration_target instrument23 GroundStation64)
	(supports instrument24 infrared2)
	(supports instrument24 thermograph0)
	(calibration_target instrument24 Star98)
	(calibration_target instrument24 GroundStation61)
	(calibration_target instrument24 Star77)
	(calibration_target instrument24 GroundStation78)
	(calibration_target instrument24 GroundStation15)
	(calibration_target instrument24 GroundStation11)
	(calibration_target instrument24 Star2)
	(calibration_target instrument24 Star1)
	(on_board instrument21 satellite9)
	(on_board instrument22 satellite9)
	(on_board instrument23 satellite9)
	(on_board instrument24 satellite9)
	(power_avail satellite9)
	(pointing satellite9 Planet199)
)
(:goal (and
	(pointing satellite1 Phenomenon182)
	(pointing satellite6 GroundStation39)
	(pointing satellite7 Planet115)
	(pointing satellite9 GroundStation86)
	(have_image Phenomenon100 spectrograph1)
	(have_image Phenomenon100 image6)
	(have_image Planet101 thermograph7)
	(have_image Planet101 spectrograph3)
	(have_image Star102 thermograph0)
	(have_image Planet103 image6)
	(have_image Star104 spectrograph3)
	(have_image Star104 spectrograph1)
	(have_image Phenomenon105 infrared5)
	(have_image Phenomenon105 infrared2)
	(have_image Phenomenon106 spectrograph1)
	(have_image Phenomenon106 infrared2)
	(have_image Phenomenon107 thermograph7)
	(have_image Phenomenon107 thermograph0)
	(have_image Phenomenon108 spectrograph4)
	(have_image Phenomenon109 spectrograph3)
	(have_image Phenomenon110 image6)
	(have_image Phenomenon110 infrared2)
	(have_image Phenomenon111 thermograph7)
	(have_image Phenomenon112 spectrograph1)
	(have_image Phenomenon112 infrared5)
	(have_image Planet113 infrared5)
	(have_image Planet113 spectrograph4)
	(have_image Star114 infrared5)
	(have_image Planet115 thermograph7)
	(have_image Planet115 spectrograph1)
	(have_image Star116 thermograph0)
	(have_image Star116 thermograph7)
	(have_image Planet117 thermograph7)
	(have_image Planet117 infrared2)
	(have_image Phenomenon118 spectrograph1)
	(have_image Planet120 spectrograph1)
	(have_image Planet120 spectrograph4)
	(have_image Planet121 thermograph0)
	(have_image Planet121 spectrograph3)
	(have_image Planet122 spectrograph1)
	(have_image Planet122 thermograph7)
	(have_image Planet123 thermograph7)
	(have_image Planet124 infrared2)
	(have_image Planet124 image6)
	(have_image Planet126 spectrograph3)
	(have_image Planet126 infrared5)
	(have_image Phenomenon129 spectrograph3)
	(have_image Phenomenon130 spectrograph4)
	(have_image Phenomenon130 spectrograph3)
	(have_image Star132 spectrograph1)
	(have_image Star132 infrared2)
	(have_image Star133 spectrograph1)
	(have_image Star133 spectrograph4)
	(have_image Planet134 thermograph7)
	(have_image Planet134 spectrograph1)
	(have_image Star135 thermograph0)
	(have_image Star135 spectrograph4)
	(have_image Star136 spectrograph4)
	(have_image Planet137 infrared5)
	(have_image Planet137 spectrograph4)
	(have_image Planet138 image6)
	(have_image Planet138 spectrograph3)
	(have_image Star139 spectrograph3)
	(have_image Star139 spectrograph4)
	(have_image Planet140 image6)
	(have_image Planet140 infrared2)
	(have_image Phenomenon141 thermograph7)
	(have_image Phenomenon141 spectrograph4)
	(have_image Planet142 thermograph7)
	(have_image Planet143 infrared2)
	(have_image Planet143 thermograph7)
	(have_image Planet144 spectrograph1)
	(have_image Planet145 spectrograph4)
	(have_image Star146 infrared2)
	(have_image Star147 infrared2)
	(have_image Star147 image6)
	(have_image Planet148 thermograph0)
	(have_image Planet148 image6)
	(have_image Planet149 thermograph7)
	(have_image Planet149 thermograph0)
	(have_image Star150 infrared5)
	(have_image Star150 spectrograph1)
	(have_image Star151 image6)
	(have_image Star151 spectrograph3)
	(have_image Phenomenon152 spectrograph1)
	(have_image Star153 spectrograph4)
	(have_image Star154 spectrograph3)
	(have_image Star155 spectrograph4)
	(have_image Star155 spectrograph3)
	(have_image Planet156 infrared2)
	(have_image Planet156 spectrograph3)
	(have_image Star157 spectrograph4)
	(have_image Star158 infrared5)
	(have_image Star158 thermograph7)
	(have_image Phenomenon159 spectrograph3)
	(have_image Phenomenon159 thermograph0)
	(have_image Star160 thermograph7)
	(have_image Phenomenon161 infrared5)
	(have_image Phenomenon161 spectrograph3)
	(have_image Planet162 thermograph7)
	(have_image Planet162 spectrograph4)
	(have_image Planet163 infrared2)
	(have_image Phenomenon164 image6)
	(have_image Phenomenon165 image6)
	(have_image Phenomenon165 infrared5)
	(have_image Star166 spectrograph1)
	(have_image Star166 image6)
	(have_image Planet167 spectrograph3)
	(have_image Star169 image6)
	(have_image Star169 infrared2)
	(have_image Planet170 spectrograph4)
	(have_image Planet171 image6)
	(have_image Star172 infrared5)
	(have_image Star173 infrared5)
	(have_image Star173 thermograph7)
	(have_image Star174 spectrograph3)
	(have_image Star174 thermograph0)
	(have_image Planet175 spectrograph4)
	(have_image Planet176 thermograph0)
	(have_image Planet176 spectrograph3)
	(have_image Phenomenon177 spectrograph4)
	(have_image Phenomenon178 infrared2)
	(have_image Phenomenon178 thermograph7)
	(have_image Phenomenon179 infrared2)
	(have_image Phenomenon179 thermograph0)
	(have_image Star180 image6)
	(have_image Star180 thermograph0)
	(have_image Planet181 infrared5)
	(have_image Planet181 image6)
	(have_image Phenomenon182 thermograph7)
	(have_image Star184 thermograph7)
	(have_image Star185 spectrograph4)
	(have_image Phenomenon186 thermograph7)
	(have_image Phenomenon186 thermograph0)
	(have_image Star187 spectrograph4)
	(have_image Planet188 infrared5)
	(have_image Phenomenon189 spectrograph4)
	(have_image Star190 infrared2)
	(have_image Phenomenon191 infrared5)
	(have_image Phenomenon191 spectrograph4)
	(have_image Planet192 infrared5)
	(have_image Planet193 spectrograph1)
	(have_image Star194 spectrograph4)
	(have_image Star194 spectrograph3)
	(have_image Star195 infrared5)
	(have_image Star196 infrared2)
	(have_image Phenomenon197 image6)
	(have_image Star198 spectrograph3)
	(have_image Star198 infrared2)
	(have_image Planet199 spectrograph4)
))

)
