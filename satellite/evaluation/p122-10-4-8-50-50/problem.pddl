(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	satellite1 - satellite
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	satellite2 - satellite
	instrument6 - instrument
	instrument7 - instrument
	satellite3 - satellite
	instrument8 - instrument
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	satellite4 - satellite
	instrument12 - instrument
	instrument13 - instrument
	satellite5 - satellite
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	satellite6 - satellite
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	satellite7 - satellite
	instrument21 - instrument
	satellite8 - satellite
	instrument22 - instrument
	instrument23 - instrument
	satellite9 - satellite
	instrument24 - instrument
	thermograph1 - mode
	spectrograph6 - mode
	thermograph7 - mode
	spectrograph2 - mode
	spectrograph5 - mode
	image4 - mode
	thermograph0 - mode
	spectrograph3 - mode
	Star11 - direction
	GroundStation20 - direction
	Star48 - direction
	Star19 - direction
	Star15 - direction
	GroundStation10 - direction
	GroundStation14 - direction
	GroundStation46 - direction
	Star25 - direction
	GroundStation5 - direction
	GroundStation12 - direction
	GroundStation3 - direction
	Star17 - direction
	Star33 - direction
	GroundStation36 - direction
	Star49 - direction
	Star1 - direction
	GroundStation28 - direction
	Star24 - direction
	Star39 - direction
	Star2 - direction
	GroundStation37 - direction
	GroundStation22 - direction
	Star13 - direction
	Star9 - direction
	GroundStation35 - direction
	GroundStation47 - direction
	Star6 - direction
	GroundStation30 - direction
	Star31 - direction
	Star16 - direction
	GroundStation45 - direction
	Star38 - direction
	Star29 - direction
	GroundStation43 - direction
	GroundStation23 - direction
	Star21 - direction
	Star42 - direction
	GroundStation27 - direction
	GroundStation41 - direction
	GroundStation18 - direction
	Star40 - direction
	Star34 - direction
	GroundStation7 - direction
	GroundStation32 - direction
	Star26 - direction
	GroundStation8 - direction
	GroundStation0 - direction
	Star4 - direction
	Star44 - direction
	Phenomenon50 - direction
	Phenomenon51 - direction
	Planet52 - direction
	Planet53 - direction
	Star54 - direction
	Star55 - direction
	Phenomenon56 - direction
	Planet57 - direction
	Planet58 - direction
	Star59 - direction
	Planet60 - direction
	Phenomenon61 - direction
	Planet62 - direction
	Star63 - direction
	Star64 - direction
	Phenomenon65 - direction
	Star66 - direction
	Phenomenon67 - direction
	Star68 - direction
	Phenomenon69 - direction
	Planet70 - direction
	Star71 - direction
	Star72 - direction
	Star73 - direction
	Phenomenon74 - direction
	Star75 - direction
	Star76 - direction
	Phenomenon77 - direction
	Planet78 - direction
	Planet79 - direction
	Star80 - direction
	Phenomenon81 - direction
	Phenomenon82 - direction
	Phenomenon83 - direction
	Star84 - direction
	Phenomenon85 - direction
	Planet86 - direction
	Planet87 - direction
	Star88 - direction
	Planet89 - direction
	Planet90 - direction
	Planet91 - direction
	Star92 - direction
	Star93 - direction
	Star94 - direction
	Phenomenon95 - direction
	Star96 - direction
	Phenomenon97 - direction
	Star98 - direction
	Star99 - direction
)
(:init
	(supports instrument0 image4)
	(supports instrument0 thermograph0)
	(supports instrument0 thermograph7)
	(calibration_target instrument0 GroundStation43)
	(calibration_target instrument0 GroundStation41)
	(calibration_target instrument0 GroundStation14)
	(calibration_target instrument0 GroundStation47)
	(calibration_target instrument0 Star26)
	(calibration_target instrument0 GroundStation28)
	(calibration_target instrument0 Star9)
	(supports instrument1 thermograph0)
	(supports instrument1 image4)
	(supports instrument1 spectrograph3)
	(calibration_target instrument1 GroundStation3)
	(calibration_target instrument1 GroundStation32)
	(calibration_target instrument1 GroundStation8)
	(calibration_target instrument1 GroundStation35)
	(calibration_target instrument1 Star40)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Star11)
	(supports instrument2 spectrograph5)
	(calibration_target instrument2 GroundStation20)
	(calibration_target instrument2 GroundStation47)
	(calibration_target instrument2 GroundStation0)
	(calibration_target instrument2 Star6)
	(calibration_target instrument2 Star39)
	(calibration_target instrument2 GroundStation45)
	(calibration_target instrument2 Star4)
	(supports instrument3 image4)
	(supports instrument3 thermograph0)
	(calibration_target instrument3 GroundStation5)
	(calibration_target instrument3 GroundStation37)
	(calibration_target instrument3 GroundStation23)
	(calibration_target instrument3 Star42)
	(calibration_target instrument3 GroundStation28)
	(supports instrument4 spectrograph6)
	(calibration_target instrument4 GroundStation36)
	(calibration_target instrument4 Star1)
	(supports instrument5 image4)
	(supports instrument5 spectrograph5)
	(calibration_target instrument5 Star48)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(power_avail satellite1)
	(pointing satellite1 GroundStation7)
	(supports instrument6 thermograph0)
	(supports instrument6 spectrograph5)
	(supports instrument6 spectrograph2)
	(calibration_target instrument6 Star49)
	(calibration_target instrument6 Star31)
	(calibration_target instrument6 GroundStation36)
	(supports instrument7 spectrograph2)
	(supports instrument7 spectrograph5)
	(supports instrument7 thermograph1)
	(calibration_target instrument7 Star48)
	(calibration_target instrument7 GroundStation3)
	(calibration_target instrument7 Star24)
	(calibration_target instrument7 Star6)
	(calibration_target instrument7 GroundStation22)
	(calibration_target instrument7 Star29)
	(calibration_target instrument7 GroundStation18)
	(calibration_target instrument7 Star25)
	(calibration_target instrument7 Star11)
	(calibration_target instrument7 Star26)
	(calibration_target instrument7 Star17)
	(on_board instrument6 satellite2)
	(on_board instrument7 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Phenomenon61)
	(supports instrument8 spectrograph3)
	(supports instrument8 spectrograph5)
	(supports instrument8 image4)
	(calibration_target instrument8 GroundStation47)
	(calibration_target instrument8 GroundStation23)
	(calibration_target instrument8 Star49)
	(calibration_target instrument8 Star26)
	(calibration_target instrument8 GroundStation22)
	(calibration_target instrument8 Star11)
	(calibration_target instrument8 GroundStation30)
	(supports instrument9 thermograph7)
	(supports instrument9 image4)
	(calibration_target instrument9 GroundStation45)
	(calibration_target instrument9 Star13)
	(calibration_target instrument9 Star42)
	(calibration_target instrument9 GroundStation14)
	(calibration_target instrument9 Star33)
	(calibration_target instrument9 Star21)
	(calibration_target instrument9 GroundStation3)
	(calibration_target instrument9 Star15)
	(calibration_target instrument9 Star11)
	(calibration_target instrument9 GroundStation23)
	(calibration_target instrument9 Star26)
	(supports instrument10 spectrograph3)
	(supports instrument10 thermograph7)
	(supports instrument10 image4)
	(calibration_target instrument10 GroundStation41)
	(calibration_target instrument10 GroundStation10)
	(calibration_target instrument10 Star17)
	(calibration_target instrument10 GroundStation14)
	(calibration_target instrument10 GroundStation36)
	(calibration_target instrument10 Star24)
	(calibration_target instrument10 GroundStation5)
	(calibration_target instrument10 GroundStation3)
	(calibration_target instrument10 Star15)
	(calibration_target instrument10 Star6)
	(calibration_target instrument10 Star48)
	(calibration_target instrument10 Star13)
	(calibration_target instrument10 GroundStation20)
	(calibration_target instrument10 Star25)
	(calibration_target instrument10 Star21)
	(supports instrument11 image4)
	(supports instrument11 spectrograph6)
	(supports instrument11 spectrograph2)
	(calibration_target instrument11 GroundStation47)
	(calibration_target instrument11 GroundStation27)
	(calibration_target instrument11 Star44)
	(calibration_target instrument11 Star1)
	(calibration_target instrument11 GroundStation10)
	(calibration_target instrument11 Star15)
	(calibration_target instrument11 Star19)
	(calibration_target instrument11 GroundStation14)
	(calibration_target instrument11 Star39)
	(calibration_target instrument11 Star25)
	(calibration_target instrument11 Star6)
	(on_board instrument8 satellite3)
	(on_board instrument9 satellite3)
	(on_board instrument10 satellite3)
	(on_board instrument11 satellite3)
	(power_avail satellite3)
	(pointing satellite3 GroundStation22)
	(supports instrument12 thermograph7)
	(supports instrument12 image4)
	(supports instrument12 spectrograph2)
	(calibration_target instrument12 GroundStation0)
	(calibration_target instrument12 Star17)
	(calibration_target instrument12 GroundStation30)
	(calibration_target instrument12 Star29)
	(calibration_target instrument12 GroundStation5)
	(calibration_target instrument12 GroundStation41)
	(calibration_target instrument12 GroundStation46)
	(calibration_target instrument12 Star33)
	(calibration_target instrument12 GroundStation14)
	(calibration_target instrument12 Star1)
	(supports instrument13 spectrograph3)
	(calibration_target instrument13 Star21)
	(calibration_target instrument13 GroundStation8)
	(calibration_target instrument13 GroundStation41)
	(calibration_target instrument13 GroundStation43)
	(calibration_target instrument13 Star25)
	(calibration_target instrument13 Star44)
	(calibration_target instrument13 GroundStation30)
	(calibration_target instrument13 GroundStation32)
	(calibration_target instrument13 GroundStation7)
	(calibration_target instrument13 Star39)
	(on_board instrument12 satellite4)
	(on_board instrument13 satellite4)
	(power_avail satellite4)
	(pointing satellite4 GroundStation37)
	(supports instrument14 spectrograph2)
	(supports instrument14 thermograph0)
	(supports instrument14 spectrograph5)
	(calibration_target instrument14 Star49)
	(supports instrument15 spectrograph2)
	(calibration_target instrument15 Star38)
	(calibration_target instrument15 GroundStation35)
	(calibration_target instrument15 Star2)
	(calibration_target instrument15 Star39)
	(calibration_target instrument15 Star17)
	(calibration_target instrument15 GroundStation3)
	(calibration_target instrument15 Star4)
	(calibration_target instrument15 Star40)
	(calibration_target instrument15 Star16)
	(calibration_target instrument15 GroundStation12)
	(calibration_target instrument15 Star42)
	(calibration_target instrument15 GroundStation27)
	(calibration_target instrument15 GroundStation5)
	(calibration_target instrument15 Star21)
	(calibration_target instrument15 Star44)
	(calibration_target instrument15 Star31)
	(supports instrument16 thermograph0)
	(calibration_target instrument16 Star16)
	(on_board instrument14 satellite5)
	(on_board instrument15 satellite5)
	(on_board instrument16 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Star13)
	(supports instrument17 spectrograph5)
	(calibration_target instrument17 Star1)
	(calibration_target instrument17 Star49)
	(calibration_target instrument17 GroundStation32)
	(calibration_target instrument17 Star2)
	(calibration_target instrument17 GroundStation36)
	(calibration_target instrument17 Star4)
	(calibration_target instrument17 Star39)
	(calibration_target instrument17 Star9)
	(calibration_target instrument17 Star33)
	(calibration_target instrument17 GroundStation35)
	(calibration_target instrument17 GroundStation28)
	(calibration_target instrument17 Star24)
	(calibration_target instrument17 Star26)
	(calibration_target instrument17 Star16)
	(calibration_target instrument17 GroundStation18)
	(calibration_target instrument17 GroundStation47)
	(supports instrument18 image4)
	(supports instrument18 spectrograph3)
	(calibration_target instrument18 GroundStation45)
	(calibration_target instrument18 GroundStation43)
	(calibration_target instrument18 Star26)
	(calibration_target instrument18 Star42)
	(calibration_target instrument18 GroundStation0)
	(calibration_target instrument18 GroundStation35)
	(calibration_target instrument18 Star39)
	(calibration_target instrument18 Star31)
	(calibration_target instrument18 Star24)
	(calibration_target instrument18 Star2)
	(calibration_target instrument18 Star9)
	(calibration_target instrument18 GroundStation28)
	(supports instrument19 spectrograph6)
	(supports instrument19 thermograph7)
	(supports instrument19 thermograph0)
	(calibration_target instrument19 Star16)
	(calibration_target instrument19 GroundStation22)
	(calibration_target instrument19 Star38)
	(calibration_target instrument19 GroundStation37)
	(calibration_target instrument19 Star2)
	(calibration_target instrument19 Star6)
	(calibration_target instrument19 GroundStation8)
	(calibration_target instrument19 GroundStation18)
	(supports instrument20 spectrograph3)
	(supports instrument20 spectrograph6)
	(supports instrument20 spectrograph5)
	(calibration_target instrument20 Star42)
	(calibration_target instrument20 Star6)
	(calibration_target instrument20 GroundStation45)
	(calibration_target instrument20 GroundStation7)
	(calibration_target instrument20 GroundStation47)
	(calibration_target instrument20 GroundStation35)
	(calibration_target instrument20 Star40)
	(calibration_target instrument20 Star9)
	(calibration_target instrument20 Star13)
	(calibration_target instrument20 Star38)
	(on_board instrument17 satellite6)
	(on_board instrument18 satellite6)
	(on_board instrument19 satellite6)
	(on_board instrument20 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Star39)
	(supports instrument21 image4)
	(supports instrument21 thermograph0)
	(supports instrument21 thermograph7)
	(calibration_target instrument21 Star21)
	(calibration_target instrument21 GroundStation30)
	(on_board instrument21 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Planet79)
	(supports instrument22 spectrograph2)
	(calibration_target instrument22 GroundStation45)
	(calibration_target instrument22 Star21)
	(calibration_target instrument22 Star16)
	(calibration_target instrument22 Star31)
	(supports instrument23 spectrograph3)
	(supports instrument23 spectrograph5)
	(supports instrument23 image4)
	(calibration_target instrument23 GroundStation43)
	(calibration_target instrument23 Star29)
	(calibration_target instrument23 GroundStation8)
	(calibration_target instrument23 Star38)
	(on_board instrument22 satellite8)
	(on_board instrument23 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Star9)
	(supports instrument24 spectrograph3)
	(supports instrument24 thermograph0)
	(supports instrument24 image4)
	(calibration_target instrument24 Star44)
	(calibration_target instrument24 Star4)
	(calibration_target instrument24 GroundStation0)
	(calibration_target instrument24 GroundStation8)
	(calibration_target instrument24 Star26)
	(calibration_target instrument24 GroundStation32)
	(calibration_target instrument24 GroundStation7)
	(calibration_target instrument24 Star34)
	(calibration_target instrument24 Star40)
	(calibration_target instrument24 GroundStation18)
	(calibration_target instrument24 GroundStation41)
	(calibration_target instrument24 GroundStation27)
	(calibration_target instrument24 Star42)
	(calibration_target instrument24 Star21)
	(calibration_target instrument24 GroundStation23)
	(on_board instrument24 satellite9)
	(power_avail satellite9)
	(pointing satellite9 Star94)
)
(:goal (and
	(pointing satellite0 Star76)
	(pointing satellite4 Star4)
	(pointing satellite6 GroundStation45)
	(pointing satellite7 Star19)
	(have_image Phenomenon50 spectrograph2)
	(have_image Phenomenon50 image4)
	(have_image Phenomenon51 thermograph0)
	(have_image Planet52 image4)
	(have_image Planet53 spectrograph2)
	(have_image Planet53 thermograph0)
	(have_image Star54 spectrograph6)
	(have_image Star55 image4)
	(have_image Star55 thermograph7)
	(have_image Phenomenon56 spectrograph3)
	(have_image Phenomenon56 thermograph7)
	(have_image Planet57 thermograph7)
	(have_image Planet57 image4)
	(have_image Star59 spectrograph2)
	(have_image Phenomenon61 image4)
	(have_image Planet62 spectrograph3)
	(have_image Planet62 spectrograph2)
	(have_image Star63 thermograph7)
	(have_image Star64 spectrograph3)
	(have_image Star64 spectrograph5)
	(have_image Phenomenon65 thermograph1)
	(have_image Star66 thermograph1)
	(have_image Phenomenon67 thermograph7)
	(have_image Phenomenon67 image4)
	(have_image Star68 thermograph0)
	(have_image Star68 image4)
	(have_image Planet70 spectrograph2)
	(have_image Planet70 spectrograph3)
	(have_image Star71 spectrograph6)
	(have_image Star73 spectrograph2)
	(have_image Star73 thermograph1)
	(have_image Phenomenon74 spectrograph6)
	(have_image Star75 spectrograph5)
	(have_image Star76 spectrograph3)
	(have_image Phenomenon77 spectrograph6)
	(have_image Phenomenon77 spectrograph2)
	(have_image Planet78 image4)
	(have_image Planet78 spectrograph2)
	(have_image Planet79 thermograph7)
	(have_image Star80 thermograph1)
	(have_image Phenomenon81 spectrograph5)
	(have_image Phenomenon82 thermograph1)
	(have_image Phenomenon82 spectrograph2)
	(have_image Phenomenon83 image4)
	(have_image Star84 thermograph1)
	(have_image Phenomenon85 image4)
	(have_image Phenomenon85 spectrograph2)
	(have_image Planet87 thermograph0)
	(have_image Planet87 spectrograph3)
	(have_image Star88 thermograph1)
	(have_image Planet89 thermograph0)
	(have_image Planet89 spectrograph5)
	(have_image Planet91 thermograph7)
	(have_image Planet91 thermograph1)
	(have_image Star92 thermograph0)
	(have_image Star92 spectrograph2)
	(have_image Star93 spectrograph2)
	(have_image Star94 spectrograph3)
	(have_image Star94 spectrograph5)
	(have_image Phenomenon95 spectrograph6)
	(have_image Phenomenon95 thermograph1)
	(have_image Phenomenon97 spectrograph2)
	(have_image Star98 spectrograph6)
))

)
