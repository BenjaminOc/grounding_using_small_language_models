(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	satellite1 - satellite
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite2 - satellite
	instrument10 - instrument
	instrument11 - instrument
	satellite3 - satellite
	instrument12 - instrument
	satellite4 - satellite
	instrument13 - instrument
	instrument14 - instrument
	satellite5 - satellite
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	instrument18 - instrument
	satellite6 - satellite
	instrument19 - instrument
	instrument20 - instrument
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	satellite7 - satellite
	instrument25 - instrument
	instrument26 - instrument
	satellite8 - satellite
	instrument27 - instrument
	instrument28 - instrument
	instrument29 - instrument
	instrument30 - instrument
	instrument31 - instrument
	instrument32 - instrument
	satellite9 - satellite
	instrument33 - instrument
	instrument34 - instrument
	instrument35 - instrument
	instrument36 - instrument
	instrument37 - instrument
	image2 - mode
	thermograph6 - mode
	image0 - mode
	spectrograph1 - mode
	spectrograph3 - mode
	image4 - mode
	image5 - mode
	Star13 - direction
	GroundStation3 - direction
	GroundStation25 - direction
	Star11 - direction
	GroundStation8 - direction
	GroundStation5 - direction
	GroundStation9 - direction
	Star14 - direction
	Star31 - direction
	GroundStation42 - direction
	GroundStation45 - direction
	GroundStation33 - direction
	Star28 - direction
	GroundStation22 - direction
	GroundStation18 - direction
	Star43 - direction
	Star2 - direction
	Star7 - direction
	GroundStation6 - direction
	Star35 - direction
	GroundStation37 - direction
	Star21 - direction
	GroundStation32 - direction
	GroundStation12 - direction
	Star24 - direction
	Star17 - direction
	GroundStation47 - direction
	GroundStation46 - direction
	GroundStation15 - direction
	GroundStation0 - direction
	Star41 - direction
	Star23 - direction
	Star36 - direction
	GroundStation19 - direction
	GroundStation4 - direction
	GroundStation34 - direction
	GroundStation27 - direction
	GroundStation40 - direction
	GroundStation10 - direction
	Star38 - direction
	Star1 - direction
	Star48 - direction
	Star20 - direction
	GroundStation26 - direction
	Star16 - direction
	GroundStation39 - direction
	Star44 - direction
	Star29 - direction
	GroundStation30 - direction
	GroundStation49 - direction
	Phenomenon50 - direction
	Phenomenon51 - direction
	Planet52 - direction
	Planet53 - direction
	Phenomenon54 - direction
	Phenomenon55 - direction
	Phenomenon56 - direction
	Star57 - direction
	Phenomenon58 - direction
	Star59 - direction
	Phenomenon60 - direction
	Planet61 - direction
	Phenomenon62 - direction
	Planet63 - direction
	Planet64 - direction
	Phenomenon65 - direction
	Planet66 - direction
	Star67 - direction
	Planet68 - direction
	Star69 - direction
	Phenomenon70 - direction
	Planet71 - direction
	Phenomenon72 - direction
	Star73 - direction
	Star74 - direction
	Planet75 - direction
	Star76 - direction
	Planet77 - direction
	Phenomenon78 - direction
	Star79 - direction
	Planet80 - direction
	Star81 - direction
	Planet82 - direction
	Planet83 - direction
	Phenomenon84 - direction
	Phenomenon85 - direction
	Star86 - direction
	Planet87 - direction
	Phenomenon88 - direction
	Planet89 - direction
	Star90 - direction
	Phenomenon91 - direction
	Phenomenon92 - direction
	Phenomenon93 - direction
	Planet94 - direction
	Phenomenon95 - direction
	Phenomenon96 - direction
	Phenomenon97 - direction
	Phenomenon98 - direction
	Star99 - direction
)
(:init
	(supports instrument0 image0)
	(supports instrument0 spectrograph1)
	(calibration_target instrument0 GroundStation32)
	(calibration_target instrument0 Star48)
	(calibration_target instrument0 GroundStation26)
	(calibration_target instrument0 Star44)
	(supports instrument1 thermograph6)
	(supports instrument1 image2)
	(calibration_target instrument1 Star44)
	(calibration_target instrument1 Star28)
	(calibration_target instrument1 Star38)
	(calibration_target instrument1 Star35)
	(calibration_target instrument1 Star2)
	(calibration_target instrument1 GroundStation27)
	(supports instrument2 image0)
	(calibration_target instrument2 Star43)
	(calibration_target instrument2 GroundStation6)
	(calibration_target instrument2 GroundStation37)
	(calibration_target instrument2 Star38)
	(supports instrument3 image4)
	(supports instrument3 image0)
	(supports instrument3 image2)
	(calibration_target instrument3 Star31)
	(calibration_target instrument3 Star16)
	(calibration_target instrument3 GroundStation4)
	(calibration_target instrument3 Star24)
	(calibration_target instrument3 Star28)
	(calibration_target instrument3 GroundStation27)
	(calibration_target instrument3 GroundStation9)
	(calibration_target instrument3 Star38)
	(calibration_target instrument3 Star21)
	(calibration_target instrument3 GroundStation15)
	(calibration_target instrument3 Star48)
	(calibration_target instrument3 Star14)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Star17)
	(supports instrument4 spectrograph1)
	(calibration_target instrument4 Star2)
	(supports instrument5 image4)
	(supports instrument5 image5)
	(supports instrument5 thermograph6)
	(calibration_target instrument5 Star16)
	(calibration_target instrument5 Star41)
	(supports instrument6 image5)
	(supports instrument6 image4)
	(calibration_target instrument6 GroundStation40)
	(calibration_target instrument6 Star17)
	(calibration_target instrument6 GroundStation8)
	(calibration_target instrument6 GroundStation10)
	(calibration_target instrument6 GroundStation34)
	(calibration_target instrument6 GroundStation39)
	(calibration_target instrument6 Star20)
	(supports instrument7 image0)
	(calibration_target instrument7 Star36)
	(calibration_target instrument7 GroundStation22)
	(calibration_target instrument7 Star14)
	(calibration_target instrument7 Star44)
	(calibration_target instrument7 GroundStation49)
	(calibration_target instrument7 Star38)
	(calibration_target instrument7 GroundStation26)
	(calibration_target instrument7 GroundStation10)
	(calibration_target instrument7 Star35)
	(calibration_target instrument7 GroundStation0)
	(calibration_target instrument7 Star29)
	(calibration_target instrument7 GroundStation25)
	(calibration_target instrument7 Star20)
	(supports instrument8 image4)
	(supports instrument8 image5)
	(supports instrument8 image2)
	(calibration_target instrument8 GroundStation18)
	(calibration_target instrument8 GroundStation4)
	(calibration_target instrument8 Star1)
	(calibration_target instrument8 GroundStation34)
	(calibration_target instrument8 GroundStation22)
	(calibration_target instrument8 GroundStation39)
	(calibration_target instrument8 Star11)
	(supports instrument9 spectrograph3)
	(calibration_target instrument9 GroundStation5)
	(calibration_target instrument9 Star31)
	(calibration_target instrument9 GroundStation18)
	(calibration_target instrument9 GroundStation30)
	(calibration_target instrument9 Star23)
	(calibration_target instrument9 GroundStation34)
	(calibration_target instrument9 GroundStation0)
	(calibration_target instrument9 Star29)
	(calibration_target instrument9 GroundStation40)
	(calibration_target instrument9 Star17)
	(calibration_target instrument9 Star43)
	(calibration_target instrument9 GroundStation27)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(on_board instrument9 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star21)
	(supports instrument10 spectrograph3)
	(calibration_target instrument10 GroundStation15)
	(calibration_target instrument10 Star7)
	(calibration_target instrument10 GroundStation42)
	(supports instrument11 spectrograph1)
	(supports instrument11 image0)
	(calibration_target instrument11 GroundStation0)
	(calibration_target instrument11 Star29)
	(calibration_target instrument11 GroundStation19)
	(calibration_target instrument11 Star36)
	(calibration_target instrument11 Star13)
	(calibration_target instrument11 Star38)
	(calibration_target instrument11 GroundStation3)
	(calibration_target instrument11 GroundStation8)
	(calibration_target instrument11 Star31)
	(calibration_target instrument11 Star14)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Phenomenon97)
	(supports instrument12 image5)
	(supports instrument12 image4)
	(supports instrument12 image2)
	(calibration_target instrument12 Star48)
	(calibration_target instrument12 GroundStation25)
	(on_board instrument12 satellite3)
	(power_avail satellite3)
	(pointing satellite3 GroundStation47)
	(supports instrument13 image2)
	(supports instrument13 thermograph6)
	(supports instrument13 spectrograph1)
	(calibration_target instrument13 Star28)
	(calibration_target instrument13 Star16)
	(calibration_target instrument13 GroundStation8)
	(calibration_target instrument13 Star1)
	(calibration_target instrument13 GroundStation26)
	(supports instrument14 image2)
	(calibration_target instrument14 GroundStation22)
	(calibration_target instrument14 Star28)
	(calibration_target instrument14 Star23)
	(calibration_target instrument14 GroundStation3)
	(calibration_target instrument14 GroundStation15)
	(calibration_target instrument14 Star36)
	(calibration_target instrument14 GroundStation30)
	(on_board instrument13 satellite4)
	(on_board instrument14 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star86)
	(supports instrument15 spectrograph1)
	(supports instrument15 image2)
	(calibration_target instrument15 GroundStation47)
	(calibration_target instrument15 Star17)
	(calibration_target instrument15 Star20)
	(calibration_target instrument15 Star2)
	(supports instrument16 image4)
	(calibration_target instrument16 GroundStation47)
	(supports instrument17 image5)
	(supports instrument17 image0)
	(calibration_target instrument17 Star24)
	(calibration_target instrument17 Star23)
	(calibration_target instrument17 GroundStation25)
	(supports instrument18 image2)
	(supports instrument18 image5)
	(calibration_target instrument18 Star36)
	(calibration_target instrument18 GroundStation19)
	(calibration_target instrument18 GroundStation8)
	(calibration_target instrument18 GroundStation12)
	(calibration_target instrument18 GroundStation26)
	(calibration_target instrument18 Star41)
	(calibration_target instrument18 Star11)
	(calibration_target instrument18 GroundStation47)
	(calibration_target instrument18 Star7)
	(calibration_target instrument18 Star17)
	(calibration_target instrument18 Star31)
	(calibration_target instrument18 Star28)
	(calibration_target instrument18 GroundStation37)
	(calibration_target instrument18 GroundStation25)
	(on_board instrument15 satellite5)
	(on_board instrument16 satellite5)
	(on_board instrument17 satellite5)
	(on_board instrument18 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Star13)
	(supports instrument19 image4)
	(calibration_target instrument19 GroundStation8)
	(calibration_target instrument19 GroundStation12)
	(supports instrument20 thermograph6)
	(supports instrument20 image0)
	(supports instrument20 image2)
	(calibration_target instrument20 GroundStation34)
	(calibration_target instrument20 GroundStation12)
	(calibration_target instrument20 Star28)
	(calibration_target instrument20 Star16)
	(calibration_target instrument20 GroundStation42)
	(supports instrument21 image5)
	(calibration_target instrument21 GroundStation6)
	(calibration_target instrument21 Star28)
	(calibration_target instrument21 GroundStation49)
	(calibration_target instrument21 GroundStation0)
	(calibration_target instrument21 GroundStation39)
	(calibration_target instrument21 GroundStation45)
	(supports instrument22 image4)
	(supports instrument22 image2)
	(supports instrument22 spectrograph1)
	(calibration_target instrument22 Star7)
	(calibration_target instrument22 Star41)
	(supports instrument23 image5)
	(calibration_target instrument23 GroundStation49)
	(calibration_target instrument23 Star7)
	(calibration_target instrument23 Star31)
	(calibration_target instrument23 Star43)
	(calibration_target instrument23 Star38)
	(calibration_target instrument23 Star21)
	(calibration_target instrument23 GroundStation25)
	(calibration_target instrument23 GroundStation30)
	(calibration_target instrument23 GroundStation46)
	(calibration_target instrument23 GroundStation42)
	(calibration_target instrument23 GroundStation45)
	(calibration_target instrument23 GroundStation18)
	(calibration_target instrument23 GroundStation8)
	(calibration_target instrument23 Star29)
	(calibration_target instrument23 GroundStation0)
	(supports instrument24 image5)
	(supports instrument24 spectrograph1)
	(calibration_target instrument24 GroundStation27)
	(calibration_target instrument24 GroundStation46)
	(calibration_target instrument24 Star14)
	(on_board instrument19 satellite6)
	(on_board instrument20 satellite6)
	(on_board instrument21 satellite6)
	(on_board instrument22 satellite6)
	(on_board instrument23 satellite6)
	(on_board instrument24 satellite6)
	(power_avail satellite6)
	(pointing satellite6 GroundStation37)
	(supports instrument25 spectrograph1)
	(calibration_target instrument25 Star38)
	(calibration_target instrument25 GroundStation45)
	(calibration_target instrument25 Star21)
	(calibration_target instrument25 Star7)
	(calibration_target instrument25 GroundStation8)
	(calibration_target instrument25 GroundStation33)
	(supports instrument26 image5)
	(supports instrument26 image0)
	(calibration_target instrument26 Star29)
	(calibration_target instrument26 Star11)
	(calibration_target instrument26 GroundStation26)
	(calibration_target instrument26 Star7)
	(calibration_target instrument26 Star41)
	(calibration_target instrument26 GroundStation8)
	(calibration_target instrument26 GroundStation32)
	(calibration_target instrument26 GroundStation4)
	(calibration_target instrument26 Star48)
	(calibration_target instrument26 GroundStation47)
	(calibration_target instrument26 Star36)
	(calibration_target instrument26 Star24)
	(on_board instrument25 satellite7)
	(on_board instrument26 satellite7)
	(power_avail satellite7)
	(pointing satellite7 GroundStation10)
	(supports instrument27 image5)
	(supports instrument27 image2)
	(calibration_target instrument27 GroundStation49)
	(calibration_target instrument27 Star16)
	(calibration_target instrument27 GroundStation0)
	(calibration_target instrument27 GroundStation45)
	(calibration_target instrument27 GroundStation42)
	(calibration_target instrument27 Star41)
	(calibration_target instrument27 Star1)
	(calibration_target instrument27 GroundStation8)
	(supports instrument28 image4)
	(supports instrument28 spectrograph3)
	(supports instrument28 image5)
	(calibration_target instrument28 GroundStation5)
	(supports instrument29 image4)
	(calibration_target instrument29 GroundStation19)
	(calibration_target instrument29 Star14)
	(calibration_target instrument29 GroundStation32)
	(calibration_target instrument29 Star7)
	(calibration_target instrument29 Star16)
	(calibration_target instrument29 GroundStation12)
	(calibration_target instrument29 Star44)
	(calibration_target instrument29 GroundStation18)
	(calibration_target instrument29 Star35)
	(calibration_target instrument29 Star38)
	(calibration_target instrument29 GroundStation34)
	(calibration_target instrument29 Star20)
	(calibration_target instrument29 GroundStation46)
	(supports instrument30 thermograph6)
	(supports instrument30 spectrograph3)
	(supports instrument30 image4)
	(calibration_target instrument30 GroundStation40)
	(calibration_target instrument30 Star31)
	(calibration_target instrument30 Star1)
	(calibration_target instrument30 GroundStation34)
	(calibration_target instrument30 GroundStation30)
	(calibration_target instrument30 Star7)
	(calibration_target instrument30 GroundStation18)
	(calibration_target instrument30 GroundStation0)
	(calibration_target instrument30 Star14)
	(calibration_target instrument30 Star21)
	(calibration_target instrument30 GroundStation9)
	(calibration_target instrument30 Star23)
	(calibration_target instrument30 GroundStation12)
	(calibration_target instrument30 Star20)
	(supports instrument31 image2)
	(calibration_target instrument31 Star36)
	(calibration_target instrument31 Star16)
	(supports instrument32 spectrograph3)
	(supports instrument32 image2)
	(supports instrument32 image4)
	(calibration_target instrument32 GroundStation26)
	(calibration_target instrument32 GroundStation19)
	(calibration_target instrument32 Star23)
	(calibration_target instrument32 GroundStation33)
	(calibration_target instrument32 GroundStation42)
	(calibration_target instrument32 Star38)
	(calibration_target instrument32 GroundStation27)
	(calibration_target instrument32 GroundStation15)
	(calibration_target instrument32 Star21)
	(on_board instrument27 satellite8)
	(on_board instrument28 satellite8)
	(on_board instrument29 satellite8)
	(on_board instrument30 satellite8)
	(on_board instrument31 satellite8)
	(on_board instrument32 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Phenomenon50)
	(supports instrument33 thermograph6)
	(calibration_target instrument33 GroundStation10)
	(calibration_target instrument33 Star28)
	(calibration_target instrument33 GroundStation33)
	(calibration_target instrument33 GroundStation45)
	(calibration_target instrument33 Star36)
	(calibration_target instrument33 GroundStation18)
	(calibration_target instrument33 GroundStation30)
	(supports instrument34 image0)
	(supports instrument34 thermograph6)
	(calibration_target instrument34 Star17)
	(calibration_target instrument34 GroundStation4)
	(calibration_target instrument34 Star43)
	(calibration_target instrument34 GroundStation6)
	(calibration_target instrument34 GroundStation18)
	(calibration_target instrument34 GroundStation22)
	(calibration_target instrument34 Star41)
	(calibration_target instrument34 GroundStation26)
	(calibration_target instrument34 Star1)
	(calibration_target instrument34 GroundStation37)
	(supports instrument35 image5)
	(supports instrument35 spectrograph1)
	(calibration_target instrument35 GroundStation49)
	(calibration_target instrument35 Star21)
	(calibration_target instrument35 Star1)
	(calibration_target instrument35 Star36)
	(calibration_target instrument35 GroundStation37)
	(calibration_target instrument35 GroundStation30)
	(calibration_target instrument35 Star35)
	(calibration_target instrument35 GroundStation6)
	(calibration_target instrument35 Star17)
	(calibration_target instrument35 Star7)
	(calibration_target instrument35 Star2)
	(calibration_target instrument35 Star41)
	(supports instrument36 spectrograph3)
	(supports instrument36 image5)
	(calibration_target instrument36 GroundStation19)
	(calibration_target instrument36 Star36)
	(calibration_target instrument36 Star23)
	(calibration_target instrument36 Star41)
	(calibration_target instrument36 GroundStation40)
	(calibration_target instrument36 GroundStation0)
	(calibration_target instrument36 GroundStation15)
	(calibration_target instrument36 GroundStation46)
	(calibration_target instrument36 GroundStation47)
	(calibration_target instrument36 Star17)
	(calibration_target instrument36 Star29)
	(calibration_target instrument36 Star24)
	(calibration_target instrument36 GroundStation12)
	(calibration_target instrument36 GroundStation32)
	(supports instrument37 image5)
	(supports instrument37 image4)
	(calibration_target instrument37 GroundStation49)
	(calibration_target instrument37 GroundStation30)
	(calibration_target instrument37 Star29)
	(calibration_target instrument37 Star44)
	(calibration_target instrument37 GroundStation39)
	(calibration_target instrument37 Star16)
	(calibration_target instrument37 GroundStation26)
	(calibration_target instrument37 Star20)
	(calibration_target instrument37 Star48)
	(calibration_target instrument37 Star1)
	(calibration_target instrument37 Star38)
	(calibration_target instrument37 GroundStation10)
	(calibration_target instrument37 GroundStation40)
	(calibration_target instrument37 GroundStation27)
	(calibration_target instrument37 GroundStation34)
	(calibration_target instrument37 GroundStation4)
	(on_board instrument33 satellite9)
	(on_board instrument34 satellite9)
	(on_board instrument35 satellite9)
	(on_board instrument36 satellite9)
	(on_board instrument37 satellite9)
	(power_avail satellite9)
	(pointing satellite9 GroundStation0)
)
(:goal (and
	(pointing satellite2 GroundStation8)
	(pointing satellite3 Star69)
	(pointing satellite4 Planet87)
	(pointing satellite6 Phenomenon91)
	(have_image Phenomenon50 image2)
	(have_image Phenomenon50 spectrograph1)
	(have_image Phenomenon51 image5)
	(have_image Planet52 image4)
	(have_image Planet52 thermograph6)
	(have_image Planet53 image2)
	(have_image Planet53 thermograph6)
	(have_image Phenomenon54 image5)
	(have_image Phenomenon55 thermograph6)
	(have_image Phenomenon56 image2)
	(have_image Star57 image4)
	(have_image Star57 image5)
	(have_image Star59 image0)
	(have_image Star59 image2)
	(have_image Phenomenon60 image5)
	(have_image Phenomenon60 image2)
	(have_image Planet61 spectrograph1)
	(have_image Phenomenon62 spectrograph1)
	(have_image Phenomenon62 image4)
	(have_image Planet63 image2)
	(have_image Phenomenon65 thermograph6)
	(have_image Planet66 image0)
	(have_image Planet66 thermograph6)
	(have_image Planet68 thermograph6)
	(have_image Planet68 image4)
	(have_image Star69 image0)
	(have_image Phenomenon70 spectrograph1)
	(have_image Planet71 image0)
	(have_image Planet71 thermograph6)
	(have_image Phenomenon72 image2)
	(have_image Phenomenon72 spectrograph3)
	(have_image Star73 thermograph6)
	(have_image Star73 image5)
	(have_image Star74 image5)
	(have_image Star74 image2)
	(have_image Planet75 image2)
	(have_image Planet75 spectrograph3)
	(have_image Star76 spectrograph1)
	(have_image Planet77 spectrograph3)
	(have_image Phenomenon78 image0)
	(have_image Phenomenon78 thermograph6)
	(have_image Star79 image4)
	(have_image Planet80 spectrograph1)
	(have_image Planet80 image2)
	(have_image Planet82 thermograph6)
	(have_image Planet83 spectrograph3)
	(have_image Planet83 image5)
	(have_image Star86 thermograph6)
	(have_image Star86 image2)
	(have_image Planet87 image5)
	(have_image Phenomenon88 image4)
	(have_image Planet89 spectrograph1)
	(have_image Star90 image0)
	(have_image Phenomenon91 spectrograph3)
	(have_image Phenomenon91 image0)
	(have_image Phenomenon92 image2)
	(have_image Phenomenon96 image0)
	(have_image Phenomenon97 thermograph6)
	(have_image Phenomenon98 spectrograph3)
	(have_image Star99 thermograph6)
	(have_image Star99 image2)
))

)
