(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	satellite1 - satellite
	instrument1 - instrument
	satellite2 - satellite
	instrument2 - instrument
	satellite3 - satellite
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	satellite4 - satellite
	instrument8 - instrument
	instrument9 - instrument
	instrument10 - instrument
	satellite5 - satellite
	instrument11 - instrument
	satellite6 - satellite
	instrument12 - instrument
	instrument13 - instrument
	instrument14 - instrument
	satellite7 - satellite
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	infrared7 - mode
	thermograph0 - mode
	image9 - mode
	thermograph6 - mode
	spectrograph2 - mode
	infrared8 - mode
	thermograph5 - mode
	spectrograph1 - mode
	image4 - mode
	infrared3 - mode
	GroundStation16 - direction
	GroundStation23 - direction
	GroundStation57 - direction
	GroundStation81 - direction
	Star91 - direction
	Star76 - direction
	GroundStation86 - direction
	Star85 - direction
	Star38 - direction
	Star74 - direction
	GroundStation35 - direction
	GroundStation73 - direction
	GroundStation34 - direction
	GroundStation27 - direction
	GroundStation53 - direction
	Star7 - direction
	Star10 - direction
	Star49 - direction
	GroundStation56 - direction
	GroundStation62 - direction
	Star18 - direction
	GroundStation95 - direction
	Star90 - direction
	GroundStation11 - direction
	GroundStation69 - direction
	GroundStation22 - direction
	Star42 - direction
	GroundStation12 - direction
	Star99 - direction
	GroundStation93 - direction
	GroundStation30 - direction
	GroundStation77 - direction
	GroundStation61 - direction
	GroundStation43 - direction
	Star20 - direction
	GroundStation3 - direction
	GroundStation50 - direction
	Star47 - direction
	GroundStation52 - direction
	Star51 - direction
	GroundStation21 - direction
	GroundStation44 - direction
	Star63 - direction
	Star13 - direction
	GroundStation45 - direction
	GroundStation2 - direction
	Star98 - direction
	GroundStation54 - direction
	GroundStation75 - direction
	Star94 - direction
	Star15 - direction
	Star96 - direction
	GroundStation48 - direction
	GroundStation88 - direction
	Star40 - direction
	GroundStation92 - direction
	GroundStation0 - direction
	Star39 - direction
	Star14 - direction
	GroundStation36 - direction
	GroundStation1 - direction
	Star19 - direction
	GroundStation26 - direction
	Star33 - direction
	Star9 - direction
	GroundStation24 - direction
	GroundStation46 - direction
	GroundStation82 - direction
	GroundStation8 - direction
	Star70 - direction
	GroundStation59 - direction
	GroundStation66 - direction
	Star84 - direction
	GroundStation72 - direction
	GroundStation65 - direction
	GroundStation31 - direction
	GroundStation78 - direction
	Star28 - direction
	Star71 - direction
	Star89 - direction
	Star32 - direction
	Star6 - direction
	Star5 - direction
	GroundStation64 - direction
	GroundStation67 - direction
	GroundStation60 - direction
	Star80 - direction
	Star17 - direction
	Star97 - direction
	GroundStation4 - direction
	Star41 - direction
	GroundStation58 - direction
	GroundStation87 - direction
	GroundStation55 - direction
	Star79 - direction
	Star83 - direction
	GroundStation68 - direction
	Star29 - direction
	Star25 - direction
	GroundStation37 - direction
	Planet100 - direction
	Star101 - direction
	Star102 - direction
	Star103 - direction
	Star104 - direction
	Star105 - direction
	Planet106 - direction
	Planet107 - direction
	Phenomenon108 - direction
	Star109 - direction
	Phenomenon110 - direction
	Star111 - direction
	Star112 - direction
	Planet113 - direction
	Planet114 - direction
	Phenomenon115 - direction
	Star116 - direction
	Phenomenon117 - direction
	Phenomenon118 - direction
	Phenomenon119 - direction
	Star120 - direction
	Phenomenon121 - direction
	Planet122 - direction
	Planet123 - direction
	Planet124 - direction
	Star125 - direction
	Phenomenon126 - direction
	Star127 - direction
	Star128 - direction
	Planet129 - direction
	Phenomenon130 - direction
	Planet131 - direction
	Star132 - direction
	Star133 - direction
	Star134 - direction
	Phenomenon135 - direction
	Planet136 - direction
	Phenomenon137 - direction
	Planet138 - direction
	Star139 - direction
	Planet140 - direction
	Planet141 - direction
	Planet142 - direction
	Star143 - direction
	Phenomenon144 - direction
	Star145 - direction
	Star146 - direction
	Planet147 - direction
	Planet148 - direction
	Phenomenon149 - direction
	Phenomenon150 - direction
	Phenomenon151 - direction
	Phenomenon152 - direction
	Planet153 - direction
	Planet154 - direction
	Planet155 - direction
	Planet156 - direction
	Phenomenon157 - direction
	Planet158 - direction
	Star159 - direction
	Phenomenon160 - direction
	Star161 - direction
	Star162 - direction
	Planet163 - direction
	Star164 - direction
	Planet165 - direction
	Phenomenon166 - direction
	Planet167 - direction
	Star168 - direction
	Phenomenon169 - direction
	Star170 - direction
	Star171 - direction
	Star172 - direction
	Planet173 - direction
	Phenomenon174 - direction
	Star175 - direction
	Phenomenon176 - direction
	Planet177 - direction
	Phenomenon178 - direction
	Planet179 - direction
	Planet180 - direction
	Phenomenon181 - direction
	Planet182 - direction
	Planet183 - direction
	Planet184 - direction
	Star185 - direction
	Planet186 - direction
	Planet187 - direction
	Phenomenon188 - direction
	Star189 - direction
	Planet190 - direction
	Planet191 - direction
	Phenomenon192 - direction
	Planet193 - direction
	Star194 - direction
	Star195 - direction
	Planet196 - direction
	Star197 - direction
	Planet198 - direction
	Planet199 - direction
)
(:init
	(supports instrument0 thermograph5)
	(calibration_target instrument0 GroundStation93)
	(calibration_target instrument0 GroundStation86)
	(calibration_target instrument0 Star63)
	(calibration_target instrument0 Star39)
	(calibration_target instrument0 Star83)
	(calibration_target instrument0 GroundStation22)
	(calibration_target instrument0 GroundStation78)
	(calibration_target instrument0 GroundStation52)
	(calibration_target instrument0 GroundStation67)
	(calibration_target instrument0 Star10)
	(calibration_target instrument0 GroundStation95)
	(calibration_target instrument0 Star7)
	(calibration_target instrument0 Star74)
	(calibration_target instrument0 GroundStation43)
	(calibration_target instrument0 Star76)
	(calibration_target instrument0 GroundStation88)
	(calibration_target instrument0 GroundStation31)
	(calibration_target instrument0 GroundStation30)
	(calibration_target instrument0 Star47)
	(calibration_target instrument0 GroundStation34)
	(calibration_target instrument0 Star28)
	(calibration_target instrument0 Star96)
	(calibration_target instrument0 Star79)
	(calibration_target instrument0 Star14)
	(calibration_target instrument0 Star32)
	(calibration_target instrument0 Star15)
	(calibration_target instrument0 GroundStation92)
	(on_board instrument0 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation45)
	(supports instrument1 infrared7)
	(supports instrument1 spectrograph2)
	(supports instrument1 thermograph0)
	(calibration_target instrument1 Star20)
	(calibration_target instrument1 GroundStation92)
	(calibration_target instrument1 GroundStation75)
	(calibration_target instrument1 Star17)
	(calibration_target instrument1 GroundStation72)
	(calibration_target instrument1 Star13)
	(on_board instrument1 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star197)
	(supports instrument2 infrared7)
	(calibration_target instrument2 Star80)
	(calibration_target instrument2 GroundStation48)
	(calibration_target instrument2 GroundStation73)
	(calibration_target instrument2 Star96)
	(calibration_target instrument2 GroundStation0)
	(calibration_target instrument2 Star9)
	(calibration_target instrument2 GroundStation64)
	(on_board instrument2 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation35)
	(supports instrument3 spectrograph2)
	(supports instrument3 image4)
	(calibration_target instrument3 GroundStation58)
	(calibration_target instrument3 GroundStation27)
	(calibration_target instrument3 GroundStation52)
	(calibration_target instrument3 Star71)
	(calibration_target instrument3 GroundStation11)
	(calibration_target instrument3 Star80)
	(calibration_target instrument3 GroundStation3)
	(calibration_target instrument3 GroundStation61)
	(calibration_target instrument3 GroundStation87)
	(calibration_target instrument3 Star74)
	(calibration_target instrument3 GroundStation60)
	(calibration_target instrument3 Star15)
	(calibration_target instrument3 GroundStation36)
	(calibration_target instrument3 GroundStation1)
	(calibration_target instrument3 GroundStation93)
	(calibration_target instrument3 Star63)
	(calibration_target instrument3 Star51)
	(calibration_target instrument3 Star47)
	(calibration_target instrument3 Star98)
	(calibration_target instrument3 Star38)
	(calibration_target instrument3 Star14)
	(calibration_target instrument3 GroundStation66)
	(supports instrument4 thermograph5)
	(calibration_target instrument4 GroundStation22)
	(calibration_target instrument4 Star49)
	(calibration_target instrument4 GroundStation55)
	(calibration_target instrument4 GroundStation1)
	(calibration_target instrument4 Star15)
	(calibration_target instrument4 Star20)
	(calibration_target instrument4 Star94)
	(calibration_target instrument4 GroundStation36)
	(calibration_target instrument4 Star85)
	(calibration_target instrument4 Star83)
	(calibration_target instrument4 Star47)
	(calibration_target instrument4 GroundStation88)
	(calibration_target instrument4 GroundStation67)
	(calibration_target instrument4 GroundStation12)
	(calibration_target instrument4 GroundStation65)
	(calibration_target instrument4 GroundStation58)
	(calibration_target instrument4 Star90)
	(calibration_target instrument4 Star80)
	(calibration_target instrument4 Star98)
	(calibration_target instrument4 GroundStation3)
	(calibration_target instrument4 Star32)
	(calibration_target instrument4 Star17)
	(calibration_target instrument4 GroundStation66)
	(supports instrument5 thermograph6)
	(supports instrument5 spectrograph1)
	(calibration_target instrument5 Star42)
	(calibration_target instrument5 Star38)
	(calibration_target instrument5 GroundStation44)
	(calibration_target instrument5 GroundStation4)
	(calibration_target instrument5 GroundStation59)
	(calibration_target instrument5 GroundStation72)
	(calibration_target instrument5 Star29)
	(calibration_target instrument5 GroundStation0)
	(calibration_target instrument5 GroundStation11)
	(calibration_target instrument5 Star13)
	(calibration_target instrument5 GroundStation43)
	(calibration_target instrument5 GroundStation75)
	(calibration_target instrument5 Star96)
	(calibration_target instrument5 GroundStation48)
	(calibration_target instrument5 GroundStation60)
	(calibration_target instrument5 GroundStation31)
	(supports instrument6 spectrograph1)
	(supports instrument6 infrared7)
	(calibration_target instrument6 Star99)
	(calibration_target instrument6 GroundStation66)
	(calibration_target instrument6 Star7)
	(calibration_target instrument6 Star19)
	(calibration_target instrument6 Star40)
	(calibration_target instrument6 GroundStation75)
	(calibration_target instrument6 Star17)
	(calibration_target instrument6 Star47)
	(calibration_target instrument6 GroundStation64)
	(calibration_target instrument6 GroundStation69)
	(calibration_target instrument6 GroundStation53)
	(calibration_target instrument6 Star25)
	(calibration_target instrument6 Star94)
	(calibration_target instrument6 GroundStation8)
	(calibration_target instrument6 GroundStation27)
	(calibration_target instrument6 Star51)
	(calibration_target instrument6 Star13)
	(calibration_target instrument6 Star49)
	(calibration_target instrument6 GroundStation26)
	(calibration_target instrument6 GroundStation34)
	(calibration_target instrument6 GroundStation31)
	(calibration_target instrument6 GroundStation43)
	(calibration_target instrument6 GroundStation73)
	(calibration_target instrument6 GroundStation24)
	(calibration_target instrument6 Star15)
	(calibration_target instrument6 GroundStation35)
	(calibration_target instrument6 Star74)
	(calibration_target instrument6 Star97)
	(supports instrument7 spectrograph1)
	(supports instrument7 spectrograph2)
	(calibration_target instrument7 Star19)
	(calibration_target instrument7 GroundStation31)
	(calibration_target instrument7 Star13)
	(calibration_target instrument7 GroundStation1)
	(calibration_target instrument7 Star14)
	(calibration_target instrument7 Star70)
	(calibration_target instrument7 Star9)
	(calibration_target instrument7 Star51)
	(calibration_target instrument7 GroundStation56)
	(calibration_target instrument7 Star94)
	(calibration_target instrument7 GroundStation24)
	(calibration_target instrument7 GroundStation93)
	(calibration_target instrument7 Star20)
	(calibration_target instrument7 Star49)
	(calibration_target instrument7 GroundStation92)
	(calibration_target instrument7 Star10)
	(calibration_target instrument7 GroundStation65)
	(calibration_target instrument7 Star25)
	(calibration_target instrument7 Star7)
	(on_board instrument3 satellite3)
	(on_board instrument4 satellite3)
	(on_board instrument5 satellite3)
	(on_board instrument6 satellite3)
	(on_board instrument7 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Planet183)
	(supports instrument8 thermograph0)
	(supports instrument8 image9)
	(calibration_target instrument8 GroundStation95)
	(calibration_target instrument8 Star18)
	(calibration_target instrument8 Star5)
	(calibration_target instrument8 GroundStation78)
	(calibration_target instrument8 Star71)
	(calibration_target instrument8 Star19)
	(calibration_target instrument8 Star17)
	(calibration_target instrument8 GroundStation1)
	(calibration_target instrument8 GroundStation30)
	(calibration_target instrument8 GroundStation93)
	(calibration_target instrument8 GroundStation59)
	(calibration_target instrument8 GroundStation62)
	(calibration_target instrument8 GroundStation44)
	(calibration_target instrument8 GroundStation54)
	(calibration_target instrument8 GroundStation92)
	(calibration_target instrument8 GroundStation50)
	(calibration_target instrument8 GroundStation87)
	(calibration_target instrument8 Star9)
	(supports instrument9 spectrograph1)
	(calibration_target instrument9 GroundStation72)
	(calibration_target instrument9 Star99)
	(calibration_target instrument9 Star79)
	(calibration_target instrument9 GroundStation64)
	(calibration_target instrument9 Star32)
	(calibration_target instrument9 GroundStation12)
	(calibration_target instrument9 Star42)
	(calibration_target instrument9 GroundStation22)
	(calibration_target instrument9 GroundStation69)
	(calibration_target instrument9 GroundStation11)
	(calibration_target instrument9 Star63)
	(calibration_target instrument9 GroundStation48)
	(calibration_target instrument9 Star90)
	(calibration_target instrument9 GroundStation2)
	(calibration_target instrument9 Star40)
	(calibration_target instrument9 Star17)
	(calibration_target instrument9 GroundStation21)
	(calibration_target instrument9 Star13)
	(calibration_target instrument9 GroundStation68)
	(calibration_target instrument9 GroundStation24)
	(calibration_target instrument9 GroundStation61)
	(supports instrument10 spectrograph2)
	(supports instrument10 thermograph5)
	(calibration_target instrument10 Star70)
	(calibration_target instrument10 GroundStation43)
	(calibration_target instrument10 Star40)
	(on_board instrument8 satellite4)
	(on_board instrument9 satellite4)
	(on_board instrument10 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star189)
	(supports instrument11 spectrograph2)
	(supports instrument11 infrared3)
	(calibration_target instrument11 Star97)
	(calibration_target instrument11 Star39)
	(calibration_target instrument11 GroundStation61)
	(calibration_target instrument11 GroundStation77)
	(calibration_target instrument11 GroundStation50)
	(calibration_target instrument11 GroundStation30)
	(calibration_target instrument11 Star80)
	(calibration_target instrument11 GroundStation93)
	(calibration_target instrument11 GroundStation37)
	(calibration_target instrument11 Star14)
	(calibration_target instrument11 Star28)
	(calibration_target instrument11 Star5)
	(on_board instrument11 satellite5)
	(power_avail satellite5)
	(pointing satellite5 GroundStation37)
	(supports instrument12 thermograph6)
	(supports instrument12 image9)
	(supports instrument12 infrared3)
	(calibration_target instrument12 GroundStation45)
	(calibration_target instrument12 Star39)
	(calibration_target instrument12 Star9)
	(calibration_target instrument12 Star89)
	(calibration_target instrument12 GroundStation44)
	(calibration_target instrument12 GroundStation21)
	(calibration_target instrument12 Star79)
	(calibration_target instrument12 Star51)
	(calibration_target instrument12 Star14)
	(calibration_target instrument12 GroundStation52)
	(calibration_target instrument12 GroundStation37)
	(calibration_target instrument12 Star97)
	(calibration_target instrument12 Star98)
	(calibration_target instrument12 Star47)
	(calibration_target instrument12 GroundStation67)
	(calibration_target instrument12 GroundStation4)
	(calibration_target instrument12 GroundStation50)
	(calibration_target instrument12 GroundStation65)
	(calibration_target instrument12 GroundStation88)
	(calibration_target instrument12 Star13)
	(calibration_target instrument12 GroundStation72)
	(calibration_target instrument12 Star83)
	(calibration_target instrument12 Star80)
	(calibration_target instrument12 GroundStation3)
	(calibration_target instrument12 GroundStation0)
	(calibration_target instrument12 GroundStation87)
	(calibration_target instrument12 GroundStation64)
	(calibration_target instrument12 Star20)
	(calibration_target instrument12 GroundStation43)
	(supports instrument13 infrared8)
	(supports instrument13 spectrograph2)
	(calibration_target instrument13 Star33)
	(calibration_target instrument13 GroundStation26)
	(calibration_target instrument13 GroundStation67)
	(calibration_target instrument13 Star84)
	(calibration_target instrument13 Star19)
	(calibration_target instrument13 GroundStation1)
	(calibration_target instrument13 Star89)
	(calibration_target instrument13 GroundStation36)
	(calibration_target instrument13 Star14)
	(calibration_target instrument13 Star39)
	(calibration_target instrument13 GroundStation0)
	(calibration_target instrument13 GroundStation92)
	(calibration_target instrument13 GroundStation60)
	(calibration_target instrument13 GroundStation72)
	(calibration_target instrument13 GroundStation46)
	(calibration_target instrument13 Star40)
	(calibration_target instrument13 GroundStation88)
	(calibration_target instrument13 Star29)
	(calibration_target instrument13 GroundStation66)
	(calibration_target instrument13 GroundStation48)
	(calibration_target instrument13 Star96)
	(calibration_target instrument13 GroundStation55)
	(calibration_target instrument13 Star15)
	(calibration_target instrument13 Star94)
	(calibration_target instrument13 GroundStation75)
	(calibration_target instrument13 Star71)
	(calibration_target instrument13 GroundStation54)
	(calibration_target instrument13 Star98)
	(calibration_target instrument13 GroundStation2)
	(calibration_target instrument13 GroundStation45)
	(calibration_target instrument13 Star13)
	(calibration_target instrument13 Star63)
	(supports instrument14 infrared8)
	(supports instrument14 spectrograph1)
	(calibration_target instrument14 Star83)
	(calibration_target instrument14 GroundStation31)
	(calibration_target instrument14 GroundStation65)
	(calibration_target instrument14 GroundStation58)
	(calibration_target instrument14 GroundStation72)
	(calibration_target instrument14 Star84)
	(calibration_target instrument14 GroundStation66)
	(calibration_target instrument14 GroundStation59)
	(calibration_target instrument14 Star70)
	(calibration_target instrument14 GroundStation8)
	(calibration_target instrument14 GroundStation64)
	(calibration_target instrument14 GroundStation82)
	(calibration_target instrument14 GroundStation46)
	(calibration_target instrument14 GroundStation24)
	(calibration_target instrument14 GroundStation60)
	(calibration_target instrument14 Star9)
	(calibration_target instrument14 GroundStation78)
	(calibration_target instrument14 Star6)
	(on_board instrument12 satellite6)
	(on_board instrument13 satellite6)
	(on_board instrument14 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Planet140)
	(supports instrument15 thermograph5)
	(supports instrument15 infrared3)
	(calibration_target instrument15 Star71)
	(calibration_target instrument15 Star28)
	(calibration_target instrument15 GroundStation78)
	(supports instrument16 infrared3)
	(supports instrument16 image4)
	(supports instrument16 spectrograph1)
	(calibration_target instrument16 Star83)
	(calibration_target instrument16 Star79)
	(calibration_target instrument16 GroundStation55)
	(calibration_target instrument16 GroundStation87)
	(calibration_target instrument16 GroundStation58)
	(calibration_target instrument16 Star41)
	(calibration_target instrument16 GroundStation4)
	(calibration_target instrument16 Star97)
	(calibration_target instrument16 Star17)
	(calibration_target instrument16 Star80)
	(calibration_target instrument16 GroundStation60)
	(calibration_target instrument16 GroundStation67)
	(calibration_target instrument16 GroundStation64)
	(calibration_target instrument16 Star5)
	(calibration_target instrument16 Star6)
	(calibration_target instrument16 Star32)
	(calibration_target instrument16 Star89)
	(supports instrument17 infrared3)
	(supports instrument17 image4)
	(calibration_target instrument17 GroundStation37)
	(calibration_target instrument17 Star25)
	(calibration_target instrument17 Star29)
	(calibration_target instrument17 GroundStation68)
	(on_board instrument15 satellite7)
	(on_board instrument16 satellite7)
	(on_board instrument17 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Phenomenon135)
)
(:goal (and
	(pointing satellite0 Phenomenon137)
	(pointing satellite3 GroundStation56)
	(pointing satellite4 GroundStation53)
	(pointing satellite5 GroundStation24)
	(pointing satellite6 GroundStation60)
	(have_image Planet100 infrared8)
	(have_image Star101 spectrograph1)
	(have_image Star102 spectrograph1)
	(have_image Star102 thermograph6)
	(have_image Star102 image9)
	(have_image Star103 spectrograph1)
	(have_image Star104 spectrograph1)
	(have_image Planet106 infrared3)
	(have_image Phenomenon108 infrared3)
	(have_image Star109 infrared3)
	(have_image Phenomenon110 thermograph5)
	(have_image Star111 thermograph5)
	(have_image Star111 infrared3)
	(have_image Star111 image4)
	(have_image Star112 infrared8)
	(have_image Phenomenon115 thermograph6)
	(have_image Star116 infrared7)
	(have_image Phenomenon117 thermograph0)
	(have_image Phenomenon117 infrared8)
	(have_image Phenomenon118 spectrograph2)
	(have_image Phenomenon118 image4)
	(have_image Phenomenon118 thermograph5)
	(have_image Phenomenon119 infrared8)
	(have_image Phenomenon119 spectrograph2)
	(have_image Phenomenon119 infrared3)
	(have_image Star120 infrared7)
	(have_image Star120 thermograph5)
	(have_image Star120 spectrograph2)
	(have_image Phenomenon121 spectrograph1)
	(have_image Planet122 infrared7)
	(have_image Planet122 image4)
	(have_image Planet122 thermograph5)
	(have_image Planet123 thermograph6)
	(have_image Planet123 spectrograph1)
	(have_image Planet123 image9)
	(have_image Planet124 image9)
	(have_image Star125 image9)
	(have_image Star125 image4)
	(have_image Star125 spectrograph2)
	(have_image Phenomenon126 thermograph0)
	(have_image Star127 spectrograph2)
	(have_image Star127 infrared8)
	(have_image Star128 image4)
	(have_image Star128 image9)
	(have_image Planet129 infrared8)
	(have_image Planet129 thermograph6)
	(have_image Phenomenon130 image9)
	(have_image Phenomenon130 infrared8)
	(have_image Phenomenon130 spectrograph2)
	(have_image Planet131 image4)
	(have_image Planet131 infrared8)
	(have_image Star132 infrared8)
	(have_image Star132 infrared7)
	(have_image Star133 infrared7)
	(have_image Star133 spectrograph2)
	(have_image Star134 image9)
	(have_image Star134 thermograph0)
	(have_image Star134 infrared8)
	(have_image Phenomenon135 infrared8)
	(have_image Phenomenon135 thermograph5)
	(have_image Phenomenon135 spectrograph1)
	(have_image Planet136 thermograph0)
	(have_image Phenomenon137 thermograph5)
	(have_image Phenomenon137 image9)
	(have_image Planet138 infrared7)
	(have_image Planet138 spectrograph2)
	(have_image Planet138 image4)
	(have_image Star139 infrared3)
	(have_image Star139 image9)
	(have_image Planet140 thermograph5)
	(have_image Planet140 infrared8)
	(have_image Planet141 infrared3)
	(have_image Planet141 thermograph0)
	(have_image Planet142 infrared8)
	(have_image Star143 infrared8)
	(have_image Star143 thermograph5)
	(have_image Star143 infrared3)
	(have_image Phenomenon144 infrared8)
	(have_image Phenomenon144 thermograph5)
	(have_image Star145 spectrograph2)
	(have_image Star145 infrared7)
	(have_image Star146 image9)
	(have_image Star146 infrared8)
	(have_image Star146 spectrograph1)
	(have_image Planet147 thermograph6)
	(have_image Planet147 image4)
	(have_image Planet147 spectrograph2)
	(have_image Planet148 thermograph0)
	(have_image Phenomenon149 infrared8)
	(have_image Phenomenon149 thermograph5)
	(have_image Phenomenon149 thermograph0)
	(have_image Phenomenon150 spectrograph2)
	(have_image Phenomenon150 image9)
	(have_image Phenomenon152 thermograph5)
	(have_image Phenomenon152 spectrograph1)
	(have_image Planet153 spectrograph2)
	(have_image Planet153 image4)
	(have_image Planet154 thermograph5)
	(have_image Planet155 infrared3)
	(have_image Planet155 spectrograph1)
	(have_image Planet155 infrared7)
	(have_image Phenomenon157 thermograph0)
	(have_image Planet158 image4)
	(have_image Star159 thermograph5)
	(have_image Star159 infrared8)
	(have_image Star159 spectrograph1)
	(have_image Phenomenon160 image9)
	(have_image Phenomenon160 image4)
	(have_image Star161 image9)
	(have_image Star161 thermograph5)
	(have_image Star162 infrared8)
	(have_image Star162 thermograph5)
	(have_image Planet163 infrared8)
	(have_image Star164 infrared3)
	(have_image Star164 image4)
	(have_image Star164 thermograph5)
	(have_image Planet165 spectrograph1)
	(have_image Planet167 spectrograph2)
	(have_image Planet167 thermograph6)
	(have_image Planet167 thermograph5)
	(have_image Star168 thermograph5)
	(have_image Phenomenon169 image4)
	(have_image Phenomenon169 thermograph0)
	(have_image Star170 thermograph5)
	(have_image Star171 image9)
	(have_image Star171 thermograph0)
	(have_image Star172 infrared3)
	(have_image Star172 spectrograph2)
	(have_image Star172 thermograph6)
	(have_image Planet173 thermograph0)
	(have_image Planet173 infrared8)
	(have_image Planet173 image4)
	(have_image Phenomenon174 thermograph6)
	(have_image Phenomenon174 image9)
	(have_image Phenomenon174 thermograph5)
	(have_image Star175 infrared3)
	(have_image Star175 thermograph6)
	(have_image Phenomenon176 infrared8)
	(have_image Planet177 image9)
	(have_image Phenomenon178 thermograph0)
	(have_image Phenomenon178 infrared3)
	(have_image Planet179 thermograph0)
	(have_image Planet179 spectrograph1)
	(have_image Planet180 infrared8)
	(have_image Phenomenon181 thermograph0)
	(have_image Phenomenon181 infrared7)
	(have_image Phenomenon181 image4)
	(have_image Planet182 spectrograph2)
	(have_image Planet182 thermograph6)
	(have_image Planet183 image4)
	(have_image Planet184 thermograph0)
	(have_image Planet184 spectrograph2)
	(have_image Planet184 infrared7)
	(have_image Star185 infrared7)
	(have_image Planet186 infrared3)
	(have_image Planet186 image4)
	(have_image Phenomenon188 thermograph6)
	(have_image Phenomenon188 thermograph5)
	(have_image Star189 infrared3)
	(have_image Star189 thermograph5)
	(have_image Planet190 thermograph5)
	(have_image Planet190 spectrograph1)
	(have_image Planet190 image9)
	(have_image Planet191 image4)
	(have_image Planet191 thermograph0)
	(have_image Phenomenon192 thermograph0)
	(have_image Phenomenon192 spectrograph2)
	(have_image Phenomenon192 image4)
	(have_image Planet193 thermograph6)
	(have_image Planet193 spectrograph2)
	(have_image Planet193 image4)
	(have_image Star194 thermograph5)
	(have_image Star195 thermograph0)
	(have_image Star195 thermograph5)
	(have_image Star195 infrared8)
	(have_image Planet196 infrared7)
	(have_image Star197 infrared3)
	(have_image Planet198 spectrograph2)
	(have_image Planet198 thermograph5)
	(have_image Planet199 infrared8)
))

)
