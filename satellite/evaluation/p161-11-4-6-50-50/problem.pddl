(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	satellite1 - satellite
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	satellite2 - satellite
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	satellite3 - satellite
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	satellite4 - satellite
	instrument13 - instrument
	satellite5 - satellite
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	satellite6 - satellite
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	satellite7 - satellite
	instrument21 - instrument
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	satellite8 - satellite
	instrument25 - instrument
	instrument26 - instrument
	satellite9 - satellite
	instrument27 - instrument
	instrument28 - instrument
	instrument29 - instrument
	instrument30 - instrument
	satellite10 - satellite
	instrument31 - instrument
	instrument32 - instrument
	image2 - mode
	spectrograph0 - mode
	image3 - mode
	infrared4 - mode
	thermograph1 - mode
	thermograph5 - mode
	GroundStation16 - direction
	Star35 - direction
	Star40 - direction
	GroundStation36 - direction
	GroundStation46 - direction
	GroundStation23 - direction
	Star49 - direction
	Star9 - direction
	Star31 - direction
	Star42 - direction
	Star47 - direction
	GroundStation34 - direction
	GroundStation18 - direction
	Star44 - direction
	Star5 - direction
	Star48 - direction
	GroundStation32 - direction
	Star1 - direction
	GroundStation0 - direction
	Star11 - direction
	Star24 - direction
	GroundStation14 - direction
	Star41 - direction
	GroundStation27 - direction
	GroundStation3 - direction
	Star20 - direction
	GroundStation19 - direction
	GroundStation10 - direction
	Star13 - direction
	Star4 - direction
	Star21 - direction
	Star28 - direction
	GroundStation45 - direction
	Star12 - direction
	Star7 - direction
	GroundStation39 - direction
	Star33 - direction
	GroundStation37 - direction
	Star25 - direction
	GroundStation17 - direction
	GroundStation38 - direction
	Star22 - direction
	Star30 - direction
	GroundStation2 - direction
	GroundStation6 - direction
	Star26 - direction
	GroundStation43 - direction
	Star29 - direction
	Star15 - direction
	Star8 - direction
	Phenomenon50 - direction
	Phenomenon51 - direction
	Star52 - direction
	Planet53 - direction
	Star54 - direction
	Star55 - direction
	Star56 - direction
	Planet57 - direction
	Star58 - direction
	Star59 - direction
	Star60 - direction
	Star61 - direction
	Planet62 - direction
	Phenomenon63 - direction
	Phenomenon64 - direction
	Star65 - direction
	Star66 - direction
	Phenomenon67 - direction
	Planet68 - direction
	Star69 - direction
	Planet70 - direction
	Phenomenon71 - direction
	Star72 - direction
	Star73 - direction
	Phenomenon74 - direction
	Phenomenon75 - direction
	Star76 - direction
	Phenomenon77 - direction
	Planet78 - direction
	Planet79 - direction
	Star80 - direction
	Star81 - direction
	Phenomenon82 - direction
	Star83 - direction
	Phenomenon84 - direction
	Planet85 - direction
	Planet86 - direction
	Phenomenon87 - direction
	Star88 - direction
	Phenomenon89 - direction
	Star90 - direction
	Star91 - direction
	Star92 - direction
	Star93 - direction
	Phenomenon94 - direction
	Phenomenon95 - direction
	Phenomenon96 - direction
	Phenomenon97 - direction
	Phenomenon98 - direction
	Phenomenon99 - direction
)
(:init
	(supports instrument0 infrared4)
	(calibration_target instrument0 GroundStation34)
	(calibration_target instrument0 GroundStation27)
	(calibration_target instrument0 Star31)
	(calibration_target instrument0 Star9)
	(calibration_target instrument0 Star29)
	(calibration_target instrument0 Star21)
	(calibration_target instrument0 Star4)
	(calibration_target instrument0 GroundStation0)
	(calibration_target instrument0 GroundStation39)
	(calibration_target instrument0 GroundStation18)
	(supports instrument1 thermograph1)
	(calibration_target instrument1 Star21)
	(calibration_target instrument1 Star15)
	(calibration_target instrument1 GroundStation23)
	(calibration_target instrument1 GroundStation37)
	(calibration_target instrument1 Star41)
	(calibration_target instrument1 Star40)
	(calibration_target instrument1 GroundStation6)
	(calibration_target instrument1 Star12)
	(calibration_target instrument1 Star20)
	(calibration_target instrument1 GroundStation45)
	(calibration_target instrument1 GroundStation0)
	(calibration_target instrument1 GroundStation32)
	(calibration_target instrument1 GroundStation14)
	(calibration_target instrument1 GroundStation17)
	(calibration_target instrument1 GroundStation43)
	(calibration_target instrument1 GroundStation16)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Star76)
	(supports instrument2 image3)
	(supports instrument2 infrared4)
	(supports instrument2 image2)
	(calibration_target instrument2 Star21)
	(calibration_target instrument2 GroundStation0)
	(calibration_target instrument2 GroundStation6)
	(calibration_target instrument2 GroundStation3)
	(calibration_target instrument2 GroundStation32)
	(calibration_target instrument2 GroundStation16)
	(calibration_target instrument2 GroundStation27)
	(calibration_target instrument2 GroundStation37)
	(calibration_target instrument2 GroundStation43)
	(calibration_target instrument2 Star24)
	(supports instrument3 thermograph5)
	(calibration_target instrument3 GroundStation17)
	(calibration_target instrument3 Star24)
	(calibration_target instrument3 GroundStation23)
	(calibration_target instrument3 Star30)
	(calibration_target instrument3 Star13)
	(calibration_target instrument3 GroundStation18)
	(calibration_target instrument3 Star48)
	(calibration_target instrument3 Star15)
	(calibration_target instrument3 Star49)
	(calibration_target instrument3 Star28)
	(calibration_target instrument3 GroundStation45)
	(calibration_target instrument3 Star4)
	(calibration_target instrument3 Star7)
	(calibration_target instrument3 Star42)
	(calibration_target instrument3 Star26)
	(supports instrument4 image2)
	(supports instrument4 image3)
	(calibration_target instrument4 GroundStation45)
	(calibration_target instrument4 Star15)
	(calibration_target instrument4 Star49)
	(calibration_target instrument4 GroundStation38)
	(calibration_target instrument4 GroundStation32)
	(calibration_target instrument4 GroundStation39)
	(supports instrument5 image2)
	(supports instrument5 thermograph5)
	(calibration_target instrument5 Star30)
	(calibration_target instrument5 GroundStation43)
	(calibration_target instrument5 Star35)
	(calibration_target instrument5 GroundStation39)
	(calibration_target instrument5 GroundStation18)
	(calibration_target instrument5 Star7)
	(calibration_target instrument5 Star40)
	(calibration_target instrument5 GroundStation16)
	(calibration_target instrument5 GroundStation34)
	(calibration_target instrument5 Star29)
	(calibration_target instrument5 Star12)
	(calibration_target instrument5 Star1)
	(calibration_target instrument5 GroundStation17)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(power_avail satellite1)
	(pointing satellite1 GroundStation14)
	(supports instrument6 thermograph1)
	(supports instrument6 image3)
	(supports instrument6 infrared4)
	(calibration_target instrument6 GroundStation19)
	(calibration_target instrument6 Star40)
	(calibration_target instrument6 Star5)
	(calibration_target instrument6 Star48)
	(calibration_target instrument6 GroundStation16)
	(calibration_target instrument6 GroundStation18)
	(calibration_target instrument6 Star49)
	(calibration_target instrument6 Star47)
	(calibration_target instrument6 Star26)
	(calibration_target instrument6 Star15)
	(calibration_target instrument6 GroundStation6)
	(supports instrument7 spectrograph0)
	(calibration_target instrument7 Star11)
	(calibration_target instrument7 Star30)
	(calibration_target instrument7 Star26)
	(calibration_target instrument7 Star1)
	(calibration_target instrument7 Star9)
	(calibration_target instrument7 GroundStation23)
	(calibration_target instrument7 Star35)
	(calibration_target instrument7 Star22)
	(calibration_target instrument7 GroundStation32)
	(calibration_target instrument7 GroundStation37)
	(calibration_target instrument7 Star5)
	(supports instrument8 thermograph5)
	(calibration_target instrument8 Star31)
	(calibration_target instrument8 Star15)
	(calibration_target instrument8 GroundStation39)
	(calibration_target instrument8 GroundStation17)
	(supports instrument9 infrared4)
	(calibration_target instrument9 GroundStation6)
	(calibration_target instrument9 GroundStation17)
	(calibration_target instrument9 Star5)
	(calibration_target instrument9 Star33)
	(calibration_target instrument9 GroundStation3)
	(calibration_target instrument9 Star48)
	(calibration_target instrument9 Star7)
	(calibration_target instrument9 GroundStation0)
	(calibration_target instrument9 Star9)
	(calibration_target instrument9 Star29)
	(calibration_target instrument9 GroundStation2)
	(on_board instrument6 satellite2)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Phenomenon84)
	(supports instrument10 thermograph1)
	(supports instrument10 image2)
	(calibration_target instrument10 Star41)
	(calibration_target instrument10 GroundStation27)
	(calibration_target instrument10 Star31)
	(calibration_target instrument10 Star9)
	(calibration_target instrument10 Star30)
	(calibration_target instrument10 GroundStation10)
	(calibration_target instrument10 GroundStation23)
	(calibration_target instrument10 Star42)
	(calibration_target instrument10 GroundStation36)
	(supports instrument11 thermograph1)
	(supports instrument11 thermograph5)
	(supports instrument11 infrared4)
	(calibration_target instrument11 Star44)
	(calibration_target instrument11 Star20)
	(calibration_target instrument11 GroundStation38)
	(calibration_target instrument11 Star15)
	(calibration_target instrument11 Star48)
	(calibration_target instrument11 GroundStation0)
	(supports instrument12 thermograph1)
	(supports instrument12 spectrograph0)
	(calibration_target instrument12 Star25)
	(calibration_target instrument12 Star35)
	(calibration_target instrument12 Star9)
	(calibration_target instrument12 Star12)
	(calibration_target instrument12 GroundStation17)
	(calibration_target instrument12 Star15)
	(calibration_target instrument12 Star30)
	(calibration_target instrument12 Star4)
	(calibration_target instrument12 Star24)
	(calibration_target instrument12 Star13)
	(calibration_target instrument12 Star48)
	(calibration_target instrument12 GroundStation45)
	(on_board instrument10 satellite3)
	(on_board instrument11 satellite3)
	(on_board instrument12 satellite3)
	(power_avail satellite3)
	(pointing satellite3 GroundStation0)
	(supports instrument13 thermograph5)
	(supports instrument13 thermograph1)
	(calibration_target instrument13 Star22)
	(calibration_target instrument13 Star4)
	(calibration_target instrument13 GroundStation36)
	(calibration_target instrument13 GroundStation2)
	(calibration_target instrument13 Star8)
	(calibration_target instrument13 GroundStation0)
	(calibration_target instrument13 Star26)
	(calibration_target instrument13 Star47)
	(calibration_target instrument13 Star29)
	(calibration_target instrument13 Star9)
	(calibration_target instrument13 Star41)
	(calibration_target instrument13 GroundStation43)
	(on_board instrument13 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star76)
	(supports instrument14 spectrograph0)
	(calibration_target instrument14 GroundStation23)
	(calibration_target instrument14 GroundStation45)
	(calibration_target instrument14 Star40)
	(calibration_target instrument14 Star41)
	(calibration_target instrument14 Star12)
	(calibration_target instrument14 Star20)
	(calibration_target instrument14 Star35)
	(calibration_target instrument14 Star33)
	(supports instrument15 spectrograph0)
	(supports instrument15 image3)
	(calibration_target instrument15 Star15)
	(calibration_target instrument15 Star29)
	(calibration_target instrument15 GroundStation3)
	(calibration_target instrument15 Star5)
	(calibration_target instrument15 Star33)
	(supports instrument16 thermograph1)
	(calibration_target instrument16 GroundStation27)
	(calibration_target instrument16 GroundStation32)
	(calibration_target instrument16 GroundStation45)
	(supports instrument17 infrared4)
	(supports instrument17 thermograph1)
	(calibration_target instrument17 Star8)
	(calibration_target instrument17 Star30)
	(calibration_target instrument17 Star44)
	(calibration_target instrument17 Star9)
	(calibration_target instrument17 Star15)
	(calibration_target instrument17 GroundStation6)
	(calibration_target instrument17 Star26)
	(calibration_target instrument17 GroundStation19)
	(calibration_target instrument17 Star5)
	(calibration_target instrument17 Star1)
	(calibration_target instrument17 Star25)
	(calibration_target instrument17 GroundStation37)
	(calibration_target instrument17 Star21)
	(on_board instrument14 satellite5)
	(on_board instrument15 satellite5)
	(on_board instrument16 satellite5)
	(on_board instrument17 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Phenomenon51)
	(supports instrument18 spectrograph0)
	(calibration_target instrument18 Star35)
	(calibration_target instrument18 GroundStation18)
	(calibration_target instrument18 GroundStation3)
	(calibration_target instrument18 Star47)
	(calibration_target instrument18 GroundStation17)
	(calibration_target instrument18 Star11)
	(calibration_target instrument18 Star33)
	(calibration_target instrument18 GroundStation36)
	(supports instrument19 thermograph5)
	(supports instrument19 thermograph1)
	(supports instrument19 image2)
	(calibration_target instrument19 GroundStation32)
	(calibration_target instrument19 GroundStation14)
	(calibration_target instrument19 Star44)
	(calibration_target instrument19 GroundStation37)
	(calibration_target instrument19 Star8)
	(calibration_target instrument19 Star15)
	(calibration_target instrument19 GroundStation39)
	(supports instrument20 spectrograph0)
	(supports instrument20 image2)
	(supports instrument20 image3)
	(calibration_target instrument20 Star22)
	(calibration_target instrument20 GroundStation23)
	(calibration_target instrument20 GroundStation10)
	(calibration_target instrument20 GroundStation46)
	(calibration_target instrument20 GroundStation36)
	(calibration_target instrument20 GroundStation2)
	(calibration_target instrument20 Star9)
	(calibration_target instrument20 Star12)
	(calibration_target instrument20 Star26)
	(calibration_target instrument20 Star42)
	(calibration_target instrument20 Star40)
	(calibration_target instrument20 GroundStation32)
	(calibration_target instrument20 Star33)
	(calibration_target instrument20 Star29)
	(calibration_target instrument20 GroundStation3)
	(calibration_target instrument20 Star24)
	(on_board instrument18 satellite6)
	(on_board instrument19 satellite6)
	(on_board instrument20 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Star8)
	(supports instrument21 thermograph1)
	(supports instrument21 infrared4)
	(calibration_target instrument21 Star22)
	(calibration_target instrument21 GroundStation39)
	(calibration_target instrument21 Star21)
	(calibration_target instrument21 Star20)
	(calibration_target instrument21 Star42)
	(calibration_target instrument21 Star9)
	(calibration_target instrument21 Star44)
	(calibration_target instrument21 Star49)
	(calibration_target instrument21 GroundStation3)
	(calibration_target instrument21 Star15)
	(supports instrument22 spectrograph0)
	(calibration_target instrument22 Star47)
	(calibration_target instrument22 GroundStation37)
	(calibration_target instrument22 Star42)
	(calibration_target instrument22 GroundStation0)
	(calibration_target instrument22 Star31)
	(calibration_target instrument22 GroundStation45)
	(calibration_target instrument22 GroundStation6)
	(calibration_target instrument22 GroundStation2)
	(calibration_target instrument22 Star41)
	(calibration_target instrument22 Star20)
	(calibration_target instrument22 GroundStation34)
	(calibration_target instrument22 GroundStation14)
	(supports instrument23 thermograph5)
	(supports instrument23 thermograph1)
	(supports instrument23 image3)
	(calibration_target instrument23 GroundStation18)
	(calibration_target instrument23 GroundStation34)
	(calibration_target instrument23 GroundStation10)
	(supports instrument24 thermograph5)
	(supports instrument24 image2)
	(supports instrument24 image3)
	(calibration_target instrument24 Star7)
	(calibration_target instrument24 Star21)
	(calibration_target instrument24 Star44)
	(on_board instrument21 satellite7)
	(on_board instrument22 satellite7)
	(on_board instrument23 satellite7)
	(on_board instrument24 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Phenomenon84)
	(supports instrument25 infrared4)
	(supports instrument25 thermograph1)
	(calibration_target instrument25 GroundStation10)
	(calibration_target instrument25 Star11)
	(calibration_target instrument25 GroundStation0)
	(calibration_target instrument25 Star1)
	(calibration_target instrument25 GroundStation38)
	(calibration_target instrument25 GroundStation32)
	(calibration_target instrument25 Star48)
	(calibration_target instrument25 GroundStation6)
	(calibration_target instrument25 Star5)
	(calibration_target instrument25 Star28)
	(calibration_target instrument25 Star20)
	(supports instrument26 infrared4)
	(supports instrument26 thermograph1)
	(supports instrument26 spectrograph0)
	(calibration_target instrument26 Star41)
	(calibration_target instrument26 Star30)
	(calibration_target instrument26 GroundStation14)
	(calibration_target instrument26 Star24)
	(calibration_target instrument26 Star13)
	(on_board instrument25 satellite8)
	(on_board instrument26 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Star60)
	(supports instrument27 image2)
	(calibration_target instrument27 Star20)
	(calibration_target instrument27 GroundStation3)
	(calibration_target instrument27 GroundStation27)
	(calibration_target instrument27 Star30)
	(supports instrument28 image2)
	(calibration_target instrument28 Star21)
	(calibration_target instrument28 Star4)
	(calibration_target instrument28 Star13)
	(calibration_target instrument28 GroundStation10)
	(calibration_target instrument28 GroundStation19)
	(calibration_target instrument28 Star28)
	(supports instrument29 image2)
	(supports instrument29 spectrograph0)
	(supports instrument29 infrared4)
	(calibration_target instrument29 GroundStation38)
	(calibration_target instrument29 GroundStation17)
	(calibration_target instrument29 Star25)
	(calibration_target instrument29 GroundStation2)
	(calibration_target instrument29 GroundStation37)
	(calibration_target instrument29 Star33)
	(calibration_target instrument29 GroundStation39)
	(calibration_target instrument29 Star7)
	(calibration_target instrument29 Star15)
	(calibration_target instrument29 Star12)
	(calibration_target instrument29 GroundStation6)
	(calibration_target instrument29 GroundStation45)
	(calibration_target instrument29 Star8)
	(calibration_target instrument29 Star28)
	(supports instrument30 spectrograph0)
	(supports instrument30 image2)
	(calibration_target instrument30 GroundStation6)
	(calibration_target instrument30 GroundStation2)
	(calibration_target instrument30 Star30)
	(calibration_target instrument30 Star22)
	(on_board instrument27 satellite9)
	(on_board instrument28 satellite9)
	(on_board instrument29 satellite9)
	(on_board instrument30 satellite9)
	(power_avail satellite9)
	(pointing satellite9 Star44)
	(supports instrument31 thermograph1)
	(supports instrument31 infrared4)
	(supports instrument31 image3)
	(calibration_target instrument31 GroundStation43)
	(calibration_target instrument31 Star26)
	(supports instrument32 thermograph5)
	(calibration_target instrument32 Star8)
	(calibration_target instrument32 Star15)
	(calibration_target instrument32 Star29)
	(on_board instrument31 satellite10)
	(on_board instrument32 satellite10)
	(power_avail satellite10)
	(pointing satellite10 GroundStation18)
)
(:goal (and
	(pointing satellite1 Planet85)
	(pointing satellite9 Phenomenon64)
	(pointing satellite10 Star59)
	(have_image Phenomenon51 infrared4)
	(have_image Star52 image2)
	(have_image Planet53 image3)
	(have_image Star55 thermograph5)
	(have_image Star56 thermograph1)
	(have_image Planet57 image2)
	(have_image Planet57 thermograph1)
	(have_image Star58 infrared4)
	(have_image Star58 thermograph5)
	(have_image Star59 image2)
	(have_image Star59 infrared4)
	(have_image Star60 spectrograph0)
	(have_image Star60 image3)
	(have_image Star61 image2)
	(have_image Star61 thermograph1)
	(have_image Planet62 image3)
	(have_image Planet62 thermograph1)
	(have_image Phenomenon63 image2)
	(have_image Phenomenon63 thermograph1)
	(have_image Phenomenon64 thermograph5)
	(have_image Phenomenon64 thermograph1)
	(have_image Star65 thermograph5)
	(have_image Star65 image2)
	(have_image Star66 infrared4)
	(have_image Star66 thermograph1)
	(have_image Phenomenon67 infrared4)
	(have_image Phenomenon67 thermograph1)
	(have_image Planet68 image2)
	(have_image Star69 thermograph5)
	(have_image Planet70 image2)
	(have_image Phenomenon71 image2)
	(have_image Star72 image2)
	(have_image Star73 image2)
	(have_image Star73 spectrograph0)
	(have_image Phenomenon74 thermograph5)
	(have_image Phenomenon75 spectrograph0)
	(have_image Phenomenon77 spectrograph0)
	(have_image Planet78 thermograph5)
	(have_image Planet79 image2)
	(have_image Planet79 infrared4)
	(have_image Star80 spectrograph0)
	(have_image Star80 infrared4)
	(have_image Star81 infrared4)
	(have_image Phenomenon82 image3)
	(have_image Star83 thermograph1)
	(have_image Phenomenon84 thermograph5)
	(have_image Phenomenon84 spectrograph0)
	(have_image Planet85 thermograph5)
	(have_image Planet86 thermograph5)
	(have_image Planet86 infrared4)
	(have_image Phenomenon87 thermograph1)
	(have_image Phenomenon87 spectrograph0)
	(have_image Star88 spectrograph0)
	(have_image Phenomenon89 spectrograph0)
	(have_image Phenomenon89 thermograph1)
	(have_image Star90 infrared4)
	(have_image Star91 thermograph5)
	(have_image Star91 spectrograph0)
	(have_image Star92 thermograph1)
	(have_image Star92 image2)
	(have_image Star93 infrared4)
	(have_image Star93 image3)
	(have_image Phenomenon94 spectrograph0)
	(have_image Phenomenon95 thermograph5)
	(have_image Phenomenon96 infrared4)
	(have_image Phenomenon96 thermograph5)
	(have_image Phenomenon97 thermograph1)
	(have_image Phenomenon97 thermograph5)
	(have_image Phenomenon98 infrared4)
	(have_image Phenomenon98 image3)
	(have_image Phenomenon99 thermograph1)
	(have_image Phenomenon99 spectrograph0)
))

)
