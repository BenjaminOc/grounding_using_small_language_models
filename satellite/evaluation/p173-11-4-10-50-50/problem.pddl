(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	satellite1 - satellite
	instrument3 - instrument
	satellite2 - satellite
	instrument4 - instrument
	satellite3 - satellite
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	satellite4 - satellite
	instrument9 - instrument
	satellite5 - satellite
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	satellite6 - satellite
	instrument14 - instrument
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	satellite7 - satellite
	instrument18 - instrument
	instrument19 - instrument
	satellite8 - satellite
	instrument20 - instrument
	instrument21 - instrument
	instrument22 - instrument
	satellite9 - satellite
	instrument23 - instrument
	instrument24 - instrument
	satellite10 - satellite
	instrument25 - instrument
	image6 - mode
	spectrograph7 - mode
	spectrograph2 - mode
	thermograph5 - mode
	image4 - mode
	infrared0 - mode
	image8 - mode
	spectrograph1 - mode
	spectrograph9 - mode
	thermograph3 - mode
	Star32 - direction
	Star31 - direction
	Star0 - direction
	Star19 - direction
	GroundStation10 - direction
	GroundStation21 - direction
	GroundStation8 - direction
	GroundStation27 - direction
	Star29 - direction
	Star48 - direction
	Star5 - direction
	GroundStation13 - direction
	GroundStation25 - direction
	Star46 - direction
	GroundStation40 - direction
	GroundStation23 - direction
	GroundStation45 - direction
	Star3 - direction
	Star36 - direction
	Star7 - direction
	GroundStation43 - direction
	Star11 - direction
	GroundStation26 - direction
	Star12 - direction
	Star18 - direction
	GroundStation33 - direction
	Star4 - direction
	GroundStation9 - direction
	GroundStation14 - direction
	GroundStation2 - direction
	Star39 - direction
	Star15 - direction
	GroundStation16 - direction
	Star34 - direction
	GroundStation17 - direction
	Star6 - direction
	Star24 - direction
	Star47 - direction
	Star1 - direction
	GroundStation30 - direction
	GroundStation22 - direction
	Star28 - direction
	GroundStation38 - direction
	Star41 - direction
	Star20 - direction
	Star42 - direction
	Star49 - direction
	GroundStation44 - direction
	GroundStation35 - direction
	Star37 - direction
	Planet50 - direction
	Star51 - direction
	Star52 - direction
	Planet53 - direction
	Planet54 - direction
	Star55 - direction
	Phenomenon56 - direction
	Planet57 - direction
	Phenomenon58 - direction
	Star59 - direction
	Star60 - direction
	Star61 - direction
	Phenomenon62 - direction
	Planet63 - direction
	Phenomenon64 - direction
	Star65 - direction
	Planet66 - direction
	Phenomenon67 - direction
	Star68 - direction
	Phenomenon69 - direction
	Star70 - direction
	Phenomenon71 - direction
	Phenomenon72 - direction
	Phenomenon73 - direction
	Phenomenon74 - direction
	Planet75 - direction
	Phenomenon76 - direction
	Planet77 - direction
	Planet78 - direction
	Phenomenon79 - direction
	Phenomenon80 - direction
	Star81 - direction
	Planet82 - direction
	Planet83 - direction
	Phenomenon84 - direction
	Phenomenon85 - direction
	Planet86 - direction
	Phenomenon87 - direction
	Planet88 - direction
	Planet89 - direction
	Star90 - direction
	Planet91 - direction
	Star92 - direction
	Planet93 - direction
	Planet94 - direction
	Planet95 - direction
	Phenomenon96 - direction
	Star97 - direction
	Star98 - direction
	Phenomenon99 - direction
)
(:init
	(supports instrument0 image4)
	(supports instrument0 thermograph3)
	(supports instrument0 thermograph5)
	(calibration_target instrument0 Star41)
	(calibration_target instrument0 Star7)
	(calibration_target instrument0 GroundStation45)
	(calibration_target instrument0 Star20)
	(calibration_target instrument0 Star42)
	(calibration_target instrument0 GroundStation23)
	(calibration_target instrument0 Star34)
	(calibration_target instrument0 Star48)
	(calibration_target instrument0 Star39)
	(calibration_target instrument0 GroundStation21)
	(calibration_target instrument0 Star31)
	(calibration_target instrument0 Star24)
	(calibration_target instrument0 GroundStation40)
	(supports instrument1 image8)
	(calibration_target instrument1 GroundStation23)
	(calibration_target instrument1 Star39)
	(calibration_target instrument1 Star28)
	(calibration_target instrument1 GroundStation2)
	(calibration_target instrument1 Star49)
	(calibration_target instrument1 Star42)
	(calibration_target instrument1 Star20)
	(calibration_target instrument1 Star12)
	(calibration_target instrument1 Star36)
	(calibration_target instrument1 Star48)
	(calibration_target instrument1 Star4)
	(supports instrument2 thermograph5)
	(supports instrument2 spectrograph7)
	(supports instrument2 spectrograph2)
	(calibration_target instrument2 GroundStation38)
	(calibration_target instrument2 Star7)
	(calibration_target instrument2 GroundStation25)
	(calibration_target instrument2 Star11)
	(calibration_target instrument2 Star24)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Star6)
	(supports instrument3 infrared0)
	(supports instrument3 spectrograph9)
	(supports instrument3 image8)
	(calibration_target instrument3 GroundStation21)
	(calibration_target instrument3 GroundStation22)
	(calibration_target instrument3 GroundStation43)
	(calibration_target instrument3 Star47)
	(calibration_target instrument3 GroundStation14)
	(calibration_target instrument3 GroundStation9)
	(calibration_target instrument3 Star0)
	(calibration_target instrument3 Star48)
	(calibration_target instrument3 GroundStation33)
	(calibration_target instrument3 GroundStation40)
	(calibration_target instrument3 Star32)
	(calibration_target instrument3 GroundStation2)
	(calibration_target instrument3 Star12)
	(calibration_target instrument3 Star3)
	(on_board instrument3 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star59)
	(supports instrument4 spectrograph1)
	(supports instrument4 image4)
	(calibration_target instrument4 Star28)
	(calibration_target instrument4 Star32)
	(on_board instrument4 satellite2)
	(power_avail satellite2)
	(pointing satellite2 GroundStation14)
	(supports instrument5 thermograph5)
	(supports instrument5 spectrograph1)
	(calibration_target instrument5 GroundStation9)
	(calibration_target instrument5 GroundStation44)
	(calibration_target instrument5 Star4)
	(calibration_target instrument5 GroundStation17)
	(calibration_target instrument5 Star6)
	(calibration_target instrument5 GroundStation23)
	(calibration_target instrument5 GroundStation10)
	(calibration_target instrument5 Star24)
	(calibration_target instrument5 GroundStation30)
	(supports instrument6 infrared0)
	(calibration_target instrument6 GroundStation30)
	(supports instrument7 thermograph3)
	(supports instrument7 spectrograph7)
	(supports instrument7 spectrograph1)
	(calibration_target instrument7 GroundStation43)
	(calibration_target instrument7 GroundStation2)
	(calibration_target instrument7 Star46)
	(calibration_target instrument7 Star12)
	(calibration_target instrument7 GroundStation44)
	(calibration_target instrument7 GroundStation21)
	(calibration_target instrument7 GroundStation22)
	(calibration_target instrument7 GroundStation30)
	(calibration_target instrument7 Star31)
	(calibration_target instrument7 Star24)
	(calibration_target instrument7 GroundStation25)
	(calibration_target instrument7 Star7)
	(calibration_target instrument7 Star15)
	(supports instrument8 spectrograph7)
	(calibration_target instrument8 GroundStation44)
	(calibration_target instrument8 GroundStation23)
	(calibration_target instrument8 Star12)
	(on_board instrument5 satellite3)
	(on_board instrument6 satellite3)
	(on_board instrument7 satellite3)
	(on_board instrument8 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star49)
	(supports instrument9 image8)
	(supports instrument9 spectrograph2)
	(calibration_target instrument9 GroundStation16)
	(calibration_target instrument9 Star5)
	(calibration_target instrument9 GroundStation14)
	(calibration_target instrument9 Star15)
	(calibration_target instrument9 Star34)
	(calibration_target instrument9 GroundStation2)
	(calibration_target instrument9 GroundStation10)
	(calibration_target instrument9 Star19)
	(calibration_target instrument9 Star20)
	(calibration_target instrument9 Star39)
	(calibration_target instrument9 GroundStation44)
	(calibration_target instrument9 Star0)
	(on_board instrument9 satellite4)
	(power_avail satellite4)
	(pointing satellite4 GroundStation9)
	(supports instrument10 spectrograph9)
	(supports instrument10 infrared0)
	(calibration_target instrument10 GroundStation33)
	(calibration_target instrument10 Star3)
	(supports instrument11 image6)
	(supports instrument11 thermograph3)
	(calibration_target instrument11 GroundStation40)
	(calibration_target instrument11 GroundStation22)
	(calibration_target instrument11 GroundStation21)
	(supports instrument12 spectrograph1)
	(calibration_target instrument12 Star12)
	(supports instrument13 spectrograph7)
	(supports instrument13 thermograph5)
	(supports instrument13 image6)
	(calibration_target instrument13 GroundStation25)
	(calibration_target instrument13 GroundStation22)
	(calibration_target instrument13 Star42)
	(calibration_target instrument13 Star3)
	(calibration_target instrument13 Star18)
	(calibration_target instrument13 GroundStation44)
	(calibration_target instrument13 GroundStation8)
	(on_board instrument10 satellite5)
	(on_board instrument11 satellite5)
	(on_board instrument12 satellite5)
	(on_board instrument13 satellite5)
	(power_avail satellite5)
	(pointing satellite5 Star51)
	(supports instrument14 thermograph5)
	(supports instrument14 spectrograph7)
	(calibration_target instrument14 GroundStation27)
	(calibration_target instrument14 Star3)
	(calibration_target instrument14 Star6)
	(supports instrument15 image6)
	(supports instrument15 thermograph5)
	(supports instrument15 thermograph3)
	(calibration_target instrument15 Star15)
	(calibration_target instrument15 GroundStation33)
	(calibration_target instrument15 Star48)
	(calibration_target instrument15 Star34)
	(calibration_target instrument15 Star46)
	(calibration_target instrument15 GroundStation45)
	(calibration_target instrument15 Star28)
	(calibration_target instrument15 GroundStation9)
	(calibration_target instrument15 GroundStation23)
	(calibration_target instrument15 Star29)
	(calibration_target instrument15 Star20)
	(calibration_target instrument15 Star24)
	(calibration_target instrument15 GroundStation43)
	(calibration_target instrument15 Star3)
	(supports instrument16 thermograph3)
	(supports instrument16 spectrograph7)
	(supports instrument16 image8)
	(calibration_target instrument16 Star34)
	(calibration_target instrument16 GroundStation13)
	(calibration_target instrument16 GroundStation26)
	(calibration_target instrument16 Star41)
	(calibration_target instrument16 Star39)
	(calibration_target instrument16 GroundStation2)
	(calibration_target instrument16 GroundStation44)
	(calibration_target instrument16 Star5)
	(calibration_target instrument16 Star46)
	(supports instrument17 image4)
	(supports instrument17 thermograph3)
	(supports instrument17 spectrograph2)
	(calibration_target instrument17 Star20)
	(calibration_target instrument17 GroundStation38)
	(calibration_target instrument17 Star36)
	(calibration_target instrument17 GroundStation16)
	(on_board instrument14 satellite6)
	(on_board instrument15 satellite6)
	(on_board instrument16 satellite6)
	(on_board instrument17 satellite6)
	(power_avail satellite6)
	(pointing satellite6 Star59)
	(supports instrument18 spectrograph2)
	(supports instrument18 spectrograph1)
	(calibration_target instrument18 GroundStation45)
	(calibration_target instrument18 GroundStation30)
	(calibration_target instrument18 GroundStation33)
	(calibration_target instrument18 Star37)
	(calibration_target instrument18 Star11)
	(calibration_target instrument18 GroundStation22)
	(calibration_target instrument18 GroundStation35)
	(supports instrument19 spectrograph2)
	(supports instrument19 infrared0)
	(supports instrument19 spectrograph7)
	(calibration_target instrument19 Star24)
	(calibration_target instrument19 GroundStation14)
	(calibration_target instrument19 Star3)
	(calibration_target instrument19 GroundStation33)
	(calibration_target instrument19 GroundStation45)
	(calibration_target instrument19 GroundStation23)
	(calibration_target instrument19 GroundStation16)
	(calibration_target instrument19 GroundStation40)
	(calibration_target instrument19 Star1)
	(calibration_target instrument19 Star15)
	(calibration_target instrument19 Star46)
	(calibration_target instrument19 GroundStation25)
	(on_board instrument18 satellite7)
	(on_board instrument19 satellite7)
	(power_avail satellite7)
	(pointing satellite7 Planet86)
	(supports instrument20 image8)
	(supports instrument20 spectrograph9)
	(calibration_target instrument20 GroundStation43)
	(calibration_target instrument20 GroundStation38)
	(calibration_target instrument20 Star7)
	(calibration_target instrument20 GroundStation22)
	(calibration_target instrument20 Star36)
	(supports instrument21 thermograph3)
	(calibration_target instrument21 GroundStation30)
	(calibration_target instrument21 Star34)
	(calibration_target instrument21 GroundStation16)
	(calibration_target instrument21 Star15)
	(calibration_target instrument21 Star24)
	(calibration_target instrument21 Star39)
	(calibration_target instrument21 GroundStation2)
	(calibration_target instrument21 GroundStation14)
	(calibration_target instrument21 GroundStation9)
	(calibration_target instrument21 Star4)
	(calibration_target instrument21 GroundStation33)
	(calibration_target instrument21 Star18)
	(calibration_target instrument21 Star12)
	(calibration_target instrument21 GroundStation26)
	(calibration_target instrument21 Star37)
	(calibration_target instrument21 Star11)
	(supports instrument22 spectrograph2)
	(supports instrument22 spectrograph1)
	(calibration_target instrument22 Star42)
	(calibration_target instrument22 Star24)
	(calibration_target instrument22 Star6)
	(calibration_target instrument22 GroundStation22)
	(calibration_target instrument22 Star49)
	(calibration_target instrument22 GroundStation17)
	(on_board instrument20 satellite8)
	(on_board instrument21 satellite8)
	(on_board instrument22 satellite8)
	(power_avail satellite8)
	(pointing satellite8 Planet93)
	(supports instrument23 image4)
	(supports instrument23 thermograph5)
	(calibration_target instrument23 Star28)
	(calibration_target instrument23 GroundStation22)
	(calibration_target instrument23 GroundStation35)
	(calibration_target instrument23 GroundStation30)
	(calibration_target instrument23 Star1)
	(calibration_target instrument23 Star47)
	(calibration_target instrument23 GroundStation44)
	(supports instrument24 image8)
	(supports instrument24 spectrograph1)
	(supports instrument24 infrared0)
	(calibration_target instrument24 Star49)
	(calibration_target instrument24 Star42)
	(calibration_target instrument24 Star20)
	(calibration_target instrument24 Star41)
	(calibration_target instrument24 GroundStation38)
	(calibration_target instrument24 GroundStation44)
	(on_board instrument23 satellite9)
	(on_board instrument24 satellite9)
	(power_avail satellite9)
	(pointing satellite9 Planet95)
	(supports instrument25 thermograph3)
	(supports instrument25 spectrograph9)
	(supports instrument25 spectrograph1)
	(calibration_target instrument25 Star37)
	(calibration_target instrument25 GroundStation35)
	(calibration_target instrument25 GroundStation44)
	(on_board instrument25 satellite10)
	(power_avail satellite10)
	(pointing satellite10 Phenomenon87)
)
(:goal (and
	(pointing satellite0 Planet53)
	(pointing satellite1 GroundStation44)
	(pointing satellite2 Phenomenon85)
	(pointing satellite3 GroundStation9)
	(pointing satellite4 GroundStation40)
	(pointing satellite5 Star59)
	(pointing satellite7 Star46)
	(pointing satellite10 GroundStation22)
	(have_image Star51 spectrograph9)
	(have_image Star52 infrared0)
	(have_image Star52 thermograph5)
	(have_image Star52 image4)
	(have_image Planet53 image6)
	(have_image Planet53 image4)
	(have_image Planet53 thermograph3)
	(have_image Star55 image8)
	(have_image Star55 spectrograph2)
	(have_image Star55 thermograph3)
	(have_image Phenomenon56 spectrograph7)
	(have_image Phenomenon56 thermograph3)
	(have_image Planet57 thermograph5)
	(have_image Planet57 spectrograph9)
	(have_image Planet57 thermograph3)
	(have_image Phenomenon58 spectrograph7)
	(have_image Phenomenon58 spectrograph2)
	(have_image Phenomenon58 image6)
	(have_image Star59 spectrograph9)
	(have_image Star59 spectrograph1)
	(have_image Star60 image8)
	(have_image Star60 infrared0)
	(have_image Star60 spectrograph7)
	(have_image Star61 image4)
	(have_image Star61 thermograph5)
	(have_image Phenomenon62 infrared0)
	(have_image Phenomenon62 spectrograph7)
	(have_image Phenomenon62 image8)
	(have_image Phenomenon64 image8)
	(have_image Star65 spectrograph1)
	(have_image Star65 image8)
	(have_image Star65 spectrograph7)
	(have_image Planet66 spectrograph7)
	(have_image Planet66 image6)
	(have_image Planet66 thermograph5)
	(have_image Phenomenon67 infrared0)
	(have_image Star68 spectrograph2)
	(have_image Star68 thermograph5)
	(have_image Phenomenon69 spectrograph2)
	(have_image Phenomenon69 spectrograph7)
	(have_image Phenomenon69 infrared0)
	(have_image Phenomenon71 image4)
	(have_image Phenomenon71 spectrograph2)
	(have_image Phenomenon71 thermograph5)
	(have_image Phenomenon72 infrared0)
	(have_image Phenomenon72 image6)
	(have_image Phenomenon73 infrared0)
	(have_image Phenomenon73 spectrograph2)
	(have_image Phenomenon74 thermograph3)
	(have_image Phenomenon74 spectrograph7)
	(have_image Planet75 thermograph3)
	(have_image Phenomenon76 thermograph3)
	(have_image Planet77 spectrograph7)
	(have_image Planet77 spectrograph1)
	(have_image Planet78 image8)
	(have_image Phenomenon79 spectrograph7)
	(have_image Phenomenon79 spectrograph9)
	(have_image Phenomenon79 spectrograph1)
	(have_image Phenomenon80 image4)
	(have_image Phenomenon80 image8)
	(have_image Star81 spectrograph7)
	(have_image Planet82 image4)
	(have_image Planet82 spectrograph7)
	(have_image Planet83 image4)
	(have_image Planet83 image6)
	(have_image Phenomenon84 spectrograph9)
	(have_image Phenomenon84 thermograph3)
	(have_image Planet86 image8)
	(have_image Planet86 thermograph3)
	(have_image Planet86 spectrograph1)
	(have_image Phenomenon87 spectrograph1)
	(have_image Phenomenon87 spectrograph9)
	(have_image Phenomenon87 spectrograph2)
	(have_image Planet88 image4)
	(have_image Planet89 infrared0)
	(have_image Planet91 spectrograph9)
	(have_image Planet93 spectrograph1)
	(have_image Planet93 spectrograph2)
	(have_image Planet94 image4)
	(have_image Planet95 infrared0)
	(have_image Planet95 spectrograph2)
	(have_image Planet95 spectrograph7)
	(have_image Phenomenon96 image4)
	(have_image Star97 thermograph5)
	(have_image Star98 spectrograph7)
	(have_image Star98 spectrograph2)
	(have_image Star98 thermograph3)
	(have_image Phenomenon99 spectrograph1)
))

)
