(define (problem ZTRAVEL-23-102)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	plane11 - aircraft
	plane12 - aircraft
	plane13 - aircraft
	plane14 - aircraft
	plane15 - aircraft
	plane16 - aircraft
	plane17 - aircraft
	plane18 - aircraft
	plane19 - aircraft
	plane20 - aircraft
	plane21 - aircraft
	plane22 - aircraft
	plane23 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	person11 - person
	person12 - person
	person13 - person
	person14 - person
	person15 - person
	person16 - person
	person17 - person
	person18 - person
	person19 - person
	person20 - person
	person21 - person
	person22 - person
	person23 - person
	person24 - person
	person25 - person
	person26 - person
	person27 - person
	person28 - person
	person29 - person
	person30 - person
	person31 - person
	person32 - person
	person33 - person
	person34 - person
	person35 - person
	person36 - person
	person37 - person
	person38 - person
	person39 - person
	person40 - person
	person41 - person
	person42 - person
	person43 - person
	person44 - person
	person45 - person
	person46 - person
	person47 - person
	person48 - person
	person49 - person
	person50 - person
	person51 - person
	person52 - person
	person53 - person
	person54 - person
	person55 - person
	person56 - person
	person57 - person
	person58 - person
	person59 - person
	person60 - person
	person61 - person
	person62 - person
	person63 - person
	person64 - person
	person65 - person
	person66 - person
	person67 - person
	person68 - person
	person69 - person
	person70 - person
	person71 - person
	person72 - person
	person73 - person
	person74 - person
	person75 - person
	person76 - person
	person77 - person
	person78 - person
	person79 - person
	person80 - person
	person81 - person
	person82 - person
	person83 - person
	person84 - person
	person85 - person
	person86 - person
	person87 - person
	person88 - person
	person89 - person
	person90 - person
	person91 - person
	person92 - person
	person93 - person
	person94 - person
	person95 - person
	person96 - person
	person97 - person
	person98 - person
	person99 - person
	person100 - person
	person101 - person
	person102 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	city10 - city
	city11 - city
	city12 - city
	city13 - city
	city14 - city
	city15 - city
	city16 - city
	city17 - city
	city18 - city
	fl0 - flevel
	fl1 - flevel
	fl2 - flevel
	fl3 - flevel
	fl4 - flevel
	fl5 - flevel
	fl6 - flevel
	)
(:init
	(ata plane1 city15)
	(fuel-level plane1 fl0)
	(ata plane2 city0)
	(fuel-level plane2 fl0)
	(ata plane3 city13)
	(fuel-level plane3 fl0)
	(ata plane4 city0)
	(fuel-level plane4 fl0)
	(ata plane5 city6)
	(fuel-level plane5 fl0)
	(ata plane6 city15)
	(fuel-level plane6 fl0)
	(ata plane7 city8)
	(fuel-level plane7 fl0)
	(ata plane8 city0)
	(fuel-level plane8 fl0)
	(ata plane9 city12)
	(fuel-level plane9 fl0)
	(ata plane10 city17)
	(fuel-level plane10 fl0)
	(ata plane11 city10)
	(fuel-level plane11 fl0)
	(ata plane12 city2)
	(fuel-level plane12 fl0)
	(ata plane13 city4)
	(fuel-level plane13 fl0)
	(ata plane14 city3)
	(fuel-level plane14 fl0)
	(ata plane15 city3)
	(fuel-level plane15 fl0)
	(ata plane16 city10)
	(fuel-level plane16 fl0)
	(ata plane17 city9)
	(fuel-level plane17 fl0)
	(ata plane18 city8)
	(fuel-level plane18 fl0)
	(ata plane19 city3)
	(fuel-level plane19 fl0)
	(ata plane20 city16)
	(fuel-level plane20 fl0)
	(ata plane21 city7)
	(fuel-level plane21 fl0)
	(ata plane22 city6)
	(fuel-level plane22 fl0)
	(ata plane23 city8)
	(fuel-level plane23 fl0)
	(atp person1 city9)
	(atp person2 city0)
	(atp person3 city13)
	(atp person4 city1)
	(atp person5 city7)
	(atp person6 city12)
	(atp person7 city16)
	(atp person8 city17)
	(atp person9 city2)
	(atp person10 city0)
	(atp person11 city10)
	(atp person12 city1)
	(atp person13 city10)
	(atp person14 city8)
	(atp person15 city6)
	(atp person16 city4)
	(atp person17 city4)
	(atp person18 city13)
	(atp person19 city5)
	(atp person20 city9)
	(atp person21 city11)
	(atp person22 city11)
	(atp person23 city0)
	(atp person24 city13)
	(atp person25 city16)
	(atp person26 city18)
	(atp person27 city9)
	(atp person28 city1)
	(atp person29 city14)
	(atp person30 city7)
	(atp person31 city4)
	(atp person32 city14)
	(atp person33 city0)
	(atp person34 city12)
	(atp person35 city15)
	(atp person36 city15)
	(atp person37 city7)
	(atp person38 city6)
	(atp person39 city12)
	(atp person40 city7)
	(atp person41 city11)
	(atp person42 city16)
	(atp person43 city4)
	(atp person44 city4)
	(atp person45 city7)
	(atp person46 city0)
	(atp person47 city5)
	(atp person48 city8)
	(atp person49 city4)
	(atp person50 city16)
	(atp person51 city17)
	(atp person52 city18)
	(atp person53 city11)
	(atp person54 city16)
	(atp person55 city9)
	(atp person56 city6)
	(atp person57 city7)
	(atp person58 city1)
	(atp person59 city10)
	(atp person60 city8)
	(atp person61 city4)
	(atp person62 city2)
	(atp person63 city16)
	(atp person64 city9)
	(atp person65 city13)
	(atp person66 city4)
	(atp person67 city18)
	(atp person68 city4)
	(atp person69 city8)
	(atp person70 city3)
	(atp person71 city17)
	(atp person72 city4)
	(atp person73 city18)
	(atp person74 city7)
	(atp person75 city12)
	(atp person76 city5)
	(atp person77 city5)
	(atp person78 city17)
	(atp person79 city12)
	(atp person80 city14)
	(atp person81 city0)
	(atp person82 city7)
	(atp person83 city15)
	(atp person84 city17)
	(atp person85 city9)
	(atp person86 city2)
	(atp person87 city1)
	(atp person88 city15)
	(atp person89 city7)
	(atp person90 city6)
	(atp person91 city2)
	(atp person92 city17)
	(atp person93 city17)
	(atp person94 city18)
	(atp person95 city10)
	(atp person96 city6)
	(atp person97 city12)
	(atp person98 city7)
	(atp person99 city15)
	(atp person100 city7)
	(atp person101 city5)
	(atp person102 city9)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
)
(:goal (and
	(ata plane2 city13)
	(ata plane5 city13)
	(ata plane9 city9)
	(ata plane11 city18)
	(ata plane12 city7)
	(ata plane13 city11)
	(ata plane16 city18)
	(ata plane17 city3)
	(atp person1 city15)
	(atp person2 city8)
	(atp person3 city4)
	(atp person4 city8)
	(atp person5 city18)
	(atp person7 city1)
	(atp person8 city2)
	(atp person9 city18)
	(atp person10 city3)
	(atp person11 city13)
	(atp person12 city13)
	(atp person13 city8)
	(atp person14 city9)
	(atp person15 city16)
	(atp person16 city10)
	(atp person17 city8)
	(atp person18 city6)
	(atp person19 city8)
	(atp person20 city8)
	(atp person21 city0)
	(atp person22 city2)
	(atp person23 city13)
	(atp person24 city2)
	(atp person26 city12)
	(atp person27 city16)
	(atp person28 city11)
	(atp person29 city17)
	(atp person30 city7)
	(atp person31 city0)
	(atp person32 city11)
	(atp person33 city12)
	(atp person34 city6)
	(atp person35 city4)
	(atp person36 city3)
	(atp person37 city12)
	(atp person38 city13)
	(atp person39 city9)
	(atp person40 city16)
	(atp person41 city1)
	(atp person42 city16)
	(atp person43 city16)
	(atp person44 city10)
	(atp person45 city6)
	(atp person46 city3)
	(atp person47 city10)
	(atp person48 city17)
	(atp person49 city10)
	(atp person50 city18)
	(atp person51 city11)
	(atp person52 city8)
	(atp person53 city12)
	(atp person54 city17)
	(atp person55 city6)
	(atp person56 city6)
	(atp person57 city12)
	(atp person58 city2)
	(atp person59 city6)
	(atp person60 city3)
	(atp person61 city2)
	(atp person62 city1)
	(atp person63 city12)
	(atp person64 city9)
	(atp person65 city0)
	(atp person66 city7)
	(atp person67 city14)
	(atp person68 city15)
	(atp person69 city7)
	(atp person70 city15)
	(atp person71 city1)
	(atp person72 city3)
	(atp person73 city0)
	(atp person74 city10)
	(atp person75 city4)
	(atp person76 city8)
	(atp person77 city8)
	(atp person78 city13)
	(atp person79 city2)
	(atp person80 city6)
	(atp person81 city5)
	(atp person82 city9)
	(atp person83 city9)
	(atp person84 city16)
	(atp person85 city10)
	(atp person86 city16)
	(atp person87 city2)
	(atp person88 city1)
	(atp person89 city14)
	(atp person90 city10)
	(atp person91 city10)
	(atp person92 city18)
	(atp person93 city14)
	(atp person94 city13)
	(atp person95 city3)
	(atp person96 city5)
	(atp person97 city7)
	(atp person98 city4)
	(atp person99 city12)
	(atp person100 city18)
	(atp person101 city1)
	(atp person102 city0)
	))

)
