(define (problem ZTRAVEL-19-101)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	plane11 - aircraft
	plane12 - aircraft
	plane13 - aircraft
	plane14 - aircraft
	plane15 - aircraft
	plane16 - aircraft
	plane17 - aircraft
	plane18 - aircraft
	plane19 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	person11 - person
	person12 - person
	person13 - person
	person14 - person
	person15 - person
	person16 - person
	person17 - person
	person18 - person
	person19 - person
	person20 - person
	person21 - person
	person22 - person
	person23 - person
	person24 - person
	person25 - person
	person26 - person
	person27 - person
	person28 - person
	person29 - person
	person30 - person
	person31 - person
	person32 - person
	person33 - person
	person34 - person
	person35 - person
	person36 - person
	person37 - person
	person38 - person
	person39 - person
	person40 - person
	person41 - person
	person42 - person
	person43 - person
	person44 - person
	person45 - person
	person46 - person
	person47 - person
	person48 - person
	person49 - person
	person50 - person
	person51 - person
	person52 - person
	person53 - person
	person54 - person
	person55 - person
	person56 - person
	person57 - person
	person58 - person
	person59 - person
	person60 - person
	person61 - person
	person62 - person
	person63 - person
	person64 - person
	person65 - person
	person66 - person
	person67 - person
	person68 - person
	person69 - person
	person70 - person
	person71 - person
	person72 - person
	person73 - person
	person74 - person
	person75 - person
	person76 - person
	person77 - person
	person78 - person
	person79 - person
	person80 - person
	person81 - person
	person82 - person
	person83 - person
	person84 - person
	person85 - person
	person86 - person
	person87 - person
	person88 - person
	person89 - person
	person90 - person
	person91 - person
	person92 - person
	person93 - person
	person94 - person
	person95 - person
	person96 - person
	person97 - person
	person98 - person
	person99 - person
	person100 - person
	person101 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	city10 - city
	city11 - city
	city12 - city
	city13 - city
	city14 - city
	fl0 - flevel
	fl1 - flevel
	fl2 - flevel
	fl3 - flevel
	fl4 - flevel
	fl5 - flevel
	fl6 - flevel
	)
(:init
	(ata plane1 city10)
	(fuel-level plane1 fl0)
	(ata plane2 city7)
	(fuel-level plane2 fl0)
	(ata plane3 city14)
	(fuel-level plane3 fl0)
	(ata plane4 city14)
	(fuel-level plane4 fl0)
	(ata plane5 city13)
	(fuel-level plane5 fl0)
	(ata plane6 city3)
	(fuel-level plane6 fl0)
	(ata plane7 city12)
	(fuel-level plane7 fl0)
	(ata plane8 city0)
	(fuel-level plane8 fl0)
	(ata plane9 city10)
	(fuel-level plane9 fl0)
	(ata plane10 city0)
	(fuel-level plane10 fl0)
	(ata plane11 city4)
	(fuel-level plane11 fl0)
	(ata plane12 city12)
	(fuel-level plane12 fl0)
	(ata plane13 city6)
	(fuel-level plane13 fl0)
	(ata plane14 city0)
	(fuel-level plane14 fl0)
	(ata plane15 city9)
	(fuel-level plane15 fl0)
	(ata plane16 city13)
	(fuel-level plane16 fl0)
	(ata plane17 city8)
	(fuel-level plane17 fl0)
	(ata plane18 city1)
	(fuel-level plane18 fl0)
	(ata plane19 city3)
	(fuel-level plane19 fl0)
	(atp person1 city2)
	(atp person2 city12)
	(atp person3 city0)
	(atp person4 city1)
	(atp person5 city7)
	(atp person6 city1)
	(atp person7 city9)
	(atp person8 city3)
	(atp person9 city6)
	(atp person10 city3)
	(atp person11 city14)
	(atp person12 city7)
	(atp person13 city5)
	(atp person14 city2)
	(atp person15 city5)
	(atp person16 city9)
	(atp person17 city12)
	(atp person18 city3)
	(atp person19 city3)
	(atp person20 city9)
	(atp person21 city0)
	(atp person22 city14)
	(atp person23 city13)
	(atp person24 city12)
	(atp person25 city2)
	(atp person26 city9)
	(atp person27 city9)
	(atp person28 city14)
	(atp person29 city13)
	(atp person30 city6)
	(atp person31 city0)
	(atp person32 city14)
	(atp person33 city1)
	(atp person34 city6)
	(atp person35 city1)
	(atp person36 city8)
	(atp person37 city14)
	(atp person38 city12)
	(atp person39 city6)
	(atp person40 city3)
	(atp person41 city6)
	(atp person42 city14)
	(atp person43 city5)
	(atp person44 city0)
	(atp person45 city1)
	(atp person46 city14)
	(atp person47 city2)
	(atp person48 city10)
	(atp person49 city10)
	(atp person50 city6)
	(atp person51 city7)
	(atp person52 city12)
	(atp person53 city8)
	(atp person54 city6)
	(atp person55 city4)
	(atp person56 city6)
	(atp person57 city6)
	(atp person58 city0)
	(atp person59 city1)
	(atp person60 city10)
	(atp person61 city1)
	(atp person62 city6)
	(atp person63 city9)
	(atp person64 city13)
	(atp person65 city8)
	(atp person66 city13)
	(atp person67 city5)
	(atp person68 city0)
	(atp person69 city9)
	(atp person70 city9)
	(atp person71 city5)
	(atp person72 city3)
	(atp person73 city2)
	(atp person74 city10)
	(atp person75 city10)
	(atp person76 city7)
	(atp person77 city13)
	(atp person78 city1)
	(atp person79 city13)
	(atp person80 city13)
	(atp person81 city8)
	(atp person82 city5)
	(atp person83 city2)
	(atp person84 city8)
	(atp person85 city13)
	(atp person86 city8)
	(atp person87 city14)
	(atp person88 city8)
	(atp person89 city6)
	(atp person90 city9)
	(atp person91 city13)
	(atp person92 city4)
	(atp person93 city5)
	(atp person94 city9)
	(atp person95 city1)
	(atp person96 city4)
	(atp person97 city2)
	(atp person98 city1)
	(atp person99 city0)
	(atp person100 city9)
	(atp person101 city7)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
)
(:goal (and
	(ata plane1 city14)
	(ata plane3 city8)
	(ata plane8 city10)
	(ata plane11 city10)
	(ata plane15 city7)
	(ata plane17 city14)
	(ata plane18 city6)
	(ata plane19 city9)
	(atp person1 city8)
	(atp person2 city14)
	(atp person3 city0)
	(atp person4 city6)
	(atp person5 city1)
	(atp person6 city7)
	(atp person7 city5)
	(atp person8 city8)
	(atp person9 city8)
	(atp person10 city2)
	(atp person11 city4)
	(atp person12 city2)
	(atp person13 city2)
	(atp person14 city4)
	(atp person15 city12)
	(atp person16 city13)
	(atp person17 city7)
	(atp person18 city11)
	(atp person19 city2)
	(atp person20 city5)
	(atp person21 city5)
	(atp person22 city12)
	(atp person23 city3)
	(atp person24 city6)
	(atp person25 city11)
	(atp person26 city6)
	(atp person27 city3)
	(atp person28 city7)
	(atp person29 city10)
	(atp person30 city4)
	(atp person31 city4)
	(atp person32 city4)
	(atp person33 city2)
	(atp person34 city0)
	(atp person35 city3)
	(atp person36 city13)
	(atp person37 city8)
	(atp person39 city6)
	(atp person40 city12)
	(atp person41 city13)
	(atp person42 city13)
	(atp person43 city0)
	(atp person44 city7)
	(atp person45 city8)
	(atp person47 city2)
	(atp person48 city14)
	(atp person49 city6)
	(atp person50 city10)
	(atp person51 city1)
	(atp person52 city1)
	(atp person53 city7)
	(atp person54 city8)
	(atp person55 city10)
	(atp person56 city9)
	(atp person57 city12)
	(atp person58 city8)
	(atp person59 city3)
	(atp person60 city9)
	(atp person61 city2)
	(atp person62 city0)
	(atp person63 city8)
	(atp person64 city14)
	(atp person65 city4)
	(atp person66 city11)
	(atp person67 city3)
	(atp person68 city3)
	(atp person70 city0)
	(atp person71 city2)
	(atp person72 city9)
	(atp person73 city4)
	(atp person74 city2)
	(atp person75 city11)
	(atp person76 city9)
	(atp person77 city0)
	(atp person78 city0)
	(atp person79 city9)
	(atp person80 city4)
	(atp person81 city9)
	(atp person83 city0)
	(atp person84 city10)
	(atp person85 city6)
	(atp person86 city13)
	(atp person87 city11)
	(atp person88 city13)
	(atp person89 city11)
	(atp person90 city9)
	(atp person91 city2)
	(atp person92 city8)
	(atp person93 city10)
	(atp person94 city4)
	(atp person95 city2)
	(atp person96 city11)
	(atp person97 city10)
	(atp person98 city4)
	(atp person99 city11)
	(atp person100 city5)
	(atp person101 city4)
	))

)
