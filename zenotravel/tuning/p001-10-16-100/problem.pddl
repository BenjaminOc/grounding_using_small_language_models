(define (problem ZTRAVEL-16-100)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	plane11 - aircraft
	plane12 - aircraft
	plane13 - aircraft
	plane14 - aircraft
	plane15 - aircraft
	plane16 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	person11 - person
	person12 - person
	person13 - person
	person14 - person
	person15 - person
	person16 - person
	person17 - person
	person18 - person
	person19 - person
	person20 - person
	person21 - person
	person22 - person
	person23 - person
	person24 - person
	person25 - person
	person26 - person
	person27 - person
	person28 - person
	person29 - person
	person30 - person
	person31 - person
	person32 - person
	person33 - person
	person34 - person
	person35 - person
	person36 - person
	person37 - person
	person38 - person
	person39 - person
	person40 - person
	person41 - person
	person42 - person
	person43 - person
	person44 - person
	person45 - person
	person46 - person
	person47 - person
	person48 - person
	person49 - person
	person50 - person
	person51 - person
	person52 - person
	person53 - person
	person54 - person
	person55 - person
	person56 - person
	person57 - person
	person58 - person
	person59 - person
	person60 - person
	person61 - person
	person62 - person
	person63 - person
	person64 - person
	person65 - person
	person66 - person
	person67 - person
	person68 - person
	person69 - person
	person70 - person
	person71 - person
	person72 - person
	person73 - person
	person74 - person
	person75 - person
	person76 - person
	person77 - person
	person78 - person
	person79 - person
	person80 - person
	person81 - person
	person82 - person
	person83 - person
	person84 - person
	person85 - person
	person86 - person
	person87 - person
	person88 - person
	person89 - person
	person90 - person
	person91 - person
	person92 - person
	person93 - person
	person94 - person
	person95 - person
	person96 - person
	person97 - person
	person98 - person
	person99 - person
	person100 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	fl0 - flevel
	fl1 - flevel
	fl2 - flevel
	fl3 - flevel
	fl4 - flevel
	fl5 - flevel
	fl6 - flevel
	)
(:init
	(ata plane1 city7)
	(fuel-level plane1 fl0)
	(ata plane2 city8)
	(fuel-level plane2 fl0)
	(ata plane3 city4)
	(fuel-level plane3 fl0)
	(ata plane4 city5)
	(fuel-level plane4 fl0)
	(ata plane5 city6)
	(fuel-level plane5 fl0)
	(ata plane6 city6)
	(fuel-level plane6 fl0)
	(ata plane7 city4)
	(fuel-level plane7 fl0)
	(ata plane8 city3)
	(fuel-level plane8 fl0)
	(ata plane9 city3)
	(fuel-level plane9 fl0)
	(ata plane10 city9)
	(fuel-level plane10 fl0)
	(ata plane11 city5)
	(fuel-level plane11 fl0)
	(ata plane12 city7)
	(fuel-level plane12 fl0)
	(ata plane13 city9)
	(fuel-level plane13 fl0)
	(ata plane14 city2)
	(fuel-level plane14 fl0)
	(ata plane15 city0)
	(fuel-level plane15 fl0)
	(ata plane16 city3)
	(fuel-level plane16 fl0)
	(atp person1 city2)
	(atp person2 city9)
	(atp person3 city4)
	(atp person4 city4)
	(atp person5 city2)
	(atp person6 city0)
	(atp person7 city6)
	(atp person8 city1)
	(atp person9 city4)
	(atp person10 city0)
	(atp person11 city4)
	(atp person12 city7)
	(atp person13 city5)
	(atp person14 city5)
	(atp person15 city5)
	(atp person16 city6)
	(atp person17 city8)
	(atp person18 city3)
	(atp person19 city2)
	(atp person20 city9)
	(atp person21 city5)
	(atp person22 city5)
	(atp person23 city3)
	(atp person24 city0)
	(atp person25 city4)
	(atp person26 city6)
	(atp person27 city4)
	(atp person28 city8)
	(atp person29 city2)
	(atp person30 city9)
	(atp person31 city5)
	(atp person32 city1)
	(atp person33 city8)
	(atp person34 city0)
	(atp person35 city1)
	(atp person36 city4)
	(atp person37 city0)
	(atp person38 city6)
	(atp person39 city2)
	(atp person40 city4)
	(atp person41 city2)
	(atp person42 city9)
	(atp person43 city5)
	(atp person44 city3)
	(atp person45 city1)
	(atp person46 city3)
	(atp person47 city6)
	(atp person48 city8)
	(atp person49 city2)
	(atp person50 city2)
	(atp person51 city6)
	(atp person52 city0)
	(atp person53 city9)
	(atp person54 city8)
	(atp person55 city8)
	(atp person56 city1)
	(atp person57 city6)
	(atp person58 city6)
	(atp person59 city9)
	(atp person60 city9)
	(atp person61 city4)
	(atp person62 city0)
	(atp person63 city9)
	(atp person64 city1)
	(atp person65 city4)
	(atp person66 city1)
	(atp person67 city5)
	(atp person68 city9)
	(atp person69 city8)
	(atp person70 city4)
	(atp person71 city2)
	(atp person72 city4)
	(atp person73 city9)
	(atp person74 city3)
	(atp person75 city0)
	(atp person76 city1)
	(atp person77 city9)
	(atp person78 city1)
	(atp person79 city6)
	(atp person80 city7)
	(atp person81 city4)
	(atp person82 city4)
	(atp person83 city8)
	(atp person84 city5)
	(atp person85 city4)
	(atp person86 city3)
	(atp person87 city4)
	(atp person88 city4)
	(atp person89 city0)
	(atp person90 city1)
	(atp person91 city6)
	(atp person92 city1)
	(atp person93 city4)
	(atp person94 city6)
	(atp person95 city8)
	(atp person96 city5)
	(atp person97 city8)
	(atp person98 city3)
	(atp person99 city0)
	(atp person100 city6)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
)
(:goal (and
	(ata plane7 city8)
	(ata plane11 city1)
	(atp person1 city4)
	(atp person2 city7)
	(atp person3 city8)
	(atp person4 city7)
	(atp person5 city9)
	(atp person6 city4)
	(atp person7 city6)
	(atp person8 city0)
	(atp person9 city3)
	(atp person10 city7)
	(atp person11 city0)
	(atp person12 city0)
	(atp person13 city4)
	(atp person14 city9)
	(atp person15 city2)
	(atp person16 city2)
	(atp person17 city9)
	(atp person18 city1)
	(atp person19 city5)
	(atp person20 city5)
	(atp person21 city9)
	(atp person22 city7)
	(atp person23 city3)
	(atp person24 city8)
	(atp person25 city4)
	(atp person26 city0)
	(atp person27 city8)
	(atp person28 city2)
	(atp person29 city5)
	(atp person30 city4)
	(atp person31 city9)
	(atp person32 city5)
	(atp person33 city9)
	(atp person34 city0)
	(atp person35 city4)
	(atp person36 city1)
	(atp person37 city5)
	(atp person38 city3)
	(atp person39 city5)
	(atp person40 city5)
	(atp person41 city1)
	(atp person42 city3)
	(atp person43 city1)
	(atp person44 city1)
	(atp person45 city3)
	(atp person46 city8)
	(atp person47 city9)
	(atp person48 city5)
	(atp person49 city7)
	(atp person50 city1)
	(atp person51 city3)
	(atp person52 city3)
	(atp person53 city8)
	(atp person54 city2)
	(atp person55 city4)
	(atp person56 city7)
	(atp person57 city4)
	(atp person58 city2)
	(atp person59 city5)
	(atp person60 city7)
	(atp person61 city3)
	(atp person62 city3)
	(atp person63 city2)
	(atp person64 city1)
	(atp person65 city0)
	(atp person66 city2)
	(atp person67 city8)
	(atp person68 city5)
	(atp person70 city4)
	(atp person71 city8)
	(atp person72 city8)
	(atp person73 city8)
	(atp person74 city0)
	(atp person75 city4)
	(atp person76 city5)
	(atp person78 city1)
	(atp person79 city9)
	(atp person80 city4)
	(atp person81 city6)
	(atp person82 city1)
	(atp person83 city1)
	(atp person84 city5)
	(atp person85 city5)
	(atp person86 city6)
	(atp person87 city6)
	(atp person88 city8)
	(atp person89 city5)
	(atp person90 city2)
	(atp person91 city6)
	(atp person92 city1)
	(atp person93 city0)
	(atp person94 city5)
	(atp person95 city9)
	(atp person96 city3)
	(atp person97 city7)
	(atp person98 city2)
	(atp person99 city2)
	))

)
