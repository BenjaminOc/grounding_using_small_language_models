(define (problem ZTRAVEL-22-125)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	plane11 - aircraft
	plane12 - aircraft
	plane13 - aircraft
	plane14 - aircraft
	plane15 - aircraft
	plane16 - aircraft
	plane17 - aircraft
	plane18 - aircraft
	plane19 - aircraft
	plane20 - aircraft
	plane21 - aircraft
	plane22 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	person11 - person
	person12 - person
	person13 - person
	person14 - person
	person15 - person
	person16 - person
	person17 - person
	person18 - person
	person19 - person
	person20 - person
	person21 - person
	person22 - person
	person23 - person
	person24 - person
	person25 - person
	person26 - person
	person27 - person
	person28 - person
	person29 - person
	person30 - person
	person31 - person
	person32 - person
	person33 - person
	person34 - person
	person35 - person
	person36 - person
	person37 - person
	person38 - person
	person39 - person
	person40 - person
	person41 - person
	person42 - person
	person43 - person
	person44 - person
	person45 - person
	person46 - person
	person47 - person
	person48 - person
	person49 - person
	person50 - person
	person51 - person
	person52 - person
	person53 - person
	person54 - person
	person55 - person
	person56 - person
	person57 - person
	person58 - person
	person59 - person
	person60 - person
	person61 - person
	person62 - person
	person63 - person
	person64 - person
	person65 - person
	person66 - person
	person67 - person
	person68 - person
	person69 - person
	person70 - person
	person71 - person
	person72 - person
	person73 - person
	person74 - person
	person75 - person
	person76 - person
	person77 - person
	person78 - person
	person79 - person
	person80 - person
	person81 - person
	person82 - person
	person83 - person
	person84 - person
	person85 - person
	person86 - person
	person87 - person
	person88 - person
	person89 - person
	person90 - person
	person91 - person
	person92 - person
	person93 - person
	person94 - person
	person95 - person
	person96 - person
	person97 - person
	person98 - person
	person99 - person
	person100 - person
	person101 - person
	person102 - person
	person103 - person
	person104 - person
	person105 - person
	person106 - person
	person107 - person
	person108 - person
	person109 - person
	person110 - person
	person111 - person
	person112 - person
	person113 - person
	person114 - person
	person115 - person
	person116 - person
	person117 - person
	person118 - person
	person119 - person
	person120 - person
	person121 - person
	person122 - person
	person123 - person
	person124 - person
	person125 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	city10 - city
	city11 - city
	city12 - city
	city13 - city
	city14 - city
	fl0 - flevel
	fl1 - flevel
	fl2 - flevel
	fl3 - flevel
	fl4 - flevel
	fl5 - flevel
	fl6 - flevel
	)
(:init
	(ata plane1 city10)
	(fuel-level plane1 fl0)
	(ata plane2 city7)
	(fuel-level plane2 fl0)
	(ata plane3 city14)
	(fuel-level plane3 fl0)
	(ata plane4 city14)
	(fuel-level plane4 fl0)
	(ata plane5 city13)
	(fuel-level plane5 fl0)
	(ata plane6 city3)
	(fuel-level plane6 fl0)
	(ata plane7 city12)
	(fuel-level plane7 fl0)
	(ata plane8 city0)
	(fuel-level plane8 fl0)
	(ata plane9 city10)
	(fuel-level plane9 fl0)
	(ata plane10 city0)
	(fuel-level plane10 fl0)
	(ata plane11 city4)
	(fuel-level plane11 fl0)
	(ata plane12 city12)
	(fuel-level plane12 fl0)
	(ata plane13 city6)
	(fuel-level plane13 fl0)
	(ata plane14 city0)
	(fuel-level plane14 fl0)
	(ata plane15 city9)
	(fuel-level plane15 fl0)
	(ata plane16 city13)
	(fuel-level plane16 fl0)
	(ata plane17 city8)
	(fuel-level plane17 fl0)
	(ata plane18 city1)
	(fuel-level plane18 fl0)
	(ata plane19 city3)
	(fuel-level plane19 fl0)
	(ata plane20 city2)
	(fuel-level plane20 fl0)
	(ata plane21 city3)
	(fuel-level plane21 fl0)
	(ata plane22 city8)
	(fuel-level plane22 fl0)
	(atp person1 city7)
	(atp person2 city5)
	(atp person3 city2)
	(atp person4 city5)
	(atp person5 city9)
	(atp person6 city12)
	(atp person7 city3)
	(atp person8 city3)
	(atp person9 city9)
	(atp person10 city0)
	(atp person11 city14)
	(atp person12 city13)
	(atp person13 city12)
	(atp person14 city2)
	(atp person15 city9)
	(atp person16 city9)
	(atp person17 city14)
	(atp person18 city13)
	(atp person19 city6)
	(atp person20 city0)
	(atp person21 city14)
	(atp person22 city1)
	(atp person23 city6)
	(atp person24 city1)
	(atp person25 city8)
	(atp person26 city14)
	(atp person27 city12)
	(atp person28 city6)
	(atp person29 city3)
	(atp person30 city6)
	(atp person31 city14)
	(atp person32 city5)
	(atp person33 city0)
	(atp person34 city1)
	(atp person35 city14)
	(atp person36 city2)
	(atp person37 city10)
	(atp person38 city10)
	(atp person39 city6)
	(atp person40 city7)
	(atp person41 city12)
	(atp person42 city8)
	(atp person43 city6)
	(atp person44 city4)
	(atp person45 city6)
	(atp person46 city6)
	(atp person47 city0)
	(atp person48 city1)
	(atp person49 city10)
	(atp person50 city1)
	(atp person51 city6)
	(atp person52 city9)
	(atp person53 city13)
	(atp person54 city8)
	(atp person55 city13)
	(atp person56 city5)
	(atp person57 city0)
	(atp person58 city9)
	(atp person59 city9)
	(atp person60 city5)
	(atp person61 city3)
	(atp person62 city2)
	(atp person63 city10)
	(atp person64 city10)
	(atp person65 city7)
	(atp person66 city13)
	(atp person67 city1)
	(atp person68 city13)
	(atp person69 city13)
	(atp person70 city8)
	(atp person71 city5)
	(atp person72 city2)
	(atp person73 city8)
	(atp person74 city13)
	(atp person75 city8)
	(atp person76 city14)
	(atp person77 city8)
	(atp person78 city6)
	(atp person79 city9)
	(atp person80 city13)
	(atp person81 city4)
	(atp person82 city5)
	(atp person83 city9)
	(atp person84 city1)
	(atp person85 city4)
	(atp person86 city2)
	(atp person87 city1)
	(atp person88 city0)
	(atp person89 city9)
	(atp person90 city7)
	(atp person91 city0)
	(atp person92 city5)
	(atp person93 city11)
	(atp person94 city12)
	(atp person95 city5)
	(atp person96 city12)
	(atp person97 city0)
	(atp person98 city2)
	(atp person99 city0)
	(atp person100 city8)
	(atp person101 city3)
	(atp person102 city7)
	(atp person103 city6)
	(atp person104 city10)
	(atp person105 city2)
	(atp person106 city5)
	(atp person107 city4)
	(atp person108 city7)
	(atp person109 city7)
	(atp person110 city12)
	(atp person111 city8)
	(atp person112 city12)
	(atp person113 city2)
	(atp person114 city1)
	(atp person115 city11)
	(atp person116 city7)
	(atp person117 city8)
	(atp person118 city14)
	(atp person119 city11)
	(atp person120 city10)
	(atp person121 city2)
	(atp person122 city4)
	(atp person123 city6)
	(atp person124 city3)
	(atp person125 city9)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
)
(:goal (and
	(ata plane1 city14)
	(ata plane3 city8)
	(ata plane8 city10)
	(ata plane11 city10)
	(ata plane15 city7)
	(ata plane17 city14)
	(ata plane18 city6)
	(ata plane19 city9)
	(ata plane22 city14)
	(atp person1 city2)
	(atp person2 city2)
	(atp person3 city4)
	(atp person4 city12)
	(atp person5 city13)
	(atp person6 city7)
	(atp person7 city11)
	(atp person8 city2)
	(atp person9 city5)
	(atp person10 city5)
	(atp person11 city12)
	(atp person12 city3)
	(atp person13 city6)
	(atp person14 city11)
	(atp person15 city6)
	(atp person16 city3)
	(atp person17 city7)
	(atp person18 city10)
	(atp person19 city4)
	(atp person20 city4)
	(atp person21 city4)
	(atp person22 city2)
	(atp person23 city0)
	(atp person24 city3)
	(atp person25 city13)
	(atp person26 city8)
	(atp person28 city6)
	(atp person29 city12)
	(atp person30 city13)
	(atp person31 city13)
	(atp person32 city0)
	(atp person33 city7)
	(atp person34 city8)
	(atp person36 city2)
	(atp person37 city14)
	(atp person38 city6)
	(atp person39 city10)
	(atp person40 city1)
	(atp person41 city1)
	(atp person42 city7)
	(atp person43 city8)
	(atp person44 city10)
	(atp person45 city9)
	(atp person46 city12)
	(atp person47 city8)
	(atp person48 city3)
	(atp person49 city9)
	(atp person50 city2)
	(atp person51 city0)
	(atp person52 city8)
	(atp person53 city14)
	(atp person54 city4)
	(atp person55 city11)
	(atp person56 city3)
	(atp person57 city3)
	(atp person59 city0)
	(atp person60 city2)
	(atp person61 city9)
	(atp person62 city4)
	(atp person63 city2)
	(atp person64 city11)
	(atp person65 city9)
	(atp person66 city0)
	(atp person67 city0)
	(atp person68 city9)
	(atp person69 city4)
	(atp person70 city9)
	(atp person72 city0)
	(atp person73 city10)
	(atp person74 city6)
	(atp person75 city13)
	(atp person76 city11)
	(atp person77 city13)
	(atp person78 city11)
	(atp person79 city9)
	(atp person80 city2)
	(atp person81 city8)
	(atp person82 city10)
	(atp person83 city4)
	(atp person84 city2)
	(atp person85 city11)
	(atp person86 city10)
	(atp person87 city4)
	(atp person88 city11)
	(atp person89 city5)
	(atp person90 city4)
	(atp person91 city9)
	(atp person92 city14)
	(atp person93 city9)
	(atp person94 city11)
	(atp person95 city1)
	(atp person96 city4)
	(atp person97 city5)
	(atp person98 city6)
	(atp person99 city1)
	(atp person100 city8)
	(atp person101 city9)
	(atp person102 city14)
	(atp person103 city10)
	(atp person104 city8)
	(atp person105 city14)
	(atp person107 city12)
	(atp person108 city14)
	(atp person109 city0)
	(atp person110 city8)
	(atp person111 city12)
	(atp person112 city4)
	(atp person113 city10)
	(atp person114 city6)
	(atp person115 city8)
	(atp person116 city13)
	(atp person117 city2)
	(atp person118 city10)
	(atp person119 city2)
	(atp person120 city0)
	(atp person121 city8)
	(atp person122 city6)
	(atp person123 city8)
	(atp person124 city10)
	(atp person125 city6)
	))

)
