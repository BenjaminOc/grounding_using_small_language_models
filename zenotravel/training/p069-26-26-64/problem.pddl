(define (problem ZTRAVEL-26-64)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	plane11 - aircraft
	plane12 - aircraft
	plane13 - aircraft
	plane14 - aircraft
	plane15 - aircraft
	plane16 - aircraft
	plane17 - aircraft
	plane18 - aircraft
	plane19 - aircraft
	plane20 - aircraft
	plane21 - aircraft
	plane22 - aircraft
	plane23 - aircraft
	plane24 - aircraft
	plane25 - aircraft
	plane26 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	person11 - person
	person12 - person
	person13 - person
	person14 - person
	person15 - person
	person16 - person
	person17 - person
	person18 - person
	person19 - person
	person20 - person
	person21 - person
	person22 - person
	person23 - person
	person24 - person
	person25 - person
	person26 - person
	person27 - person
	person28 - person
	person29 - person
	person30 - person
	person31 - person
	person32 - person
	person33 - person
	person34 - person
	person35 - person
	person36 - person
	person37 - person
	person38 - person
	person39 - person
	person40 - person
	person41 - person
	person42 - person
	person43 - person
	person44 - person
	person45 - person
	person46 - person
	person47 - person
	person48 - person
	person49 - person
	person50 - person
	person51 - person
	person52 - person
	person53 - person
	person54 - person
	person55 - person
	person56 - person
	person57 - person
	person58 - person
	person59 - person
	person60 - person
	person61 - person
	person62 - person
	person63 - person
	person64 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	city10 - city
	city11 - city
	city12 - city
	city13 - city
	city14 - city
	city15 - city
	city16 - city
	city17 - city
	city18 - city
	city19 - city
	city20 - city
	city21 - city
	city22 - city
	city23 - city
	city24 - city
	city25 - city
	fl0 - flevel
	fl1 - flevel
	fl2 - flevel
	fl3 - flevel
	fl4 - flevel
	fl5 - flevel
	fl6 - flevel
	)
(:init
	(ata plane1 city5)
	(fuel-level plane1 fl0)
	(ata plane2 city14)
	(fuel-level plane2 fl0)
	(ata plane3 city13)
	(fuel-level plane3 fl0)
	(ata plane4 city12)
	(fuel-level plane4 fl0)
	(ata plane5 city4)
	(fuel-level plane5 fl0)
	(ata plane6 city23)
	(fuel-level plane6 fl0)
	(ata plane7 city9)
	(fuel-level plane7 fl0)
	(ata plane8 city8)
	(fuel-level plane8 fl0)
	(ata plane9 city11)
	(fuel-level plane9 fl0)
	(ata plane10 city12)
	(fuel-level plane10 fl0)
	(ata plane11 city23)
	(fuel-level plane11 fl0)
	(ata plane12 city3)
	(fuel-level plane12 fl0)
	(ata plane13 city2)
	(fuel-level plane13 fl0)
	(ata plane14 city3)
	(fuel-level plane14 fl0)
	(ata plane15 city11)
	(fuel-level plane15 fl0)
	(ata plane16 city0)
	(fuel-level plane16 fl0)
	(ata plane17 city14)
	(fuel-level plane17 fl0)
	(ata plane18 city9)
	(fuel-level plane18 fl0)
	(ata plane19 city17)
	(fuel-level plane19 fl0)
	(ata plane20 city4)
	(fuel-level plane20 fl0)
	(ata plane21 city2)
	(fuel-level plane21 fl0)
	(ata plane22 city10)
	(fuel-level plane22 fl0)
	(ata plane23 city11)
	(fuel-level plane23 fl0)
	(ata plane24 city11)
	(fuel-level plane24 fl0)
	(ata plane25 city9)
	(fuel-level plane25 fl0)
	(ata plane26 city19)
	(fuel-level plane26 fl0)
	(atp person1 city16)
	(atp person2 city13)
	(atp person3 city0)
	(atp person4 city9)
	(atp person5 city19)
	(atp person6 city21)
	(atp person7 city9)
	(atp person8 city21)
	(atp person9 city1)
	(atp person10 city4)
	(atp person11 city1)
	(atp person12 city14)
	(atp person13 city6)
	(atp person14 city12)
	(atp person15 city11)
	(atp person16 city18)
	(atp person17 city3)
	(atp person18 city9)
	(atp person19 city7)
	(atp person20 city13)
	(atp person21 city12)
	(atp person22 city22)
	(atp person23 city14)
	(atp person24 city22)
	(atp person25 city3)
	(atp person26 city2)
	(atp person27 city20)
	(atp person28 city13)
	(atp person29 city14)
	(atp person30 city24)
	(atp person31 city20)
	(atp person32 city18)
	(atp person33 city5)
	(atp person34 city8)
	(atp person35 city10)
	(atp person36 city6)
	(atp person37 city16)
	(atp person38 city25)
	(atp person39 city2)
	(atp person40 city1)
	(atp person41 city25)
	(atp person42 city24)
	(atp person43 city12)
	(atp person44 city22)
	(atp person45 city12)
	(atp person46 city22)
	(atp person47 city17)
	(atp person48 city1)
	(atp person49 city9)
	(atp person50 city22)
	(atp person51 city1)
	(atp person52 city11)
	(atp person53 city23)
	(atp person54 city24)
	(atp person55 city11)
	(atp person56 city13)
	(atp person57 city7)
	(atp person58 city13)
	(atp person59 city19)
	(atp person60 city12)
	(atp person61 city19)
	(atp person62 city7)
	(atp person63 city20)
	(atp person64 city12)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
)
(:goal (and
	(ata plane2 city24)
	(ata plane3 city4)
	(ata plane11 city10)
	(ata plane12 city15)
	(ata plane13 city18)
	(ata plane22 city9)
	(ata plane23 city5)
	(ata plane26 city11)
	(atp person1 city10)
	(atp person2 city7)
	(atp person3 city16)
	(atp person4 city25)
	(atp person5 city16)
	(atp person6 city19)
	(atp person7 city1)
	(atp person8 city7)
	(atp person9 city9)
	(atp person10 city11)
	(atp person11 city1)
	(atp person12 city15)
	(atp person13 city15)
	(atp person14 city25)
	(atp person15 city19)
	(atp person16 city14)
	(atp person17 city24)
	(atp person19 city21)
	(atp person20 city25)
	(atp person21 city0)
	(atp person22 city15)
	(atp person23 city21)
	(atp person24 city7)
	(atp person25 city18)
	(atp person26 city11)
	(atp person27 city14)
	(atp person28 city23)
	(atp person29 city5)
	(atp person30 city18)
	(atp person31 city4)
	(atp person32 city1)
	(atp person33 city15)
	(atp person34 city11)
	(atp person35 city15)
	(atp person36 city17)
	(atp person37 city11)
	(atp person38 city25)
	(atp person39 city14)
	(atp person40 city12)
	(atp person41 city7)
	(atp person42 city25)
	(atp person43 city4)
	(atp person44 city12)
	(atp person45 city23)
	(atp person46 city3)
	(atp person47 city19)
	(atp person48 city19)
	(atp person49 city21)
	(atp person50 city23)
	(atp person51 city22)
	(atp person52 city20)
	(atp person53 city7)
	(atp person54 city3)
	(atp person55 city16)
	(atp person56 city13)
	(atp person57 city4)
	(atp person58 city6)
	(atp person59 city16)
	(atp person60 city12)
	(atp person61 city13)
	(atp person62 city25)
	(atp person63 city23)
	(atp person64 city21)
	))

)
