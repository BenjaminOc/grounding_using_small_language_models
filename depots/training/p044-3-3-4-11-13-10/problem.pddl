(define (problem depot-3-3-4-11-13-10) (:domain depots)
(:objects
	depot0 depot1 depot2 - Depot
	distributor0 distributor1 distributor2 - Distributor
	truck0 truck1 truck2 truck3 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 - Hoist)
(:init
	(at pallet0 depot0)
	(clear pallet0)
	(at pallet1 depot1)
	(clear crate8)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 distributor0)
	(clear crate9)
	(at pallet4 distributor1)
	(clear crate2)
	(at pallet5 distributor2)
	(clear pallet5)
	(at pallet6 distributor0)
	(clear pallet6)
	(at pallet7 distributor2)
	(clear crate3)
	(at pallet8 distributor2)
	(clear crate6)
	(at pallet9 distributor2)
	(clear pallet9)
	(at pallet10 depot1)
	(clear pallet10)
	(at truck0 depot0)
	(at truck1 distributor2)
	(at truck2 depot2)
	(at truck3 depot1)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 distributor0)
	(available hoist3)
	(at hoist4 distributor1)
	(available hoist4)
	(at hoist5 distributor2)
	(available hoist5)
	(at hoist6 distributor1)
	(available hoist6)
	(at hoist7 distributor2)
	(available hoist7)
	(at hoist8 depot0)
	(available hoist8)
	(at hoist9 depot2)
	(available hoist9)
	(at hoist10 distributor0)
	(available hoist10)
	(at hoist11 depot0)
	(available hoist11)
	(at hoist12 depot1)
	(available hoist12)
	(at crate0 distributor0)
	(on crate0 pallet3)
	(at crate1 depot1)
	(on crate1 pallet1)
	(at crate2 distributor1)
	(on crate2 pallet4)
	(at crate3 distributor2)
	(on crate3 pallet7)
	(at crate4 depot1)
	(on crate4 crate1)
	(at crate5 distributor2)
	(on crate5 pallet8)
	(at crate6 distributor2)
	(on crate6 crate5)
	(at crate7 depot1)
	(on crate7 crate4)
	(at crate8 depot1)
	(on crate8 crate7)
	(at crate9 distributor0)
	(on crate9 crate0)
)

(:goal (and
		(on crate0 pallet5)
		(on crate1 pallet1)
		(on crate2 crate1)
		(on crate3 pallet0)
		(on crate4 crate7)
		(on crate5 pallet4)
		(on crate7 crate8)
		(on crate8 pallet3)
		(on crate9 pallet10)
	)
))
