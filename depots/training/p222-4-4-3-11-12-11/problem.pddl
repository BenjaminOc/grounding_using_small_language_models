(define (problem depot-4-4-3-11-12-11) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 - Depot
	distributor0 distributor1 distributor2 distributor3 - Distributor
	truck0 truck1 truck2 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 crate10 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate7)
	(at pallet1 depot1)
	(clear pallet1)
	(at pallet2 depot2)
	(clear crate4)
	(at pallet3 depot3)
	(clear crate2)
	(at pallet4 distributor0)
	(clear pallet4)
	(at pallet5 distributor1)
	(clear crate8)
	(at pallet6 distributor2)
	(clear crate10)
	(at pallet7 distributor3)
	(clear pallet7)
	(at pallet8 distributor3)
	(clear crate9)
	(at pallet9 depot2)
	(clear crate0)
	(at pallet10 depot0)
	(clear crate3)
	(at truck0 depot0)
	(at truck1 depot0)
	(at truck2 depot0)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 distributor0)
	(available hoist4)
	(at hoist5 distributor1)
	(available hoist5)
	(at hoist6 distributor2)
	(available hoist6)
	(at hoist7 distributor3)
	(available hoist7)
	(at hoist8 depot1)
	(available hoist8)
	(at hoist9 distributor3)
	(available hoist9)
	(at hoist10 depot3)
	(available hoist10)
	(at hoist11 distributor1)
	(available hoist11)
	(at crate0 depot2)
	(on crate0 pallet9)
	(at crate1 depot0)
	(on crate1 pallet0)
	(at crate2 depot3)
	(on crate2 pallet3)
	(at crate3 depot0)
	(on crate3 pallet10)
	(at crate4 depot2)
	(on crate4 pallet2)
	(at crate5 distributor1)
	(on crate5 pallet5)
	(at crate6 distributor2)
	(on crate6 pallet6)
	(at crate7 depot0)
	(on crate7 crate1)
	(at crate8 distributor1)
	(on crate8 crate5)
	(at crate9 distributor3)
	(on crate9 pallet8)
	(at crate10 distributor2)
	(on crate10 crate6)
)

(:goal (and
		(on crate0 crate5)
		(on crate1 crate9)
		(on crate2 pallet1)
		(on crate3 pallet2)
		(on crate4 pallet3)
		(on crate5 crate7)
		(on crate6 pallet7)
		(on crate7 crate6)
		(on crate8 crate3)
		(on crate9 pallet8)
		(on crate10 pallet4)
	)
))
