(define (problem depot-4-3-4-11-11-10) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 - Depot
	distributor0 distributor1 distributor2 - Distributor
	truck0 truck1 truck2 truck3 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate8)
	(at pallet1 depot1)
	(clear crate6)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 depot3)
	(clear crate9)
	(at pallet4 distributor0)
	(clear crate4)
	(at pallet5 distributor1)
	(clear crate1)
	(at pallet6 distributor2)
	(clear pallet6)
	(at pallet7 depot3)
	(clear crate0)
	(at pallet8 distributor2)
	(clear pallet8)
	(at pallet9 distributor0)
	(clear crate7)
	(at pallet10 depot2)
	(clear pallet10)
	(at truck0 distributor2)
	(at truck1 depot2)
	(at truck2 depot2)
	(at truck3 distributor2)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 distributor0)
	(available hoist4)
	(at hoist5 distributor1)
	(available hoist5)
	(at hoist6 distributor2)
	(available hoist6)
	(at hoist7 distributor2)
	(available hoist7)
	(at hoist8 distributor0)
	(available hoist8)
	(at hoist9 depot1)
	(available hoist9)
	(at hoist10 distributor2)
	(available hoist10)
	(at crate0 depot3)
	(on crate0 pallet7)
	(at crate1 distributor1)
	(on crate1 pallet5)
	(at crate2 distributor0)
	(on crate2 pallet4)
	(at crate3 distributor0)
	(on crate3 pallet9)
	(at crate4 distributor0)
	(on crate4 crate2)
	(at crate5 distributor0)
	(on crate5 crate3)
	(at crate6 depot1)
	(on crate6 pallet1)
	(at crate7 distributor0)
	(on crate7 crate5)
	(at crate8 depot0)
	(on crate8 pallet0)
	(at crate9 depot3)
	(on crate9 pallet3)
)

(:goal (and
		(on crate0 crate9)
		(on crate1 pallet5)
		(on crate2 pallet1)
		(on crate3 pallet2)
		(on crate5 pallet4)
		(on crate6 crate2)
		(on crate7 pallet8)
		(on crate8 crate7)
		(on crate9 pallet10)
	)
))
