(define (problem depot-4-4-3-13-14-10) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 - Depot
	distributor0 distributor1 distributor2 distributor3 - Distributor
	truck0 truck1 truck2 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate2)
	(at pallet1 depot1)
	(clear crate7)
	(at pallet2 depot2)
	(clear crate9)
	(at pallet3 depot3)
	(clear pallet3)
	(at pallet4 distributor0)
	(clear pallet4)
	(at pallet5 distributor1)
	(clear pallet5)
	(at pallet6 distributor2)
	(clear pallet6)
	(at pallet7 distributor3)
	(clear crate3)
	(at pallet8 depot3)
	(clear pallet8)
	(at pallet9 depot1)
	(clear pallet9)
	(at pallet10 distributor1)
	(clear pallet10)
	(at pallet11 distributor1)
	(clear crate8)
	(at pallet12 depot2)
	(clear pallet12)
	(at truck0 depot0)
	(at truck1 depot1)
	(at truck2 distributor0)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 distributor0)
	(available hoist4)
	(at hoist5 distributor1)
	(available hoist5)
	(at hoist6 distributor2)
	(available hoist6)
	(at hoist7 distributor3)
	(available hoist7)
	(at hoist8 distributor1)
	(available hoist8)
	(at hoist9 depot0)
	(available hoist9)
	(at hoist10 depot0)
	(available hoist10)
	(at hoist11 depot2)
	(available hoist11)
	(at hoist12 distributor2)
	(available hoist12)
	(at hoist13 distributor3)
	(available hoist13)
	(at crate0 depot2)
	(on crate0 pallet2)
	(at crate1 depot2)
	(on crate1 crate0)
	(at crate2 depot0)
	(on crate2 pallet0)
	(at crate3 distributor3)
	(on crate3 pallet7)
	(at crate4 depot2)
	(on crate4 crate1)
	(at crate5 distributor1)
	(on crate5 pallet11)
	(at crate6 distributor1)
	(on crate6 crate5)
	(at crate7 depot1)
	(on crate7 pallet1)
	(at crate8 distributor1)
	(on crate8 crate6)
	(at crate9 depot2)
	(on crate9 crate4)
)

(:goal (and
		(on crate0 pallet10)
		(on crate1 pallet7)
		(on crate2 pallet11)
		(on crate3 pallet9)
		(on crate4 pallet2)
		(on crate5 pallet8)
		(on crate6 pallet5)
		(on crate7 pallet6)
	)
))
