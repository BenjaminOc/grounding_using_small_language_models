(define (problem depot-4-4-4-11-14-9) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 - Depot
	distributor0 distributor1 distributor2 distributor3 - Distributor
	truck0 truck1 truck2 truck3 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate3)
	(at pallet1 depot1)
	(clear crate1)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 depot3)
	(clear crate8)
	(at pallet4 distributor0)
	(clear pallet4)
	(at pallet5 distributor1)
	(clear pallet5)
	(at pallet6 distributor2)
	(clear crate5)
	(at pallet7 distributor3)
	(clear pallet7)
	(at pallet8 distributor1)
	(clear crate6)
	(at pallet9 depot2)
	(clear crate7)
	(at pallet10 distributor2)
	(clear pallet10)
	(at truck0 depot0)
	(at truck1 depot1)
	(at truck2 distributor0)
	(at truck3 distributor1)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 distributor0)
	(available hoist4)
	(at hoist5 distributor1)
	(available hoist5)
	(at hoist6 distributor2)
	(available hoist6)
	(at hoist7 distributor3)
	(available hoist7)
	(at hoist8 depot2)
	(available hoist8)
	(at hoist9 distributor2)
	(available hoist9)
	(at hoist10 depot2)
	(available hoist10)
	(at hoist11 distributor0)
	(available hoist11)
	(at hoist12 depot1)
	(available hoist12)
	(at hoist13 depot2)
	(available hoist13)
	(at crate0 distributor2)
	(on crate0 pallet6)
	(at crate1 depot1)
	(on crate1 pallet1)
	(at crate2 depot2)
	(on crate2 pallet9)
	(at crate3 depot0)
	(on crate3 pallet0)
	(at crate4 distributor2)
	(on crate4 crate0)
	(at crate5 distributor2)
	(on crate5 crate4)
	(at crate6 distributor1)
	(on crate6 pallet8)
	(at crate7 depot2)
	(on crate7 crate2)
	(at crate8 depot3)
	(on crate8 pallet3)
)

(:goal (and
		(on crate0 pallet8)
		(on crate1 pallet5)
		(on crate2 pallet9)
		(on crate3 pallet7)
		(on crate4 crate6)
		(on crate5 crate1)
		(on crate6 pallet3)
	)
))
