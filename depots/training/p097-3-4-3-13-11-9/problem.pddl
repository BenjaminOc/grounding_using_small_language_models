(define (problem depot-3-4-3-13-11-9) (:domain depots)
(:objects
	depot0 depot1 depot2 - Depot
	distributor0 distributor1 distributor2 distributor3 - Distributor
	truck0 truck1 truck2 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 - Hoist)
(:init
	(at pallet0 depot0)
	(clear pallet0)
	(at pallet1 depot1)
	(clear crate4)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 distributor0)
	(clear pallet3)
	(at pallet4 distributor1)
	(clear crate8)
	(at pallet5 distributor2)
	(clear pallet5)
	(at pallet6 distributor3)
	(clear crate7)
	(at pallet7 depot0)
	(clear pallet7)
	(at pallet8 depot0)
	(clear pallet8)
	(at pallet9 depot0)
	(clear pallet9)
	(at pallet10 distributor0)
	(clear crate2)
	(at pallet11 distributor3)
	(clear pallet11)
	(at pallet12 distributor1)
	(clear pallet12)
	(at truck0 distributor3)
	(at truck1 depot2)
	(at truck2 depot2)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 distributor0)
	(available hoist3)
	(at hoist4 distributor1)
	(available hoist4)
	(at hoist5 distributor2)
	(available hoist5)
	(at hoist6 distributor3)
	(available hoist6)
	(at hoist7 depot2)
	(available hoist7)
	(at hoist8 distributor2)
	(available hoist8)
	(at hoist9 distributor2)
	(available hoist9)
	(at hoist10 distributor0)
	(available hoist10)
	(at crate0 depot1)
	(on crate0 pallet1)
	(at crate1 distributor0)
	(on crate1 pallet10)
	(at crate2 distributor0)
	(on crate2 crate1)
	(at crate3 depot1)
	(on crate3 crate0)
	(at crate4 depot1)
	(on crate4 crate3)
	(at crate5 distributor1)
	(on crate5 pallet4)
	(at crate6 distributor1)
	(on crate6 crate5)
	(at crate7 distributor3)
	(on crate7 pallet6)
	(at crate8 distributor1)
	(on crate8 crate6)
)

(:goal (and
		(on crate0 pallet9)
		(on crate1 pallet1)
		(on crate2 pallet7)
		(on crate3 pallet11)
		(on crate4 crate8)
		(on crate5 pallet2)
		(on crate6 crate0)
		(on crate8 pallet4)
	)
))
