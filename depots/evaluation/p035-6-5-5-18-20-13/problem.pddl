(define (problem depot-6-5-5-18-20-13) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 depot4 depot5 - Depot
	distributor0 distributor1 distributor2 distributor3 distributor4 - Distributor
	truck0 truck1 truck2 truck3 truck4 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 pallet13 pallet14 pallet15 pallet16 pallet17 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 crate10 crate11 crate12 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 hoist14 hoist15 hoist16 hoist17 hoist18 hoist19 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate2)
	(at pallet1 depot1)
	(clear crate8)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 depot3)
	(clear pallet3)
	(at pallet4 depot4)
	(clear pallet4)
	(at pallet5 depot5)
	(clear crate3)
	(at pallet6 distributor0)
	(clear crate7)
	(at pallet7 distributor1)
	(clear crate10)
	(at pallet8 distributor2)
	(clear crate0)
	(at pallet9 distributor3)
	(clear pallet9)
	(at pallet10 distributor4)
	(clear crate11)
	(at pallet11 distributor4)
	(clear pallet11)
	(at pallet12 distributor0)
	(clear pallet12)
	(at pallet13 depot1)
	(clear crate5)
	(at pallet14 depot2)
	(clear crate12)
	(at pallet15 distributor4)
	(clear crate4)
	(at pallet16 depot0)
	(clear crate1)
	(at pallet17 depot4)
	(clear crate6)
	(at truck0 distributor1)
	(at truck1 depot4)
	(at truck2 distributor4)
	(at truck3 distributor4)
	(at truck4 depot5)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 depot4)
	(available hoist4)
	(at hoist5 depot5)
	(available hoist5)
	(at hoist6 distributor0)
	(available hoist6)
	(at hoist7 distributor1)
	(available hoist7)
	(at hoist8 distributor2)
	(available hoist8)
	(at hoist9 distributor3)
	(available hoist9)
	(at hoist10 distributor4)
	(available hoist10)
	(at hoist11 distributor4)
	(available hoist11)
	(at hoist12 depot3)
	(available hoist12)
	(at hoist13 depot1)
	(available hoist13)
	(at hoist14 depot4)
	(available hoist14)
	(at hoist15 depot5)
	(available hoist15)
	(at hoist16 distributor1)
	(available hoist16)
	(at hoist17 depot1)
	(available hoist17)
	(at hoist18 depot4)
	(available hoist18)
	(at hoist19 depot4)
	(available hoist19)
	(at crate0 distributor2)
	(on crate0 pallet8)
	(at crate1 depot0)
	(on crate1 pallet16)
	(at crate2 depot0)
	(on crate2 pallet0)
	(at crate3 depot5)
	(on crate3 pallet5)
	(at crate4 distributor4)
	(on crate4 pallet15)
	(at crate5 depot1)
	(on crate5 pallet13)
	(at crate6 depot4)
	(on crate6 pallet17)
	(at crate7 distributor0)
	(on crate7 pallet6)
	(at crate8 depot1)
	(on crate8 pallet1)
	(at crate9 distributor4)
	(on crate9 pallet10)
	(at crate10 distributor1)
	(on crate10 pallet7)
	(at crate11 distributor4)
	(on crate11 crate9)
	(at crate12 depot2)
	(on crate12 pallet14)
)

(:goal (and
		(on crate1 crate7)
		(on crate2 pallet4)
		(on crate3 crate4)
		(on crate4 crate2)
		(on crate5 pallet2)
		(on crate6 pallet14)
		(on crate7 pallet13)
		(on crate8 pallet16)
		(on crate9 pallet10)
		(on crate10 pallet0)
		(on crate11 crate10)
		(on crate12 pallet9)
	)
))
