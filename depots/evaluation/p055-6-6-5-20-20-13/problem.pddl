(define (problem depot-6-6-5-20-20-13) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 depot4 depot5 - Depot
	distributor0 distributor1 distributor2 distributor3 distributor4 distributor5 - Distributor
	truck0 truck1 truck2 truck3 truck4 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 pallet13 pallet14 pallet15 pallet16 pallet17 pallet18 pallet19 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 crate10 crate11 crate12 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 hoist14 hoist15 hoist16 hoist17 hoist18 hoist19 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate11)
	(at pallet1 depot1)
	(clear pallet1)
	(at pallet2 depot2)
	(clear crate4)
	(at pallet3 depot3)
	(clear pallet3)
	(at pallet4 depot4)
	(clear crate3)
	(at pallet5 depot5)
	(clear pallet5)
	(at pallet6 distributor0)
	(clear pallet6)
	(at pallet7 distributor1)
	(clear pallet7)
	(at pallet8 distributor2)
	(clear crate5)
	(at pallet9 distributor3)
	(clear crate12)
	(at pallet10 distributor4)
	(clear crate10)
	(at pallet11 distributor5)
	(clear pallet11)
	(at pallet12 depot0)
	(clear pallet12)
	(at pallet13 depot4)
	(clear pallet13)
	(at pallet14 distributor1)
	(clear crate6)
	(at pallet15 depot3)
	(clear crate9)
	(at pallet16 depot4)
	(clear pallet16)
	(at pallet17 depot2)
	(clear pallet17)
	(at pallet18 distributor0)
	(clear pallet18)
	(at pallet19 depot4)
	(clear pallet19)
	(at truck0 depot4)
	(at truck1 distributor5)
	(at truck2 depot2)
	(at truck3 depot1)
	(at truck4 distributor4)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 depot4)
	(available hoist4)
	(at hoist5 depot5)
	(available hoist5)
	(at hoist6 distributor0)
	(available hoist6)
	(at hoist7 distributor1)
	(available hoist7)
	(at hoist8 distributor2)
	(available hoist8)
	(at hoist9 distributor3)
	(available hoist9)
	(at hoist10 distributor4)
	(available hoist10)
	(at hoist11 distributor5)
	(available hoist11)
	(at hoist12 depot2)
	(available hoist12)
	(at hoist13 distributor1)
	(available hoist13)
	(at hoist14 distributor5)
	(available hoist14)
	(at hoist15 depot2)
	(available hoist15)
	(at hoist16 distributor5)
	(available hoist16)
	(at hoist17 depot5)
	(available hoist17)
	(at hoist18 depot5)
	(available hoist18)
	(at hoist19 distributor5)
	(available hoist19)
	(at crate0 distributor1)
	(on crate0 pallet14)
	(at crate1 depot3)
	(on crate1 pallet15)
	(at crate2 depot0)
	(on crate2 pallet0)
	(at crate3 depot4)
	(on crate3 pallet4)
	(at crate4 depot2)
	(on crate4 pallet2)
	(at crate5 distributor2)
	(on crate5 pallet8)
	(at crate6 distributor1)
	(on crate6 crate0)
	(at crate7 distributor3)
	(on crate7 pallet9)
	(at crate8 distributor4)
	(on crate8 pallet10)
	(at crate9 depot3)
	(on crate9 crate1)
	(at crate10 distributor4)
	(on crate10 crate8)
	(at crate11 depot0)
	(on crate11 crate2)
	(at crate12 distributor3)
	(on crate12 crate7)
)

(:goal (and
		(on crate0 pallet2)
		(on crate1 pallet16)
		(on crate2 pallet15)
		(on crate3 pallet4)
		(on crate4 pallet5)
		(on crate5 crate4)
		(on crate6 pallet12)
		(on crate8 pallet17)
		(on crate9 pallet11)
		(on crate10 pallet7)
		(on crate12 pallet13)
	)
))
