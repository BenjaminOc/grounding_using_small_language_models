(define (problem depot-5-5-5-20-20-15) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 depot4 - Depot
	distributor0 distributor1 distributor2 distributor3 distributor4 - Distributor
	truck0 truck1 truck2 truck3 truck4 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 pallet13 pallet14 pallet15 pallet16 pallet17 pallet18 pallet19 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 crate10 crate11 crate12 crate13 crate14 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 hoist14 hoist15 hoist16 hoist17 hoist18 hoist19 - Hoist)
(:init
	(at pallet0 depot0)
	(clear crate8)
	(at pallet1 depot1)
	(clear crate14)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 depot3)
	(clear crate11)
	(at pallet4 depot4)
	(clear crate7)
	(at pallet5 distributor0)
	(clear crate5)
	(at pallet6 distributor1)
	(clear crate13)
	(at pallet7 distributor2)
	(clear crate10)
	(at pallet8 distributor3)
	(clear pallet8)
	(at pallet9 distributor4)
	(clear crate12)
	(at pallet10 distributor4)
	(clear crate3)
	(at pallet11 depot4)
	(clear crate6)
	(at pallet12 distributor2)
	(clear crate2)
	(at pallet13 distributor1)
	(clear pallet13)
	(at pallet14 distributor0)
	(clear crate1)
	(at pallet15 depot1)
	(clear pallet15)
	(at pallet16 depot2)
	(clear pallet16)
	(at pallet17 depot4)
	(clear crate0)
	(at pallet18 distributor2)
	(clear pallet18)
	(at pallet19 depot4)
	(clear pallet19)
	(at truck0 depot4)
	(at truck1 distributor3)
	(at truck2 distributor4)
	(at truck3 distributor4)
	(at truck4 depot4)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 depot4)
	(available hoist4)
	(at hoist5 distributor0)
	(available hoist5)
	(at hoist6 distributor1)
	(available hoist6)
	(at hoist7 distributor2)
	(available hoist7)
	(at hoist8 distributor3)
	(available hoist8)
	(at hoist9 distributor4)
	(available hoist9)
	(at hoist10 distributor4)
	(available hoist10)
	(at hoist11 distributor2)
	(available hoist11)
	(at hoist12 depot3)
	(available hoist12)
	(at hoist13 distributor3)
	(available hoist13)
	(at hoist14 depot1)
	(available hoist14)
	(at hoist15 depot0)
	(available hoist15)
	(at hoist16 distributor4)
	(available hoist16)
	(at hoist17 distributor4)
	(available hoist17)
	(at hoist18 depot4)
	(available hoist18)
	(at hoist19 depot1)
	(available hoist19)
	(at crate0 depot4)
	(on crate0 pallet17)
	(at crate1 distributor0)
	(on crate1 pallet14)
	(at crate2 distributor2)
	(on crate2 pallet12)
	(at crate3 distributor4)
	(on crate3 pallet10)
	(at crate4 depot1)
	(on crate4 pallet1)
	(at crate5 distributor0)
	(on crate5 pallet5)
	(at crate6 depot4)
	(on crate6 pallet11)
	(at crate7 depot4)
	(on crate7 pallet4)
	(at crate8 depot0)
	(on crate8 pallet0)
	(at crate9 distributor4)
	(on crate9 pallet9)
	(at crate10 distributor2)
	(on crate10 pallet7)
	(at crate11 depot3)
	(on crate11 pallet3)
	(at crate12 distributor4)
	(on crate12 crate9)
	(at crate13 distributor1)
	(on crate13 pallet6)
	(at crate14 depot1)
	(on crate14 crate4)
)

(:goal (and
		(on crate0 pallet6)
		(on crate2 crate14)
		(on crate3 crate6)
		(on crate4 crate11)
		(on crate5 pallet1)
		(on crate6 pallet4)
		(on crate7 pallet16)
		(on crate8 pallet7)
		(on crate9 crate0)
		(on crate11 pallet13)
		(on crate12 pallet14)
		(on crate13 pallet9)
		(on crate14 pallet8)
	)
))
