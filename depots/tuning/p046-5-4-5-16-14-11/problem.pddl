(define (problem depot-5-4-5-16-14-11) (:domain depots)
(:objects
	depot0 depot1 depot2 depot3 depot4 - Depot
	distributor0 distributor1 distributor2 distributor3 - Distributor
	truck0 truck1 truck2 truck3 truck4 - Truck
	pallet0 pallet1 pallet2 pallet3 pallet4 pallet5 pallet6 pallet7 pallet8 pallet9 pallet10 pallet11 pallet12 pallet13 pallet14 pallet15 - Pallet
	crate0 crate1 crate2 crate3 crate4 crate5 crate6 crate7 crate8 crate9 crate10 - Crate
	hoist0 hoist1 hoist2 hoist3 hoist4 hoist5 hoist6 hoist7 hoist8 hoist9 hoist10 hoist11 hoist12 hoist13 - Hoist)
(:init
	(at pallet0 depot0)
	(clear pallet0)
	(at pallet1 depot1)
	(clear pallet1)
	(at pallet2 depot2)
	(clear pallet2)
	(at pallet3 depot3)
	(clear crate6)
	(at pallet4 depot4)
	(clear crate9)
	(at pallet5 distributor0)
	(clear pallet5)
	(at pallet6 distributor1)
	(clear crate10)
	(at pallet7 distributor2)
	(clear pallet7)
	(at pallet8 distributor3)
	(clear pallet8)
	(at pallet9 depot3)
	(clear pallet9)
	(at pallet10 depot4)
	(clear pallet10)
	(at pallet11 distributor1)
	(clear crate8)
	(at pallet12 depot3)
	(clear crate7)
	(at pallet13 distributor2)
	(clear crate2)
	(at pallet14 depot2)
	(clear pallet14)
	(at pallet15 depot4)
	(clear pallet15)
	(at truck0 depot4)
	(at truck1 depot2)
	(at truck2 distributor1)
	(at truck3 depot4)
	(at truck4 depot0)
	(at hoist0 depot0)
	(available hoist0)
	(at hoist1 depot1)
	(available hoist1)
	(at hoist2 depot2)
	(available hoist2)
	(at hoist3 depot3)
	(available hoist3)
	(at hoist4 depot4)
	(available hoist4)
	(at hoist5 distributor0)
	(available hoist5)
	(at hoist6 distributor1)
	(available hoist6)
	(at hoist7 distributor2)
	(available hoist7)
	(at hoist8 distributor3)
	(available hoist8)
	(at hoist9 depot0)
	(available hoist9)
	(at hoist10 distributor0)
	(available hoist10)
	(at hoist11 distributor1)
	(available hoist11)
	(at hoist12 depot3)
	(available hoist12)
	(at hoist13 distributor1)
	(available hoist13)
	(at crate0 distributor2)
	(on crate0 pallet13)
	(at crate1 distributor1)
	(on crate1 pallet11)
	(at crate2 distributor2)
	(on crate2 crate0)
	(at crate3 depot4)
	(on crate3 pallet4)
	(at crate4 distributor1)
	(on crate4 pallet6)
	(at crate5 depot3)
	(on crate5 pallet12)
	(at crate6 depot3)
	(on crate6 pallet3)
	(at crate7 depot3)
	(on crate7 crate5)
	(at crate8 distributor1)
	(on crate8 crate1)
	(at crate9 depot4)
	(on crate9 crate3)
	(at crate10 distributor1)
	(on crate10 crate4)
)

(:goal (and
		(on crate0 pallet11)
		(on crate1 pallet14)
		(on crate2 pallet3)
		(on crate3 pallet15)
		(on crate4 pallet13)
		(on crate5 pallet6)
		(on crate6 pallet1)
		(on crate7 pallet10)
		(on crate8 pallet12)
		(on crate9 pallet2)
		(on crate10 pallet8)
	)
))
