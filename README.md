# Partial Grounding in Planning using Small Language Models

This repository contains machine learning models for action schemas across 7 planning domains. The models are trained, tuned, and evaluated in their respective directories for each domain. Additionally, the models are saved in the `models` directory, along with their pickle files, metrics, parameters, and plots. There's also a directory called `fasttext_models` that contains pre-trained fasttext language models using data of all the domains.

## Domains

The following planning domains are included in this repository:

- agricola
- blocksworld
- caldera
- depots
- hiking
- satellite
- tpp
- zenotravel

Each domain directory contains the following subdirectories:

### Training

This directory contains planning problems for training machine learning models for the corresponding planning domain.

### Tuning

This directory contains planning problems for tuning the hyperparameters of the machine learning models for the corresponding planning domain.

### Evaluation

This directory contains planning problems for evaluating the performance of the machine learning models for the corresponding planning domain.

### Models

This directory contains the trained machine learning models for each action schema in the corresponding planning domain, along with their pickle files, metrics, parameters, and plots.

## Fasttext Models

This directory contains two pre-trained fasttext language models using data of all the planning domains: One using useful-facts sentences and other one using good operators sentences.

## Usage

To train, tune, and evaluate machine learning models for a specific planning domain, you can navigate to the corresponding directory. To use the pre-trained models, you can load them using `pickle` or `mlflow`. Similarly, fasttext models can be load from the `fasttext` python facebook library.
