#!/usr/bin/python3

import subprocess
import os
from os import path
import time
import glob
import shutil

files = glob.glob("*")
command = "/home/areces/FastDownwards/fast-downward.py"
param1 = "--search-time-limit"
param2 = str(10*60)
param3 = "--alias"
param4 = "lama"
#param4 = "lama-first"
#param4 = "seq-sat-fdss-2018"
#param4 = "seq-sat-fd-autotune-2"
param5 = "../../domain.pddl"
param6 = "problem.pddl"
processes = set()
max_processes = 5

files.sort()

for name in files:
    print("###############################################################")
    print("")
    print(name)
    print("")
    print("###############################################################")
    os.chdir(name)
    if path.exists("sas_plan"):
       shutil.copy2("sas_plan","sas_plan.1")
#    processes.add(subprocess.Popen([command, param1, param2, param3, param4, param5,param6]))
#    if len(processes) >= max_processes:
#        os.wait()
#        processes.difference_update([
#            p for p in processes if p.poll() is not None])
    os.chdir("..")

