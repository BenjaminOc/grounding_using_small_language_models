;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 10 hosts, 20 user, 2 admin per host, 2 active account per host
(define (problem p10_hosts_trial_16)
(:domain caldera)
(:objects id_aDomain - ObservedDomain 
 id_gmRat id_gjRat id_goRat id_gkRat id_gqRat id_fwRat id_giRat id_gnRat id_glRat id_gpRat id_grRat - ObservedRat 
 id_hlShare id_hjShare id_hgShare id_heShare id_hcShare id_hdShare id_hkShare id_hfShare id_hhShare id_hiShare - ObservedShare 
 id_cdDomainCredential id_brDomainCredential id_bzDomainCredential id_hDomainCredential id_bvDomainCredential id_dDomainCredential id_lDomainCredential id_cpDomainCredential id_bjDomainCredential id_chDomainCredential id_bbDomainCredential id_cxDomainCredential id_bnDomainCredential id_ctDomainCredential id_xDomainCredential id_dbDomainCredential id_tDomainCredential id_pDomainCredential id_clDomainCredential id_bfDomainCredential - ObservedDomainCredential 
 id_fyFile id_ghFile id_gaFile id_ggFile id_gcFile id_gbFile id_geFile id_fzFile id_gfFile id_gdFile - ObservedFile 
 id_enHost id_fiHost id_deHost id_dlHost id_dsHost id_egHost id_fbHost id_fpHost id_euHost id_dzHost - ObservedHost 
 id_fcTimeDelta id_eaTimeDelta id_fjTimeDelta id_dtTimeDelta id_fqTimeDelta id_eoTimeDelta id_dmTimeDelta id_evTimeDelta id_ehTimeDelta id_dfTimeDelta - ObservedTimeDelta 
 str__robert str__cz str__bx str__maria str__cu str__e str__cf str__fm str__fn str__cm str__michael str__bg str__ez str__margaret str__v str__dj str__ek str__dk str__fx str__jennifer str__thomas str__dp str__alpha str__dr str__u str__cn str__james str__fv str__joseph str__dorothy str__q str__patricia str__linda str__et str__dw str__f str__es str__cr str__fg str__william str__ee str__fh str__dd str__bs str__ef str__david str__richard str__barbara str__bk str__el str__bt str__ff str__b str__di str__m str__cy str__dc str__dy str__elizabeth str__dx str__john str__ce str__ca str__cb str__dq str__bp str__ci str__ey str__j str__cq str__y str__er str__z str__bo str__cv str__bc str__r str__ft str__i str__n str__cj str__fu str__mary str__em str__bw str__susan str__charles str__bl str__ed str__fo str__bd str__bh str__fa - string 
 id_gySchtask id_gzSchtask id_gtSchtask id_gvSchtask id_gsSchtask id_gxSchtask id_hbSchtask id_gwSchtask id_guSchtask id_haSchtask - ObservedSchtask 
 id_ccDomainUser id_bqDomainUser id_cDomainUser id_cwDomainUser id_ckDomainUser id_gDomainUser id_beDomainUser id_biDomainUser id_buDomainUser id_cgDomainUser id_kDomainUser id_sDomainUser id_bmDomainUser id_wDomainUser id_csDomainUser id_baDomainUser id_byDomainUser id_oDomainUser id_coDomainUser id_daDomainUser - ObservedDomainUser 
 num__84 num__133 num__112 num__119 num__148 num__91 num__85 num__99 num__92 num__105 num__106 num__113 num__98 num__147 num__127 num__126 num__120 num__140 num__134 num__141 - num 
)
(:init     (knows id_dzHost)
     (knows id_fwRat)
     (knows_property id_dzHost pfqdn)
     (knows_property id_fwRat pexecutable)
     (knows_property id_fwRat phost)
     (mem_cached_domain_creds id_deHost id_bnDomainCredential)
     (mem_cached_domain_creds id_deHost id_clDomainCredential)
     (mem_cached_domain_creds id_dlHost id_clDomainCredential)
     (mem_cached_domain_creds id_dlHost id_lDomainCredential)
     (mem_cached_domain_creds id_dsHost id_brDomainCredential)
     (mem_cached_domain_creds id_dsHost id_tDomainCredential)
     (mem_cached_domain_creds id_dzHost id_brDomainCredential)
     (mem_cached_domain_creds id_dzHost id_pDomainCredential)
     (mem_cached_domain_creds id_egHost id_chDomainCredential)
     (mem_cached_domain_creds id_egHost id_clDomainCredential)
     (mem_cached_domain_creds id_enHost id_cxDomainCredential)
     (mem_cached_domain_creds id_enHost id_dDomainCredential)
     (mem_cached_domain_creds id_euHost id_chDomainCredential)
     (mem_cached_domain_creds id_euHost id_clDomainCredential)
     (mem_cached_domain_creds id_fbHost id_cdDomainCredential)
     (mem_cached_domain_creds id_fbHost id_ctDomainCredential)
     (mem_cached_domain_creds id_fiHost id_brDomainCredential)
     (mem_cached_domain_creds id_fiHost id_chDomainCredential)
     (mem_cached_domain_creds id_fpHost id_bbDomainCredential)
     (mem_cached_domain_creds id_fpHost id_ctDomainCredential)
     (mem_domain_user_admins id_deHost id_cgDomainUser)
     (mem_domain_user_admins id_deHost id_gDomainUser)
     (mem_domain_user_admins id_dlHost id_byDomainUser)
     (mem_domain_user_admins id_dlHost id_sDomainUser)
     (mem_domain_user_admins id_dsHost id_ckDomainUser)
     (mem_domain_user_admins id_dsHost id_daDomainUser)
     (mem_domain_user_admins id_dzHost id_ckDomainUser)
     (mem_domain_user_admins id_dzHost id_oDomainUser)
     (mem_domain_user_admins id_egHost id_bqDomainUser)
     (mem_domain_user_admins id_egHost id_cwDomainUser)
     (mem_domain_user_admins id_enHost id_ccDomainUser)
     (mem_domain_user_admins id_enHost id_csDomainUser)
     (mem_domain_user_admins id_euHost id_byDomainUser)
     (mem_domain_user_admins id_euHost id_cDomainUser)
     (mem_domain_user_admins id_fbHost id_cDomainUser)
     (mem_domain_user_admins id_fbHost id_sDomainUser)
     (mem_domain_user_admins id_fiHost id_cDomainUser)
     (mem_domain_user_admins id_fiHost id_daDomainUser)
     (mem_domain_user_admins id_fpHost id_bqDomainUser)
     (mem_domain_user_admins id_fpHost id_gDomainUser)
     (mem_hosts id_aDomain id_deHost)
     (mem_hosts id_aDomain id_dlHost)
     (mem_hosts id_aDomain id_dsHost)
     (mem_hosts id_aDomain id_dzHost)
     (mem_hosts id_aDomain id_egHost)
     (mem_hosts id_aDomain id_enHost)
     (mem_hosts id_aDomain id_euHost)
     (mem_hosts id_aDomain id_fbHost)
     (mem_hosts id_aDomain id_fiHost)
     (mem_hosts id_aDomain id_fpHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_buDomainUser id_bvDomainCredential)
     (prop_cred id_byDomainUser id_bzDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_ccDomainUser id_cdDomainCredential)
     (prop_cred id_cgDomainUser id_chDomainCredential)
     (prop_cred id_ckDomainUser id_clDomainCredential)
     (prop_cred id_coDomainUser id_cpDomainCredential)
     (prop_cred id_csDomainUser id_ctDomainCredential)
     (prop_cred id_cwDomainUser id_cxDomainCredential)
     (prop_cred id_daDomainUser id_dbDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_deHost no)
     (prop_dc id_dlHost no)
     (prop_dc id_dsHost no)
     (prop_dc id_dzHost no)
     (prop_dc id_egHost no)
     (prop_dc id_enHost no)
     (prop_dc id_euHost no)
     (prop_dc id_fbHost no)
     (prop_dc id_fiHost no)
     (prop_dc id_fpHost yes)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_deHost str__dj)
     (prop_dns_domain_name id_dlHost str__dq)
     (prop_dns_domain_name id_dsHost str__dx)
     (prop_dns_domain_name id_dzHost str__ee)
     (prop_dns_domain_name id_egHost str__el)
     (prop_dns_domain_name id_enHost str__es)
     (prop_dns_domain_name id_euHost str__ez)
     (prop_dns_domain_name id_fbHost str__fg)
     (prop_dns_domain_name id_fiHost str__fn)
     (prop_dns_domain_name id_fpHost str__fu)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buDomainUser id_aDomain)
     (prop_domain id_bvDomainCredential id_aDomain)
     (prop_domain id_byDomainUser id_aDomain)
     (prop_domain id_bzDomainCredential id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_ccDomainUser id_aDomain)
     (prop_domain id_cdDomainCredential id_aDomain)
     (prop_domain id_cgDomainUser id_aDomain)
     (prop_domain id_chDomainCredential id_aDomain)
     (prop_domain id_ckDomainUser id_aDomain)
     (prop_domain id_clDomainCredential id_aDomain)
     (prop_domain id_coDomainUser id_aDomain)
     (prop_domain id_cpDomainCredential id_aDomain)
     (prop_domain id_csDomainUser id_aDomain)
     (prop_domain id_ctDomainCredential id_aDomain)
     (prop_domain id_cwDomainUser id_aDomain)
     (prop_domain id_cxDomainCredential id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_daDomainUser id_aDomain)
     (prop_domain id_dbDomainCredential id_aDomain)
     (prop_domain id_deHost id_aDomain)
     (prop_domain id_dlHost id_aDomain)
     (prop_domain id_dsHost id_aDomain)
     (prop_domain id_dzHost id_aDomain)
     (prop_domain id_egHost id_aDomain)
     (prop_domain id_enHost id_aDomain)
     (prop_domain id_euHost id_aDomain)
     (prop_domain id_fbHost id_aDomain)
     (prop_domain id_fiHost id_aDomain)
     (prop_domain id_fpHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_fwRat yes)
     (prop_executable id_fwRat str__fx)
     (prop_fqdn id_deHost str__di)
     (prop_fqdn id_dlHost str__dp)
     (prop_fqdn id_dsHost str__dw)
     (prop_fqdn id_dzHost str__ed)
     (prop_fqdn id_egHost str__ek)
     (prop_fqdn id_enHost str__er)
     (prop_fqdn id_euHost str__ey)
     (prop_fqdn id_fbHost str__ff)
     (prop_fqdn id_fiHost str__fm)
     (prop_fqdn id_fpHost str__ft)
     (prop_host id_dfTimeDelta id_deHost)
     (prop_host id_dmTimeDelta id_dlHost)
     (prop_host id_dtTimeDelta id_dsHost)
     (prop_host id_eaTimeDelta id_dzHost)
     (prop_host id_ehTimeDelta id_egHost)
     (prop_host id_eoTimeDelta id_enHost)
     (prop_host id_evTimeDelta id_euHost)
     (prop_host id_fcTimeDelta id_fbHost)
     (prop_host id_fjTimeDelta id_fiHost)
     (prop_host id_fqTimeDelta id_fpHost)
     (prop_host id_fwRat id_dzHost)
     (prop_hostname id_deHost str__dk)
     (prop_hostname id_dlHost str__dr)
     (prop_hostname id_dsHost str__dy)
     (prop_hostname id_dzHost str__ef)
     (prop_hostname id_egHost str__em)
     (prop_hostname id_enHost str__et)
     (prop_hostname id_euHost str__fa)
     (prop_hostname id_fbHost str__fh)
     (prop_hostname id_fiHost str__fo)
     (prop_hostname id_fpHost str__fv)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_buDomainUser no)
     (prop_is_group id_byDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_ccDomainUser no)
     (prop_is_group id_cgDomainUser no)
     (prop_is_group id_ckDomainUser no)
     (prop_is_group id_coDomainUser no)
     (prop_is_group id_csDomainUser no)
     (prop_is_group id_cwDomainUser no)
     (prop_is_group id_daDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_dfTimeDelta num__84)
     (prop_microseconds id_dmTimeDelta num__91)
     (prop_microseconds id_dtTimeDelta num__98)
     (prop_microseconds id_eaTimeDelta num__105)
     (prop_microseconds id_ehTimeDelta num__112)
     (prop_microseconds id_eoTimeDelta num__119)
     (prop_microseconds id_evTimeDelta num__126)
     (prop_microseconds id_fcTimeDelta num__133)
     (prop_microseconds id_fjTimeDelta num__140)
     (prop_microseconds id_fqTimeDelta num__147)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_bvDomainCredential str__bw)
     (prop_password id_bzDomainCredential str__ca)
     (prop_password id_cdDomainCredential str__ce)
     (prop_password id_chDomainCredential str__ci)
     (prop_password id_clDomainCredential str__cm)
     (prop_password id_cpDomainCredential str__cq)
     (prop_password id_ctDomainCredential str__cu)
     (prop_password id_cxDomainCredential str__cy)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_dbDomainCredential str__dc)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_dfTimeDelta num__85)
     (prop_seconds id_dmTimeDelta num__92)
     (prop_seconds id_dtTimeDelta num__99)
     (prop_seconds id_eaTimeDelta num__106)
     (prop_seconds id_ehTimeDelta num__113)
     (prop_seconds id_eoTimeDelta num__120)
     (prop_seconds id_evTimeDelta num__127)
     (prop_seconds id_fcTimeDelta num__134)
     (prop_seconds id_fjTimeDelta num__141)
     (prop_seconds id_fqTimeDelta num__148)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_buDomainUser str__bx)
     (prop_sid id_byDomainUser str__cb)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_ccDomainUser str__cf)
     (prop_sid id_cgDomainUser str__cj)
     (prop_sid id_ckDomainUser str__cn)
     (prop_sid id_coDomainUser str__cr)
     (prop_sid id_csDomainUser str__cv)
     (prop_sid id_cwDomainUser str__cz)
     (prop_sid id_daDomainUser str__dd)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_deHost id_dfTimeDelta)
     (prop_timedelta id_dlHost id_dmTimeDelta)
     (prop_timedelta id_dsHost id_dtTimeDelta)
     (prop_timedelta id_dzHost id_eaTimeDelta)
     (prop_timedelta id_egHost id_ehTimeDelta)
     (prop_timedelta id_enHost id_eoTimeDelta)
     (prop_timedelta id_euHost id_evTimeDelta)
     (prop_timedelta id_fbHost id_fcTimeDelta)
     (prop_timedelta id_fiHost id_fjTimeDelta)
     (prop_timedelta id_fpHost id_fqTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_bvDomainCredential id_buDomainUser)
     (prop_user id_bzDomainCredential id_byDomainUser)
     (prop_user id_cdDomainCredential id_ccDomainUser)
     (prop_user id_chDomainCredential id_cgDomainUser)
     (prop_user id_clDomainCredential id_ckDomainUser)
     (prop_user id_cpDomainCredential id_coDomainUser)
     (prop_user id_ctDomainCredential id_csDomainUser)
     (prop_user id_cxDomainCredential id_cwDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_dbDomainCredential id_daDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_buDomainUser str__jennifer)
     (prop_username id_byDomainUser str__richard)
     (prop_username id_cDomainUser str__james)
     (prop_username id_ccDomainUser str__maria)
     (prop_username id_cgDomainUser str__charles)
     (prop_username id_ckDomainUser str__susan)
     (prop_username id_coDomainUser str__joseph)
     (prop_username id_csDomainUser str__margaret)
     (prop_username id_cwDomainUser str__thomas)
     (prop_username id_daDomainUser str__dorothy)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_grRat id_euHost)
     (prop_host id_gmRat id_enHost)
     (prop_host id_gjRat id_fiHost)
     (prop_host id_goRat id_deHost)
     (prop_host id_gkRat id_dlHost)
     (prop_host id_gqRat id_dsHost)
     (prop_host id_giRat id_egHost)
     (prop_host id_gnRat id_fbHost)
     (prop_host id_glRat id_fpHost)
)
)
)