;Copyright 2018 The MITRE Corporation. All rights reserved. Approved for public release. Distribution unlimited 17-2122.
; For more information on CALDERA, the automated adversary emulation system, visit https://github.com/mitre/caldera or email attack@mitre.org
; This has 12 hosts, 11 user, 1 admin per host, 4 active account per host
(define (problem p12_hosts_trial_10)
(:domain caldera)
(:objects str__mary str__ct str__cv str__db str__john str__ek str__ej str__barbara str__z str__u str__dc str__es str__ec str__ex str__v str__bl str__michael str__elizabeth str__dw str__bs str__m str__b str__ey str__bd str__bz str__patricia str__ez str__bp str__william str__di str__linda str__james str__robert str__ca str__co str__cu str__dp str__da str__dq str__alpha str__f str__cg str__cf str__bc str__bg str__bo str__el str__y str__j str__david str__by str__bk str__n str__ee str__e str__bt str__i str__q str__bh str__do str__cm str__dx str__r str__ed str__fb str__dh str__dj str__dv str__eq str__ch str__er str__cn - string 
 id_bqDomainUser id_oDomainUser id_kDomainUser id_biDomainUser id_gDomainUser id_wDomainUser id_sDomainUser id_beDomainUser id_bmDomainUser id_cDomainUser id_baDomainUser - ObservedDomainUser 
 id_aDomain - ObservedDomain 
 id_ddHost id_dyHost id_cbHost id_etHost id_ciHost id_efHost id_cpHost id_cwHost id_emHost id_drHost id_buHost id_dkHost - ObservedHost 
 id_fkShare id_fjShare id_fdShare id_flShare id_fhShare id_ffShare id_fgShare id_fiShare id_fmShare id_feShare id_fcShare id_fnShare - ObservedShare 
 id_fsFile id_fzFile id_fyFile id_foFile id_fxFile id_frFile id_fqFile id_ftFile id_fuFile id_fvFile id_fpFile id_fwFile - ObservedFile 
 id_gbRat id_gjRat id_geRat id_gcRat id_glRat id_ggRat id_gkRat id_gfRat id_faRat id_giRat id_gdRat id_gaRat id_ghRat - ObservedRat 
 id_deTimeDelta id_dlTimeDelta id_egTimeDelta id_dzTimeDelta id_enTimeDelta id_cxTimeDelta id_bvTimeDelta id_cqTimeDelta id_euTimeDelta id_dsTimeDelta id_cjTimeDelta id_ccTimeDelta - ObservedTimeDelta 
 id_tDomainCredential id_bbDomainCredential id_brDomainCredential id_xDomainCredential id_bnDomainCredential id_hDomainCredential id_dDomainCredential id_bjDomainCredential id_pDomainCredential id_lDomainCredential id_bfDomainCredential - ObservedDomainCredential 
 num__118 num__90 num__62 num__119 num__48 num__69 num__56 num__125 num__97 num__49 num__91 num__77 num__112 num__111 num__104 num__83 num__105 num__76 num__70 num__126 num__55 num__63 num__98 num__84 - num 
 id_gpSchtask id_grSchtask id_guSchtask id_gxSchtask id_gwSchtask id_gvSchtask id_gtSchtask id_gnSchtask id_goSchtask id_gmSchtask id_gqSchtask id_gsSchtask - ObservedSchtask 
)
(:init     (knows id_dkHost)
     (knows id_faRat)
     (knows_property id_dkHost pfqdn)
     (knows_property id_faRat pexecutable)
     (knows_property id_faRat phost)
     (mem_cached_domain_creds id_buHost id_bfDomainCredential)
     (mem_cached_domain_creds id_buHost id_bjDomainCredential)
     (mem_cached_domain_creds id_buHost id_dDomainCredential)
     (mem_cached_domain_creds id_buHost id_hDomainCredential)
     (mem_cached_domain_creds id_cbHost id_bfDomainCredential)
     (mem_cached_domain_creds id_cbHost id_dDomainCredential)
     (mem_cached_domain_creds id_cbHost id_lDomainCredential)
     (mem_cached_domain_creds id_cbHost id_pDomainCredential)
     (mem_cached_domain_creds id_ciHost id_bbDomainCredential)
     (mem_cached_domain_creds id_ciHost id_dDomainCredential)
     (mem_cached_domain_creds id_ciHost id_hDomainCredential)
     (mem_cached_domain_creds id_ciHost id_xDomainCredential)
     (mem_cached_domain_creds id_cpHost id_bfDomainCredential)
     (mem_cached_domain_creds id_cpHost id_bjDomainCredential)
     (mem_cached_domain_creds id_cpHost id_dDomainCredential)
     (mem_cached_domain_creds id_cpHost id_lDomainCredential)
     (mem_cached_domain_creds id_cwHost id_bjDomainCredential)
     (mem_cached_domain_creds id_cwHost id_bnDomainCredential)
     (mem_cached_domain_creds id_cwHost id_hDomainCredential)
     (mem_cached_domain_creds id_cwHost id_pDomainCredential)
     (mem_cached_domain_creds id_ddHost id_bbDomainCredential)
     (mem_cached_domain_creds id_ddHost id_brDomainCredential)
     (mem_cached_domain_creds id_ddHost id_hDomainCredential)
     (mem_cached_domain_creds id_ddHost id_lDomainCredential)
     (mem_cached_domain_creds id_dkHost id_bbDomainCredential)
     (mem_cached_domain_creds id_dkHost id_bjDomainCredential)
     (mem_cached_domain_creds id_dkHost id_hDomainCredential)
     (mem_cached_domain_creds id_dkHost id_xDomainCredential)
     (mem_cached_domain_creds id_drHost id_bbDomainCredential)
     (mem_cached_domain_creds id_drHost id_brDomainCredential)
     (mem_cached_domain_creds id_drHost id_pDomainCredential)
     (mem_cached_domain_creds id_drHost id_xDomainCredential)
     (mem_cached_domain_creds id_dyHost id_brDomainCredential)
     (mem_cached_domain_creds id_dyHost id_dDomainCredential)
     (mem_cached_domain_creds id_dyHost id_hDomainCredential)
     (mem_cached_domain_creds id_dyHost id_xDomainCredential)
     (mem_cached_domain_creds id_efHost id_dDomainCredential)
     (mem_cached_domain_creds id_efHost id_pDomainCredential)
     (mem_cached_domain_creds id_efHost id_tDomainCredential)
     (mem_cached_domain_creds id_efHost id_xDomainCredential)
     (mem_cached_domain_creds id_emHost id_bfDomainCredential)
     (mem_cached_domain_creds id_emHost id_bnDomainCredential)
     (mem_cached_domain_creds id_emHost id_hDomainCredential)
     (mem_cached_domain_creds id_emHost id_lDomainCredential)
     (mem_cached_domain_creds id_etHost id_bfDomainCredential)
     (mem_cached_domain_creds id_etHost id_bjDomainCredential)
     (mem_cached_domain_creds id_etHost id_bnDomainCredential)
     (mem_cached_domain_creds id_etHost id_pDomainCredential)
     (mem_domain_user_admins id_buHost id_bmDomainUser)
     (mem_domain_user_admins id_cbHost id_gDomainUser)
     (mem_domain_user_admins id_ciHost id_kDomainUser)
     (mem_domain_user_admins id_cpHost id_sDomainUser)
     (mem_domain_user_admins id_cwHost id_gDomainUser)
     (mem_domain_user_admins id_ddHost id_bqDomainUser)
     (mem_domain_user_admins id_dkHost id_gDomainUser)
     (mem_domain_user_admins id_drHost id_kDomainUser)
     (mem_domain_user_admins id_dyHost id_cDomainUser)
     (mem_domain_user_admins id_efHost id_baDomainUser)
     (mem_domain_user_admins id_emHost id_kDomainUser)
     (mem_domain_user_admins id_etHost id_biDomainUser)
     (mem_hosts id_aDomain id_buHost)
     (mem_hosts id_aDomain id_cbHost)
     (mem_hosts id_aDomain id_ciHost)
     (mem_hosts id_aDomain id_cpHost)
     (mem_hosts id_aDomain id_cwHost)
     (mem_hosts id_aDomain id_ddHost)
     (mem_hosts id_aDomain id_dkHost)
     (mem_hosts id_aDomain id_drHost)
     (mem_hosts id_aDomain id_dyHost)
     (mem_hosts id_aDomain id_efHost)
     (mem_hosts id_aDomain id_emHost)
     (mem_hosts id_aDomain id_etHost)
     (prop_cred id_baDomainUser id_bbDomainCredential)
     (prop_cred id_beDomainUser id_bfDomainCredential)
     (prop_cred id_biDomainUser id_bjDomainCredential)
     (prop_cred id_bmDomainUser id_bnDomainCredential)
     (prop_cred id_bqDomainUser id_brDomainCredential)
     (prop_cred id_cDomainUser id_dDomainCredential)
     (prop_cred id_gDomainUser id_hDomainCredential)
     (prop_cred id_kDomainUser id_lDomainCredential)
     (prop_cred id_oDomainUser id_pDomainCredential)
     (prop_cred id_sDomainUser id_tDomainCredential)
     (prop_cred id_wDomainUser id_xDomainCredential)
     (prop_dc id_buHost no)
     (prop_dc id_cbHost yes)
     (prop_dc id_ciHost no)
     (prop_dc id_cpHost no)
     (prop_dc id_cwHost no)
     (prop_dc id_ddHost yes)
     (prop_dc id_dkHost no)
     (prop_dc id_drHost no)
     (prop_dc id_dyHost no)
     (prop_dc id_efHost no)
     (prop_dc id_emHost no)
     (prop_dc id_etHost yes)
     (prop_dns_domain id_aDomain str__b)
     (prop_dns_domain_name id_buHost str__ca)
     (prop_dns_domain_name id_cbHost str__ch)
     (prop_dns_domain_name id_ciHost str__co)
     (prop_dns_domain_name id_cpHost str__cv)
     (prop_dns_domain_name id_cwHost str__dc)
     (prop_dns_domain_name id_ddHost str__dj)
     (prop_dns_domain_name id_dkHost str__dq)
     (prop_dns_domain_name id_drHost str__dx)
     (prop_dns_domain_name id_dyHost str__ee)
     (prop_dns_domain_name id_efHost str__el)
     (prop_dns_domain_name id_emHost str__es)
     (prop_dns_domain_name id_etHost str__ez)
     (prop_domain id_baDomainUser id_aDomain)
     (prop_domain id_bbDomainCredential id_aDomain)
     (prop_domain id_beDomainUser id_aDomain)
     (prop_domain id_bfDomainCredential id_aDomain)
     (prop_domain id_biDomainUser id_aDomain)
     (prop_domain id_bjDomainCredential id_aDomain)
     (prop_domain id_bmDomainUser id_aDomain)
     (prop_domain id_bnDomainCredential id_aDomain)
     (prop_domain id_bqDomainUser id_aDomain)
     (prop_domain id_brDomainCredential id_aDomain)
     (prop_domain id_buHost id_aDomain)
     (prop_domain id_cDomainUser id_aDomain)
     (prop_domain id_cbHost id_aDomain)
     (prop_domain id_ciHost id_aDomain)
     (prop_domain id_cpHost id_aDomain)
     (prop_domain id_cwHost id_aDomain)
     (prop_domain id_dDomainCredential id_aDomain)
     (prop_domain id_ddHost id_aDomain)
     (prop_domain id_dkHost id_aDomain)
     (prop_domain id_drHost id_aDomain)
     (prop_domain id_dyHost id_aDomain)
     (prop_domain id_efHost id_aDomain)
     (prop_domain id_emHost id_aDomain)
     (prop_domain id_etHost id_aDomain)
     (prop_domain id_gDomainUser id_aDomain)
     (prop_domain id_hDomainCredential id_aDomain)
     (prop_domain id_kDomainUser id_aDomain)
     (prop_domain id_lDomainCredential id_aDomain)
     (prop_domain id_oDomainUser id_aDomain)
     (prop_domain id_pDomainCredential id_aDomain)
     (prop_domain id_sDomainUser id_aDomain)
     (prop_domain id_tDomainCredential id_aDomain)
     (prop_domain id_wDomainUser id_aDomain)
     (prop_domain id_xDomainCredential id_aDomain)
     (prop_elevated id_faRat yes)
     (prop_executable id_faRat str__fb)
     (prop_fqdn id_buHost str__bz)
     (prop_fqdn id_cbHost str__cg)
     (prop_fqdn id_ciHost str__cn)
     (prop_fqdn id_cpHost str__cu)
     (prop_fqdn id_cwHost str__db)
     (prop_fqdn id_ddHost str__di)
     (prop_fqdn id_dkHost str__dp)
     (prop_fqdn id_drHost str__dw)
     (prop_fqdn id_dyHost str__ed)
     (prop_fqdn id_efHost str__ek)
     (prop_fqdn id_emHost str__er)
     (prop_fqdn id_etHost str__ey)
     (prop_host id_bvTimeDelta id_buHost)
     (prop_host id_ccTimeDelta id_cbHost)
     (prop_host id_cjTimeDelta id_ciHost)
     (prop_host id_cqTimeDelta id_cpHost)
     (prop_host id_cxTimeDelta id_cwHost)
     (prop_host id_deTimeDelta id_ddHost)
     (prop_host id_dlTimeDelta id_dkHost)
     (prop_host id_dsTimeDelta id_drHost)
     (prop_host id_dzTimeDelta id_dyHost)
     (prop_host id_egTimeDelta id_efHost)
     (prop_host id_enTimeDelta id_emHost)
     (prop_host id_euTimeDelta id_etHost)
     (prop_host id_faRat id_dkHost)
     (prop_hostname id_buHost str__by)
     (prop_hostname id_cbHost str__cf)
     (prop_hostname id_ciHost str__cm)
     (prop_hostname id_cpHost str__ct)
     (prop_hostname id_cwHost str__da)
     (prop_hostname id_ddHost str__dh)
     (prop_hostname id_dkHost str__do)
     (prop_hostname id_drHost str__dv)
     (prop_hostname id_dyHost str__ec)
     (prop_hostname id_efHost str__ej)
     (prop_hostname id_emHost str__eq)
     (prop_hostname id_etHost str__ex)
     (prop_is_group id_baDomainUser no)
     (prop_is_group id_beDomainUser no)
     (prop_is_group id_biDomainUser no)
     (prop_is_group id_bmDomainUser no)
     (prop_is_group id_bqDomainUser no)
     (prop_is_group id_cDomainUser no)
     (prop_is_group id_gDomainUser no)
     (prop_is_group id_kDomainUser no)
     (prop_is_group id_oDomainUser no)
     (prop_is_group id_sDomainUser no)
     (prop_is_group id_wDomainUser no)
     (prop_microseconds id_bvTimeDelta num__48)
     (prop_microseconds id_ccTimeDelta num__55)
     (prop_microseconds id_cjTimeDelta num__62)
     (prop_microseconds id_cqTimeDelta num__69)
     (prop_microseconds id_cxTimeDelta num__76)
     (prop_microseconds id_deTimeDelta num__83)
     (prop_microseconds id_dlTimeDelta num__90)
     (prop_microseconds id_dsTimeDelta num__97)
     (prop_microseconds id_dzTimeDelta num__104)
     (prop_microseconds id_egTimeDelta num__111)
     (prop_microseconds id_enTimeDelta num__118)
     (prop_microseconds id_euTimeDelta num__125)
     (prop_password id_bbDomainCredential str__bc)
     (prop_password id_bfDomainCredential str__bg)
     (prop_password id_bjDomainCredential str__bk)
     (prop_password id_bnDomainCredential str__bo)
     (prop_password id_brDomainCredential str__bs)
     (prop_password id_dDomainCredential str__e)
     (prop_password id_hDomainCredential str__i)
     (prop_password id_lDomainCredential str__m)
     (prop_password id_pDomainCredential str__q)
     (prop_password id_tDomainCredential str__u)
     (prop_password id_xDomainCredential str__y)
     (prop_seconds id_bvTimeDelta num__49)
     (prop_seconds id_ccTimeDelta num__56)
     (prop_seconds id_cjTimeDelta num__63)
     (prop_seconds id_cqTimeDelta num__70)
     (prop_seconds id_cxTimeDelta num__77)
     (prop_seconds id_deTimeDelta num__84)
     (prop_seconds id_dlTimeDelta num__91)
     (prop_seconds id_dsTimeDelta num__98)
     (prop_seconds id_dzTimeDelta num__105)
     (prop_seconds id_egTimeDelta num__112)
     (prop_seconds id_enTimeDelta num__119)
     (prop_seconds id_euTimeDelta num__126)
     (prop_sid id_baDomainUser str__bd)
     (prop_sid id_beDomainUser str__bh)
     (prop_sid id_biDomainUser str__bl)
     (prop_sid id_bmDomainUser str__bp)
     (prop_sid id_bqDomainUser str__bt)
     (prop_sid id_cDomainUser str__f)
     (prop_sid id_gDomainUser str__j)
     (prop_sid id_kDomainUser str__n)
     (prop_sid id_oDomainUser str__r)
     (prop_sid id_sDomainUser str__v)
     (prop_sid id_wDomainUser str__z)
     (prop_timedelta id_buHost id_bvTimeDelta)
     (prop_timedelta id_cbHost id_ccTimeDelta)
     (prop_timedelta id_ciHost id_cjTimeDelta)
     (prop_timedelta id_cpHost id_cqTimeDelta)
     (prop_timedelta id_cwHost id_cxTimeDelta)
     (prop_timedelta id_ddHost id_deTimeDelta)
     (prop_timedelta id_dkHost id_dlTimeDelta)
     (prop_timedelta id_drHost id_dsTimeDelta)
     (prop_timedelta id_dyHost id_dzTimeDelta)
     (prop_timedelta id_efHost id_egTimeDelta)
     (prop_timedelta id_emHost id_enTimeDelta)
     (prop_timedelta id_etHost id_euTimeDelta)
     (prop_user id_bbDomainCredential id_baDomainUser)
     (prop_user id_bfDomainCredential id_beDomainUser)
     (prop_user id_bjDomainCredential id_biDomainUser)
     (prop_user id_bnDomainCredential id_bmDomainUser)
     (prop_user id_brDomainCredential id_bqDomainUser)
     (prop_user id_dDomainCredential id_cDomainUser)
     (prop_user id_hDomainCredential id_gDomainUser)
     (prop_user id_lDomainCredential id_kDomainUser)
     (prop_user id_pDomainCredential id_oDomainUser)
     (prop_user id_tDomainCredential id_sDomainUser)
     (prop_user id_xDomainCredential id_wDomainUser)
     (prop_username id_baDomainUser str__michael)
     (prop_username id_beDomainUser str__barbara)
     (prop_username id_biDomainUser str__william)
     (prop_username id_bmDomainUser str__elizabeth)
     (prop_username id_bqDomainUser str__david)
     (prop_username id_cDomainUser str__james)
     (prop_username id_gDomainUser str__mary)
     (prop_username id_kDomainUser str__john)
     (prop_username id_oDomainUser str__patricia)
     (prop_username id_sDomainUser str__robert)
     (prop_username id_wDomainUser str__linda)
     (prop_windows_domain id_aDomain str__alpha)
)
(:goal   (and 
     (prop_host id_ghRat id_buHost)
     (prop_host id_gbRat id_ddHost)
     (prop_host id_gjRat id_dyHost)
     (prop_host id_geRat id_cbHost)
     (prop_host id_gcRat id_etHost)
     (prop_host id_glRat id_ciHost)
     (prop_host id_ggRat id_efHost)
     (prop_host id_gkRat id_cpHost)
     (prop_host id_gfRat id_cwHost)
     (prop_host id_giRat id_emHost)
     (prop_host id_gdRat id_drHost)
)
)
)